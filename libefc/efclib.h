/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__EFCLIB_H__)
#define __EFCLIB_H__

#include "scsi/fc/fc_els.h"
#include "scsi/fc/fc_fs.h"
#include "scsi/fc/fc_ns.h"
#include <scsi/scsi_host.h>
#include <scsi/scsi_transport_fc.h>
#include <linux/completion.h>
#include "efc_fcp.h"

#define EFC_SERVICE_PARMS_LENGTH		0x74
#define EFC_DISPLAY_NAME_LENGTH		32
#define EFC_DISPLAY_BUS_INFO_LENGTH		16

#define EFC_WWN_LENGTH				32

/**
 * Local port topology.
 */

enum efc_sport_topology_e {
	EFC_SPORT_TOPOLOGY_UNKNOWN = 0,
	EFC_SPORT_TOPOLOGY_FABRIC,
	EFC_SPORT_TOPOLOGY_P2P,
	EFC_SPORT_TOPOLOGY_LOOP,
};

/**
 * Common (transport agnostic) shared declarations
 */

#define enable_target_rscn(efc)	1

/* Used for error injection testing. */
enum efc_err_injection_e {
	NO_ERR_INJECT = 0,
	INJECT_DROP_CMD,
	INJECT_FREE_DROPPED,
	INJECT_DROP_DATA,
	INJECT_DROP_RESP,
	INJECT_DELAY_CMD,
};

enum efc_node_shutd_rsn_e {
	EFC_NODE_SHUTDOWN_DEFAULT = 0,
	EFC_NODE_SHUTDOWN_EXPLICIT_LOGO,
	EFC_NODE_SHUTDOWN_IMPLICIT_LOGO,
};

enum efc_node_send_ls_acc_e {
	EFC_NODE_SEND_LS_ACC_NONE = 0,
	EFC_NODE_SEND_LS_ACC_PLOGI,
	EFC_NODE_SEND_LS_ACC_PRLI,
};

#define EFC_LINK_STATUS_UP   0
#define EFC_LINK_STATUS_DOWN 1

/* State machine context header  */
struct efc_sm_ctx_s {
	void *(*current_state)(struct efc_sm_ctx_s *ctx,
			       u32 evt, void *arg);

	const char *description;
	void	*app;			/** Application-specific handle. */
};

union efc_pid_u {
	struct {
		u32 pid;/* < cpuid if interrupt, threadid
			 * otherwise
			 */
		bool irq;/**< true if in interrupt context */
	} s;
	u32 l;
};

/***************************************************************************
 * Locking
 */

/**
 * @brief recursive lock structure
 *
 * The recursive lock is implemented by maintaining a thread id (pid)
 * and a count.
 * A count value of zero means the lock is not taken.
 *
 */

struct efc_rlock_s {
	union efc_pid_u pid;	/* < thread ID of the lock's owner,
				 * if count != 0
				 */
	spinlock_t lock;	/**< EFC lock object */
	u32 count;		/**< nesting count */
	const char *name;
	struct efc_lport *efc;
};

/**
 * @ingroup os
 * @brief generic DMA memory descriptor for driver allocations
 *
 * Memory regions ultimately used by the hardware are described using
 * this structure. All implementations must include the structure members
 * defined in the first section, and may also add their own structure
 * members in the second section.
 *
 * Note that each region described by efc_dma_s is assumed to be physically
 * contiguous.
 */
struct efc_dma_s {
	struct efct_os_s	*os;
	/*
	 * EFC layer requires the following members
	 */
	void		*virt;	/* virtual address of the memory
				 * used by the CPU
				 */
	void            *alloc;
	dma_addr_t	phys;	/* physical or bus address of the memory used
				 * by the hardware
				 */
	size_t		size;	/* size in bytes of the memory */
	size_t          len;
	struct pci_dev	*pdev;
};

/**
 * @brief Description of discovered Fabric Domain
 * struct efc_domain_record_s - libefc discovered Fabric Domain
 * @index:	FCF table index (used in REG_FCFI)
 * @priority:	FCF reported priority
 * @address:	Switch WWN
 * @vlan:	bitmap of valid VLAN IDs
 * @loop:	FC-AL position map
 * @speed:	link speed
 * @fc_id:	our ports fc_id
 */
struct efc_domain_record_s {
	u32	index;
	u32	priority;
	u8		address[6];
	u8		wwn[8];
	union {
		u8	vlan[512];
		u8	loop[128];
	} map;
	u32	speed;
	u32	fc_id;
	bool		is_fc;
	bool		is_ethernet;
	bool		is_loop;
	bool		is_nport;
};

/*
 * @brief Fabric/Domain events
 */
enum efc_hw_domain_event_e {
	EFC_HW_DOMAIN_ALLOC_OK,		/**< domain successfully allocated */
	EFC_HW_DOMAIN_ALLOC_FAIL,	/**< domain allocation failed */
	EFC_HW_DOMAIN_ATTACH_OK,	/**< successfully attached to domain */
	EFC_HW_DOMAIN_ATTACH_FAIL,	/**< domain attach failed */
	EFC_HW_DOMAIN_FREE_OK,		/**< successfully freed domain */
	EFC_HW_DOMAIN_FREE_FAIL,	/**< domain free failed */
	EFC_HW_DOMAIN_LOST,
	/**< prev discovered domain no longer available */
	EFC_HW_DOMAIN_FOUND,		/**< new domain discovered */
	/**< prev discovered domain props have changed */
	EFC_HW_DOMAIN_CHANGED,
};

enum efc_hw_port_event_e {
	EFC_HW_PORT_ALLOC_OK,		/**< port successfully allocated */
	EFC_HW_PORT_ALLOC_FAIL,		/**< port allocation failed */
	EFC_HW_PORT_ATTACH_OK,		/**< successfully attached to port */
	EFC_HW_PORT_ATTACH_FAIL,	/**< port attach failed */
	EFC_HW_PORT_FREE_OK,		/**< successfully freed port */
	EFC_HW_PORT_FREE_FAIL,		/**< port free failed */
};

enum efc_hw_remote_node_event_e {
EFC_HW_NODE_ATTACH_OK,
EFC_HW_NODE_ATTACH_FAIL,
EFC_HW_NODE_FREE_OK,
EFC_HW_NODE_FREE_FAIL,
EFC_HW_NODE_FREE_ALL_OK,
EFC_HW_NODE_FREE_ALL_FAIL,
};

enum efc_hw_node_els_event_e {
	EFC_HW_SRRS_ELS_REQ_OK,
	EFC_HW_SRRS_ELS_CMPL_OK,
	EFC_HW_SRRS_ELS_REQ_FAIL,
	EFC_HW_SRRS_ELS_CMPL_FAIL,
	EFC_HW_SRRS_ELS_REQ_RJT,
	EFC_HW_ELS_REQ_ABORTED,
};

/**
 * @brief SLI Port object
 *
 * The SLI Port object represents the connection between the driver and the
 * FC/FCoE domain. In some topologies / hardware, it is possible to have
 * multiple connections to the domain via different WWN. Each would require
 * a separate SLI port object.
 *
 * @efc:		pointer to efc
 * @tgt_id:		target id
 * @display_name:	sport display name
 * @domain:		current fabric domain
 * @is_vport:		this SPORT is a virtual port
 * @wwpn:		WWPN from HW (host endian)
 * @wwnn:		WWNN from HW (host endian)
 * @node_list:		list of nodes
 * @ini_sport:		initiator backend private sport data
 * @tgt_sport:		target backend private sport data
 * @tgt_data:		target backend private pointer
 * @ini_data:		initiator backend private pointer
 * @ctx:		state machine context
 * @hw:			pointer to HW
 * @indicator:		VPI
 * @fc_id:		FC address
 * @efc_dma_s:		memory for Service Parameter
 * @wwnn_str:		WWN (ASCII)
 * @sli_wwpn:		WWPN (wire endian)
 * @sli_wwnn:		WWNN (wire endian)
 * @sm_free_req_pending:Free request received while waiting for attach response
 * @sm:			sport context state machine
 * @lookup:		fc_id to node lookup object
 * @enable_ini:		SCSI initiator enabled for this node
 * @enable_tgt:		SCSI target enabled for this node
 * @enable_rscn:	This SPORT will be expecting RSCN
 * @shutting_down:	sport in process of shutting down
 * @p2p_winner:		TRUE if we're the point-to-point winner
 * @topology:		topology: fabric/p2p/unknown
 * @service_params:	Login parameters
 * @p2p_remote_port_id:	Remote node's port id for p2p
 * @p2p_port_id:	our port's id
 */
struct efc_sli_port_s {
	struct list_head list_entry;
	struct efc_lport *efc;
	u32 tgt_id;
	u32 index;
	u32 instance_index;
	char display_name[EFC_DISPLAY_NAME_LENGTH];
	struct efc_domain_s *domain;
	bool is_vport;
	u64	wwpn;
	u64	wwnn;
	struct list_head node_list;
	void	*ini_sport;
	void	*tgt_sport;
	void	*tgt_data;
	void	*ini_data;

	/*
	 * Members private to HW/SLI
	 */
	void		*hw;
	u32	indicator;
	u32	fc_id;
	struct efc_dma_s	dma;

	u8		wwnn_str[EFC_WWN_LENGTH];
	__be64		sli_wwpn;
	__be64		sli_wwnn;
	bool		free_req_pending;
	bool		attached;

	/*
	 * Implementation specific fields allowed here
	 */
	struct efc_sm_ctx_s	sm;
	struct sparse_vector_s *lookup;
	bool		enable_ini;
	bool		enable_tgt;
	bool		enable_rscn;
	bool		shutting_down;
	bool		p2p_winner;
	enum efc_sport_topology_e topology;
	u8		service_params[EFC_SERVICE_PARMS_LENGTH];
	u32	p2p_remote_port_id;
	u32	p2p_port_id;
};

/**
 * @brief Fibre Channel domain object
 *
 * This object is a container for the various SLI components needed
 * to connect to the domain of a FC or FCoE switch
 * @efc:		pointer back to efc
 * @instance_index:	unique instance index value
 * @display_name:	Node display name
 * @sport_list:		linked list of SLI ports
 * @ini_domain:		initiator backend private domain data
 * @tgt_domain:		target backend private domain data
 * @hw:			pointer to HW
 * @sm:			state machine context
 * @fcf:		FC Forwarder table index
 * @fcf_indicator:	FCFI
 * @vlan_id:		VLAN tag for this domain
 * @indicator:		VFI
 * @dma:		memory for Service Parameters
 * @req_rediscover_fcf:	TRUE if fcf rediscover is needed
 *			(in response to Vlink Clear async event)
 * @fcf_wwn:		WWN for FCF/switch
 * @drvsm:		driver domain sm context
 * @drvsm_lock:		driver domain sm lock
 * @attached:		set true after attach completes
 * @is_fc:		is FC
 * @is_loop:		is loop topology
 * @is_nlport:		is public loop
 * @domain_found_pending:A domain found is pending, drec is updated
 * @req_domain_free:	True if domain object should be free'd
 * @req_accept_frames:	set in domain state machine to enable frames
 * @domain_notify_pend:	Set in domain SM to avoid duplicate node event post
 * @pending_drec:	Pending drec if a domain found is pending
 * @service_params:	any sports service parameters
 * @flogi_service_params:Fabric/P2p service parameters from FLOGI
 * @femul_enable:	TRUE if Fabric Emulation mode is enabled
 * @lookup:		d_id to node lookup object
 * @sport:		Pointer to first (physical) SLI port
 */
struct efc_domain_s {
	struct list_head	list_entry;
	struct efc_lport *efc;
	u32 instance_index;
	char display_name[EFC_DISPLAY_NAME_LENGTH];
	struct list_head sport_list;
	void	*ini_domain;
	void	*tgt_domain;

	/* Declarations private to HW/SLI */
	void		*hw;
	struct efc_sm_ctx_s	sm;
	u32	fcf;
	u32	fcf_indicator;
	u32	vlan_id;
	u32	indicator;
	struct efc_dma_s	dma;
	bool req_rediscover_fcf;

	/* Declarations private to FC transport */
	u64	fcf_wwn;
	struct efc_sm_ctx_s	drvsm;
	struct efc_rlock_s	drvsm_lock;
	bool attached;
	bool is_fc;
	bool is_loop;
	bool is_nlport;
	bool domain_found_pending;
	bool req_domain_free;
	bool req_accept_frames;
	bool domain_notify_pend;

	struct efc_domain_record_s pending_drec;
	u8		service_params[EFC_SERVICE_PARMS_LENGTH];
	u8		flogi_service_params[EFC_SERVICE_PARMS_LENGTH];
	u8		femul_enable;

	struct sparse_vector_s *lookup;
	spinlock_t	lookup_lock;

	struct efc_sli_port_s	*sport;
	u32	sport_instance_count;

	/* Fabric Emulation */
	unsigned long *portid_pool;
	struct efc_ns_s *efc_ns;
};

/**
 * @brief Remote Node object
 *
 * This object represents a connection between the SLI port and another
 * Nx_Port on the fabric. Note this can be either a well known port such
 * as a F_Port (i.e. ff:ff:fe) or another N_Port.
 * @indicator:		RPI
 * @fc_id:		FC address
 * @attached:		true if attached
 * @node_group:		true if in node group
 * @free_group:		true if the node group should be free'd
 * @sport:		associated SLI port
 * @node:		associated node
 */
struct efc_remote_node_s {
	/*
	 * Members private to HW/SLI
	 */
	u32	indicator;
	u32	index;
	u32	fc_id;

	bool attached;
	bool node_group;
	bool free_group;

	struct efc_sli_port_s	*sport;
	void *node;
};

/**
 * @brief FC Node object
 * @efc:		pointer back to efc structure
 * @instance_index:	unique instance index value
 * @display_name:	Node display name
 * @hold_frames:	hold incoming frames if true
 * @lock:		node wide lock
 * @active_ios:		active I/O's for this node
 * @max_wr_xfer_size:	Max write IO size per phase for the transport
 * @ini_node:		backend initiator private node data
 * @tgt_node:		backend target private node data
 * @rnode:		Remote node
 * @sm:			state machine context
 * @evtdepth:		current event posting nesting depth
 * @req_free:		this node is to be free'd
 * @attached:		node is attached (REGLOGIN complete)
 * @fcp_enabled:	node is enabled to handle FCP
 * @rscn_pending:	for name server node RSCN is pending
 * @send_plogi:		send PLOGI accept, upon completion of node attach
 * @send_plogi_acc:	TRUE if io_alloc() is enabled.
 * @send_ls_acc:	type of LS acc to send
 * @ls_acc_io:		SCSI IO for LS acc
 * @ls_acc_oxid:	OX_ID for pending accept
 * @ls_acc_did:		D_ID for pending accept
 * @shutdown_reason:	reason for node shutdown
 * @sparm_dma_buf:	service parameters buffer
 * @service_params:	plogi/acc frame from remote device
 * @pend_frames_lock:	lock for inbound pending frames list
 * @pend_frames:	inbound pending frames list
 * @pend_frames_processed:count of frames processed in hold frames interval
 * @ox_id_in_use:	used to verify one at a time us of ox_id
 * @els_retries_remaining:for ELS, number of retries remaining
 * @els_req_cnt:	number of outstanding ELS requests
 * @els_cmpl_cnt:	number of outstanding ELS completions
 * @abort_cnt:		Abort counter for debugging purpos
 * @current_state_name:	current node state
 * @prev_state_name:	previous node state
 * @current_evt:	current event
 * @prev_evt:		previous event
 * @targ:		node is target capable
 * @init:		node is init capable
 * @refound:		Handle node refound case when node is being deleted
 * @els_io_pend_list:	list of pending (not yet processed) ELS IOs
 * @els_io_active_list:	list of active (processed) ELS IOs
 * @nodedb_state:	Node debugging, saved state
 * @gidpt_delay_timer:	GIDPT delay timer
 * @time_last_gidpt_msec:Start time of last target RSCN GIDPT
 * @wwnn:		remote port WWNN
 * @wwpn:		remote port WWPN
 * @chained_io_count:	Statistics : count of IOs with chained SGL's
 */
struct efc_node_s {
	struct list_head list_entry;
	struct efc_lport *efc;
	u32 instance_index;
	char display_name[EFC_DISPLAY_NAME_LENGTH];
	struct efc_sli_port_s *sport;
	bool hold_frames;
	struct efc_rlock_s lock;
	spinlock_t active_ios_lock;
	struct list_head active_ios;
	u64 max_wr_xfer_size;
	void *ini_node;
	void *tgt_node;

	struct efc_remote_node_s	rnode;
	/* Declarations private to FC transport */
	struct efc_sm_ctx_s		sm;
	u32		evtdepth;

	bool req_free;
	bool attached;
	bool fcp_enabled;
	bool rscn_pending;
	bool send_plogi;
	bool send_plogi_acc;
	bool io_alloc_enabled;

	enum efc_node_send_ls_acc_e	send_ls_acc;
	void			*ls_acc_io;
	u32		ls_acc_oxid;
	u32		ls_acc_did;
	enum efc_node_shutd_rsn_e	shutdown_reason;
	struct efc_dma_s		sparm_dma_buf;
	u8			service_params[EFC_SERVICE_PARMS_LENGTH];
	spinlock_t		pend_frames_lock;
	struct list_head	pend_frames;
	u32		pend_frames_processed;
	u32		ox_id_in_use;
	u32		els_retries_remaining;
	u32		els_req_cnt;
	u32		els_cmpl_cnt;
	u32		abort_cnt;

	char current_state_name[EFC_DISPLAY_NAME_LENGTH];
	char prev_state_name[EFC_DISPLAY_NAME_LENGTH];
	int		current_evt;
	int		prev_evt;
	bool targ;
	bool init;
	bool refound;
	struct list_head	els_io_pend_list;
	struct list_head	els_io_active_list;

	void *(*nodedb_state)(struct efc_sm_ctx_s *ctx,
			      u32 evt, void *arg);
	struct timer_list		gidpt_delay_timer;
	time_t			time_last_gidpt_msec;

	char wwnn[EFC_WWN_LENGTH];
	char wwpn[EFC_WWN_LENGTH];

	u32		chained_io_count;
};

/**
 * @brief Virtual port specification
 *
 * Collection of the information required to restore a virtual port across
 * link events
 * @domain_instance:	instance index of this domain for the sport
 * @wwnn:		node name
 * @wwpn:		port name
 * @fc_id:		port id
 * @tgt_data:		target backend pointer
 * @ini_data:		initiator backend pointe
 * @sport:		Used to match record after attaching for update
 *
 */

struct efc_vport_spec_s {
	struct list_head list_entry;
	u32 domain_instance;
	u64 wwnn;
	u64 wwpn;
	u32 fc_id;
	bool enable_tgt;
	bool enable_ini;
	void	*tgt_data;
	void	*ini_data;
	struct efc_sli_port_s *sport;
};

#define node_printf(node, fmt, args...) \
	pr_info("[%s] " fmt, node->display_name, ##args)

/**
 * @brief Node SM IO Context Callback structure
 *
 * Structure used as callback argument
 * @status:	completion status
 * @ext_status:	extended completion status
 * @header:	completion header buffer
 * @payload:	completion payload buffers
 * @els_rsp:	ELS response buffer
 */

struct efc_node_cb_s {
	int status;
	int ext_status;
	struct efc_hw_rq_buffer_s *header;
	struct efc_hw_rq_buffer_s *payload;
	struct efc_dma_s els_rsp;
};

/*
 * @brief HW unsolicited callback status
 */
enum efc_hw_unsol_status_e {
	EFC_HW_UNSOL_SUCCESS,
	EFC_HW_UNSOL_ERROR,
	EFC_HW_UNSOL_ABTS_RCVD,
	EFC_HW_UNSOL_MAX,	/**< must be last */
};

/*
 * @brief Defines the type of RQ buffer
 */
enum efc_hw_rq_buffer_type_e {
	EFC_HW_RQ_BUFFER_TYPE_HDR,
	EFC_HW_RQ_BUFFER_TYPE_PAYLOAD,
	EFC_HW_RQ_BUFFER_TYPE_MAX,
};

/*
 * @brief Defines a wrapper for the RQ payload buffers so that we can place it
 * back on the proper queue.
 */
struct efc_hw_rq_buffer_s {
	u16 rqindex;
	struct efc_dma_s dma;
};

/*
 * @brief Defines a general FC sequence object,
 * consisting of a header, payload buffers
 * and a HW IO in the case of port owned XRI
 */
struct efc_hw_sequence_s {
	struct list_head list_entry;
	void *hw;	/* HW that owns this sequence */
	/* sequence information */
	u8 fcfi;		/* FCFI associated with sequence */
	u8 auto_xrdy;	/* If auto XFER_RDY was generated */
	u8 out_of_xris;	/* If IO wld have been
			 *assisted if XRIs were available
			 */
	struct efc_hw_rq_buffer_s *header;
	struct efc_hw_rq_buffer_s *payload; /* rcvd frame payload buff */

	/* other "state" information from the SRB (sequence coalescing) */
	enum efc_hw_unsol_status_e status;
	u32 xri;		/* XRI assoc with seq; seq coalescing only */
	struct efct_hw_io_s *hio;/* HW IO */

	void *hw_priv;		/* HW private context */
};

struct libefc_function_template {
	/*Domain*/
	int (*hw_domain_alloc)(struct efc_lport *efc,
			       struct efc_domain_s *domain, u32 fcf, u32 vlan);

	int (*hw_domain_attach)(struct efc_lport *efc,
				struct efc_domain_s *domain, u32 fc_id);

	int (*hw_domain_free)(struct efc_lport *hw, struct efc_domain_s *d);

	int (*hw_domain_force_free)(struct efc_lport *efc,
				    struct efc_domain_s *domain);

	struct efc_domain_s *(*hw_domain_get)(struct efc_lport *hw, u16 fcfi);

	void (*domain_hold_frames)(struct efc_lport *efc,
				   struct efc_domain_s *domain);

	void (*domain_accept_frames)(struct efc_lport *efc,
				     struct efc_domain_s *domain);

	/*Port*/
	int (*hw_port_alloc)(struct efc_lport *hw, struct efc_sli_port_s *sp,
			     struct efc_domain_s *d, u8 *val);

	int (*hw_port_attach)(struct efc_lport *hw, struct efc_sli_port_s *sp,
			      u32 fc_id);

	int (*hw_port_free)(struct efc_lport *hw, struct efc_sli_port_s *sp);

	/*Node*/
	int (*hw_node_alloc)(struct efc_lport *hw, struct efc_remote_node_s *n,
			     u32 fc_addr, struct efc_sli_port_s *sport);

	int (*hw_node_attach)(struct efc_lport *hw, struct efc_remote_node_s *n,
			      struct efc_dma_s *sparams);

	int (*hw_node_detach)(struct efc_lport *hw,
			      struct efc_remote_node_s *r);

	int (*hw_node_free_resources)(struct efc_lport *efc,
				      struct efc_remote_node_s *node);
	int (*node_purge_pending)(struct efc_lport *efc, struct efc_node_s *n);

	void (*node_io_cleanup)(struct efc_lport *efc, struct efc_node_s *node,
				bool force);
	void (*node_els_cleanup)(struct efc_lport *efc, struct efc_node_s *node,
				 bool force);
	void (*node_abort_all_els)(struct efc_lport *efc, struct efc_node_s *n);

	/*Scsi*/

	void (*scsi_io_alloc_disable)(struct efc_lport *efc,
				      struct efc_node_s *node);
	void (*scsi_io_alloc_enable)(struct efc_lport *efc,
				     struct efc_node_s *node);

	int (*scsi_validate_initiator)(struct efc_lport *efc,
				       struct efc_node_s *node);

	int (*scsi_del_initiator)(struct efc_lport *efc,
				  struct efc_node_s *node, int reason);

	void (*tgt_del_sport)(struct efc_lport *efc, struct efc_sli_port_s *sp);

	void (*ini_del_sport)(struct efc_lport *efc, struct efc_sli_port_s *sp);
	int (*tgt_new_domain)(struct efc_lport *efc, struct efc_domain_s *d);
	void (*tgt_del_domain)(struct efc_lport *efc, struct efc_domain_s *d);
	void (*ini_del_domain)(struct efc_lport *efc, struct efc_domain_s *d);
	int (*ini_new_domain)(struct efc_lport *efc, struct efc_domain_s *d);
	int (*scsi_del_target)(struct efc_lport *efc,
			       struct efc_node_s *node, int reason);

	int (*tgt_new_sport)(struct efc_lport *efc, struct efc_sli_port_s *sp);
	int (*ini_new_sport)(struct efc_lport *efc, struct efc_sli_port_s *sp);
	int (*scsi_new_initiator)(struct efc_lport *efc, struct efc_node_s *sp);

	int (*scsi_new_target)(struct efc_lport *efc, struct efc_node_s *node);

	/*Send ELS*/

	void *(*els_send)(struct efc_lport *efc, struct efc_node_s *node,
			  u32 cmd, u32 timeout_sec, u32 retries);

	void *(*els_send_ct)(struct efc_lport *efc, struct efc_node_s *node,
			     u32 cmd, u32 timeout_sec, u32 retries);

	void *(*els_send_resp)(struct efc_lport *efc, struct efc_node_s *node,
			       u32 cmd, u16 ox_id);

	void *(*bls_send_acc_hdr)(struct efc_lport *efc, struct efc_node_s *n,
				  struct fc_frame_header *hdr);
	void *(*send_flogi_p2p_acc)(struct efc_lport *efc, struct efc_node_s *n,
				    u32 ox_id, u32 s_id);

	int (*send_ct_rsp)(struct efc_lport *efc, struct efc_node_s *node,
			   __be16 ox_id, struct fcct_iu_header_s *hdr,
			   u32 rsp_code, u32 reason_code, u32 rsn_code_expl);

	void *(*send_ls_rjt)(struct efc_lport *efc, struct efc_node_s *node,
			     u32 ox, u32 rcode, u32 rcode_expl, u32 vendor);

	int (*dispatch_fcp_cmd)(struct efc_node_s *node,
				struct efc_hw_sequence_s *seq);

	int (*dispatch_fcp_cmd_auto_xfer_rdy)(struct efc_node_s *node,
					      struct efc_hw_sequence_s *s);

	int (*dispatch_fcp_data)(struct efc_node_s *node,
				 struct efc_hw_sequence_s *s);

	int (*recv_abts_frame)(struct efc_lport *efc, struct efc_node_s *node,
			       struct efc_hw_sequence_s *seq);
};

/**
 * @brief efc library port structure
 * @base:	ponter to host structure
 * @req_wwpn:	wwpn requested by user for primary sport
 * @req_wwnn:	wwnn requested by user for primary sport
 * @nodes_count:number of allocated nodes
 * @nodes:	array of pointers to nodes
 * @nodes_free_list: linked list of free nodes
 * @vport_list:	list of VPORTS (NPIV)
 * @configured_link_state:requested link state
 * @lock:	Device wide lock
 * @domain_list:linked list of virtual fabric objects
 * @domain:	pointer to first (physical) domain (also on domain_list)
 * @domain_instance_count:domain instance count
 * @domain_list_empty_cb:domain list empty callback
 *
 */
struct efc_lport {
	void *base;
	struct pci_dev  *pcidev;
	u64 req_wwpn;
	u64 req_wwnn;

	u64 def_wwpn;
	u64 def_wwnn;
	u64 max_xfer_size;
	u32 nodes_count;
	struct efc_node_s **nodes;
	struct list_head nodes_free_list;

	/* vport */
	struct list_head vport_list;
	u32 link_status;
	u32 configured_link_state;

	struct libefc_function_template tt;
	struct efc_rlock_s lock;
	struct list_head domain_list;
	struct efc_ramlog_s *ramlog;

	bool enable_ini;
	bool enable_tgt;

	const char *desc;
	u32 instance_index;
	u16 pci_vendor;
	u16 pci_device;
	u16 pci_subsystem_vendor;
	u16 pci_subsystem_device;
	char businfo[EFC_DISPLAY_BUS_INFO_LENGTH];

	const char *model;
	const char *driver_version;
	const char *fw_version;
	struct efc_domain_s *domain;
	u32 domain_instance_count;
	void (*domain_list_empty_cb)(struct efc_lport *efc, void *arg);
	void *domain_list_empty_cb_arg;

	struct efc_domain_s	*domains[64];

	/*
	 * tgt_rscn_delay - delay in kicking off RSCN processing
	 * (nameserver queries) after receiving an RSCN on the
	 * target. This prevents thrashing of nameserver
	 * requests due to a huge burst of RSCNs received in a
	 * short period of time
	 * Note: this is only valid when target RSCN handling
	 * is enabled -- see ctrlmask.
	 */
	time_t tgt_rscn_delay_msec;

	/*
	 * tgt_rscn_period - determines maximum frequency when
	 * processing back-to-back
	 * RSCNs; e.g. if this value is 30, there will never be any
	 * more than 1 RSCN handling per 30s window. This prevents
	 * initiators on a faulty link generating
	 * many RSCN from causing the target to continually query the
	 * nameserver.
	 * Note:this is only valid when target RSCN handling is enabled
	 */
	time_t tgt_rscn_period_msec;

	/*
	 * Target IO timer value:
	 * Zero: target command timeout disabled.
	 * Non-zero: Timeout value, in seconds, for target commands
	 */
	u32 target_io_timer_sec;

	bool external_loopback;
	u32 nodedb_mask;
};

/*
 * EFC library registration
 * **********************************/
int efcport_init(struct efc_lport *efc);
void efcport_destroy(struct efc_lport *efc);
/*
 * EFC Domain
 * **********************************/
int efc_domain_cb(void *arg, int event, void *data);
void efc_domain_force_free(struct efc_domain_s *domain);
void efc_register_domain_list_empty_cb(struct efc_lport *efc,
				       void (*callback)(struct efc_lport *efc,
							void *arg),
				       void *arg);

/*
 * EFC Local port
 * **********************************/
int efc_lport_cb(void *arg, int event, void *data);
int8_t efc_vport_create_spec(struct efc_lport *efc, u64 wwnn,
			     u64 wwpn, u32 fc_id, bool enable_ini,
			     bool enable_tgt, void *tgt_data, void *ini_data);
int efc_sport_vport_new(struct efc_domain_s *domain, u64 wwpn,
			u64 wwnn, u32 fc_id, bool ini, bool tgt,
			void *tgt_data, void *ini_data, bool restore_vport);
int efc_sport_vport_del(struct efc_lport *efc, struct efc_domain_s *domain,
			u64 wwpn, u64 wwnn);

void efc_vport_del_all(struct efc_lport *efc);

struct efc_sli_port_s *efc_sport_find(struct efc_domain_s *domain, u32 d_id);

/*
 * EFC Node
 * **********************************/
int efc_remote_node_cb(void *arg, int event, void *data);
u64 efc_node_get_wwnn(struct efc_node_s *node);
u64 efc_node_get_wwpn(struct efc_node_s *node);
struct efc_node_s *efc_node_find(struct efc_sli_port_s *sport, u32 id);
void efc_node_fcid_display(u32 fc_id, char *buffer, u32 buf_len);

void efc_node_post_els_resp(struct efc_node_s *node, u32 evt, void *arg);
void efc_node_post_shutdown(struct efc_node_s *node, u32 evt, void *arg);
/*
 * EFC FCP/ELS/CT interface
 * **********************************/
int efc_node_recv_abts_frame(struct efc_lport *efc,
			     struct efc_node_s *node,
			     struct efc_hw_sequence_s *seq);
int
efc_node_recv_els_frame(struct efc_node_s *node, struct efc_hw_sequence_s *s);
int efc_domain_dispatch_frame(void *arg, struct efc_hw_sequence_s *seq);

int efc_node_dispatch_frame(void *arg, struct efc_hw_sequence_s *seq);

int
efc_node_recv_ct_frame(struct efc_node_s *node, struct efc_hw_sequence_s *seq);
int
efc_node_recv_fcp_cmd(struct efc_node_s *node, struct efc_hw_sequence_s *seq);
int
efc_node_recv_bls_no_sit(struct efc_node_s *node, struct efc_hw_sequence_s *s);

/*
 * EFC SCSI INTERACTION LAYER
 * **********************************/
void
efc_scsi_del_initiator_complete(struct efc_lport *efc, struct efc_node_s *node);
void
efc_scsi_del_target_complete(struct efc_lport *efc, struct efc_node_s *node);
void efc_scsi_io_list_empty(struct efc_lport *efc, struct efc_node_s *node);

/*
 * EFC Node lock helpers
 * **********************************/

int efct_node_lock_try(struct efc_node_s *node);
void efct_node_lock(struct efc_node_s *node);
void efct_node_unlock(struct efc_node_s *node);

#endif /* __EFCLIB_H__ */
