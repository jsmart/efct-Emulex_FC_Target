/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#include "efct_driver.h"
#include "efct_els.h"
#include "efct_utils.h"
#include "efct_hw.h"

#define enable_tsend_auto_resp(efct)	((efct->ctrlmask & \
	  EFCT_CTRLMASK_XPORT_DISABLE_AUTORSP_TSEND) == 0)
#define enable_treceive_auto_resp(efct)	((efct->ctrlmask & \
	  EFCT_CTRLMASK_XPORT_DISABLE_AUTORSP_TRECEIVE) == 0)

#define SCSI_IOFMT "[%04x][i:%04x t:%04x h:%04x]"
#define SCSI_ITT_SIZE(efct)    4

#define SCSI_IOFMT_ARGS(io)	\
	(io->instance_index,	\
	io->init_task_tag,	\
	io->tgt_task_tag, io->hw_tag)

#define scsi_io_printf(io, fmt, ...) \
	efct_log_debug(io->efct, "[%s]" SCSI_IOFMT fmt, \
		io->node->display_name, io->instance_index,\
		io->init_task_tag, io->tgt_task_tag, io->hw_tag, ##__VA_ARGS__)

#define scsi_io_trace(io, fmt, ...) \
	do { \
		if (EFCT_LOG_ENABLE_SCSI_TRACE(io->efct)) \
			scsi_io_printf(io, fmt, ##__VA_ARGS__); \
	} while (0)

#define scsi_log(efct, fmt, ...) \
	do { \
		if (EFCT_LOG_ENABLE_SCSI_TRACE(efct)) \
			efct_log_info(efct, fmt, ##__VA_ARGS__); \
	} while (0)

static int
efct_target_send_bls_resp(struct efct_io_s *, efct_scsi_io_cb_t, void *);
static int
efct_scsi_abort_io_cb(const struct efct_hw_io_s *, struct efc_remote_node_s *,
		      u32, int, u32, void *);

static void
efct_scsi_io_free_ovfl(struct efct_io_s *);
static u32
efct_scsi_count_sgls(struct efct_hw_dif_info_s *, struct efct_scsi_sgl_s *,
		     u32);
static int
efct_scsi_io_dispatch_hw_io(struct efct_io_s *, struct efct_hw_io_s *);
static int
efct_scsi_io_dispatch_no_hw_io(struct efct_io_s *);

/**
 * @ingroup scsi_api_base
 * @brief Returns a big-endian 32-bit value given a pointer.
 *
 * @param p Pointer to the 32-bit big-endian location.
 *
 * @return Returns the byte-swapped 32-bit value.
 */

static inline u32
efct_fc_getbe32(void *p)
{
	return be32_to_cpu(*((u32 *)p));
}

/**
 * @ingroup scsi_api_base
 * @brief Enable IO allocation.
 *
 * @par Description
 * The SCSI and Transport IO allocation functions are enabled.
 * If the allocation functions are not enabled, then calls to
 * efct_scsi_io_alloc() (and efct_els_io_alloc() for FC) will fail.
 *
 * @param node Pointer to node object.
 *
 * @return None.
 */
void
efct_scsi_io_alloc_enable(struct efc_lport *efc, struct efc_node_s *node)
{
	unsigned long flags = 0;

	efct_assert(node);
	spin_lock_irqsave(&node->active_ios_lock, flags);
		node->io_alloc_enabled = true;
	spin_unlock_irqrestore(&node->active_ios_lock, flags);
}

/**
 * @ingroup scsi_api_base
 * @brief Disable IO allocation
 *
 * @par Description
 * The SCSI and Transport IO allocation functions are disabled.
 * If the allocation functions are not enabled, then calls to
 * efct_scsi_io_alloc() (and efct_els_io_alloc() for FC) will fail.
 *
 * @param node Pointer to node object
 *
 * @return None.
 */
void
efct_scsi_io_alloc_disable(struct efc_lport *efc, struct efc_node_s *node)
{
	unsigned long flags = 0;

	efct_assert(node);
	spin_lock_irqsave(&node->active_ios_lock, flags);
		node->io_alloc_enabled = false;
	spin_unlock_irqrestore(&node->active_ios_lock, flags);
}

/**
 * @ingroup scsi_api_base
 * @brief Allocate a SCSI IO context.
 *
 * @par Description
 * A SCSI IO context is allocated and associated with a @c node.
 * This function is called by an initiator-client when issuing SCSI
 * commands to remote target devices. On completion, efct_scsi_io_free()
 * is called.
 * The returned struct efct_io_s structure has an element of type
 * struct efct_scsi_ini_io_s named &quot;ini_io&quot; that is declared
 * and used by an initiator-client
 * for private information.
 *
 * @param node Pointer to the associated node structure.
 * @param role Role for IO (originator/responder).
 *
 * @return Returns the pointer to the IO context, or NULL.
 *
 */

struct efct_io_s *
efct_scsi_io_alloc(struct efc_node_s *node, enum efct_scsi_io_role_e role)
{
	struct efct_s *efct;
	struct efc_lport *efcp;
	struct efct_xport_s *xport;
	struct efct_io_s *io;
	unsigned long flags = 0;

	efct_assert(node, NULL);
	efct_assert(node->efc, NULL);

	efcp = node->efc;
	efct = efcp->base;
	efct_assert(efct->xport, NULL);
	xport = efct->xport;

	spin_lock_irqsave(&node->active_ios_lock, flags);

		if (!node->io_alloc_enabled) {
			spin_unlock_irqrestore(&node->active_ios_lock, flags);
			return NULL;
		}

		io = efct_io_pool_io_alloc(efct->xport->io_pool);
		if (!io) {
			atomic_add_return(1, &xport->io_alloc_failed_count);
			spin_unlock_irqrestore(&node->active_ios_lock, flags);
			return NULL;
		}

		/* initialize refcount */
		kref_init(&io->ref);
		io->release = _efct_scsi_io_free;

		if (io->hio) {
			efct_log_err(efct,
				     "assertion failed: io->hio is not NULL\n");
			spin_unlock_irqrestore(&node->active_ios_lock, flags);
			return NULL;
		}

		/* set generic fields */
		io->efct = efct;
		io->node = node;

		/* set type and name */
		io->io_type = EFCT_IO_TYPE_IO;
		io->display_name = "scsi_io";

		switch (role) {
		case EFCT_SCSI_IO_ROLE_ORIGINATOR:
			io->cmd_ini = true;
			io->cmd_tgt = false;
			break;
		case EFCT_SCSI_IO_ROLE_RESPONDER:
			io->cmd_ini = false;
			io->cmd_tgt = true;
			break;
		}

		/* Add to node's active_ios list */
		INIT_LIST_HEAD(&io->list_entry);
		list_add_tail(&io->list_entry, &node->active_ios);

	spin_unlock_irqrestore(&node->active_ios_lock, flags);

	return io;
}

/**
 * @ingroup scsi_api_base
 * @brief Free a SCSI IO context (internal).
 *
 * @par Description
 * The IO context previously allocated using efct_scsi_io_alloc()
 * is freed. This is called from within the transport layer,
 * when the reference count goes to zero.
 *
 * @param arg Pointer to the IO context.
 *
 * @return None.
 */
void
_efct_scsi_io_free(struct kref *arg)
{
	struct efct_io_s *io = container_of(arg, struct efct_io_s, ref);
	struct efct_s *efct = io->efct;
	struct efc_node_s *node = io->node;
	int send_empty_event;
	unsigned long flags = 0;

	efct_assert(io);

	scsi_io_trace(io, "freeing io 0x%p %s\n", io, io->display_name);

	efct_assert(efct_io_busy(io));

	spin_lock_irqsave(&node->active_ios_lock, flags);
		list_del(&io->list_entry);
		send_empty_event = (!node->io_alloc_enabled) &&
					list_empty(&node->active_ios);
	spin_unlock_irqrestore(&node->active_ios_lock, flags);

	if (send_empty_event)
		efc_scsi_io_list_empty(node->efc, node);

	io->node = NULL;
	efct_io_pool_io_free(efct->xport->io_pool, io);
}

/**
 * @ingroup scsi_api_base
 * @brief Free a SCSI IO context.
 *
 * @par Description
 * The IO context previously allocated using efct_scsi_io_alloc() is freed.
 *
 * @param io Pointer to the IO context.
 *
 * @return None.
 */
void
efct_scsi_io_free(struct efct_io_s *io)
{
	scsi_io_trace(io, "freeing io 0x%p %s\n", io, io->display_name);
	efct_assert(refcount_read(&io->ref.refcount) > 0);
	kref_put(&io->ref, io->release);
}

static int
efct_scsi_send_io(enum efct_hw_io_type_e type, struct efc_node_s *node,
		  struct efct_io_s *io,
	u32 lun, enum efct_scsi_tmf_cmd_e tmf, u8 *cdb,
	u32 cdb_len, struct efct_scsi_dif_info_s *dif_info,
	struct efct_scsi_sgl_s *sgl, u32 sgl_count, u32 wire_len,
	u32 first_burst, efct_scsi_rsp_io_cb_t cb, void *arg);

/**
 * @brief Target response completion callback.
 *
 * @par Description
 * Function is called upon the completion of a target IO request.
 *
 * @param hio Pointer to the HW IO structure.
 * @param rnode Remote node associated with the IO that is completing.
 * @param length Length of the response payload.
 * @param status Completion status.
 * @param ext_status Extended completion status.
 * @param app Application-specific data (generally a pointer to
 * the IO context).
 *
 * @return None.
 */

static void
efct_target_io_cb(struct efct_hw_io_s *hio, struct efc_remote_node_s *rnode,
		  u32 length, int status, u32 ext_status, void *app)
{
	struct efct_io_s *io = app;
	struct efct_s *efct;
	enum efct_scsi_io_status_e scsi_stat = EFCT_SCSI_STATUS_GOOD;

	efct_assert(io);

	scsi_io_trace(io, "status x%x ext_status x%x\n", status, ext_status);

	efct = io->efct;
	efct_assert(efct);

	efct_scsi_io_free_ovfl(io);

	io->transferred += length;

	/* Call target server completion */
	if (io->scsi_tgt_cb) {
		efct_scsi_io_cb_t cb = io->scsi_tgt_cb;
		u32 flags = 0;

		/* Clear the callback before invoking the callback */
		io->scsi_tgt_cb = NULL;

		/* if status was good, and auto-good-response was set,
		 * then callback target-server with IO_CMPL_RSP_SENT,
		 * otherwise send IO_CMPL
		 */
		if (status == 0 && io->auto_resp)
			flags |= EFCT_SCSI_IO_CMPL_RSP_SENT;
		else
			flags |= EFCT_SCSI_IO_CMPL;

		switch (status) {
		case SLI4_FC_WCQE_STATUS_SUCCESS:
			scsi_stat = EFCT_SCSI_STATUS_GOOD;
			break;
		case SLI4_FC_WCQE_STATUS_DI_ERROR:
			if (ext_status & SLI4_FC_DI_ERROR_GE)
				scsi_stat = EFCT_SCSI_STATUS_DIF_GUARD_ERR;
			else if (ext_status & SLI4_FC_DI_ERROR_AE)
				scsi_stat = EFCT_SCSI_STATUS_DIF_APP_TAG_ERROR;
			else if (ext_status & SLI4_FC_DI_ERROR_RE)
				scsi_stat = EFCT_SCSI_STATUS_DIF_REF_TAG_ERROR;
			else
				scsi_stat = EFCT_SCSI_STATUS_DIF_UNKNOWN_ERROR;
			break;
		case SLI4_FC_WCQE_STATUS_LOCAL_REJECT:
			switch (ext_status) {
			case SLI4_FC_LOCAL_REJECT_INVALID_RELOFFSET:
			case SLI4_FC_LOCAL_REJECT_ABORT_REQUESTED:
				scsi_stat = EFCT_SCSI_STATUS_ABORTED;
				break;
			case SLI4_FC_LOCAL_REJECT_INVALID_RPI:
				scsi_stat = EFCT_SCSI_STATUS_NEXUS_LOST;
				break;
			case SLI4_FC_LOCAL_REJECT_NO_XRI:
				scsi_stat = EFCT_SCSI_STATUS_NO_IO;
				break;
			default:
				/*we have seen 0x0d(TX_DMA_FAILED err)*/
				scsi_stat = EFCT_SCSI_STATUS_ERROR;
				break;
			}
			break;

		case SLI4_FC_WCQE_STATUS_TARGET_WQE_TIMEOUT:
			/* target IO timed out */
			scsi_stat = EFCT_SCSI_STATUS_TIMEDOUT_AND_ABORTED;
			break;

		case SLI4_FC_WCQE_STATUS_SHUTDOWN:
			/* Target IO cancelled by HW */
			scsi_stat = EFCT_SCSI_STATUS_SHUTDOWN;
			break;

		default:
			scsi_stat = EFCT_SCSI_STATUS_ERROR;
			break;
		}

		cb(io, scsi_stat, flags, io->scsi_tgt_cb_arg);
	}
	efct_scsi_check_pending(efct);
}

/**
 * @brief Return count of SGE's required for request
 *
 * @par Description
 * An accurate count of SGEs is computed and returned.
 *
 * @param hw_dif Pointer to HW dif information.
 * @param sgl Pointer to SGL from back end.
 * @param sgl_count Count of SGEs in SGL.
 *
 * @return Count of SGEs.
 */
static u32
efct_scsi_count_sgls(struct efct_hw_dif_info_s *hw_dif,
		     struct efct_scsi_sgl_s *sgl, u32 sgl_count)
{
	u32 count = 0;
	u32 i;

	/* Convert DIF Information */
	if (hw_dif->dif_oper != EFCT_HW_DIF_OPER_DISABLED) {
		/* If we're not DIF separate, then emit a seed SGE */
		if (!hw_dif->dif_separate)
			count++;

		for (i = 0; i < sgl_count; i++) {
			/* If DIF is enabled, and DIF is separate,
			 * then append a SEED then DIF SGE
			 */
			if (hw_dif->dif_separate)
				count += 2;

			count++;
		}
	} else {
		count = sgl_count;
	}
	return count;
}

static int
efct_scsi_build_sgls(struct efct_hw_s *hw, struct efct_hw_io_s *hio,
		     struct efct_hw_dif_info_s *hw_dif,
		struct efct_scsi_sgl_s *sgl, u32 sgl_count,
		enum efct_hw_io_type_e type)
{
	int rc;
	u32 i;
	struct efct_s *efct = hw->os;
	u32 blocksize = 0;
	u32 blockcount;

	efct_assert(hio, -1);

	/* Initialize HW SGL */
	rc = efct_hw_io_init_sges(hw, hio, type);
	if (rc) {
		efct_log_err(efct, "efct_hw_io_init_sges failed: %d\n", rc);
		return -1;
	}

	/* Convert DIF Information */
	if (hw_dif->dif_oper != EFCT_HW_DIF_OPER_DISABLED) {
		/* If we're not DIF separate, then emit a seed SGE */
		if (!hw_dif->dif_separate) {
			rc = efct_hw_io_add_seed_sge(hw, hio, hw_dif);
			if (rc)
				return rc;
		}

		/* if we are doing DIF separate, then figure out the
		 * block size so that we can update the ref tag in the
		 * DIF seed SGE.   Also verify that the
		 * the sgl lengths are all multiples of the blocksize
		 */
		if (hw_dif->dif_separate) {
			switch (hw_dif->blk_size) {
			case EFCT_HW_DIF_BK_SIZE_512:
				blocksize = 512;
				break;
			case EFCT_HW_DIF_BK_SIZE_1024:
				blocksize = 1024;
				break;
			case EFCT_HW_DIF_BK_SIZE_2048:
				blocksize = 2048;
				break;
			case EFCT_HW_DIF_BK_SIZE_4096:
				blocksize = 4096;
				break;
			case EFCT_HW_DIF_BK_SIZE_520:
				blocksize = 520;
				break;
			case EFCT_HW_DIF_BK_SIZE_4104:
				blocksize = 4104;
				break;
			default:
				efct_log_test(hw->os,
					      "Inavlid hw_dif blocksize %d\n",
					hw_dif->blk_size);
				return -1;
			}
			for (i = 0; i < sgl_count; i++) {
				if ((sgl[i].len % blocksize) != 0) {
					efct_log_test(hw->os,
						      "sgl[%d] len of %ld is not multiple of blocksize\n",
					i, sgl[i].len);
					return -1;
				}
			}
		}

		for (i = 0; i < sgl_count; i++) {
			efct_assert(sgl[i].addr, -1);
			efct_assert(sgl[i].len, -1);

			/* If DIF is enabled, and DIF is separate,
			 * then append a SEED then DIF SGE
			 */
			if (hw_dif->dif_separate) {
				rc = efct_hw_io_add_seed_sge(hw, hio,
							     hw_dif);
				if (rc)
					return rc;
				rc = efct_hw_io_add_dif_sge(hw, hio,
							    sgl[i].dif_addr);
				if (rc)
					return rc;
				/* Update the ref_tag for next DIF seed SGE*/
				blockcount = sgl[i].len / blocksize;
				if (hw_dif->dif_oper ==
					EFCT_HW_DIF_OPER_INSERT)
					hw_dif->ref_tag_repl += blockcount;
				else
					hw_dif->ref_tag_cmp += blockcount;
			}

			/* Add data SGE */
			rc = efct_hw_io_add_sge(hw, hio,
						sgl[i].addr, sgl[i].len);
			if (rc) {
				efct_log_err(efct,
					     "add sge failed cnt=%d rc=%d\n",
					     sgl_count, rc);
				return rc;
			}
		}
	} else {
		for (i = 0; i < sgl_count; i++) {
			efct_assert(sgl[i].addr, -1);
			efct_assert(sgl[i].len, -1);

			/* Add data SGE */
			rc = efct_hw_io_add_sge(hw, hio,
						sgl[i].addr, sgl[i].len);
			if (rc) {
				efct_log_err(efct,
					     "add sge failed cnt=%d rc=%d\n",
					     sgl_count, rc);
				return rc;
			}
		}
	}
	return 0;
}

/**
 * @ingroup scsi_api_base
 * @brief Convert SCSI API T10 DIF information into the FC HW format.
 *
 * @param efct Pointer to the efct structure for logging.
 * @param scsi_dif_info Pointer to the SCSI API T10 DIF fields.
 * @param hw_dif_info Pointer to the FC HW API T10 DIF fields.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

static int
efct_scsi_convert_dif_info(struct efct_s *efct,
			   struct efct_scsi_dif_info_s *scsi_dif_info,
			  struct efct_hw_dif_info_s *hw_dif_info)
{
	u32 dif_seed;

	memset(hw_dif_info, 0,
	       sizeof(struct efct_hw_dif_info_s));

	if (!scsi_dif_info) {
		hw_dif_info->dif_oper = EFCT_HW_DIF_OPER_DISABLED;
		hw_dif_info->blk_size =  EFCT_HW_DIF_BK_SIZE_NA;
		return 0;
	}

	/* Convert the DIF operation */
	switch (scsi_dif_info->dif_oper) {
	case EFCT_SCSI_DIF_OPER_IN_NODIF_OUT_CRC:
		hw_dif_info->dif_oper = EFCT_HW_SGE_DIFOP_INNODIFOUTCRC;
		hw_dif_info->dif = SLI4_DIF_INSERT;
		break;
	case EFCT_SCSI_DIF_OPER_IN_CRC_OUT_NODIF:
		hw_dif_info->dif_oper = EFCT_HW_SGE_DIFOP_INCRCOUTNODIF;
		hw_dif_info->dif = SLI4_DIF_STRIP;
		break;
	case EFCT_SCSI_DIF_OPER_IN_NODIF_OUT_CHKSUM:
		hw_dif_info->dif_oper =
				EFCT_HW_SGE_DIFOP_INNODIFOUTCHKSUM;
		hw_dif_info->dif = SLI4_DIF_INSERT;
		break;
	case EFCT_SCSI_DIF_OPER_IN_CHKSUM_OUT_NODIF:
		hw_dif_info->dif_oper = EFCT_HW_SGE_DIFOP_INCHKSUMOUTNODIF;
		hw_dif_info->dif = SLI4_DIF_STRIP;
		break;
	case EFCT_SCSI_DIF_OPER_IN_CRC_OUT_CRC:
		hw_dif_info->dif_oper = EFCT_HW_SGE_DIFOP_INCRCOUTCRC;
		hw_dif_info->dif = SLI4_DIF_PASS_THROUGH;
		break;
	case EFCT_SCSI_DIF_OPER_IN_CHKSUM_OUT_CHKSUM:
		hw_dif_info->dif_oper =
			EFCT_HW_SGE_DIFOP_INCHKSUMOUTCHKSUM;
		hw_dif_info->dif = SLI4_DIF_PASS_THROUGH;
		break;
	case EFCT_SCSI_DIF_OPER_IN_CRC_OUT_CHKSUM:
		hw_dif_info->dif_oper = EFCT_HW_SGE_DIFOP_INCRCOUTCHKSUM;
		hw_dif_info->dif = SLI4_DIF_PASS_THROUGH;
		break;
	case EFCT_SCSI_DIF_OPER_IN_CHKSUM_OUT_CRC:
		hw_dif_info->dif_oper = EFCT_HW_SGE_DIFOP_INCHKSUMOUTCRC;
		hw_dif_info->dif = SLI4_DIF_PASS_THROUGH;
		break;
	case EFCT_SCSI_DIF_OPER_IN_RAW_OUT_RAW:
		hw_dif_info->dif_oper = EFCT_HW_SGE_DIFOP_INRAWOUTRAW;
		hw_dif_info->dif = SLI4_DIF_PASS_THROUGH;
		break;
	default:
		efct_log_test(efct, "unhandled SCSI DIF operation %d\n",
			      scsi_dif_info->dif_oper);
		return -1;
	}

	switch (scsi_dif_info->blk_size) {
	case EFCT_SCSI_DIF_BK_SIZE_512:
		hw_dif_info->blk_size = EFCT_HW_DIF_BK_SIZE_512;
		break;
	case EFCT_SCSI_DIF_BK_SIZE_1024:
		hw_dif_info->blk_size = EFCT_HW_DIF_BK_SIZE_1024;
		break;
	case EFCT_SCSI_DIF_BK_SIZE_2048:
		hw_dif_info->blk_size = EFCT_HW_DIF_BK_SIZE_2048;
		break;
	case EFCT_SCSI_DIF_BK_SIZE_4096:
		hw_dif_info->blk_size = EFCT_HW_DIF_BK_SIZE_4096;
		break;
	case EFCT_SCSI_DIF_BK_SIZE_520:
		hw_dif_info->blk_size = EFCT_HW_DIF_BK_SIZE_520;
		break;
	case EFCT_SCSI_DIF_BK_SIZE_4104:
		hw_dif_info->blk_size = EFCT_HW_DIF_BK_SIZE_4104;
		break;
	default:
		efct_log_test(efct, "unhandled SCSI DIF block size %d\n",
			      scsi_dif_info->blk_size);
		return -1;
	}

	/* If the operation is an INSERT the tags provided are the
	 * ones that should be inserted, otherwise they're the ones
	 * to be checked against.
	 */
	if (hw_dif_info->dif == SLI4_DIF_INSERT) {
		hw_dif_info->ref_tag_repl = scsi_dif_info->ref_tag;
		hw_dif_info->app_tag_repl = scsi_dif_info->app_tag;
	} else {
		hw_dif_info->ref_tag_cmp = scsi_dif_info->ref_tag;
		hw_dif_info->app_tag_cmp = scsi_dif_info->app_tag;
	}

	hw_dif_info->check_ref_tag = scsi_dif_info->check_ref_tag;
	hw_dif_info->check_app_tag = scsi_dif_info->check_app_tag;
	hw_dif_info->check_guard = scsi_dif_info->check_guard;
	hw_dif_info->auto_incr_ref_tag = true;
	hw_dif_info->dif_separate = scsi_dif_info->dif_separate;
	hw_dif_info->disable_app_ffff = scsi_dif_info->disable_app_ffff;
	hw_dif_info->disable_app_ref_ffff =
			scsi_dif_info->disable_app_ref_ffff;

	efct_hw_get(&efct->hw, EFCT_HW_DIF_SEED, &dif_seed);
	hw_dif_info->dif_seed = dif_seed;

	return 0;
}

/**
 * @ingroup scsi_api_base
 * @brief This function logs the SGLs for an IO.
 *
 * @param io Pointer to the IO context.
 */
static void efct_log_sgl(struct efct_io_s *io)
{
	struct efct_hw_io_s *hio = io->hio;
	struct sli4_sge_s *data = NULL;
	u32 *dword = NULL;
	u32 i;
	u32 n_sge;

	scsi_io_trace(io, "def_sgl at 0x%x 0x%08x\n",
		      upper_32_bits(hio->def_sgl.phys),
		      lower_32_bits(hio->def_sgl.phys));
	n_sge = (hio->sgl == &hio->def_sgl ?
			hio->n_sge : hio->def_sgl_count);
	for (i = 0, data = hio->def_sgl.virt; i < n_sge; i++, data++) {
		dword = (u32 *)data;

		scsi_io_trace(io, "SGL %2d 0x%08x 0x%08x 0x%08x 0x%08x\n",
			      i, dword[0], dword[1], dword[2], dword[3]);

		if (dword[2] & (1U << 31))
			break;
	}

	if (hio->ovfl_sgl &&
	    hio->sgl == hio->ovfl_sgl) {
		scsi_io_trace(io, "Overflow at 0x%x 0x%08x\n",
			      upper_32_bits(hio->ovfl_sgl->phys),
			      lower_32_bits(hio->ovfl_sgl->phys));
		for (i = 0, data = hio->ovfl_sgl->virt; i < hio->n_sge;
			i++, data++) {
			dword = (u32 *)data;

			scsi_io_trace(io,
				      "SGL %2d 0x%08x 0x%08x 0x%08x 0x%08x\n",
				i, dword[0], dword[1], dword[2], dword[3]);
			if (dword[2] & (1U << 31))
				break;
		}
	}
}

/* @brief Check pending error asynchronous callback function.
 *
 * @par Description
 * Invoke the HW callback function for a given IO. This
 * function is called from the NOP mailbox completion context.
 *
 * @param hw Pointer to HW object.
 * @param status Completion status.
 * @param mqe Mailbox completion queue entry.
 * @param arg General purpose argument.
 *
 * @return Returns 0.
 */
static int
efct_scsi_check_pending_async_cb(struct efct_hw_s *hw, int status,
				 u8 *mqe, void *arg)
{
	struct efct_io_s *io = arg;

	if (io) {
		if (io->hw_cb) {
			efct_hw_done_t cb = io->hw_cb;

			io->hw_cb = NULL;
			(cb)(io->hio, NULL, 0,
			 SLI4_FC_WCQE_STATUS_DISPATCH_ERROR, 0, io);
		}
	}
	return 0;
}

/**
 * @brief Check for pending IOs to dispatch.
 *
 * @par Description
 * If there are IOs on the pending list, and a HW IO is available, then
 * dispatch the IOs.
 *
 * @param efct Pointer to the EFCT structure.
 *
 * @return None.
 */

void
efct_scsi_check_pending(struct efct_s *efct)
{
	struct efct_xport_s *xport = efct->xport;
	struct efct_io_s *io = NULL;
	struct efct_hw_io_s *hio;
	int status;
	int count = 0;
	int dispatch;
	unsigned long flags = 0;

	/* Guard against recursion */
	if (atomic_add_return(1, &xport->io_pending_recursing)) {
		/* This function is already running.  Decrement and return. */
		atomic_sub_return(1, &xport->io_pending_recursing);
		return;
	}

	do {
		spin_lock_irqsave(&xport->io_pending_lock, flags);
		status = 0;
		hio = NULL;
		if (!list_empty(&xport->io_pending_list)) {
			io = list_first_entry(&xport->io_pending_list,
					      struct efct_io_s,
					      io_pending_link);
		}
		if (io) {
			list_del(&io->io_pending_link);
			if (io->io_type == EFCT_IO_TYPE_ABORT) {
				hio = NULL;
			} else {
				hio = efct_hw_io_alloc(&efct->hw);
				if (!hio) {
					/*
					 * No HW IO available.Put IO back on
					 * the front of pending list
					 */
					list_add(&xport->io_pending_list,
						 &io->io_pending_link);
					io = NULL;
				} else {
					hio->eq = io->hw_priv;
				}
			}
		}
		/* Must drop the lock before dispatching the IO */
		spin_unlock_irqrestore(&xport->io_pending_lock, flags);

		if (io) {
			count++;

			/*
			 * We pulled an IO off the pending list,
			 * and either got an HW IO or don't need one
			 */
			atomic_sub_return(1, &xport->io_pending_count);
			if (!hio)
				status = efct_scsi_io_dispatch_no_hw_io(io);
			else
				status = efct_scsi_io_dispatch_hw_io(io, hio);
			if (status) {
				/*
				 * Invoke the HW callback, but do so in the
				 * separate execution context,provided by the
				 * NOP mailbox completion processing context
				 * by using efct_hw_async_call()
				 */
				if (efct_hw_async_call(&efct->hw,
					       efct_scsi_check_pending_async_cb,
					io)) {
					efct_log_test(efct,
						      "call hw async failed\n");
				}
			}
		}
	} while (io);

	/*
	 * If nothing was removed from the list,
	 * we might be in a case where we need to abort an
	 * active IO and the abort is on the pending list.
	 * Look for an abort we can dispatch.
	 */
	if (count == 0) {
		dispatch = 0;

		spin_lock_irqsave(&xport->io_pending_lock, flags);
		list_for_each_entry(io, &xport->io_pending_list,
				    io_pending_link) {
			if (io->io_type == EFCT_IO_TYPE_ABORT) {
				if (io->io_to_abort->hio) {
					/* This IO has a HW IO, so it is
					 * active.  Dispatch the abort.
					 */
					dispatch = 1;
				} else {
					/* Leave this abort on the pending
					 * list and keep looking
					 */
					dispatch = 0;
				}
			}
			if (dispatch) {
				list_del(&io->list_entry);
				atomic_sub_return(1, &xport->io_pending_count);
				break;
			}
		}
		spin_unlock_irqrestore(&xport->io_pending_lock, flags);

		if (dispatch) {
			status = efct_scsi_io_dispatch_no_hw_io(io);
			if (status) {
				if (efct_hw_async_call(&efct->hw,
					       efct_scsi_check_pending_async_cb,
					io)) {
					efct_log_test(efct,
						      "call to hw async failed\n");
				}
			}
		}
	}

	atomic_sub_return(1, &xport->io_pending_recursing);
}

/**
 * @brief Attempt to dispatch a non-abort IO
 *
 * @par Description
 * An IO is dispatched:
 * - if the pending list is not empty, add IO to pending list
 *   and call a function to process the pending list.
 * - if pending list is empty, try to allocate a HW IO. If none
 *   is available, place this IO at the tail of the pending IO
 *   list.
 * - if HW IO is available, attach this IO to the HW IO and
 *   submit it.
 *
 * @param io Pointer to IO structure.
 * @param cb Callback function.
 *
 * @return Returns 0 on success, a negative error code value on failure.
 */

int
efct_scsi_io_dispatch(struct efct_io_s *io, void *cb)
{
	struct efct_hw_io_s *hio;
	struct efct_s *efct = io->efct;
	struct efct_xport_s *xport = efct->xport;
	unsigned long flags = 0;

	efct_assert(io->cmd_tgt || io->cmd_ini, -1);
	efct_assert((io->io_type != EFCT_IO_TYPE_ABORT), -1);
	io->hw_cb = cb;

	/*
	 * if this IO already has a HW IO, then this is either
	 * not the first phase of the IO. Send it to the HW.
	 */
	if (io->hio)
		return efct_scsi_io_dispatch_hw_io(io, io->hio);

	/*
	 * We don't already have a HW IO associated with the IO. First check
	 * the pending list. If not empty, add IO to the tail and process the
	 * pending list.
	 */
	spin_lock_irqsave(&xport->io_pending_lock, flags);
		if (!list_empty(&xport->io_pending_list)) {
			/*
			 * If this is a low latency request,
			 * the put at the front of the IO pending
			 * queue, otherwise put it at the end of the queue.
			 */
			if (io->low_latency) {
				INIT_LIST_HEAD(&io->io_pending_link);
				list_add(&xport->io_pending_list,
					 &io->io_pending_link);
			} else {
				INIT_LIST_HEAD(&io->io_pending_link);
				list_add_tail(&io->io_pending_link,
					      &xport->io_pending_list);
			}
			spin_unlock_irqrestore(&xport->io_pending_lock, flags);
			atomic_add_return(1, &xport->io_pending_count);
			atomic_add_return(1, &xport->io_total_pending);

			/* process pending list */
			efct_scsi_check_pending(efct);
			return 0;
		}
	spin_unlock_irqrestore(&xport->io_pending_lock, flags);

	/*
	 * We don't have a HW IO associated with the IO and there's nothing
	 * on the pending list. Attempt to allocate a HW IO and dispatch it.
	 */
	hio = efct_hw_io_alloc(&io->efct->hw);
	if (!hio) {
		/* Couldn't get a HW IO. Save this IO on the pending list */
		spin_lock_irqsave(&xport->io_pending_lock, flags);
		INIT_LIST_HEAD(&io->io_pending_link);
		list_add_tail(&io->io_pending_link, &xport->io_pending_list);
		spin_unlock_irqrestore(&xport->io_pending_lock, flags);

		atomic_add_return(1, &xport->io_total_pending);
		atomic_add_return(1, &xport->io_pending_count);
		return 0;
	}

	/* We successfully allocated a HW IO; dispatch to HW */
	return efct_scsi_io_dispatch_hw_io(io, hio);
}

/**
 * @brief Attempt to dispatch an Abort IO.
 *
 * @par Description
 * An Abort IO is dispatched:
 * - if the pending list is not empty, add IO to pending list
 *   and call a function to process the pending list.
 * - if pending list is empty, send abort to the HW.
 *
 * @param io Pointer to IO structure.
 * @param cb Callback function.
 *
 * @return Returns 0 on success, a negative error code value on failure.
 */

int
efct_scsi_io_dispatch_abort(struct efct_io_s *io, void *cb)
{
	struct efct_s *efct = io->efct;
	struct efct_xport_s *xport = efct->xport;
	unsigned long flags = 0;

	efct_assert((io->io_type == EFCT_IO_TYPE_ABORT), -1);
	io->hw_cb = cb;

	/*
	 * For aborts, we don't need a HW IO, but we still want
	 * to pass through the pending list to preserve ordering.
	 * Thus, if the pending list is not empty, add this abort
	 * to the pending list and process the pending list.
	 */
	spin_lock_irqsave(&xport->io_pending_lock, flags);
		if (!list_empty(&xport->io_pending_list)) {
			INIT_LIST_HEAD(&io->io_pending_link);
			list_add_tail(&io->io_pending_link,
				      &xport->io_pending_list);
			spin_unlock_irqrestore(&xport->io_pending_lock, flags);
			atomic_add_return(1, &xport->io_pending_count);
			atomic_add_return(1, &xport->io_total_pending);

			/* process pending list */
			efct_scsi_check_pending(efct);
			return 0;
		}
	spin_unlock_irqrestore(&xport->io_pending_lock, flags);

	/* nothing on pending list, dispatch abort */
	return efct_scsi_io_dispatch_no_hw_io(io);
}

/**
 * @brief Dispatch IO
 *
 * @par Description
 * An IO and its associated HW IO is dispatched to the HW.
 *
 * @param io Pointer to IO structure.
 * @param hio Pointer to HW IO structure from which IO will be
 * dispatched.
 *
 * @return Returns 0 on success, a negative error code value on failure.
 */

static int
efct_scsi_io_dispatch_hw_io(struct efct_io_s *io, struct efct_hw_io_s *hio)
{
	int rc;
	struct efct_s *efct = io->efct;

	/* Got a HW IO;
	 * update ini/tgt_task_tag with HW IO info and dispatch
	 */
	io->hio = hio;
	if (io->cmd_tgt)
		io->tgt_task_tag = hio->indicator;
	else if (io->cmd_ini)
		io->init_task_tag = hio->indicator;
	io->hw_tag = hio->reqtag;

	hio->eq = io->hw_priv;

	/* Copy WQ steering */
	switch (io->wq_steering) {
	case EFCT_SCSI_WQ_STEERING_CLASS >> EFCT_SCSI_WQ_STEERING_SHIFT:
		hio->wq_steering = EFCT_HW_WQ_STEERING_CLASS;
		break;
	case EFCT_SCSI_WQ_STEERING_REQUEST >> EFCT_SCSI_WQ_STEERING_SHIFT:
		hio->wq_steering = EFCT_HW_WQ_STEERING_REQUEST;
		break;
	case EFCT_SCSI_WQ_STEERING_CPU >> EFCT_SCSI_WQ_STEERING_SHIFT:
		hio->wq_steering = EFCT_HW_WQ_STEERING_CPU;
		break;
	}

	switch (io->io_type) {
	case EFCT_IO_TYPE_IO: {
		u32 max_sgl;
		u32 total_count;
		u32 host_allocated;

		efct_hw_get(&efct->hw, EFCT_HW_N_SGL, &max_sgl);
		efct_hw_get(&efct->hw, EFCT_HW_SGL_CHAINING_HOST_ALLOCATED,
			    &host_allocated);

		/*
		 * If the requested SGL is larger than the default size,
		 * then we can allocate an overflow SGL.
		 */
		total_count = efct_scsi_count_sgls(&io->hw_dif,
						   io->sgl, io->sgl_count);

		/*
		 * Lancer requires us to allocate the chained memory area
		 */
		if (host_allocated && total_count > max_sgl) {
			/* Compute count needed, the number
			 * extra plus 1 for the link sge
			 */
			u32 count = total_count - max_sgl + 1;

			io->ovfl_sgl.size = count * sizeof(struct sli4_sge_s);
			io->ovfl_sgl.virt = dma_alloc_coherent(&efct->pdev->dev,
						       io->ovfl_sgl.size,
						&io->ovfl_sgl.phys, GFP_DMA);
			if (!io->ovfl_sgl.virt) {
				efct_log_err(efct,
					     "dma alloc overflow sgl failed\n");
				break;
			}
			rc = efct_hw_io_register_sgl(&efct->hw,
						     io->hio, &io->ovfl_sgl,
						     count);
			if (rc) {
				efct_scsi_io_free_ovfl(io);
				efct_log_err(efct,
					     "efct_hw_io_register_sgl() failed\n");
				break;
			}
			/* EVT: update chained_io_count */
			io->node->chained_io_count++;
		}

		rc = efct_scsi_build_sgls(&efct->hw, io->hio, &io->hw_dif,
					  io->sgl, io->sgl_count, io->hio_type);
		if (rc) {
			efct_scsi_io_free_ovfl(io);
			break;
		}

		if (EFCT_LOG_ENABLE_SCSI_TRACE(efct))
			efct_log_sgl(io);

		if (io->app_id)
			io->iparam.fcp_tgt.app_id = io->app_id;

		rc = efct_hw_io_send(&io->efct->hw, io->hio_type, io->hio,
				     io->wire_len, &io->iparam,
				     &io->node->rnode, io->hw_cb, io);
		break;
	}
	case EFCT_IO_TYPE_ELS:
	case EFCT_IO_TYPE_CT: {
		rc = efct_hw_srrs_send(&efct->hw, io->hio_type, io->hio,
				       &io->els_req, io->wire_len,
			&io->els_rsp, &io->node->rnode, &io->iparam,
			io->hw_cb, io);
		break;
	}
	case EFCT_IO_TYPE_CT_RESP: {
		rc = efct_hw_srrs_send(&efct->hw, io->hio_type, io->hio,
				       &io->els_rsp, io->wire_len,
			NULL, &io->node->rnode, &io->iparam,
			io->hw_cb, io);
		break;
	}
	case EFCT_IO_TYPE_BLS_RESP: {
		/* no need to update tgt_task_tag for BLS response since
		 * the RX_ID will be specified by the payload, not the XRI
		 */
		rc = efct_hw_srrs_send(&efct->hw, io->hio_type, io->hio,
				       NULL, 0, NULL, &io->node->rnode,
			&io->iparam, io->hw_cb, io);
		break;
	}
	default:
		scsi_io_printf(io, "Unknown IO type=%d\n", io->io_type);
		rc = -1;
		break;
	}
	return rc;
}

/**
 * @brief Dispatch IO
 *
 * @par Description
 * An IO that does require a HW IO is dispatched to the HW.
 *
 * @param io Pointer to IO structure.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

static int
efct_scsi_io_dispatch_no_hw_io(struct efct_io_s *io)
{
	int rc;

	switch (io->io_type) {
	case EFCT_IO_TYPE_ABORT: {
		struct efct_hw_io_s *hio_to_abort = NULL;
		struct efct_io_s *io_abrt = NULL;

		efct_assert(io->io_to_abort, -1);

		hio_to_abort = io->io_to_abort->hio;
		io_abrt = io->io_to_abort;

		if (!hio_to_abort) {
			/*
			 * If "IO to abort" does not have an
			 * associated HW IO, immediately make callback with
			 * success. The command must have been sent to
			 * the backend, but the data phase has not yet
			 * started, so we don't have a HW IO.
			 *
			 * Note: since the backend shims should be
			 * taking a reference on io_to_abort, it should not
			 * be possible to have been completed and freed by
			 * the backend before the abort got here.
			 */
			scsi_io_printf(io, "IO: not active\n");
			((efct_hw_done_t)io->hw_cb)(io->hio, NULL, 0,
					SLI4_FC_WCQE_STATUS_SUCCESS, 0, io);
			rc = 0;
		} else {
			/* HW IO is valid, abort it */
			scsi_io_printf(io, "aborting\n");
			rc = efct_hw_io_abort(&io->efct->hw, hio_to_abort,
					      io->send_abts, io->hw_cb, io);
			if (rc) {
				int status = SLI4_FC_WCQE_STATUS_SUCCESS;

				if (rc != EFCT_HW_RTN_IO_NOT_ACTIVE &&
				    rc != EFCT_HW_RTN_IO_ABORT_IN_PROGRESS) {
					status = -1;
					scsi_io_printf(io,
						       "Failed to abort IO: status=%d\n",
						rc);
				}
				((efct_hw_done_t)io->hw_cb)(io->hio,
						NULL, 0, status, 0, io);
				rc = 0;
			}
		}

		break;
	}
	default:
		scsi_io_printf(io, "Unknown IO type=%d\n", io->io_type);
		rc = -1;
		break;
	}
	return rc;
}

/**
 * @ingroup scsi_api_base
 * @brief Send read/write data.
 *
 * @par Description
 * This call is made by a target-server to initiate a SCSI read
 * or write data phase, transferring data between the target to
 * the remote initiator. The payload is specified by the scatter-gather
 * list @c sgl of length @c sgl_count.
 * The @c wire_len argument specifies the payload length (independent
 * of the scatter-gather list cumulative length).
 * @n @n
 * The @c flags argument has one bit, EFCT_SCSI_LAST_DATAPHASE, which is
 * a hint to the base driver that it may use auto SCSI response features
 * if the hardware supports it.
 * @n @n
 * Upon completion, the callback function @b cb is called with flags
 * indicating that the IO has completed (EFCT_SCSI_IO_COMPL) and another
 * data phase or response may be sent; that the IO has completed and no
 * response needs to be sent (EFCT_SCSI_IO_COMPL_NO_RSP); or that the IO
 * was aborted (EFCT_SCSI_IO_ABORTED).
 *
 * @param io Pointer to the IO context.
 * @param flags Flags controlling the sending of data.
 * @param dif_info Pointer to T10 DIF fields, or NULL if no DIF.
 * @param sgl Pointer to the payload scatter-gather list.
 * @param sgl_count Count of the scatter-gather list elements.
 * @param xwire_len Length of the payload on wire, in bytes.
 * @param type HW IO type.
 * @param enable_ar Enable auto-response if true.
 * @param cb Completion callback.
 * @param arg Application-supplied callback data.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

static inline int
efct_scsi_xfer_data(struct efct_io_s *io, u32 flags,
		    struct efct_scsi_dif_info_s *dif_info,
	struct efct_scsi_sgl_s *sgl, u32 sgl_count, u64 xwire_len,
	enum efct_hw_io_type_e type, int enable_ar,
	efct_scsi_io_cb_t cb, void *arg)
{
	int rc;
	struct efct_s *efct;
	u32 disable_ar_tgt_dif = false;
	size_t residual = 0;

	if (dif_info &&
	    dif_info->dif_oper == EFCT_SCSI_DIF_OPER_DISABLED)
		dif_info = NULL;

	efct_assert(io, -1);

	if (dif_info) {
		efct_hw_get(&io->efct->hw, EFCT_HW_DISABLE_AR_TGT_DIF,
			    &disable_ar_tgt_dif);
		if (disable_ar_tgt_dif)
			enable_ar = false;
	}

	io->sgl_count = sgl_count;

	/* If needed, copy SGL */
	if (sgl && sgl != io->sgl) {
		efct_assert(sgl_count <= io->sgl_allocated, -1);
		memcpy(io->sgl, sgl, sgl_count * sizeof(*io->sgl));
	}

	efct = io->efct;
	efct_assert(efct, -1);
	efct_assert(io->node, -1);

	scsi_io_trace(io, "%s wire_len %llu\n",
		      (type == EFCT_HW_IO_TARGET_READ) ? "send" : "recv",
		      xwire_len);

	efct_assert(sgl, -1);
	efct_assert(sgl_count > 0, -1);
	efct_assert(io->exp_xfer_len > io->transferred, -1);

	io->hio_type = type;

	io->scsi_tgt_cb = cb;
	io->scsi_tgt_cb_arg = arg;

	rc = efct_scsi_convert_dif_info(efct, dif_info, &io->hw_dif);
	if (rc)
		return rc;

	/* If DIF is used, then save lba for error recovery */
	if (dif_info)
		io->scsi_dif_info = *dif_info;

	residual = io->exp_xfer_len - io->transferred;
	io->wire_len = (xwire_len < residual) ? xwire_len : residual;
	residual = (xwire_len - io->wire_len);

	memset(&io->iparam, 0, sizeof(io->iparam));
	io->iparam.fcp_tgt.ox_id = io->init_task_tag;
	io->iparam.fcp_tgt.offset = io->transferred;
	io->iparam.fcp_tgt.dif_oper = io->hw_dif.dif;
	io->iparam.fcp_tgt.blk_size = io->hw_dif.blk_size;
	io->iparam.fcp_tgt.cs_ctl = io->cs_ctl;
	io->iparam.fcp_tgt.timeout = io->timeout;

	/* if this is the last data phase and there is no residual, enable
	 * auto-good-response
	 */
	if (enable_ar && (flags & EFCT_SCSI_LAST_DATAPHASE) &&
	    residual == 0 &&
		((io->transferred + io->wire_len) == io->exp_xfer_len) &&
		(!(flags & EFCT_SCSI_NO_AUTO_RESPONSE))) {
		io->iparam.fcp_tgt.flags |= SLI4_IO_AUTO_GOOD_RESPONSE;
		io->auto_resp = true;
	} else {
		io->auto_resp = false;
	}

	/* save this transfer length */
	io->xfer_req = io->wire_len;

	/* Adjust the transferred count to account for overrun
	 * when the residual is calculated in efct_scsi_send_resp
	 */
	io->transferred += residual;

	/* Adjust the SGL size if there is overrun */

	if (residual) {
		struct efct_scsi_sgl_s  *sgl_ptr = &io->sgl[sgl_count - 1];

		while (residual) {
			size_t len = sgl_ptr->len;

			if (len > residual) {
				sgl_ptr->len = len - residual;
				residual = 0;
			} else {
				sgl_ptr->len = 0;
				residual -= len;
				io->sgl_count--;
			}
			sgl_ptr--;
		}
	}

	/* Set latency and WQ steering */
	io->low_latency = (flags & EFCT_SCSI_LOW_LATENCY) != 0;
	io->wq_steering = (flags & EFCT_SCSI_WQ_STEERING_MASK) >>
				EFCT_SCSI_WQ_STEERING_SHIFT;
	io->wq_class = (flags & EFCT_SCSI_WQ_CLASS_MASK) >>
				EFCT_SCSI_WQ_CLASS_SHIFT;

	if (efct->xport) {
		struct efct_xport_s *xport = efct->xport;

		if (type == EFCT_HW_IO_TARGET_READ) {
			xport->fcp_stats.input_requests++;
			xport->fcp_stats.input_bytes += xwire_len;
		} else if (type == EFCT_HW_IO_TARGET_WRITE) {
			xport->fcp_stats.output_requests++;
			xport->fcp_stats.output_bytes += xwire_len;
		}
	}
	return efct_scsi_io_dispatch(io, efct_target_io_cb);
}

int
efct_scsi_send_rd_data(struct efct_io_s *io, u32 flags,
		       struct efct_scsi_dif_info_s *dif_info,
	struct efct_scsi_sgl_s *sgl, u32 sgl_count, u64 len,
	efct_scsi_io_cb_t cb, void *arg)
{
	return efct_scsi_xfer_data(io, flags, dif_info, sgl, sgl_count,
				 len, EFCT_HW_IO_TARGET_READ,
				 enable_tsend_auto_resp(io->efct), cb, arg);
}

int
efct_scsi_recv_wr_data(struct efct_io_s *io, u32 flags,
		       struct efct_scsi_dif_info_s *dif_info,
	struct efct_scsi_sgl_s *sgl, u32 sgl_count, u64 len,
	efct_scsi_io_cb_t cb, void *arg)
{
	return efct_scsi_xfer_data(io, flags, dif_info, sgl, sgl_count, len,
				 EFCT_HW_IO_TARGET_WRITE,
				 enable_treceive_auto_resp(io->efct), cb, arg);
}

/**
 * @ingroup scsi_api_base
 * @brief Free overflow SGL.
 *
 * @par Description
 * Free the overflow SGL if it is present.
 *
 * @param io Pointer to IO object.
 *
 * @return None.
 */
static void
efct_scsi_io_free_ovfl(struct efct_io_s *io)
{
	if (io->ovfl_sgl.size) {
		dma_free_coherent(&io->efct->pdev->dev,
				  io->ovfl_sgl.size, io->ovfl_sgl.virt,
				  io->ovfl_sgl.phys);
		memset(&io->ovfl_sgl, 0, sizeof(struct efc_dma_s));
	}
}

/**
 * @ingroup scsi_api_base
 * @brief Send response data.
 *
 * @par Description
 * This function is used by a target-server to send the SCSI response
 * data to a remote initiator node. The target-server populates the
 * @c struct efct_scsi_cmd_resp_s argument with scsi status, status qualifier,
 * sense data, and response data, as needed.
 * @n @n
 * Upon completion, the callback function @c cb is invoked. The
 * target-server will generally clean up its IO context resources and
 * call efct_scsi_io_complete().
 *
 * @param io Pointer to the IO context.
 * @param flags Flags to control sending of the SCSI response.
 * @param rsp Pointer to the response data populated by the caller.
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.

 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_scsi_send_resp(struct efct_io_s *io, u32 flags,
		    struct efct_scsi_cmd_resp_s *rsp,
		   efct_scsi_io_cb_t cb, void *arg)
{
	struct efct_s *efct;
	int residual;
	bool auto_resp = true;		/* Always try auto resp */
	u8 scsi_status = 0;
	u16 scsi_status_qualifier = 0;
	u8 *sense_data = NULL;
	u32 sense_data_length = 0;

	efct_assert(io, -1);

	efct = io->efct;
	efct_assert(efct, -1);

	efct_assert(io->node, -1);

	efct_scsi_convert_dif_info(efct, NULL, &io->hw_dif);

	if (rsp) {
		scsi_status = rsp->scsi_status;
		scsi_status_qualifier = rsp->scsi_status_qualifier;
		sense_data = rsp->sense_data;
		sense_data_length = rsp->sense_data_length;
		residual = rsp->residual;
	} else {
		residual = io->exp_xfer_len - io->transferred;
	}

	io->wire_len = 0;
	io->hio_type = EFCT_HW_IO_TARGET_RSP;

	io->scsi_tgt_cb = cb;
	io->scsi_tgt_cb_arg = arg;

	memset(&io->iparam, 0, sizeof(io->iparam));
	io->iparam.fcp_tgt.ox_id = io->init_task_tag;
	io->iparam.fcp_tgt.offset = 0;
	io->iparam.fcp_tgt.cs_ctl = io->cs_ctl;
	io->iparam.fcp_tgt.timeout = io->timeout;

	/* Set low latency queueing request */
	io->low_latency = (flags & EFCT_SCSI_LOW_LATENCY) != 0;
	io->wq_steering = (flags & EFCT_SCSI_WQ_STEERING_MASK) >>
				EFCT_SCSI_WQ_STEERING_SHIFT;
	io->wq_class = (flags & EFCT_SCSI_WQ_CLASS_MASK) >>
				EFCT_SCSI_WQ_CLASS_SHIFT;

	if (scsi_status != 0 || residual || sense_data_length) {
		struct fcp_rsp_iu_s *fcprsp = io->rspbuf.virt;

		if (!fcprsp) {
			efct_log_err(efct, "NULL response buffer\n");
			return -1;
		}

		auto_resp = false;

		memset(fcprsp, 0, sizeof(*fcprsp));

		io->wire_len += (sizeof(*fcprsp) - sizeof(fcprsp->data));

		fcprsp->scsi_status = scsi_status;
		*((u16 *)fcprsp->status_qualifier) =
			cpu_to_be16(scsi_status_qualifier);

		/* set residual status if necessary */
		if (residual != 0) {
			/* FCP: if data transferred is less than the
			 * amount expected, then this is an underflow.
			 * If data transferred would have been greater
			 * than the amount expected this is an overflow
			 */
			if (residual > 0) {
				fcprsp->flags |= FCP_RESID_UNDER;
				*((u32 *)fcprsp->fcp_resid) =
						cpu_to_be32(residual);
			} else {
				fcprsp->flags |= FCP_RESID_OVER;
				*((u32 *)fcprsp->fcp_resid) =
						cpu_to_be32(-residual);
			}
		}

		if (EFCT_SCSI_SNS_BUF_VALID(sense_data) && sense_data_length) {
			efct_assert(sense_data_length <= sizeof(fcprsp->data),
				    -1);
			fcprsp->flags |= FCP_SNS_LEN_VALID;
			memcpy(fcprsp->data, sense_data, sense_data_length);
			*((u32 *)fcprsp->fcp_sns_len) =
					cpu_to_be32(sense_data_length);
			io->wire_len += sense_data_length;
		}

		io->sgl[0].addr = io->rspbuf.phys;
		io->sgl[0].dif_addr = 0;
		io->sgl[0].len = io->wire_len;
		io->sgl_count = 1;
	}

	if (auto_resp)
		io->iparam.fcp_tgt.flags |= SLI4_IO_AUTO_GOOD_RESPONSE;

	return efct_scsi_io_dispatch(io, efct_target_io_cb);
}

/**
 * @ingroup scsi_api_base
 * @brief Send TMF response data.
 *
 * @par Description
 * This function is used by a target-server to send SCSI TMF response\
 * data to a remote initiator node.
 * Upon completion, the callback function @c cb is invoked.
 * The target-server will generally
 * clean up its IO context resources and call efct_scsi_io_complete().
 *
 * @param io Pointer to the IO context.
 * @param rspcode TMF response code.
 * @param addl_rsp_info Additional TMF response information
 *		(may be NULL for zero data).
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_scsi_send_tmf_resp(struct efct_io_s *io,
			enum efct_scsi_tmf_resp_e rspcode,
			u8 addl_rsp_info[3],
			efct_scsi_io_cb_t cb, void *arg)
{
	int rc = -1;
	struct efct_s *efct = NULL;
	struct fcp_rsp_iu_s *fcprsp = NULL;
	struct fcp_rsp_info_s *rspinfo = NULL;
	u8 fcp_rspcode;

	efct_assert(io, -1);
	efct_assert(io->efct, -1);
	efct_assert(io->node, -1);

	efct = io->efct;

	io->wire_len = 0;
	efct_scsi_convert_dif_info(efct, NULL, &io->hw_dif);

	switch (rspcode) {
	case EFCT_SCSI_TMF_FUNCTION_COMPLETE:
		fcp_rspcode = FCP_TMF_COMPLETE;
		break;
	case EFCT_SCSI_TMF_FUNCTION_SUCCEEDED:
	case EFCT_SCSI_TMF_FUNCTION_IO_NOT_FOUND:
		fcp_rspcode = FCP_TMF_SUCCEEDED;
		break;
	case EFCT_SCSI_TMF_FUNCTION_REJECTED:
		fcp_rspcode = FCP_TMF_REJECTED;
		break;
	case EFCT_SCSI_TMF_INCORRECT_LOGICAL_UNIT_NUMBER:
		fcp_rspcode = FCP_TMF_INCORRECT_LUN;
		break;
	case EFCT_SCSI_TMF_SERVICE_DELIVERY:
		fcp_rspcode = FCP_TMF_FAILED;
		break;
	default:
		fcp_rspcode = FCP_TMF_REJECTED;
		break;
	}

	io->hio_type = EFCT_HW_IO_TARGET_RSP;

	io->scsi_tgt_cb = cb;
	io->scsi_tgt_cb_arg = arg;

	if (io->tmf_cmd == EFCT_SCSI_TMF_ABORT_TASK) {
		rc = efct_target_send_bls_resp(io, cb, arg);
		return rc;
	}

	/* populate the FCP TMF response */
	fcprsp = io->rspbuf.virt;
	memset(fcprsp, 0, sizeof(*fcprsp));

	fcprsp->flags |= FCP_RSP_LEN_VALID;

	rspinfo = (struct fcp_rsp_info_s *)fcprsp->data;
	if (addl_rsp_info) {
		memcpy(rspinfo->addl_rsp_info, addl_rsp_info,
		       sizeof(rspinfo->addl_rsp_info));
	}
	rspinfo->rsp_code = fcp_rspcode;

	io->wire_len = sizeof(*fcprsp) - sizeof(fcprsp->data) +
				sizeof(*rspinfo);

	*((u32 *)fcprsp->fcp_rsp_len) = cpu_to_be32(sizeof(*rspinfo));

	io->sgl[0].addr = io->rspbuf.phys;
	io->sgl[0].dif_addr = 0;
	io->sgl[0].len = io->wire_len;
	io->sgl_count = 1;

	memset(&io->iparam, 0, sizeof(io->iparam));
	io->iparam.fcp_tgt.ox_id = io->init_task_tag;
	io->iparam.fcp_tgt.offset = 0;
	io->iparam.fcp_tgt.cs_ctl = io->cs_ctl;
	io->iparam.fcp_tgt.timeout = io->timeout;

	rc = efct_scsi_io_dispatch(io, efct_target_io_cb);

	return rc;
}

/**
 * @brief Process target abort callback.
 *
 * @par Description
 * Accepts HW abort requests.
 *
 * @param hio HW IO context.
 * @param rnode Remote node.
 * @param length Length of response data.
 * @param status Completion status.
 * @param ext_status Extended completion status.
 * @param app Application-specified callback data.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

static int
efct_target_abort_cb(struct efct_hw_io_s *hio,
		     struct efc_remote_node_s *rnode,
		     u32 length, int status,
		     u32 ext_status, void *app)
{
	struct efct_io_s *io = app;
	struct efct_s *efct;
	enum efct_scsi_io_status_e scsi_status;

	efct_assert(io, -1);
	efct_assert(io->efct, -1);

	efct = io->efct;

	if (io->abort_cb) {
		efct_scsi_io_cb_t abort_cb = io->abort_cb;
		void *abort_cb_arg = io->abort_cb_arg;

		io->abort_cb = NULL;
		io->abort_cb_arg = NULL;

		switch (status) {
		case SLI4_FC_WCQE_STATUS_SUCCESS:
			scsi_status = EFCT_SCSI_STATUS_GOOD;
			break;
		case SLI4_FC_WCQE_STATUS_LOCAL_REJECT:
			switch (ext_status) {
			case SLI4_FC_LOCAL_REJECT_NO_XRI:
				scsi_status = EFCT_SCSI_STATUS_NO_IO;
				break;
			case SLI4_FC_LOCAL_REJECT_ABORT_IN_PROGRESS:
				scsi_status =
					EFCT_SCSI_STATUS_ABORT_IN_PROGRESS;
				break;
			default:
				/*we have seen 0x15 (abort in progress)*/
				scsi_status = EFCT_SCSI_STATUS_ERROR;
				break;
			}
			break;
		case SLI4_FC_WCQE_STATUS_FCP_RSP_FAILURE:
			scsi_status = EFCT_SCSI_STATUS_CHECK_RESPONSE;
			break;
		default:
			scsi_status = EFCT_SCSI_STATUS_ERROR;
			break;
		}
		/* invoke callback */
		abort_cb(io->io_to_abort, scsi_status, 0, abort_cb_arg);
	}

	efct_assert(io != io->io_to_abort, -1);

	/* done with IO to abort,efct_ref_get(): efct_scsi_tgt_abort_io() */
	kref_put(&io->io_to_abort->ref, io->io_to_abort->release);

	efct_io_pool_io_free(efct->xport->io_pool, io);

	efct_scsi_check_pending(efct);
	return 0;
}

/**
 * @ingroup scsi_api_base
 * @brief Abort a target IO.
 *
 * @par Description
 * This routine is called from a SCSI target-server. It initiates an
 * abort of a previously-issued target data phase or response request.
 *
 * @param io IO context.
 * @param cb SCSI target server callback.
 * @param arg SCSI target server supplied callback argument.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
efct_scsi_tgt_abort_io(struct efct_io_s *io, efct_scsi_io_cb_t cb, void *arg)
{
	struct efct_s *efct;
	struct efct_xport_s *xport;
	int rc;
	struct efct_io_s *abort_io = NULL;

	efct_assert(io, -1);
	efct_assert(io->node, -1);
	efct_assert(io->efct, -1);

	efct = io->efct;
	xport = efct->xport;

	/* take a reference on IO being aborted */
	if ((kref_get_unless_zero(&io->ref) == 0)) {
		/* command no longer active */
		scsi_io_printf(io, "command no longer active\n");
		return -1;
	}

	/*
	 * allocate a new IO to send the abort request. Use efct_io_alloc()
	 * directly, as we need an IO object that will not fail allocation
	 * due to allocations being disabled (in efct_scsi_io_alloc())
	 */
	abort_io = efct_io_pool_io_alloc(efct->xport->io_pool);
	if (!abort_io) {
		atomic_add_return(1, &xport->io_alloc_failed_count);
		kref_put(&io->ref, io->release);
		return -1;
	}

	/* Save the target server callback and argument */
	efct_assert(!abort_io->hio, -1);

	/* set generic fields */
	abort_io->cmd_tgt = true;
	abort_io->node = io->node;

	/* set type and abort-specific fields */
	abort_io->io_type = EFCT_IO_TYPE_ABORT;
	abort_io->display_name = "tgt_abort";
	abort_io->io_to_abort = io;
	abort_io->send_abts = false;
	abort_io->abort_cb = cb;
	abort_io->abort_cb_arg = arg;

	/* now dispatch IO */
	rc = efct_scsi_io_dispatch_abort(abort_io, efct_target_abort_cb);
	if (rc)
		kref_put(&io->ref, io->release);
	return rc;
}

/**
 * @brief Process target BLS response callback.
 *
 * @par Description
 * Accepts HW abort requests.
 *
 * @param hio HW IO context.
 * @param rnode Remote node.
 * @param length Length of response data.
 * @param status Completion status.
 * @param ext_status Extended completion status.
 * @param app Application-specified callback data.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

static int
efct_target_bls_resp_cb(struct efct_hw_io_s *hio,
			struct efc_remote_node_s *rnode,
	u32 length, int status, u32 ext_status, void *app)
{
	struct efct_io_s *io = app;
	struct efct_s *efct;
	enum efct_scsi_io_status_e bls_status;

	efct_assert(io, -1);
	efct_assert(io->efct, -1);

	efct = io->efct;

	/* BLS isn't really a "SCSI" concept, but use SCSI status */
	if (status) {
		io_error_log(io, "s=%#x x=%#x\n", status, ext_status);
		bls_status = EFCT_SCSI_STATUS_ERROR;
	} else {
		bls_status = EFCT_SCSI_STATUS_GOOD;
	}

	if (io->bls_cb) {
		efct_scsi_io_cb_t bls_cb = io->bls_cb;
		void *bls_cb_arg = io->bls_cb_arg;

		io->bls_cb = NULL;
		io->bls_cb_arg = NULL;

		/* invoke callback */
		bls_cb(io, bls_status, 0, bls_cb_arg);
	}

	efct_scsi_check_pending(efct);
	return 0;
}

/**
 * @brief Complete abort request.
 *
 * @par Description
 * An abort request is completed by posting a BA_ACC for the IO that
 * requested the abort.
 *
 * @param io Pointer to the IO context.
 * @param cb Callback function to invoke upon completion.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

static int
efct_target_send_bls_resp(struct efct_io_s *io,
			  efct_scsi_io_cb_t cb, void *arg)
{
	int rc;
	struct fc_ba_acc_payload_s *acc;

	efct_assert(io, -1);

	/* fill out IO structure with everything needed to send BA_ACC */
	memset(&io->iparam, 0, sizeof(io->iparam));
	io->iparam.bls.ox_id = io->init_task_tag;
	io->iparam.bls.rx_id = io->abort_rx_id;

	acc = (void *)io->iparam.bls.payload;

	memset(io->iparam.bls.payload, 0,
	       sizeof(io->iparam.bls.payload));
	acc->ox_id = io->iparam.bls.ox_id;
	acc->rx_id = io->iparam.bls.rx_id;
	acc->high_seq_cnt = U16_MAX;

	/* generic io fields have already been populated */

	/* set type and BLS-specific fields */
	io->io_type = EFCT_IO_TYPE_BLS_RESP;
	io->display_name = "bls_rsp";
	io->hio_type = EFCT_HW_BLS_ACC;
	io->bls_cb = cb;
	io->bls_cb_arg = arg;

	/* dispatch IO */
	rc = efct_scsi_io_dispatch(io, efct_target_bls_resp_cb);
	return rc;
}

/**
 * @ingroup scsi_api_base
 * @brief Notify the base driver that the IO is complete.
 *
 * @par Description
 * This function is called by a target-server to notify the base
 * driver that an IO has completed, allowing for the base driver
 * to free resources.
 * @n
 * @n @b Note: This function is not called by initiator-clients.
 *
 * @param io Pointer to IO context.
 *
 * @return None.
 */
void
efct_scsi_io_complete(struct efct_io_s *io)
{
	efct_assert(io);

	if (!efct_io_busy(io)) {
		efct_log_test(io->efct,
			      "Got completion for non-busy io with tag 0x%x\n",
		    io->tag);
		return;
	}

	scsi_io_trace(io, "freeing io 0x%p %s\n", io, io->display_name);
	efct_assert(refcount_read(&io->ref.refcount) > 0);
	kref_put(&io->ref, io->release);
}

/**
 * @brief Handle initiator IO completion.
 *
 * @par Description
 * This callback is made upon completion of an initiator operation
 * (initiator read/write command).
 *
 * @param hio HW IO context.
 * @param rnode Remote node.
 * @param length Length of completion data.
 * @param status Completion status.
 * @param ext_status Extended completion status.
 * @param app Application-specified callback data.
 *
 * @return None.
 */

static void
efct_initiator_io_cb(struct efct_hw_io_s *hio,
		     struct efc_remote_node_s *rnode,
		     u32 length, int status,
		     u32 ext_status, void *app)
{
	struct efct_io_s *io = app;
	struct efct_s *efct;
	enum efct_scsi_io_status_e scsi_status;

	efct_assert(io);
	efct_assert(io->scsi_ini_cb);

	scsi_io_trace(io, "status x%x ext_status x%x\n", status, ext_status);

	efct = io->efct;
	efct_assert(efct);

	efct_scsi_io_free_ovfl(io);

	/* Call target server completion */
	if (io->scsi_ini_cb) {
		struct fcp_rsp_iu_s *fcprsp = io->rspbuf.virt;
		struct efct_scsi_cmd_resp_s rsp;
		efct_scsi_rsp_io_cb_t cb = io->scsi_ini_cb;
		u32 flags = 0;
		u8 *pd = fcprsp->data;

		/* Clear the callback before invoking the callback */
		io->scsi_ini_cb = NULL;

		memset(&rsp, 0, sizeof(rsp));

		/* Unless status is FCP_RSP_FAILURE,fcprsp is not filled in */
		switch (status) {
		case SLI4_FC_WCQE_STATUS_SUCCESS:
			scsi_status = EFCT_SCSI_STATUS_GOOD;
			break;
		case SLI4_FC_WCQE_STATUS_FCP_RSP_FAILURE:
			scsi_status = EFCT_SCSI_STATUS_CHECK_RESPONSE;
			rsp.scsi_status = fcprsp->scsi_status;
			rsp.scsi_status_qualifier = be16_to_cpu(*((u16 *)
				fcprsp->status_qualifier));

			if (fcprsp->flags & FCP_RSP_LEN_VALID) {
				rsp.response_data = pd;
				rsp.response_data_length =
					efct_fc_getbe32(fcprsp->fcp_rsp_len);
				pd += rsp.response_data_length;
			}
			if (fcprsp->flags & FCP_SNS_LEN_VALID) {
				u32 sns_len =
					efct_fc_getbe32(fcprsp->fcp_sns_len);
				rsp.sense_data = pd;
				rsp.sense_data_length = sns_len;
				pd += sns_len;
			}
			/* Set residual */
			if (fcprsp->flags & FCP_RESID_OVER) {
				rsp.residual =
					-efct_fc_getbe32(fcprsp->fcp_resid);
				rsp.response_wire_length = length;
			} else	if (fcprsp->flags & FCP_RESID_UNDER) {
				rsp.residual =
					efct_fc_getbe32(fcprsp->fcp_resid);
				rsp.response_wire_length = length;
			}

			/*
			 * Note: The FCP_RSP_FAILURE can be returned
			 * for initiator IOs when the total data placed
			 * does not match the requested length even if
			 * the status is good. If the status is all
			 * zeroes, then we have to assume
			 * that a frame(s) were dropped and change the
			 * status to LOCAL_REJECT/OUT_OF_ORDER_DATA
			 */
			if (length != io->wire_len) {
				u32 rsp_len = ext_status;
				u8 *rsp_bytes = io->rspbuf.virt;
				u32 i;
				u8 all_zeroes = (rsp_len > 0);
				/* Check if the rsp is zero */
				for (i = 0; i < rsp_len; i++) {
					if (rsp_bytes[i] != 0) {
						all_zeroes = false;
						break;
					}
				}
				if (all_zeroes) {
					scsi_status = EFCT_SCSI_STATUS_ERROR;
					scsi_io_printf(io,
						       "local reject=0x%02x\n",
					SLI4_FC_LOCAL_REJECT_OUTOFORDER_DATA);
				}
			}
			break;
		case SLI4_FC_WCQE_STATUS_LOCAL_REJECT:
			if (ext_status ==
				SLI4_FC_LOCAL_REJECT_SEQUENCE_TIMEOUT)
				scsi_status = EFCT_SCSI_STATUS_COMMAND_TIMEOUT;
			else
				scsi_status = EFCT_SCSI_STATUS_ERROR;
			break;
		case SLI4_FC_WCQE_STATUS_DI_ERROR:
			if (ext_status & 0x01)
				scsi_status = EFCT_SCSI_STATUS_DIF_GUARD_ERR;
			else if (ext_status & 0x02)
				scsi_status =
					EFCT_SCSI_STATUS_DIF_APP_TAG_ERROR;
			else if (ext_status & 0x04)
				scsi_status =
					EFCT_SCSI_STATUS_DIF_REF_TAG_ERROR;
			else
				scsi_status =
					EFCT_SCSI_STATUS_DIF_UNKNOWN_ERROR;
			break;
		default:
			scsi_status = EFCT_SCSI_STATUS_ERROR;
			break;
		}

		cb(io, scsi_status, &rsp, flags, io->scsi_ini_cb_arg);
	}
	efct_scsi_check_pending(efct);
}

/**
 * @ingroup scsi_api_base
 * @brief Initiate initiator read IO.
 *
 * @par Description
 * This call is made by an initiator-client to send a SCSI read command.
 * The payload for the command is given by a scatter-gather list @c sgl
 * for @c sgl_count entries.
 * @n @n
 * Upon completion, the callback @b cb is invoked and passed request
 * status. If the command completed successfully, the callback is
 * givenSCSI response data.
 *
 * @param node Pointer to the node.
 * @param io Pointer to the IO context.
 * @param lun LUN value.
 * @param cdb Pointer to the CDB.
 * @param cdb_len Length of the CDB.
 * @param dif_info Pointer to the T10 DIF fields, or NULL if no DIF.
 * @param sgl Pointer to the scatter-gather list.
 * @param sgl_count Count of the scatter-gather list elements.
 * @param wire_len Length of the payload.
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_scsi_send_rd_io(struct efc_node_s *node, struct efct_io_s *io,
		     u32 lun, void *cdb, u32 cdb_len,
		     struct efct_scsi_dif_info_s *dif_info,
		     struct efct_scsi_sgl_s *sgl, u32 sgl_count,
		     u32 wire_len,
		     efct_scsi_rsp_io_cb_t cb, void *arg)
{
	int rc;

	rc = efct_scsi_send_io(EFCT_HW_IO_INITIATOR_READ, node, io, lun,
			       0, cdb, cdb_len, dif_info, sgl, sgl_count,
			     wire_len, 0, cb, arg);

	return rc;
}

/**
 * @ingroup scsi_api_base
 * @brief Initiate initiator write IO.
 *
 * @par Description
 * This call is made by an initiator-client to send a SCSI write command.
 * The payload for the command is given by a scatter-gather list @c sgl
 * for @c sgl_count entries.
 * @n @n
 * Upon completion, the callback @c cb is invoked and passed
 * request status. If the command completed successfully, the callback
 * is given SCSI response data.
 *
 * @param node Pointer to the node.
 * @param io Pointer to IO context.
 * @param lun LUN value.
 * @param cdb Pointer to the CDB.
 * @param cdb_len Length of the CDB.
 * @param dif_info Pointer to the T10 DIF fields, or NULL if no DIF.
 * @param sgl Pointer to the scatter-gather list.
 * @param sgl_count Count of the scatter-gather list elements.
 * @param wire_len Length of the payload.
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int efct_scsi_send_wr_io(struct efc_node_s *node, struct efct_io_s *io,
			 u32 lun, void *cdb, u32 cdb_len,
	struct efct_scsi_dif_info_s *dif_info, struct efct_scsi_sgl_s *sgl,
	u32 sgl_count, u32 wire_len,
	efct_scsi_rsp_io_cb_t cb, void *arg)
{
	int rc;

	rc = efct_scsi_send_io(EFCT_HW_IO_INITIATOR_WRITE, node, io, lun, 0,
			       cdb, cdb_len, dif_info, sgl, sgl_count,
			       wire_len, 0, cb, arg);

	return rc;
}

/**
 * @ingroup scsi_api_base
 * @brief Initiate initiator write IO.
 *
 * @par Description
 * This call is made by an initiator-client to send a SCSI write
 * command. The payload for the command is given by a scatter-gather
 * list @c sgl for @c sgl_count entries.
 * @n @n
 * Upon completion, the callback @c cb is invoked and passed request status.If
 * the command completed successfully, the callback is given
 * SCSI response data.
 *
 * @param node Pointer to the node.
 * @param io Pointer to IO context.
 * @param lun LUN value.
 * @param cdb Pointer to the CDB.
 * @param cdb_len Length of the CDB.
 * @param dif_info Pointer to the T10 DIF fields, or NULL if no DIF.
 * @param sgl Pointer to the scatter-gather list.
 * @param sgl_count Count of the scatter-gather list elements.
 * @param wire_len Length of the payload.
 * @param first_burst Number of first burst bytes to send.
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_scsi_send_wr_io_first_burst(struct efc_node_s *node,
				 struct efct_io_s *io, u32 lun,
				 void *cdb, u32 cdb_len,
				 struct efct_scsi_dif_info_s *dif_info,
				 struct efct_scsi_sgl_s *sgl,
				 u32 sgl_count, u32 wire_len,
				 u32 first_burst,
				 efct_scsi_rsp_io_cb_t cb, void *arg)
{
	int rc;

	rc = efct_scsi_send_io(EFCT_HW_IO_INITIATOR_WRITE, node, io, lun,
			       0, cdb, cdb_len, dif_info, sgl, sgl_count,
			     wire_len, 0, cb, arg);

	return rc;
}

/**
 * @ingroup scsi_api_base
 * @brief Initiate initiator SCSI command with no data.
 *
 * @par Description
 * This call is made by an initiator-client to send a SCSI
 * command with no data.
 * @n @n
 * Upon completion, the callback @c cb is invoked and passed request
 * status. If the command completed successfully, the callback is
 * given SCSI response data.
 *
 * @param node Pointer to the node.
 * @param io Pointer to the IO context.
 * @param lun LUN value.
 * @param cdb Pointer to the CDB.
 * @param cdb_len Length of the CDB.
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int efct_scsi_send_nodata_io(struct efc_node_s *node,
			     struct efct_io_s *io, u32 lun, void *cdb,
				 u32 cdb_len, efct_scsi_rsp_io_cb_t cb,
				 void *arg)
{
	int rc;

	rc = efct_scsi_send_io(EFCT_HW_IO_INITIATOR_NODATA, node, io, lun,
			       0, cdb, cdb_len, NULL, NULL, 0, 0, 0, cb, arg);

	return rc;
}

/**
 * @ingroup scsi_api_base
 * @brief Initiate initiator task management operation.
 *
 * @par Description
 * This command is used to send a SCSI task management function command.
 * If the command requires it (QUERY_TASK_SET for example), a payload may
 * be associated with the command.
 * If no payload is required, then @c sgl_count may be zero and @c sgl
 * is ignored.
 * @n @n
 * Upon completion @c cb is invoked with status and SCSI response data.
 *
 * @param node Pointer to the node.
 * @param io Pointer to the IO context.
 * @param io_to_abort Pointer to the IO context to abort in the
 * case of EFCT_SCSI_TMF_ABORT_TASK. Note: this can point to the
 * same the same struct efct_io_s as @c io, provided that @c io does not
 * have any outstanding work requests.
 * @param lun LUN value.
 * @param tmf Task management command.
 * @param sgl Pointer to the scatter-gather list.
 * @param sgl_count Count of the scatter-gather list elements.
 * @param len Length of the payload.
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */
int
efct_scsi_send_tmf(struct efc_node_s *node, struct efct_io_s *io,
		   struct efct_io_s *io_to_abort, u32 lun,
		enum efct_scsi_tmf_cmd_e tmf, struct efct_scsi_sgl_s *sgl,
		u32 sgl_count, u32 len, efct_scsi_rsp_io_cb_t cb,
		void *arg)
{
	int rc;

	efct_assert(io, -1);

	if (tmf == EFCT_SCSI_TMF_ABORT_TASK) {
		efct_assert(io_to_abort, -1);

		/* take a reference on IO being aborted */
		if ((kref_get_unless_zero(&io_to_abort->ref) == 0)) {
			/* command no longer active */
			scsi_io_printf(io, "command no longer active\n");
			return -1;
		}
		/* generic io fields have already been populated */

		/* abort-specific fields */
		io->io_type = EFCT_IO_TYPE_ABORT;
		io->display_name = "abort_task";
		io->io_to_abort = io_to_abort;
		io->send_abts = true;
		io->scsi_ini_cb = cb;
		io->scsi_ini_cb_arg = arg;

		/* now dispatch IO */
		rc = efct_scsi_io_dispatch_abort(io, efct_scsi_abort_io_cb);
		if (rc) {
			scsi_io_printf(io, "Failed to dispatch abort\n");
			/* efct_ref_get(): same function */
			kref_put(&io->ref, io->release);
		}
	} else {
		io->display_name = "tmf";
		rc = efct_scsi_send_io(EFCT_HW_IO_INITIATOR_READ, node, io,
				       lun, tmf, NULL, 0, NULL, sgl,
				       sgl_count, len, 0, cb, arg);
	}

	return rc;
}

/**
 * @ingroup scsi_api_base
 * @brief Send an FCP IO.
 *
 * @par Description
 * An FCP read/write IO command, with optional task management flags,
 * is sent to @c node.
 *
 * @param type HW IO type to send.
 * @param node Pointer to the node destination of the IO.
 * @param io Pointer to the IO context.
 * @param lun LUN value.
 * @param tmf Task management command.
 * @param cdb Pointer to the SCSI CDB.
 * @param cdb_len Length of the CDB, in bytes.
 * @param dif_info Pointer to the T10 DIF fields, or NULL if no DIF.
 * @param sgl Pointer to the scatter-gather list.
 * @param sgl_count Number of SGL entries in SGL.
 * @param wire_len Payload length, in bytes, of data on wire.
 * @param first_burst Number of first burst bytes to send.
 * @param cb Completion callback.
 * @param arg Application-specified completion callback argument.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

/*tc: could elminiate LUN, as it's part of the IO structure */

static int efct_scsi_send_io(enum efct_hw_io_type_e type,
			     struct efc_node_s *node,
	struct efct_io_s *io, u32 lun, enum efct_scsi_tmf_cmd_e tmf,
	u8 *cdb, u32 cdb_len, struct efct_scsi_dif_info_s *dif_info,
	struct efct_scsi_sgl_s *sgl, u32 sgl_count, u32 wire_len,
	u32 first_burst, efct_scsi_rsp_io_cb_t cb, void *arg)
{
	int rc;
	struct efct_s *efct;
	struct fcp_cmnd_iu_s *cmnd;
	u32 cmnd_bytes = 0;
	u8 abort_test_bits;
	u32 *fcp_dl;
	u8 tmf_flags = 0;

	efct_assert(io->node, -1);
	efct_assert(io->node == node, -1);
	efct_assert(io, -1);
	efct = io->efct;
	efct_assert(cb, -1);

	io->sgl_count = sgl_count;

	/* Copy SGL if needed */
	if (sgl != io->sgl) {
		efct_assert(sgl_count <= io->sgl_allocated, -1);
		memcpy(io->sgl, sgl, sizeof(*io->sgl) * sgl_count);
	}

	/* save initiator and target task tags for debugging */
	io->tgt_task_tag = 0xffff;

	io->wire_len = wire_len;
	io->hio_type = type;

	if (EFCT_LOG_ENABLE_SCSI_TRACE(efct)) {
		scsi_io_printf(io, "%s len %llu, cdb:%02X\n",
			       (io->hio_type == EFCT_HW_IO_INITIATOR_READ) ?
			"read" : (io->hio_type == EFCT_HW_IO_INITIATOR_WRITE) ?
			"write" : "",  io->wire_len, cdb[0]);
	}

	efct_assert(io->cmdbuf.virt, -1);

	cmnd = io->cmdbuf.virt;

	efct_assert(sizeof(*cmnd) <= io->cmdbuf.size, -1);

	memset(cmnd, 0, sizeof(*cmnd));

	/* Default FCP_CMND IU doesn't include additional CDB
	 * bytes but does include FCP_DL
	 */
	cmnd_bytes = sizeof(struct fcp_cmnd_iu_s) -
			sizeof(cmnd->fcp_cdb_and_dl) + sizeof(u32);

	fcp_dl = (u32 *)(&cmnd->fcp_cdb_and_dl);

	if (cdb) {
		if (cdb_len <= 16) {
			memcpy(cmnd->fcp_cdb, cdb, cdb_len);
		} else {
			u32 addl_cdb_bytes;

			memcpy(cmnd->fcp_cdb, cdb, 16);
			addl_cdb_bytes = cdb_len - 16;
			memcpy(cmnd->fcp_cdb_and_dl,
			       &cdb[16], addl_cdb_bytes);
			/*additional_fcp_cdb_length is in words, not bytes*/
			cmnd->additional_fcp_cdb_length =
				(addl_cdb_bytes + 3) / 4;
			fcp_dl += cmnd->additional_fcp_cdb_length;

			/* Round up additional CDB bytes */
			cmnd_bytes += (addl_cdb_bytes + 3) & ~0x3;
		}
	}

	abort_test_bits = (lun >> 24);
	lun &= 0xffffff;

	if (lun <= FCP_LUN_ADDR_SIMPLE_MAX) {
		cmnd->fcp_lun[1] = lun & 0xff;
	} else if (lun <= FCP_LUN_ADDR_FLAT_MAX) {
		/*
		 * Use single level, flat space LUN
		 */
		cmnd->fcp_lun[0] = (FCP_LUN_ADDR_METHOD_FLAT <<
			FCP_LUN_ADDRESS_METHOD_SHIFT) |
			((lun >> 8) & FCP_LUN_ADDRESS_METHOD_MASK);
		cmnd->fcp_lun[1] = lun & 0xff;
	} else {
		efct_log_test(efct, "unsupported LU %X\n", lun);
		/* need an error code that indicates LUN INVALID */
		return -1;
	}
	cmnd->fcp_lun[2] = abort_test_bits;

	switch (tmf) {
	case EFCT_SCSI_TMF_QUERY_TASK_SET:
		tmf_flags = FCP_QUERY_TASK_SET;
		break;
	case EFCT_SCSI_TMF_ABORT_TASK_SET:
		tmf_flags = FCP_ABORT_TASK_SET;
		break;
	case EFCT_SCSI_TMF_CLEAR_TASK_SET:
		tmf_flags = FCP_CLEAR_TASK_SET;
		break;
	case EFCT_SCSI_TMF_QUERY_ASYNCHRONOUS_EVENT:
		tmf_flags = FCP_QUERY_ASYNCHRONOUS_EVENT;
		break;
	case EFCT_SCSI_TMF_LOGICAL_UNIT_RESET:
		tmf_flags = FCP_LOGICAL_UNIT_RESET;
		break;
	case EFCT_SCSI_TMF_CLEAR_ACA:
		tmf_flags = FCP_CLEAR_ACA;
		break;
	case EFCT_SCSI_TMF_TARGET_RESET:
		tmf_flags = FCP_TARGET_RESET;
		break;
	default:
		tmf_flags = 0;
	}
	cmnd->task_management_flags = tmf_flags;

	*fcp_dl = cpu_to_be32(io->wire_len);

	switch (io->hio_type) {
	case EFCT_HW_IO_INITIATOR_READ:
		cmnd->rddata = 1;
		break;
	case EFCT_HW_IO_INITIATOR_WRITE:
		cmnd->wrdata = 1;
		break;
	case  EFCT_HW_IO_INITIATOR_NODATA:
		/* sets neither */
		break;
	default:
		efct_log_test(efct, "bad IO type %d\n", io->hio_type);
		return -1;
	}

	rc = efct_scsi_convert_dif_info(efct, dif_info, &io->hw_dif);
	if (rc)
		return rc;

	io->scsi_ini_cb = cb;
	io->scsi_ini_cb_arg = arg;

	/* set command and response buffers in the iparam */
	io->iparam.fcp_ini.cmnd = &io->cmdbuf;
	io->iparam.fcp_ini.cmnd_size = cmnd_bytes;
	io->iparam.fcp_ini.rsp = &io->rspbuf;
	io->iparam.fcp_ini.flags = 0;
	io->iparam.fcp_ini.dif_oper = io->hw_dif.dif;
	io->iparam.fcp_ini.blk_size = io->hw_dif.blk_size;
	io->iparam.fcp_ini.timeout = io->timeout;
	io->iparam.fcp_ini.first_burst = first_burst;

	return efct_scsi_io_dispatch(io, efct_initiator_io_cb);
}

/**
 * @ingroup scsi_api_base
 * @brief Callback for an aborted IO.
 *
 * @par Description
 * Callback function invoked upon completion of an IO abort request.
 *
 * @param hio HW IO context.
 * @param rnode Remote node.
 * @param len Response length.
 * @param status Completion status.
 * @param ext_status Extended completion status.
 * @param arg Application-specific callback, usually IO context.

 * @return Returns 0 on success, or a negative error code value on failure.
 */

static int
efct_scsi_abort_io_cb(const struct efct_hw_io_s *hio,
		      struct efc_remote_node_s *rnode,
	u32 len, int status, u32 ext_status, void *arg)
{
	struct efct_io_s *io = arg;
	struct efct_s *efct;
	enum efct_scsi_io_status_e scsi_status = EFCT_SCSI_STATUS_GOOD;

	efct_assert(io, -1);
	efct_assert(efct_io_busy(io), -1);
	efct_assert(io->efct, -1);
	efct_assert(io->io_to_abort, -1);
	efct = io->efct;

	efct_log_debug(efct, "status %d ext %d\n", status, ext_status);

	/* done with IO to abort; efct_ref_get(): efct_scsi_send_tmf() */
	kref_put(&io->io_to_abort->ref, io->io_to_abort->release);

	efct_scsi_io_free_ovfl(io);

	switch (status) {
	case SLI4_FC_WCQE_STATUS_SUCCESS:
		scsi_status = EFCT_SCSI_STATUS_GOOD;
		break;
	case SLI4_FC_WCQE_STATUS_LOCAL_REJECT:
		if (ext_status == SLI4_FC_LOCAL_REJECT_ABORT_REQUESTED) {
			scsi_status = EFCT_SCSI_STATUS_ABORTED;
		} else if (ext_status == SLI4_FC_LOCAL_REJECT_NO_XRI) {
			scsi_status = EFCT_SCSI_STATUS_NO_IO;
		} else if (ext_status ==
				SLI4_FC_LOCAL_REJECT_ABORT_IN_PROGRESS) {
			scsi_status = EFCT_SCSI_STATUS_ABORT_IN_PROGRESS;
		} else {
			efct_log_test(efct,
				      "Unhandled local reject 0x%x/0x%x\n",
				      status, ext_status);
			scsi_status = EFCT_SCSI_STATUS_ERROR;
		}
		break;
	default:
		scsi_status = EFCT_SCSI_STATUS_ERROR;
		break;
	}

	if (io->scsi_ini_cb)
		(*io->scsi_ini_cb)(io, scsi_status, NULL,
				  0, io->scsi_ini_cb_arg);
	else
		efct_scsi_io_free(io);

	efct_scsi_check_pending(efct);
	return 0;
}

/**
 * @ingroup scsi_api_base
 * @brief Return SCSI API integer valued property.
 *
 * @par Description
 * This function is called by a target-server or initiator-client to
 * retrieve an integer valued property.
 *
 * @param efct Pointer to the efct.
 * @param prop Property value to return.
 *
 * @return Returns a value, or 0 if invalid property was requested.
 */
u32
efct_scsi_get_property(struct efct_s *efct, enum efct_scsi_property_e prop)
{
	struct efct_xport_s *xport = efct->xport;
	u32	val;

	switch (prop) {
	case EFCT_SCSI_MAX_SGE:
		if (efct_hw_get(&efct->hw, EFCT_HW_MAX_SGE, &val) == 0)
			return val;
		break;
	case EFCT_SCSI_MAX_SGL:
		if (efct_hw_get(&efct->hw, EFCT_HW_N_SGL, &val) == 0)
			return val;
		break;
	case EFCT_SCSI_MAX_IOS:
		return efct_io_pool_allocated(xport->io_pool);
	case EFCT_SCSI_DIF_CAPABLE:
		if (efct_hw_get(&efct->hw,
				EFCT_HW_DIF_CAPABLE, &val) == 0) {
			return val;
		}
		break;
	case EFCT_SCSI_MAX_FIRST_BURST:
		return 0;
	case EFCT_SCSI_DIF_MULTI_SEPARATE:
		if (efct_hw_get(&efct->hw,
				EFCT_HW_DIF_MULTI_SEPARATE, &val) == 0) {
			return val;
		}
		break;
	case EFCT_SCSI_ENABLE_TASK_SET_FULL:
		/* Return FALSE if we are send frame capable */
		if (efct_hw_get(&efct->hw,
				EFCT_HW_SEND_FRAME_CAPABLE, &val) == 0) {
			return !val;
		}
		break;
	default:
		break;
	}

	efct_log_debug(efct, "invalid property request %d\n", prop);
	return 0;
}

/**
 * @brief Update transferred count
 *
 * @par Description
 * Updates io->transferred, as required when using first burst,
 * when the amount of first burst data processed differs from the
 * amount of first burst data received.
 *
 * @param io Pointer to the io object.
 * @param transferred Number of bytes transferred out of first burst buffers.
 *
 * @return None.
 */
void
efct_scsi_update_first_burst_transferred(struct efct_io_s *io,
					 u32 transferred)
{
	io->transferred = transferred;
}

/**
 * @page fc_transport_api_overview Transport APIs and State
 * Machine Functions
 * - @ref scsi_api_base
 * - @ref domain_sm
 * - @ref sport_sm
 * - @ref device_sm
 * - @ref p2p_sm
 * - @ref fabric_sm
 * - @ref ns_sm
 * - @ref unsol
 * - @ref els_api
 *
 * <div class="overview">
 * <img src="elx_fc_trans.jpg" alt="FC/FCoE Transport"
 *	title="FC/FCoE Transport" align="right"/>
 *
 * <h2>FC/FCoE Transport</h2>
 *
 * The FC/FCoE transport consists of the SCSI API, domain/port/node
 * state machines, and unsolicited frame handler components. At the
 * highest level, the transport serves two functions:
 * <ul><li>Handles the logic to manage port and process logins with
 * remote ports (nodes), as well as remote target node discovery.</li>
 * <li>Provides an abstracted SCSI interface to a back-end target and/or
 * initiator. This allows access to the functionality of the HW while
 * using a transport agnostic API.</li></ul>
 *
 * The transport also performs the following:
 * <ul><li>Handles domain event callback notifications from the HW.</li>
 * <li>Instantiates remote nodes in an FC arbitrated loop (FC-AL)
 *	topology.</li>
 * <li>Performs fabric login and directory services registration, and
 * registration of SLI port with HW.</li>
 * <li>Performs N_PORT to N_PORT (point to point) initialization.</li>
 * <li>Performs remote port discovery using directory services.</li>
 * <li>Performs port and process logins (PLOGI/PRLI) with remote
 *	target nodes.</li>
 * <li>Processes remote port ELS.</li>
 * <li>Exports target and initiator functions through the
 *	SCSI API.</li></ul>
 *
 * <h3>SCSI API</h3>
 *
 * To enable the development of a SCSI initiator or target at a higher level
 * than the SLI-4 and HW, entry points have been defined and implemented in
 * an API that uses SCSI semantics and concepts. For details on the SCSI API,
 * see chapter 4, SCSI API,
 * in the <i><a href="../../../../../doc/efct_reference_driver_manual.pdf"
 *  target="_blank">OneCore Storage Reference Driver Manual</a></i>.
 *
 * <h3>Domain/SLI Port/Node State Machines</h3>
 *
 * The domain/SLI port/node state machines implement the functions required to
 * establish and maintain port and process logins with remote nodes. Each node
 * object has its own fully independent state machine instance.There are
 * several node types that are created during the port/node state machine
 * operation:
 * <ul><li>Fabric/domain node used to establish fabric logins in a switched
 * fabric topology and during the initial steps of point to point login</li>
 * <li>Name services node used to communicate with the director services
 *	node</li>
 * <li>Fabric control node used to register for state change notifications
 *	(SCR/RSCN)</li>
 * <li>Remote SCSI initiator nodes</li>
 * <li>Remote SCSI target nodes</li></ul>
 *
 * The port/node state machine consists of several "sub" state machines:
 * <ul><li>__efct_fabric &ndash; the fabric login state machine</li>
 * <li>__efct_ns &ndash; the name/directory services state machine</li>
 * <li>__efct_p2p &ndash; the point to point protocol state machine</li>
 * <li>__efct_d &ndash; the remote initiator/target device state machine</li>
 * <li>__efct_fabctl &ndash; the fabric control node state machine</li></ul>
 *
 * The general flow of a node in any of the state machines is as follows:
 *  -# The node is created and has transitioned to an initial state,
 *   depending on the type of node.
 *  -# Events are posted to the node state machine instance.
 *   - ELS/BLS/FCP/TMF unsolicited frame received
 *   - ELS/BLS/FCP/TMF response received
 *   - HW domain/remote node events received
 *   - HW send I/O completion events received
 *   - Node shutdown
 *  -# The back-end components are notified of creation and deletion of nodes
 * through the Domain portion of the SCSI API.
 *
 * <h3>Unsolicited Frame Handler</h3>
 *
 * The unsolicited frame handler provides the interface to the transport
 * from the HW's unsolicited FC frame events. These include:
 * <ul><li>ELS/BLS frames
 * <li>FCP command frames
 * <li>FCP TMF frames</li></ul>
 *
 * On receipt of an unsolicited frame, the S_ID in the FC header is used
 * to find a corresponding node object instance. If a node does not exist
 * for the S_ID, a node object is allocated. An event applicable to the frame
 * type is posted to the node objects state machine. In general,
 * the unsolicited frame handler rarely makes any function or protocol
 * decisions, but merely dispatches events to the node state machines.
 * <br><br>
 * </div><!-- overview -->
 */

