/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__EFCT_LIO_H__)
#define __EFCT_LIO_H__

#define EFCT_INCLUDE_LIO

#include "efct_scsi.h"
#include <target/target_core_base.h>

enum efct_lio_wq_msg_s {
	EFCT_LIO_WQ_SUBMIT_CMD,
	EFCT_LIO_WQ_SUBMIT_TMF,
	EFCT_LIO_WQ_UNREG_SESSION,
	EFCT_LIO_WQ_NEW_INITIATOR,
	EFCT_LIO_WQ_STOP,
};

struct efct_lio_wq_data_s {
	enum efct_lio_wq_msg_s message;
	void *ptr;
	struct {
		struct efct_io_s *tmfio;
		u64 lun;
		enum efct_scsi_tmf_cmd_e cmd;
		struct efct_io_s *abortio;
		u32 flags;
	} tmf;
};

/**
 * @brief EFCT message queue object
 *
 * The EFCT message queue may be used to pass
 * messages between two threads (or an ISR and thread).
 * A message is defined here as a pointer to an instance
 * of application specific data (message data).
 * The message queue allocates a message header,
 * saves the message data pointer, and places the
 * header on the message queue's linked list.
 * A counting semaphore is used to synchronize access
 * to the message queue consumer.
 *
 */

struct efct_mqueue_s {
	void *os;
	spinlock_t lock;		/**< message queue lock */
	struct semaphore prod_sem;		/**< producer semaphore */
	struct list_head queue;
};

struct efct_lio_worker_s {
	struct task_struct *thread;
	struct efct_mqueue_s wq;
	struct semaphore sem;
};

/**
 * @brief target private efct structure
 */
struct efct_scsi_tgt_s {
	u32 max_sge;
	u32 max_sgl;

	/*
	 * Variables used to send task set full. We are using a high watermark
	 * method to send task set full. We will reserve a fixed number of IOs
	 * per initiator plus a fudge factor. Once we reach this number,
	 * then the target will start sending task set full/busy responses.
	 */
	atomic_t initiator_count;	/**< count of initiators */
	atomic_t ios_in_use;	/**< num of IOs in use */
	atomic_t io_high_watermark;	/**< used to send task set full */
	/**< used to track how often IO pool almost empty */
	atomic_t watermark_hit;
	int watermark_min;		/**< lower limit for watermark */
	int watermark_max;		/**< upper limit for watermark */

	struct efct_lio_sport *lio_sport;
	struct efct_lio_tpg *tpg;
	/**< list of VPORTS waiting to be created */
	struct list_head vport_pending_enable_list;
	struct list_head vport_list;		/**< list of existing VPORTS*/
	/* Protects vport list*/
	spinlock_t	efct_lio_lock;

	u64 wwnn;

	/* worker thread for making upcalls related to asynchronous
	 * events e.g. node (session) found, node (session) deleted,
	 * new command received
	 */
	struct efct_lio_worker_s async_worker;
};

/**
 * @brief target private domain structure
 */

struct efct_scsi_tgt_domain_s {
	/* efct_lio decls */
	;
};

/**
 * @brief target private sport structure
 */

struct efct_scsi_tgt_sport_s {
	struct efct_lio_sport *lio_sport;
};

/**
 * @brief target private node structure
 */

#define SCSI_TRANSPORT_ID_FCP   0

struct efct_scsi_tgt_node_s {
	struct se_session *session;
};

/**
 * @brief target private IO structure
 */

struct efct_scsi_tgt_io_s {
	struct se_cmd cmd;
	unsigned char sense_buffer[TRANSPORT_SENSE_BUFFER];
	enum {
		DDIR_NONE, DDIR_FROM_INITIATOR, DDIR_TO_INITIATOR, DDIR_BIDIR
	} ddir;
	int task_attr;
	struct semaphore sem;			/* for synchronizing aborts */
	u64 lun;

#define EFCT_LIO_STATE_SCSI_RECV_CMD		BIT(0)
#define EFCT_LIO_STATE_TGT_SUBMIT_CMD		BIT(1)
#define EFCT_LIO_STATE_TFO_QUEUE_DATA_IN		BIT(2)
#define EFCT_LIO_STATE_TFO_WRITE_PENDING		BIT(3)
#define EFCT_LIO_STATE_TGT_EXECUTE_CMD		BIT(4)
#define EFCT_LIO_STATE_SCSI_SEND_RD_DATA		BIT(5)
#define EFCT_LIO_STATE_TFO_CHK_STOP_FREE		BIT(6)
#define EFCT_LIO_STATE_SCSI_DATA_DONE		BIT(7)
#define EFCT_LIO_STATE_TFO_QUEUE_STATUS		BIT(8)
#define EFCT_LIO_STATE_SCSI_SEND_RSP		BIT(9)
#define EFCT_LIO_STATE_SCSI_RSP_DONE		BIT(10)
#define EFCT_LIO_STATE_TGT_GENERIC_FREE		BIT(11)
#define EFCT_LIO_STATE_SCSI_RECV_TMF		BIT(12)
#define EFCT_LIO_STATE_TGT_SUBMIT_TMR		BIT(13)
#define EFCT_LIO_STATE_TFO_WRITE_PEND_STATUS	BIT(14)
#define EFCT_LIO_STATE_TGT_GENERIC_REQ_FAILURE  BIT(15)

#define EFCT_LIO_STATE_TFO_ABORTED_TASK		BIT(29)
#define EFCT_LIO_STATE_TFO_RELEASE_CMD		BIT(30)
#define EFCT_LIO_STATE_SCSI_CMPL_CMD		BIT(31)
	u32 state;
	u8 *cdb;
	u8 tmf;
	struct efct_io_s *io_to_abort;
	u32 cdb_len;
	u32 seg_map_cnt;	/* current number of segments mapped for dma */
	u32 seg_cnt;	/* total segment count for i/o */
	u32 cur_seg;	/* current segment counter */
	enum efct_scsi_io_status_e err;	/* current error */
	/* context associated with thread work queue request */
	struct efct_lio_wq_data_s wq_data;
	bool	aborting;  /* IO is in process of being aborted */
	bool	rsp_sent; /* a response has been sent for this IO */
	uint32_t transfered_len;
};

/* Handler return codes */
enum {
	SCSI_HANDLER_DATAPHASE_STARTED = 1,
	SCSI_HANDLER_RESP_STARTED,
	SCSI_HANDLER_VALIDATED_DATAPHASE_STARTED,
	SCSI_CMD_NOT_SUPPORTED,
	};

#define scsi_pack_result(key, code, qualifier) (((key & 0xff) << 16) | \
				((code && 0xff) << 8) | (qualifier & 0xff))

int efct_scsi_tgt_driver_init(void);
int efct_scsi_tgt_driver_exit(void);
int scsi_dataphase_cb(struct efct_io_s *io,
		      enum efct_scsi_io_status_e scsi_status,
		      u32 flags, void *arg);
const char *efct_lio_get_msg_name(enum efct_lio_wq_msg_s msg);

#define FABRIC_SNPRINTF_LEN     32
struct efct_lio_vport {
	u64 wwpn;
	u64 npiv_wwpn;
	u64 npiv_wwnn;
	unsigned char wwpn_str[FABRIC_SNPRINTF_LEN];
	struct se_wwn vport_wwn;
	struct efct_lio_tpg *tpg;
	struct efct_s *efct;
	struct dentry *sessions;
	struct Scsi_Host *shost;
	struct fc_vport *fc_vport;
	atomic_t enable;
};

/***************************************************************************
 * Message Queues
 *
 */

/**
 * @brief EFCT message queue message
 *
 */

struct efct_mqueue_hdr_s {
	struct list_head list_entry;
	void *msgdata;				/**< message data (payload) */
};

/*
 * Define a structure used to pass to the interrupt handlers and the tasklets.
 */
struct efct_os_intr_context_s {
	struct efct_s *efct;
	u32 index;
	struct semaphore intsem;
	struct task_struct *thread;
};

#define EFCT_PCI_MAX_BAR	6
#define MAX_PCI_INTERRUPTS 16
struct efct_os_s {
	struct pci_dev	*pdev;
	void __iomem *reg[EFCT_PCI_MAX_BAR];

	struct msix_entry msix_vec[MAX_PCI_INTERRUPTS];
	u32 n_msix_vec;

	struct efct_os_intr_context_s intr_context[MAX_PCI_INTERRUPTS];

	u32 numa_node;
};

/**
 * @brief initialize an EFCT message queue
 *
 * The elements of the message queue  are initialized
 *
 * @param os OS handle
 * @param q pointer to message queue
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

static inline int
efct_mqueue_init(void *os, struct efct_mqueue_s *q)
{
	memset(q, 0, sizeof(*q));
	q->os = os;
	spin_lock_init(&q->lock);
	sema_init(&q->prod_sem, 0);
	INIT_LIST_HEAD(&q->queue);
	return 0;
}

/**
 * @brief put a message in a message queue
 *
 * A message header is allocated, it's payload set to point to the
 * requested message data, and the
 * header posted to the message queue.
 *
 * @param q pointer to message queue
 * @param msgdata pointer to message data
 *
 * @return returns 0 for success, a negative error code value for failure.
 */

static inline int
efct_mqueue_put(struct efct_mqueue_s *q, void *msgdata)
{
	struct efct_mqueue_hdr_s *hdr = NULL;
	unsigned long flags = 0;
	struct efct_os_s *efct = q->os;

	hdr = kmalloc_node(sizeof(*hdr), GFP_ATOMIC, efct->numa_node);
	if (!hdr)
		return -1;

	memset(hdr, 0, sizeof(*hdr));
	hdr->msgdata = msgdata;

	/* lock the queue wide lock, add to tail of linked list
	 * and increment the semaphore
	 */
	spin_lock_irqsave(&q->lock, flags);
		INIT_LIST_HEAD(&hdr->list_entry);
		list_add_tail(&hdr->list_entry, &q->queue);
	spin_unlock_irqrestore(&q->lock, flags);
	up(&q->prod_sem);
	return 0;
}

/**
 * @brief read next message
 *
 * Reads next message header from the message queue, or times out.
 * The timeout_usec value
 * if zero will try one time, if negative will try forever, and if positive
 * will try for that many micro-seconds.
 *
 * @param q pointer to message queue
 * @param timeout_usec timeout
 * (0 - try once, < 0 try forever, > 0 try micro-seconds)
 *
 * @return returns pointer to next message, or NULL
 */

static inline void *
efct_mqueue_get(struct efct_mqueue_s *q, int timeout_usec)
{
	int rc;
	struct efct_mqueue_hdr_s *hdr = NULL;
	void *msgdata = NULL;
	unsigned long flags = 0;

	if (!q) {
		efct_log_err(NULL, "q is NULL\n");
		return NULL;
	}

	rc = down_timeout(&q->prod_sem, usecs_to_jiffies(timeout_usec));
	if (rc != 0)
		return NULL;

	spin_lock_irqsave(&q->lock, flags);
	if (!list_empty(&q->queue)) {
		hdr = list_first_entry(&q->queue,
				       struct efct_mqueue_hdr_s, list_entry);
		list_del(&hdr->list_entry);
	}
	spin_unlock_irqrestore(&q->lock, flags);

	if (hdr) {
		msgdata = hdr->msgdata;
		kfree(hdr);
	}
	return msgdata;
}

/**
 * @brief free an EFCT message queue
 *
 * The message queue and its resources are free'd.
 * In this case, the message queue is
 * drained, and all the messages free'd
 *
 * @param q pointer to message queue
 *
 * @return none
 */

static inline void
efct_mqueue_free(struct efct_mqueue_s *q)
{
	struct efct_mqueue_hdr_s *hdr;
	struct efct_mqueue_hdr_s *next;
	unsigned long flags = 0;

	spin_lock_irqsave(&q->lock, flags);
	list_for_each_entry_safe(hdr, next, &q->queue, list_entry) {
		efct_log_err(NULL,
			     "Warning: freeing queue, payload %p may leak\n",
			    hdr->msgdata);
		kfree(hdr);
	}
	spin_unlock_irqrestore(&q->lock, flags);
}

#endif /*__EFCT_LIO_H__ */
