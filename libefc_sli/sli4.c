/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/**
 * All common (i.e. transport-independent) SLI-4 functions are implemented
 * in this file.
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include "../libefc/efclib.h"
#include "sli4.h"

#define SLI4_BMBX_DELAY_US 1000 /* 1 ms */
#define SLI4_INIT_PORT_DELAY_US 10000 /* 10 ms */

static int sli_fw_init(struct sli4_s *sli4);
static int sli_fw_term(struct sli4_s *sli4);
static int sli_sliport_reset(struct sli4_s *sli4);

struct sli4_asic_entry_t {
	u32 rev_id;
	u32 family;	/* generation */
	enum sli4_asic_type_e type;
	enum sli4_asic_rev_e rev;
};

static struct sli4_asic_entry_t sli4_asic_table[] = {
	/* not documented */
	{ 0x10, SLI4_ASIC_GEN_5, SLI4_ASIC_INTF_2, SLI4_ASIC_REV_B0},
	{ 0x30, SLI4_ASIC_GEN_5, SLI4_ASIC_INTF_2, SLI4_ASIC_REV_D0},
	{ 0x3, SLI4_ASIC_GEN_6, SLI4_ASIC_INTF_2, SLI4_ASIC_REV_A3},
	{ 0x0, SLI4_ASIC_GEN_6, SLI4_ASIC_INTF_2_G6, SLI4_ASIC_REV_A0},
	{ 0x1, SLI4_ASIC_GEN_6, SLI4_ASIC_INTF_2_G6, SLI4_ASIC_REV_A1},
	{ 0x3, SLI4_ASIC_GEN_6, SLI4_ASIC_INTF_2_G6, SLI4_ASIC_REV_A3},
	{ 0x1, SLI4_ASIC_GEN_7, SLI4_ASIC_INTF_2_G7, SLI4_ASIC_REV_FPGA},
};

/*
 * @brief Convert queue type enum (SLI_QTYPE_*) into a string.
 */
static char *SLI_QNAME[] = {
	"Event Queue",
	"Completion Queue",
	"Mailbox Queue",
	"Work Queue",
	"Receive Queue",
	"Undefined"
};

/*
 * @brief Wait for the bootstrap mailbox to report "ready".
 *
 * @param sli4 SLI context pointer.
 * @param msec Number of milliseconds to wait.
 *
 * @return Returns 0 if BMBX is ready, or non-zero otherwise
 * (i.e. time out occurred).
 */
static int
sli_bmbx_wait(struct sli4_s *sli4, u32 msec)
{
	u32	val = 0;

	do {
		mdelay(1);	/* 1 ms */
		val = readl(sli4->reg[0] + SLI4_BMBX_REGOFFSET);
		msec--;
	} while (msec && !(val & SLI4_BMBX_RDY));

	val = (!(val & SLI4_BMBX_RDY));
	return val;
}

/**
 * @brief Write bootstrap mailbox.
 *
 * @param sli4 SLI context pointer.
 *
 * @return Returns 0 if command succeeded, or non-zero otherwise.
 */
static int
sli_bmbx_write(struct sli4_s *sli4)
{
	u32	val = 0;

	/* write buffer location to bootstrap mailbox register */
	val = SLI4_BMBX_WRITE_HI(sli4->bmbx.phys);
	writel(val, (sli4->reg[0] + SLI4_BMBX_REGOFFSET));

	if (sli_bmbx_wait(sli4, SLI4_BMBX_DELAY_US)) {
		pr_crit("BMBX WRITE_HI failed\n");
		return -1;
	}
	val = SLI4_BMBX_WRITE_LO(sli4->bmbx.phys);
	writel(val, (sli4->reg[0] + SLI4_BMBX_REGOFFSET));

	/* wait for SLI Port to set ready bit */
	return sli_bmbx_wait(sli4, SLI4_BMBX_TIMEOUT_MSEC);
}

/**
 * @ingroup sli
 * @brief Submit a command to the bootstrap mailbox and check the status.
 *
 * @param sli4 SLI context pointer.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_bmbx_command(struct sli4_s *sli4)
{
	void *cqe = (u8 *)sli4->bmbx.virt + SLI4_BMBX_SIZE;

	if (sli_fw_error_status(sli4) > 0) {
		pr_crit("Chip is in an error state -Mailbox command rejected");
		pr_crit(" status=%#x error1=%#x error2=%#x\n",
			sli_reg_read_status(sli4),
			sli_reg_read_err1(sli4),
			sli_reg_read_err2(sli4));
		return -1;
	}

	if (sli_bmbx_write(sli4)) {
		pr_crit("bootstrap mailbox write fail phys=%p reg=%#x\n",
			(void *)sli4->bmbx.phys,
			readl(sli4->reg[0] + SLI4_BMBX_REGOFFSET));
		return -1;
	}

	/* check completion queue entry status */
	if (le32_to_cpu(((struct sli4_mcqe_s *)cqe)->dw3_flags) &
	    SLI4_MCQE_VALID) {
		return sli_cqe_mq(cqe);
	}
	pr_crit("invalid or wrong type\n");
	return -1;
}

/*
 * Messages
 */

/**
 * @ingroup sli
 * @brief Write a CONFIG_LINK command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_config_link(struct sli4_s *sli4, void *buf, size_t size)
{
	struct sli4_cmd_config_link_s	*config_link = buf;

	memset(buf, 0, size);

	config_link->hdr.command = SLI4_MBOX_COMMAND_CONFIG_LINK;

	/* Port interprets zero in a field as "use default value" */

	return sizeof(struct sli4_cmd_config_link_s);
}

/**
 * @ingroup sli
 * @brief Write a DOWN_LINK command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_down_link(struct sli4_s *sli4, void *buf, size_t size)
{
	struct sli4_mbox_command_header_s	*hdr = buf;

	memset(buf, 0, size);

	hdr->command = SLI4_MBOX_COMMAND_DOWN_LINK;

	/* Port interprets zero in a field as "use default value" */

	return sizeof(struct sli4_mbox_command_header_s);
}

/**
 * @ingroup sli
 * @brief Write a DUMP Type 4 command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param wki The well known item ID.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_dump_type4(struct sli4_s *sli4, void *buf,
		   size_t size, u16 wki)
{
	struct sli4_cmd_dump4_s	*cmd = buf;

	memset(buf, 0, size);

	cmd->hdr.command = SLI4_MBOX_COMMAND_DUMP;
	cmd->type_dword = cpu_to_le32(0x4);
	cmd->wki_selection = cpu_to_le16(wki);
	return sizeof(struct sli4_cmd_dump4_s);
}

/**
 * @ingroup sli
 * @brief Write a COMMON_READ_TRANSCEIVER_DATA command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param page_num The page of SFP data to retrieve (0xa0 or 0xa2).
 * @param dma DMA structure from which the data will be copied.
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_read_transceiver_data(struct sli4_s *sli4, void *buf,
				     size_t size, u32 page_num,
				     struct efc_dma_s *dma)
{
	struct sli4_req_common_read_transceiver_data_s *req = NULL;
	u32	sli_config_off = 0;
	u32	payload_size, size1, size2;

	if (!dma) {
		/* Payload length must accommodate both request and response */
		size1 = sizeof(struct sli4_req_common_read_transceiver_data_s);
		size2 = sizeof(struct sli4_res_common_read_transceiver_data_s);
		payload_size = max(size1, size2);
	} else {
		payload_size = dma->size;
	}

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, dma);

	if (!dma) {
		req = (struct sli4_req_common_read_transceiver_data_s *)
			((u8 *)buf + sli_config_off);
	} else {
		req =
		   (struct sli4_req_common_read_transceiver_data_s *)dma->virt;
		memset(req, 0, dma->size);
	}

	req->hdr.opcode = SLI4_OPC_COMMON_READ_TRANSCEIVER_DATA;
	req->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	req->hdr.request_length =
	cpu_to_le32(payload_size - sizeof(struct sli4_req_hdr_s));

	req->page_number = cpu_to_le32(page_num);
	req->port = cpu_to_le32(sli4->physical_port);

	return sli_config_off +
			sizeof(struct sli4_req_common_read_transceiver_data_s);
}

/**
 * @ingroup sli
 * @brief Write a READ_LINK_STAT command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param req_ext_counters If TRUE,
 * then the extended counters will be requested.
 * @param clear_overflow_flags If TRUE, then overflow flags will be cleared.
 * @param clear_all_counters If TRUE, the counters will be cleared.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_read_link_stats(struct sli4_s *sli4, void *buf, size_t size,
			u8 req_ext_counters,
			u8 clear_overflow_flags,
			u8 clear_all_counters)
{
	struct sli4_cmd_read_link_stats_s	*cmd = buf;
	u32 flags;

	memset(buf, 0, size);

	cmd->hdr.command = SLI4_MBOX_COMMAND_READ_LNK_STAT;

	flags = 0;
	if (req_ext_counters)
		flags |= SLI4_READ_LNKSTAT_REC;
	if (clear_all_counters)
		flags |= SLI4_READ_LNKSTAT_CLRC;
	if (clear_overflow_flags)
		flags |= SLI4_READ_LNKSTAT_CLOF;

	cmd->dw1_flags = cpu_to_le32(flags);
	return sizeof(struct sli4_cmd_read_link_stats_s);
}

/**
 * @ingroup sli
 * @brief Write a READ_STATUS command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param clear_counters If TRUE, the counters will be cleared.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_read_status(struct sli4_s *sli4, void *buf, size_t size,
		    u8 clear_counters)
{
	struct sli4_cmd_read_status_s	*cmd = buf;
	u32 flags = 0;

	memset(buf, 0, size);

	cmd->hdr.command = SLI4_MBOX_COMMAND_READ_STATUS;
	if (clear_counters)
		flags |= SLI4_READSTATUS_CLEAR_COUNTERS;
	else
		flags &= ~SLI4_READSTATUS_CLEAR_COUNTERS;

	cmd->dw1_flags = cpu_to_le32(flags);
	return sizeof(struct sli4_cmd_read_status_s);
}

/**
 * @ingroup sli
 * @brief Write an INIT_LINK command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param speed Link speed.
 * @param reset_alpa For native FC, this is the selective reset AL_PA
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_init_link(struct sli4_s *sli4, void *buf, size_t size,
		  u32 speed, u8 reset_alpa)
{
	struct sli4_cmd_init_link_s	*init_link = buf;
	u32 flags = 0;

	memset(buf, 0, size);

	init_link->hdr.command = SLI4_MBOX_COMMAND_INIT_LINK;

	init_link->sel_reset_al_pa_dword =
				cpu_to_le32(reset_alpa);
	flags &= ~SLI4_INIT_LINK_FLAG_LOOPBACK;

	init_link->link_speed_sel_code = cpu_to_le32(speed);
	switch (speed) {
	case FC_LINK_SPEED_1G:
	case FC_LINK_SPEED_2G:
	case FC_LINK_SPEED_4G:
	case FC_LINK_SPEED_8G:
	case FC_LINK_SPEED_16G:
	case FC_LINK_SPEED_32G:
		flags |= SLI4_INIT_LINK_FLAG_FIXED_SPEED;
		break;
	case FC_LINK_SPEED_10G:
		pr_info("unsupported FC speed %d\n", speed);
		init_link->flags0 = cpu_to_le32(flags);
		return 0;
	}

	switch (sli4->topology) {
	case SLI4_READ_CFG_TOPO_FC:
		/* Attempt P2P but failover to FC-AL */
		flags |= SLI4_INIT_LINK_FLAG_EN_TOPO_FAILOVER;

		flags &= ~SLI4_INIT_LINK_FLAG_TOPOLOGY;
		flags |= (SLI4_INIT_LINK_F_P2P_FAIL_OVER << 1);
		break;
	case SLI4_READ_CFG_TOPO_FC_AL:
		flags &= ~SLI4_INIT_LINK_FLAG_TOPOLOGY;
		flags |= (SLI4_INIT_LINK_F_FCAL_ONLY << 1);
		if (speed == FC_LINK_SPEED_16G ||
		    speed == FC_LINK_SPEED_32G) {
			pr_info("unsupported FC-AL speed %d\n",
				speed);
			init_link->flags0 = cpu_to_le32(flags);
			return 0;
		}
		break;
	case SLI4_READ_CFG_TOPO_FC_DA:
		flags &= ~SLI4_INIT_LINK_FLAG_TOPOLOGY;
		flags |= (FC_TOPOLOGY_P2P << 1);
		break;
	default:

		pr_info("unsupported topology %#x\n",
			sli4->topology);

		init_link->flags0 = cpu_to_le32(flags);
		return 0;
	}

	flags &= (~SLI4_INIT_LINK_FLAG_UNFAIR);
	flags &= (~SLI4_INIT_LINK_FLAG_SKIP_LIRP_LILP);
	flags &= (~SLI4_INIT_LINK_FLAG_LOOP_VALIDITY);
	flags &= (~SLI4_INIT_LINK_FLAG_SKIP_LISA);
	flags &= (~SLI4_INIT_LINK_FLAG_SEL_HIGHTEST_AL_PA);
	init_link->flags0 = cpu_to_le32(flags);

	return sizeof(struct sli4_cmd_init_link_s);
}

/**
 * @ingroup sli
 * @brief Write an INIT_VFI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param vfi VFI
 * @param fcfi FCFI
 * @param vpi VPI (Set to -1 if unused.)
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_init_vfi(struct sli4_s *sli4, void *buf, size_t size,
		 u16 vfi, u16 fcfi, u16 vpi)
{
	struct sli4_cmd_init_vfi_s	*init_vfi = buf;
	u16 flags = 0;

	memset(buf, 0, size);

	init_vfi->hdr.command = SLI4_MBOX_COMMAND_INIT_VFI;

	init_vfi->vfi = cpu_to_le16(vfi);
	init_vfi->fcfi = cpu_to_le16(fcfi);

	/*
	 * If the VPI is valid, initialize it at the same time as
	 * the VFI
	 */
	if (vpi != 0xffff) {
		flags |= SLI4_INIT_VFI_FLAG_VP;
		init_vfi->flags0_word = cpu_to_le16(flags);
		init_vfi->vpi = cpu_to_le16(vpi);
	}

	return sizeof(struct sli4_cmd_init_vfi_s);
}

/**
 * @ingroup sli
 * @brief Write an INIT_VPI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param vpi VPI allocated.
 * @param vfi VFI associated with this VPI.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_init_vpi(struct sli4_s *sli4, void *buf, size_t size,
		 u16 vpi, u16 vfi)
{
	struct sli4_cmd_init_vpi_s	*init_vpi = buf;

	memset(buf, 0, size);

	init_vpi->hdr.command = SLI4_MBOX_COMMAND_INIT_VPI;
	init_vpi->vpi = cpu_to_le16(vpi);
	init_vpi->vfi = cpu_to_le16(vfi);

	return sizeof(struct sli4_cmd_init_vpi_s);
}

/**
 * @ingroup sli
 * @brief Write a POST_XRI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param xri_base Starting XRI value for range of XRI given to SLI Port.
 * @param xri_count Number of XRIs provided to the SLI Port.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_post_xri(struct sli4_s *sli4, void *buf, size_t size,
		 u16 xri_base, u16 xri_count)
{
	struct sli4_cmd_post_xri_s	*post_xri = buf;
	u16 xri_count_flags = 0;

	memset(buf, 0, size);

	post_xri->hdr.command = SLI4_MBOX_COMMAND_POST_XRI;
	post_xri->xri_base = cpu_to_le16(xri_base);
	xri_count_flags = (xri_count & SLI4_POST_XRI_COUNT);
	xri_count_flags |= SLI4_POST_XRI_FLAG_ENX;
	xri_count_flags |= SLI4_POST_XRI_FLAG_VAL;
	post_xri->xri_count_flags = cpu_to_le16(xri_count_flags);

	return sizeof(struct sli4_cmd_post_xri_s);
}

/**
 * @ingroup sli
 * @brief Write a RELEASE_XRI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param num_xri The number of XRIs to be released.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_release_xri(struct sli4_s *sli4, void *buf, size_t size,
		    u8 num_xri)
{
	struct sli4_cmd_release_xri_s	*release_xri = buf;

	memset(buf, 0, size);

	release_xri->hdr.command = SLI4_MBOX_COMMAND_RELEASE_XRI;
	release_xri->xri_count_word = cpu_to_le16(num_xri &
					SLI4_RELEASE_XRI_COUNT);

	return sizeof(struct sli4_cmd_release_xri_s);
}

/**
 * @brief Write a READ_CONFIG command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_read_config(struct sli4_s *sli4, void *buf, size_t size)
{
	struct sli4_cmd_read_config_s	*read_config = buf;

	memset(buf, 0, size);

	read_config->hdr.command = SLI4_MBOX_COMMAND_READ_CONFIG;

	return sizeof(struct sli4_cmd_read_config_s);
}

/**
 * @brief Write a READ_NVPARMS command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_read_nvparms(struct sli4_s *sli4, void *buf, size_t size)
{
	struct sli4_cmd_read_nvparms_s	*read_nvparms = buf;

	memset(buf, 0, size);

	read_nvparms->hdr.command = SLI4_MBOX_COMMAND_READ_NVPARMS;

	return sizeof(struct sli4_cmd_read_nvparms_s);
}

/**
 * @brief Write a WRITE_NVPARMS command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param wwpn WWPN to write - pointer to array of 8 u8.
 * @param wwnn WWNN to write - pointer to array of 8 u8.
 * @param hard_alpa Hard ALPA to write.
 * @param preferred_d_id  Preferred D_ID to write.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_write_nvparms(struct sli4_s *sli4, void *buf, size_t size,
		      u8 *wwpn, u8 *wwnn, u8 hard_alpa,
		u32 preferred_d_id)
{
	struct sli4_cmd_write_nvparms_s	*write_nvparms = buf;

	memset(buf, 0, size);

	write_nvparms->hdr.command = SLI4_MBOX_COMMAND_WRITE_NVPARMS;
	memcpy(write_nvparms->wwpn, wwpn, 8);
	memcpy(write_nvparms->wwnn, wwnn, 8);

	write_nvparms->hard_alpa_d_id =
			cpu_to_le32((preferred_d_id << 8) | hard_alpa);

	return sizeof(struct sli4_cmd_write_nvparms_s);
}

/**
 * @brief Write a READ_REV command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param vpd Pointer to the buffer.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_read_rev(struct sli4_s *sli4, void *buf, size_t size,
		 struct efc_dma_s *vpd)
{
	struct sli4_cmd_read_rev_s	*read_rev = buf;

	memset(buf, 0, size);

	read_rev->hdr.command = SLI4_MBOX_COMMAND_READ_REV;

	if (vpd && vpd->size) {
		read_rev->flags0_word |= cpu_to_le16(SLI4_READ_REV_FLAG_VPD);

		read_rev->available_length_dword =
			cpu_to_le16(vpd->size &
				    SLI4_READ_REV_AVAILABLE_LENGTH);

		read_rev->phy_addr_low =
				cpu_to_le32(lower_32_bits(vpd->phys));
		read_rev->phy_addr_high =
				cpu_to_le32(upper_32_bits(vpd->phys));
	}

	return sizeof(struct sli4_cmd_read_rev_s);
}

/**
 * @ingroup sli
 * @brief Write a READ_SPARM64 command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param dma DMA buffer for the service parameters.
 * @param vpi VPI used to determine the WWN.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_read_sparm64(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *dma,
		     u16 vpi)
{
	struct sli4_cmd_read_sparm64_s	*read_sparm64 = buf;

	memset(buf, 0, size);

	if (vpi == SLI4_READ_SPARM64_VPI_SPECIAL) {
		pr_info("special VPI not supported!!!\n");
		return -1;
	}

	if (!dma || !dma->phys) {
		pr_info("bad DMA buffer\n");
		return -1;
	}

	read_sparm64->hdr.command = SLI4_MBOX_COMMAND_READ_SPARM64;

	read_sparm64->bde_64.bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				    (dma->size & SLI4_BDE_MASK_BUFFER_LEN));
	read_sparm64->bde_64.u.data.buffer_address_low =
			cpu_to_le32(lower_32_bits(dma->phys));
	read_sparm64->bde_64.u.data.buffer_address_high =
			cpu_to_le32(upper_32_bits(dma->phys));

	read_sparm64->vpi = cpu_to_le16(vpi);

	return sizeof(struct sli4_cmd_read_sparm64_s);
}

/**
 * @ingroup sli
 * @brief Write a READ_TOPOLOGY command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param dma DMA buffer for loop map (optional).
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_read_topology(struct sli4_s *sli4, void *buf, size_t size,
		      struct efc_dma_s *dma)
{
	struct sli4_cmd_read_topology_s *read_topo = buf;

	memset(buf, 0, size);

	read_topo->hdr.command = SLI4_MBOX_COMMAND_READ_TOPOLOGY;

	if (dma && dma->size) {
		if (dma->size < SLI4_MIN_LOOP_MAP_BYTES) {
			pr_info("loop map buffer too small %jd\n",
				dma->size);
			return 0;
		}

		memset(dma->virt, 0, dma->size);

		read_topo->bde_loop_map.bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				    (dma->size & SLI4_BDE_MASK_BUFFER_LEN));
		read_topo->bde_loop_map.u.data.buffer_address_low  =
			cpu_to_le32(lower_32_bits(dma->phys));
		read_topo->bde_loop_map.u.data.buffer_address_high =
			cpu_to_le32(upper_32_bits(dma->phys));
	}

	return sizeof(struct sli4_cmd_read_topology_s);
}

/**
 * @ingroup sli
 * @brief Write a REG_FCFI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param index FCF index returned by READ_FCF_TABLE.
 * @param rq_cfg RQ_ID/R_CTL/TYPE routing information
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_reg_fcfi(struct sli4_s *sli4, void *buf, size_t size,
		 u16 index,
		 struct sli4_cmd_rq_cfg_s rq_cfg[SLI4_CMD_REG_FCFI_NUM_RQ_CFG])
{
	struct sli4_cmd_reg_fcfi_s	*reg_fcfi = buf;
	u32		i;

	memset(buf, 0, size);

	reg_fcfi->hdr.command = SLI4_MBOX_COMMAND_REG_FCFI;

	reg_fcfi->fcf_index = cpu_to_le16(index);

	for (i = 0; i < SLI4_CMD_REG_FCFI_NUM_RQ_CFG; i++) {
		switch (i) {
		case 0:
			reg_fcfi->rqid0 = cpu_to_le16(rq_cfg[0].rq_id);
			break;
		case 1:
			reg_fcfi->rqid1 = cpu_to_le16(rq_cfg[1].rq_id);
			break;
		case 2:
			reg_fcfi->rqid2 = cpu_to_le16(rq_cfg[2].rq_id);
			break;
		case 3:
			reg_fcfi->rqid3 = cpu_to_le16(rq_cfg[3].rq_id);
			break;
		}
		reg_fcfi->rq_cfg[i].r_ctl_mask = rq_cfg[i].r_ctl_mask;
		reg_fcfi->rq_cfg[i].r_ctl_match = rq_cfg[i].r_ctl_match;
		reg_fcfi->rq_cfg[i].type_mask = rq_cfg[i].type_mask;
		reg_fcfi->rq_cfg[i].type_match = rq_cfg[i].type_match;
	}

	return sizeof(struct sli4_cmd_reg_fcfi_s);
}

/**
 * @brief Write REG_FCFI_MRQ to provided command buffer
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param fcf_index FCF index returned by READ_FCF_TABLE.
 * @param rr_quant Round robin quanta if RQ selection policy is 2
 * @param rq_selection_policy RQ selection policy
 * @param num_rqs Array of count of RQs per filter
 * @param rq_ids Array of RQ ids per filter
 * @param rq_cfg RQ_ID/R_CTL/TYPE routing information
 *
 * @return returns 0 for success, a negative error code value for failure.
 */
int
sli_cmd_reg_fcfi_mrq(struct sli4_s *sli4, void *buf, size_t size,
		     u8 mode, u16 fcf_index,
		     u8 rq_selection_policy, u8 mrq_bit_mask,
		     u16 num_mrqs,
		struct sli4_cmd_rq_cfg_s rq_cfg[SLI4_CMD_REG_FCFI_NUM_RQ_CFG])
{
	struct sli4_cmd_reg_fcfi_mrq_s	*reg_fcfi_mrq = buf;
	u32 i;
	u32 mrq_flags = 0;

	memset(buf, 0, size);

	reg_fcfi_mrq->hdr.command = SLI4_MBOX_COMMAND_REG_FCFI_MRQ;
	if (mode == SLI4_CMD_REG_FCFI_SET_FCFI_MODE) {
		reg_fcfi_mrq->fcf_index = cpu_to_le16(fcf_index);
		goto done;
	}

	for (i = 0; i < SLI4_CMD_REG_FCFI_NUM_RQ_CFG; i++) {
		reg_fcfi_mrq->rq_cfg[i].r_ctl_mask = rq_cfg[i].r_ctl_mask;
		reg_fcfi_mrq->rq_cfg[i].r_ctl_match = rq_cfg[i].r_ctl_match;
		reg_fcfi_mrq->rq_cfg[i].type_mask = rq_cfg[i].type_mask;
		reg_fcfi_mrq->rq_cfg[i].type_match = rq_cfg[i].type_match;

		switch (i) {
		case 3:
			reg_fcfi_mrq->rqid3 = cpu_to_le16(rq_cfg[i].rq_id);
			break;
		case 2:
			reg_fcfi_mrq->rqid2 = cpu_to_le16(rq_cfg[i].rq_id);
			break;
		case 1:
			reg_fcfi_mrq->rqid1 = cpu_to_le16(rq_cfg[i].rq_id);
			break;
		case 0:
			reg_fcfi_mrq->rqid0 = cpu_to_le16(rq_cfg[i].rq_id);
			break;
		}
	}

	mrq_flags = num_mrqs & SLI4_REGFCFI_MRQ_MASK_NUM_PAIRS;
	mrq_flags |= (mrq_bit_mask << 8);
	mrq_flags |= (rq_selection_policy << 12);
	reg_fcfi_mrq->dw9_mrqflags = cpu_to_le32(mrq_flags);
done:
	return sizeof(struct sli4_cmd_reg_fcfi_mrq_s);
}

/**
 * @ingroup sli
 * @brief Write a REG_RPI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param nport_id Remote F/N_Port_ID.
 * @param rpi Previously-allocated Remote Port Indicator.
 * @param vpi Previously-allocated Virtual Port Indicator.
 * @param dma DMA buffer that contains the remote port's service parameters.
 * @param update Boolean indicating an update to an existing RPI (TRUE)
 * or a new registration (FALSE).
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_reg_rpi(struct sli4_s *sli4, void *buf, size_t size,
		u32 nport_id, u16 rpi, u16 vpi,
		struct efc_dma_s *dma, u8 update,
		u8 enable_t10_pi)
{
	struct sli4_cmd_reg_rpi_s *reg_rpi = buf;
	u32 rportid_flags = 0;

	memset(buf, 0, size);

	reg_rpi->hdr.command = SLI4_MBOX_COMMAND_REG_RPI;

	reg_rpi->rpi = cpu_to_le16(rpi);

	rportid_flags = nport_id & SLI4_REGRPI_REMOTE_N_PORTID;

	if (update)
		rportid_flags |= SLI4_REGRPI_UPD;
	else
		rportid_flags &= ~SLI4_REGRPI_UPD;

	if (enable_t10_pi)
		rportid_flags |= SLI4_REGRPI_ETOW;
	else
		rportid_flags &= ~SLI4_REGRPI_ETOW;

	reg_rpi->dw2_rportid_flags = cpu_to_le32(rportid_flags);

	reg_rpi->bde_64.bde_type_buflen =
		cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			    (SLI4_REG_RPI_BUF_LEN & SLI4_BDE_MASK_BUFFER_LEN));
	reg_rpi->bde_64.u.data.buffer_address_low  =
		cpu_to_le32(lower_32_bits(dma->phys));
	reg_rpi->bde_64.u.data.buffer_address_high =
		cpu_to_le32(upper_32_bits(dma->phys));

	reg_rpi->vpi = cpu_to_le16(vpi);

	return sizeof(struct sli4_cmd_reg_rpi_s);
}

/**
 * @ingroup sli
 * @brief Write a REG_VFI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param domain Pointer to the domain object.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_reg_vfi(struct sli4_s *sli4, void *buf, size_t size,
		u16 vfi, u16 fcfi, struct efc_dma_s dma,
		u16 vpi, __be64 sli_wwpn, u32 fc_id)
{
	struct sli4_cmd_reg_vfi_s	*reg_vfi = buf;

	if (!sli4 || !buf)
		return 0;

	memset(buf, 0, size);

	reg_vfi->hdr.command = SLI4_MBOX_COMMAND_REG_VFI;

	reg_vfi->vfi = cpu_to_le16(vfi);

	reg_vfi->fcfi = cpu_to_le16(fcfi);

	reg_vfi->sparm.bde_type_buflen =
		cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) | 0x70);
	reg_vfi->sparm.u.data.buffer_address_low  =
		cpu_to_le32(lower_32_bits(dma.phys));
	reg_vfi->sparm.u.data.buffer_address_high =
		cpu_to_le32(upper_32_bits(dma.phys));

	reg_vfi->e_d_tov = cpu_to_le32(sli4->e_d_tov);
	reg_vfi->r_a_tov = cpu_to_le32(sli4->r_a_tov);

	reg_vfi->dw0w1_flags |= SLI4_REGVFI_VP;
	reg_vfi->vpi = cpu_to_le16(vpi);
	memcpy(reg_vfi->wwpn, &sli_wwpn, sizeof(reg_vfi->wwpn));
	reg_vfi->dw10_lportid_flags = cpu_to_le32(fc_id);

	return sizeof(struct sli4_cmd_reg_vfi_s);
}

/**
 * @ingroup sli
 * @brief Write a REG_VPI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param sport Point to SLI Port object.
 * @param update Boolean indicating whether to update the existing VPI (true)
 * or create a new VPI (false).
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_reg_vpi(struct sli4_s *sli4, void *buf, size_t size,
		u32 fc_id, __be64 sli_wwpn, u16 vpi, u16 vfi,
		bool update)
{
	struct sli4_cmd_reg_vpi_s	*reg_vpi = buf;
	u32 flags = 0;

	if (!sli4 || !buf)
		return 0;

	memset(buf, 0, size);

	reg_vpi->hdr.command = SLI4_MBOX_COMMAND_REG_VPI;

	flags = (fc_id & SLI4_REGVPI_LOCAL_N_PORTID);
	if (update)
		flags |= SLI4_REGVPI_UPD;
	else
		flags &= ~SLI4_REGVPI_UPD;

	reg_vpi->dw2_lportid_flags = cpu_to_le32(flags);
	memcpy(reg_vpi->wwpn, &sli_wwpn, sizeof(reg_vpi->wwpn));
	reg_vpi->vpi = cpu_to_le16(vpi);
	reg_vpi->vfi = cpu_to_le16(vfi);

	return sizeof(struct sli4_cmd_reg_vpi_s);
}

/**
 * @brief Write a REQUEST_FEATURES command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param mask Features to request.
 * @param query Use feature query mode (does not change FW).
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_request_features(struct sli4_s *sli4, void *buf, size_t size,
			 u32 features_mask, bool query)
{
	struct sli4_cmd_request_features_s *req_features = buf;

	memset(buf, 0, size);

	req_features->hdr.command = SLI4_MBOX_COMMAND_REQUEST_FEATURES;

	if (query)
		req_features->dw1_qry = cpu_to_le32(SLI4_REQFEAT_QRY);

	req_features->cmd = cpu_to_le32(features_mask);

	return sizeof(struct sli4_cmd_request_features_s);
}

/**
 * @ingroup sli
 * @brief Write a SLI_CONFIG command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param length Length in bytes of attached command.
 * @param dma DMA buffer for non-embedded commands.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_sli_config(struct sli4_s *sli4, void *buf,
		   size_t size, u32 length,
		   struct efc_dma_s *dma)
{
	struct sli4_cmd_sli_config_s	*sli_config = NULL;
	u32 flags = 0;

	if (length > sizeof(sli_config->payload.embed) && !dma) {
		pr_info("length(%d) > payload(%ld)\n",
			length, sizeof(sli_config->payload.embed));
		return -1;
	}

	sli_config = buf;

	memset(buf, 0, size);

	sli_config->hdr.command = SLI4_MBOX_COMMAND_SLI_CONFIG;
	if (!dma) {
		flags |= SLI4_SLICONFIG_EMB;
		sli_config->dw1_flags = cpu_to_le32(flags);
		sli_config->payload_len = cpu_to_le32(length);
	} else {
		flags = (1 << 3);	/* pmd_count = 1 */
		flags &= ~SLI4_SLICONFIG_EMB;
		sli_config->dw1_flags = cpu_to_le32(flags);

		sli_config->payload.mem.addr_low =
			cpu_to_le32(lower_32_bits(dma->phys));
		sli_config->payload.mem.addr_high =
			cpu_to_le32(upper_32_bits(dma->phys));
		sli_config->payload.mem.length_dword =
			cpu_to_le32(dma->size & SLI4_SLICONFIG_PMD_LEN);
		sli_config->payload_len = cpu_to_le32(dma->size);
		/* save pointer to DMA for BMBX dumping purposes */
		sli4->bmbx_non_emb_pmd = dma;
	}

	return offsetof(struct sli4_cmd_sli_config_s, payload.embed);
}

/**
 * @brief Initialize SLI Port control register.
 *
 * @param sli4 SLI context pointer.
 * @param endian Endian value to write.
 *
 * @return Returns 0 on success, or a negative error code value on failure.
 */

static int
sli_sliport_reset(struct sli4_s *sli4)
{
	u32 iter, val;
	int rc = -1;

	val = SLI4_SLIPORT_CONTROL_IP;
	/* Initialize port, endian */
	writel(val, (sli4->reg[0] + SLI4_SLIPORT_CONTROL_REGOFFSET));

	for (iter = 0; iter < 3000; iter++) {
		mdelay(10);	/* 10 ms */
		if (sli_fw_ready(sli4) == 1) {
			rc = 0;
			break;
		}
	}

	if (rc != 0)
		pr_crit("port failed to become ready after initialization\n");

	return rc;
}

/**
 * @ingroup sli
 * @brief Write a UNREG_FCFI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param indicator Indicator value.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_unreg_fcfi(struct sli4_s *sli4, void *buf, size_t size,
		   u16 indicator)
{
	struct sli4_cmd_unreg_fcfi_s	*unreg_fcfi = buf;

	if (!sli4 || !buf)
		return 0;

	memset(buf, 0, size);

	unreg_fcfi->hdr.command = SLI4_MBOX_COMMAND_UNREG_FCFI;

	unreg_fcfi->fcfi = cpu_to_le16(indicator);

	return sizeof(struct sli4_cmd_unreg_fcfi_s);
}

/**
 * @ingroup sli
 * @brief Write an UNREG_RPI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param indicator Indicator value.
 * @param which Type of unregister, such as node, port, domain, or FCF.
 * @param fc_id FC address.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_unreg_rpi(struct sli4_s *sli4, void *buf, size_t size,
		  u16 indicator,
		  enum sli4_resource_e which, u32 fc_id)
{
	struct sli4_cmd_unreg_rpi_s	*unreg_rpi = buf;
	u32 flags = 0;

	if (!sli4 || !buf)
		return 0;

	memset(buf, 0, size);

	unreg_rpi->hdr.command = SLI4_MBOX_COMMAND_UNREG_RPI;

	switch (which) {
	case SLI_RSRC_RPI:
		flags |= (SLI4_UNREG_RPI_II_RPI << 14);
		if (fc_id != U32_MAX) {
			flags |= SLI4_UNREG_RPI_DP;
			unreg_rpi->dw2_dest_n_portid =
				cpu_to_le32(fc_id &
					    SLI4_UNREG_RPI_DEST_N_PORTID);
		}
		break;
	case SLI_RSRC_VPI:
		flags |= (SLI4_UNREG_RPI_II_VPI << 14);
		break;
	case SLI_RSRC_VFI:
		flags |= (SLI4_UNREG_RPI_II_VFI << 14);
		break;
	case SLI_RSRC_FCFI:
		flags |= (SLI4_UNREG_RPI_II_FCFI << 14);
		break;
	default:
		pr_info("unknown type %#x\n", which);
		return 0;
	}

	unreg_rpi->dw1w0_flags = cpu_to_le16(flags);
	unreg_rpi->index = cpu_to_le16(indicator);

	return sizeof(struct sli4_cmd_unreg_rpi_s);
}

/**
 * @ingroup sli
 * @brief Write an UNREG_VFI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param domain Pointer to the domain object
 * @param which Type of unregister, such as domain, FCFI, or everything.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_unreg_vfi(struct sli4_s *sli4, void *buf, size_t size,
		  u16 index, u32 which)
{
	struct sli4_cmd_unreg_vfi_s	*unreg_vfi = buf;

	if (!sli4 || !buf)
		return 0;

	memset(buf, 0, size);

	unreg_vfi->hdr.command = SLI4_MBOX_COMMAND_UNREG_VFI;
	switch (which) {
	case SLI4_UNREG_TYPE_DOMAIN:
		unreg_vfi->index = cpu_to_le16(index);
		break;
	case SLI4_UNREG_TYPE_FCF:
		unreg_vfi->index = cpu_to_le16(index);
		break;
	case SLI4_UNREG_TYPE_ALL:
		unreg_vfi->index = cpu_to_le16(U32_MAX);
		break;
	default:
		return 0;
	}

	if (which != SLI4_UNREG_TYPE_DOMAIN)
		unreg_vfi->dw2_flags =
			cpu_to_le16(SLI4_UNREG_VFI_II_FCFI << 14);

	return sizeof(struct sli4_cmd_unreg_vfi_s);
}

/**
 * @ingroup sli
 * @brief Write an UNREG_VPI command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param indicator Indicator value.
 * @param which Type of unregister: port, domain, FCFI, everything
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_unreg_vpi(struct sli4_s *sli4, void *buf, size_t size,
		  u16 indicator, u32 which)
{
	struct sli4_cmd_unreg_vpi_s	*unreg_vpi = buf;
	u32 flags = 0;

	if (!sli4 || !buf)
		return 0;

	memset(buf, 0, size);

	unreg_vpi->hdr.command = SLI4_MBOX_COMMAND_UNREG_VPI;
	unreg_vpi->index = cpu_to_le16(indicator);
	switch (which) {
	case SLI4_UNREG_TYPE_PORT:
		flags |= (SLI4_UNREG_VPI_II_VPI << 14);
		break;
	case SLI4_UNREG_TYPE_DOMAIN:
		flags |= (SLI4_UNREG_VPI_II_VFI << 14);
		break;
	case SLI4_UNREG_TYPE_FCF:
		flags |= (SLI4_UNREG_VPI_II_FCFI << 14);
		break;
	case SLI4_UNREG_TYPE_ALL:
		/* override indicator */
		unreg_vpi->index = cpu_to_le16(U32_MAX);
		flags |= (SLI4_UNREG_VPI_II_FCFI << 14);
		break;
	default:
		return 0;
	}

	unreg_vpi->dw2w0_flags = cpu_to_le16(flags);
	return sizeof(struct sli4_cmd_unreg_vpi_s);
}

/**
 * @ingroup sli
 * @brief Write an CONFIG_AUTO_XFER_RDY command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to the destination buffer.
 * @param size Buffer size, in bytes.
 * @param max_burst_len if the write FCP_DL is less than this size,
 * then the SLI port will generate the auto XFER_RDY.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_config_auto_xfer_rdy(struct sli4_s *sli4, void *buf, size_t size,
			     u32 max_burst_len)
{
	struct sli4_cmd_config_auto_xfer_rdy_s	*req = buf;

	if (!sli4 || !buf)
		return 0;

	memset(buf, 0, size);

	req->hdr.command = SLI4_MBOX_COMMAND_CONFIG_AUTO_XFER_RDY;
	req->max_burst_len = cpu_to_le32(max_burst_len);

	return sizeof(struct sli4_cmd_config_auto_xfer_rdy_s);
}

/**
 * @brief Write a COMMON_CREATE_CQ command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param qmem DMA memory for the queue.
 * @param eq_id Associated EQ_ID
 * @param ignored This parameter carries the ULP
 * which is only used for WQ and RQs
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_create_cq(struct sli4_s *sli4, void *buf, size_t size,
			 struct efc_dma_s *qmem,
			 u16 eq_id, u16 ignored)
{
	struct sli4_req_common_create_cq_v2_s	*cqv2 = NULL;
	u32	sli_config_off = 0;
	u32	p;
	uintptr_t	addr;
	u32	page_bytes = 0;
	u32	num_pages = 0;
	size_t		cmd_size = 0;
	u32	page_size = 0;
	u32	n_cqe = 0;
	u32 dw5_flags = 0;
	u16 dw6w1_arm = 0;
	u32 payload_size;

	/* First calculate number of pages and the mailbox cmd length */
	n_cqe = qmem->size / SLI4_CQE_BYTES;
	switch (n_cqe) {
	case 256:
	case 512:
	case 1024:
	case 2048:
		page_size = 1;
		break;
	case 4096:
		page_size = 2;
		break;
	default:
		return 0;
	}
	page_bytes = page_size * SLI_PAGE_SIZE;
	num_pages = sli_page_count(qmem->size, page_bytes);
	cmd_size = sizeof(struct sli4_req_common_create_cq_v2_s) +
			(8 * num_pages);

	/*
	 * now that we have the mailbox command size,
	 * we can set SLI_CONFIG fields
	 */

	/* Payload length must accommodate both request and response */
	payload_size =
		max(cmd_size,
		    sizeof(struct sli4_res_common_create_queue_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);

	cqv2 = (struct sli4_req_common_create_cq_v2_s *)
		((u8 *)buf + sli_config_off);
	cqv2->hdr.opcode = SLI4_OPC_COMMON_CREATE_CQ;
	cqv2->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	cqv2->hdr.dw3_version = cpu_to_le32(0x2);
	cqv2->hdr.request_length =
	cpu_to_le32(cmd_size - sizeof(struct sli4_req_hdr_s));
	cqv2->page_size = page_size;

	/* valid values for number of pages: 1, 2, 4, 8 (sec 4.4.3) */
	cqv2->num_pages = cpu_to_le16(num_pages);
	if (!num_pages ||
	    num_pages > SLI4_COMMON_CREATE_CQ_V2_MAX_PAGES) {
		return 0;
	}

	switch (num_pages) {
	case 1:
		dw5_flags |= (SLI4_CQ_CNT_256 << 27);
		break;
	case 2:
		dw5_flags |= (SLI4_CQ_CNT_512 << 27);
		break;
	case 4:
		dw5_flags |= (SLI4_CQ_CNT_1024 << 27);
		break;
	case 8:
		dw5_flags |= (SLI4_CQ_CNT_LARGE << 27);
		cqv2->cqe_count = cpu_to_le16(n_cqe);
		break;
	default:
		pr_info("num_pages %d not valid\n", num_pages);
		return -1;
	}

	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		dw5_flags |= SLI4_REQCREATE_CQV2_AUTOVALID;

	dw5_flags |= SLI4_REQCREATE_CQV2_EVT;
	dw5_flags |= SLI4_REQCREATE_CQV2_VALID;
	dw6w1_arm &= (~SLI4_REQCREATE_CQV2_ARM);

	cqv2->dw5_flags = cpu_to_le32(dw5_flags);
	cqv2->dw6w1_arm = cpu_to_le16(dw6w1_arm);
	cqv2->eq_id = cpu_to_le16(eq_id);

	for (p = 0, addr = qmem->phys; p < num_pages;
	     p++, addr += page_bytes) {
		cqv2->page_physical_address[p].low =
			cpu_to_le32(lower_32_bits(addr));
		cqv2->page_physical_address[p].high =
			cpu_to_le32(upper_32_bits(addr));
	}

	return sli_config_off + cmd_size;
}

/**
 * @brief Write a COMMON_DESTROY_CQ command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param cq_id CQ ID
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_destroy_cq(struct sli4_s *sli4, void *buf,
			  size_t size, u16 cq_id)
{
	struct sli4_req_common_destroy_cq_s	*cq = NULL;
	u32	sli_config_off = 0;
	u32	val;

	/* Payload length must accommodate both request and response */
	val = max(sizeof(struct sli4_req_common_destroy_cq_s),
		  sizeof(struct sli4_res_hdr_s));
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);

	cq = (struct sli4_req_common_destroy_cq_s *)
				((u8 *)buf + sli_config_off);

	cq->hdr.opcode = SLI4_OPC_COMMON_DESTROY_CQ;
	cq->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	val = sizeof(struct sli4_req_common_destroy_cq_s) -
			sizeof(struct sli4_req_hdr_s);
	cq->hdr.request_length = cpu_to_le32(val);
	cq->cq_id = cpu_to_le16(cq_id);

	return sli_config_off + sizeof(struct sli4_req_common_destroy_cq_s);
}

/**
 * @brief Write a COMMON_MODIFY_EQ_DELAY command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param q Queue object array.
 * @param num_q Queue object array count.
 * @param shift Phase shift for staggering interrupts.
 * @param delay_mult Delay multiplier for limiting interrupt frequency.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_modify_eq_delay(struct sli4_s *sli4, void *buf, size_t size,
			       struct sli4_queue_s *q, int num_q, u32 shift,
			       u32 delay_mult)
{
	struct sli4_req_common_modify_eq_delay_s *modify_delay = NULL;
	u32	sli_config_off = 0;
	int i;
	u32 val;

	/* Payload length must accommodate both request and response */
	val = max(sizeof(struct sli4_req_common_modify_eq_delay_s),
		  sizeof(struct sli4_res_hdr_s)),
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);

	modify_delay = (struct sli4_req_common_modify_eq_delay_s *)
		((u8 *)buf + sli_config_off);

	modify_delay->hdr.opcode = SLI4_OPC_COMMON_MODIFY_EQ_DELAY;
	modify_delay->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	val = sizeof(struct sli4_req_common_modify_eq_delay_s) -
		sizeof(struct sli4_req_hdr_s);
	modify_delay->hdr.request_length = cpu_to_le32(val);
	modify_delay->num_eq = cpu_to_le32(num_q);

	for (i = 0; i < num_q; i++) {
		modify_delay->eq_delay_record[i].eq_id = cpu_to_le32(q[i].id);
		modify_delay->eq_delay_record[i].phase = cpu_to_le32(shift);
		modify_delay->eq_delay_record[i].delay_multiplier =
			cpu_to_le32(delay_mult);
	}

	return sli_config_off +
			sizeof(struct sli4_req_common_modify_eq_delay_s);
}

/**
 * @brief Write a COMMON_CREATE_EQ command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param qmem DMA memory for the queue.
 * @param ignored1 Ignored
 * (used for consistency among queue creation functions).
 * @param ignored2 Ignored
 * (used for consistency among queue creation functions).
 *
 * @note Other queue creation routines use the last parameter to pass in
 * the associated Q_ID and ULP. EQ doesn't have an associated queue or ULP,
 * so these parameters are ignored
 *
 * @note This creates a Version 0 message
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_create_eq(struct sli4_s *sli4, void *buf, size_t size,
			 struct efc_dma_s *qmem,
			 u16 ignored1, u16 ignored2)
{
	struct sli4_req_common_create_eq_s	*eq = NULL;
	u32	sli_config_off = 0;
	u32	p;
	uintptr_t	addr;
	u32 val;
	u16 num_pages;
	u32 dw5_flags = 0;
	u32 dw6_flags = 0;

	/* Payload length must accommodate both request and response */
	val = max(sizeof(struct sli4_req_common_create_eq_s),
		  sizeof(struct sli4_res_common_create_queue_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    val, NULL);
	eq = (struct sli4_req_common_create_eq_s *)
			((u8 *)buf + sli_config_off);

	eq->hdr.opcode = SLI4_OPC_COMMON_CREATE_EQ;
	eq->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		eq->hdr.dw3_version = cpu_to_le32(2);

	val = sizeof(struct sli4_req_common_create_eq_s) -
				sizeof(struct sli4_req_hdr_s);
	eq->hdr.request_length = cpu_to_le32(val);

	/* valid values for number of pages: 1, 2, 4 (sec 4.4.3) */
	num_pages = qmem->size / SLI_PAGE_SIZE;
	eq->num_pages = cpu_to_le16(num_pages);

	switch (num_pages) {
	case 1:
		dw5_flags |= SLI4_EQE_SIZE_4;
		dw6_flags |= (SLI4_EQ_CNT_1024 << 26);
		break;
	case 2:
		dw5_flags |= SLI4_EQE_SIZE_4;
		dw6_flags |= (SLI4_EQ_CNT_2048 << 26);
		break;
	case 4:
		dw5_flags |= SLI4_EQE_SIZE_4;
		dw6_flags |= (SLI4_EQ_CNT_4096 << 26);
		break;
	default:
		pr_info("num_pages %d not valid\n", num_pages);
		return -1;
	}

	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		dw5_flags |= SLI4_REQCREATE_EQ_AUTOVALID;

	dw5_flags |= SLI4_REQCREATE_EQ_VALID;
	dw6_flags &= (~SLI4_REQCREATE_EQ_ARM);
	eq->dw5_flags = cpu_to_le32(dw5_flags);
	eq->dw6_flags = cpu_to_le32(dw6_flags);
	eq->dw7_delaymulti =
		cpu_to_le32((32 << 13) & SLI4_REQCREATE_EQ_DELAYMULTI);

	for (p = 0, addr = qmem->phys; p < num_pages;
	     p++, addr += SLI_PAGE_SIZE) {
		eq->page_address[p].low = cpu_to_le32(lower_32_bits(addr));
		eq->page_address[p].high = cpu_to_le32(upper_32_bits(addr));
	}

	return sli_config_off + sizeof(struct sli4_req_common_create_eq_s);
}

/**
 * @brief Write a COMMON_DESTROY_EQ command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param eq_id Queue ID to destroy.
 *
 * @note Other queue creation routines use the last parameter to pass in
 * the associated Q_ID. EQ doesn't have an associated queue so this
 * parameter is ignored.
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_destroy_eq(struct sli4_s *sli4, void *buf, size_t size,
			  u16 eq_id)
{
	struct sli4_req_common_destroy_eq_s	*eq = NULL;
	u32	sli_config_off = 0;
	u32 val;

	/* Payload length must accommodate both request and response */
	val = max(sizeof(struct sli4_req_common_destroy_eq_s),
		  sizeof(struct sli4_res_hdr_s)),
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);
	eq = (struct sli4_req_common_destroy_eq_s *)
				((u8 *)buf + sli_config_off);

	eq->hdr.opcode = SLI4_OPC_COMMON_DESTROY_EQ;
	eq->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	val = sizeof(struct sli4_req_common_destroy_eq_s) -
			sizeof(struct sli4_req_hdr_s);
	eq->hdr.request_length = cpu_to_le32(val);

	eq->eq_id = cpu_to_le16(eq_id);

	return sli_config_off + sizeof(struct sli4_req_common_destroy_eq_s);
}

/**
 * @brief Write a LOWLEVEL_SET_WATCHDOG command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param timeout watchdog timer timeout in seconds
 *
 * @return void
 */
void
sli4_cmd_lowlevel_set_watchdog(struct sli4_s *sli4, void *buf,
			       size_t size, u16 timeout)
{
	struct sli4_req_lowlevel_set_watchdog_s *req = NULL;
	u32	sli_config_off = 0;
	u32 val;

	/* Payload length must accommodate both request and response */
	val = max(sizeof(struct sli4_req_lowlevel_set_watchdog_s),
		  sizeof(struct sli4_res_lowlevel_set_watchdog_s)),
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);
	req = (struct sli4_req_lowlevel_set_watchdog_s *)
		((u8 *)buf + sli_config_off);

	req->hdr.opcode = SLI4_OPC_LOWLEVEL_SET_WATCHDOG;
	req->hdr.subsystem = SLI4_SUBSYSTEM_LOWLEVEL;
	val = sizeof(struct sli4_req_lowlevel_set_watchdog_s) -
		sizeof(struct sli4_req_hdr_s);
	req->hdr.request_length = cpu_to_le32(val);
	req->watchdog_timeout = cpu_to_le16(timeout);
}

static int
sli_cmd_common_get_cntl_attributes(struct sli4_s *sli4, void *buf, size_t size,
				   struct efc_dma_s *dma)
{
	struct sli4_req_hdr_s *hdr = NULL;
	u32	sli_config_off = 0;

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    sizeof(struct sli4_req_hdr_s), dma);

	if (!dma)
		return 0;

	memset(dma->virt, 0, dma->size);

	hdr = dma->virt;

	hdr->opcode = SLI4_OPC_COMMON_GET_CNTL_ATTRIBUTES;
	hdr->subsystem = SLI4_SUBSYSTEM_COMMON;
	hdr->request_length = cpu_to_le32(dma->size);

	return sli_config_off + sizeof(struct sli4_req_hdr_s);
}

/**
 * @brief Write a COMMON_GET_CNTL_ADDL_ATTRIBUTES command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param dma DMA structure from which the data will be copied.
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_get_cntl_addl_attributes(struct sli4_s *sli4, void *buf,
					size_t size, struct efc_dma_s *dma)
{
	struct sli4_req_hdr_s *hdr = NULL;
	u32	sli_config_off = 0;

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    sizeof(struct sli4_req_hdr_s), dma);

	if (!dma)
		return 0;

	memset(dma->virt, 0, dma->size);

	hdr = dma->virt;

	hdr->opcode = SLI4_OPC_COMMON_GET_CNTL_ADDL_ATTRIBUTES;
	hdr->subsystem = SLI4_SUBSYSTEM_COMMON;
	hdr->request_length = cpu_to_le32(dma->size);

	return sli_config_off + sizeof(struct sli4_req_hdr_s);
}

/**
 * @brief Write a COMMON_CREATE_MQ_EXT command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param qmem DMA memory for the queue.
 * @param cq_id Associated CQ_ID.
 * @param ignored This parameter carries the ULP
 * which is only used for WQ and RQs
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_create_mq_ext(struct sli4_s *sli4, void *buf, size_t size,
			     struct efc_dma_s *qmem,
			     u16 cq_id, u16 ignored)
{
	struct sli4_req_common_create_mq_ext_s	*mq = NULL;
	u32	sli_config_off = 0;
	u32	p;
	uintptr_t	addr;
	u32 num_pages;
	u16 dw6w1_flags = 0;
	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size =
		max(sizeof(struct sli4_req_common_create_mq_ext_s),
		    sizeof(struct sli4_res_common_create_queue_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);

	mq = (struct sli4_req_common_create_mq_ext_s *)
		((u8 *)buf + sli_config_off);

	mq->hdr.opcode = SLI4_OPC_COMMON_CREATE_MQ_EXT;
	mq->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	mq->hdr.request_length =
	cpu_to_le32(sizeof(struct sli4_req_common_create_mq_ext_s) -
		sizeof(struct sli4_req_hdr_s));
	/* valid values for number of pages: 1, 2, 4, 8 (sec 4.4.12) */
	num_pages = qmem->size / SLI_PAGE_SIZE;
	mq->num_pages = cpu_to_le16(num_pages);
	switch (num_pages) {
	case 1:
		dw6w1_flags |= SLI4_MQE_SIZE_16;
		break;
	case 2:
		dw6w1_flags |= SLI4_MQE_SIZE_32;
		break;
	case 4:
		dw6w1_flags |= SLI4_MQE_SIZE_64;
		break;
	case 8:
		dw6w1_flags |= SLI4_MQE_SIZE_128;
		break;
	default:
		pr_info("num_pages %d not valid\n", num_pages);
		return -1;
	}

	mq->async_event_bitmap = cpu_to_le32(SLI4_ASYNC_EVT_FC_ALL);

	if (sli4->mq_create_version) {
		mq->cq_id_v1 = cpu_to_le16(cq_id);
		mq->hdr.dw3_version = cpu_to_le32(1);
	} else {
		dw6w1_flags |= (cq_id << 6);
	}
	mq->dw7_val = cpu_to_le32(SLI4_REQCREATE_MQEXT_VAL);

	mq->dw6w1_flags = cpu_to_le16(dw6w1_flags);
	for (p = 0, addr = qmem->phys; p < num_pages;
	     p++, addr += SLI_PAGE_SIZE) {
		mq->page_physical_address[p].low =
			cpu_to_le32(lower_32_bits(addr));
		mq->page_physical_address[p].high =
			cpu_to_le32(upper_32_bits(addr));
	}

	return sli_config_off + sizeof(struct sli4_req_common_create_mq_ext_s);
}

/**
 * @brief Write a COMMON_DESTROY_MQ command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param mq_id MQ ID
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_destroy_mq(struct sli4_s *sli4, void *buf, size_t size,
			  u16 mq_id)
{
	struct sli4_req_common_destroy_mq_s	*mq = NULL;
	u32	sli_config_off = 0;
	u32 val;

	/* Payload length must accommodate both request and response */
	val = max(sizeof(struct sli4_req_common_destroy_mq_s),
		  sizeof(struct sli4_res_hdr_s)),
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);

	mq = (struct sli4_req_common_destroy_mq_s *)
			((u8 *)buf + sli_config_off);

	mq->hdr.opcode = SLI4_OPC_COMMON_DESTROY_MQ;
	mq->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	val = sizeof(struct sli4_req_common_destroy_mq_s) -
				sizeof(struct sli4_req_hdr_s);
	mq->hdr.request_length = cpu_to_le32(val);

	mq->mq_id = cpu_to_le16(mq_id);

	return sli_config_off + sizeof(struct sli4_req_common_destroy_mq_s);
}

/**
 * @ingroup sli
 * @brief Write a COMMON_NOP command
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param context NOP context value (passed to response, except on FC/FCoE).
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_nop(struct sli4_s *sli4, void *buf,
		   size_t size, uint64_t context)
{
	struct sli4_req_common_nop_s *nop = NULL;
	u32	sli_config_off = 0;
	u32 val;

	/* Payload length must accommodate both request and response */
	val = max(sizeof(struct sli4_req_common_nop_s),
		  sizeof(struct sli4_res_common_nop_s)),
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);

	nop = (struct sli4_req_common_nop_s *)((u8 *)buf +
					       sli_config_off);

	nop->hdr.opcode = SLI4_OPC_COMMON_NOP;
	nop->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	nop->hdr.request_length = cpu_to_le32(8);

	memcpy(&nop->context, &context, sizeof(context));

	return sli_config_off + sizeof(struct sli4_req_common_nop_s);
}

/**
 * @ingroup sli
 * @brief Write a COMMON_GET_RESOURCE_EXTENT_INFO command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param rtype Resource type (for example, XRI, VFI, VPI, and RPI).
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_get_resource_extent_info(struct sli4_s *sli4, void *buf,
					size_t size, u16 rtype)
{
	struct sli4_req_common_get_resource_extent_info_s *extent = NULL;
	u32	sli_config_off = 0;
	u32 val =
		sizeof(struct sli4_req_common_get_resource_extent_info_s);

	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);

	extent = (struct sli4_req_common_get_resource_extent_info_s *)
		((u8 *)buf + sli_config_off);

	extent->hdr.opcode = SLI4_OPC_COMMON_GET_RESOURCE_EXTENT_INFO;
	extent->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	extent->hdr.request_length = cpu_to_le32(4);

	extent->resource_type = cpu_to_le16(rtype);

	val = sli_config_off +
		sizeof(struct sli4_req_common_get_resource_extent_info_s);
	return (int)val;
}

/**
 * @ingroup sli
 * @brief Write a COMMON_GET_SLI4_PARAMETERS command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_get_sli4_parameters(struct sli4_s *sli4, void *buf,
				   size_t size)
{
	struct sli4_req_hdr_s	*hdr = NULL;
	u32	sli_config_off = 0;
	u32	psize = sizeof(struct sli4_res_common_get_sli4_parameters_s);

	sli_config_off = sli_cmd_sli_config(sli4, buf, size, psize, NULL);

	hdr = (struct sli4_req_hdr_s *)((u8 *)buf + sli_config_off);

	hdr->opcode = SLI4_OPC_COMMON_GET_SLI4_PARAMETERS;
	hdr->subsystem = SLI4_SUBSYSTEM_COMMON;
	hdr->request_length = cpu_to_le32(0x50);

	return sli_config_off + sizeof(struct sli4_req_hdr_s);
}

/**
 * @brief Write a COMMON_QUERY_FW_CONFIG command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to destination buffer.
 * @param size Buffer size in bytes.
 *
 * @return Returns the number of bytes written
 */
static int
sli_cmd_common_query_fw_config(struct sli4_s *sli4, void *buf, size_t size)
{
	struct sli4_req_common_query_fw_config_s   *fw_config;
	u32	sli_config_off = 0;
	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_common_query_fw_config_s),
			   sizeof(struct sli4_res_common_query_fw_config_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);

	fw_config = (struct sli4_req_common_query_fw_config_s *)
		((u8 *)buf + sli_config_off);
	fw_config->hdr.opcode	      = SLI4_OPC_COMMON_QUERY_FW_CONFIG;
	fw_config->hdr.subsystem      = SLI4_SUBSYSTEM_COMMON;
	fw_config->hdr.request_length =
		cpu_to_le32(payload_size - sizeof(struct sli4_req_hdr_s));
	return sli_config_off +
			sizeof(struct sli4_req_common_query_fw_config_s);
}

/**
 * @brief Write a COMMON_GET_PORT_NAME command to the provided buffer.
 *
 * @param sli4 SLI context pointer.
 * @param buf Virtual pointer to destination buffer.
 * @param size Buffer size in bytes.
 *
 * @note Function supports both version 0 and 1 forms of this command via
 * the IF_TYPE.
 *
 * @return Returns the number of bytes written.
 */
static int
sli_cmd_common_get_port_name(struct sli4_s *sli4, void *buf, size_t size)
{
	struct sli4_req_common_get_port_name_s	*port_name;
	u32	sli_config_off = 0;
	u32	payload_size;
	u8		version = 0;
	u8		pt = 0;

	/* Select command version for Lancer */
	version = 1;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_common_get_port_name_s),
			   sizeof(struct sli4_res_common_get_port_name_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);

	pt = 1;

	port_name = (struct sli4_req_common_get_port_name_s *)
		((u8 *)buf + sli_config_off);

	port_name->hdr.opcode		= SLI4_OPC_COMMON_GET_PORT_NAME;
	port_name->hdr.subsystem	= SLI4_SUBSYSTEM_COMMON;
	port_name->hdr.request_length	=
		cpu_to_le32(sizeof(struct sli4_req_hdr_s) +
		(version * sizeof(u32)));
	port_name->hdr.dw3_version	= cpu_to_le32(version);

	/* Set the port type value (ethernet=0, FC=1) for V1 commands */
	port_name->port_type = pt;

	return sli_config_off + port_name->hdr.request_length;
}

/**
 * @ingroup sli
 * @brief Write a COMMON_WRITE_OBJECT command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param noc True if the object should be written but not committed to flash.
 * @param eof True if this is the last write for this object.
 * @param desired_write_length Number of bytes of data to write to the object.
 * @param offset Offset, in bytes, from the start of the object.
 * @param object_name Name of the object to write.
 * @param dma DMA structure from which the data will be copied.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_write_object(struct sli4_s *sli4, void *buf, size_t size,
			    u16 noc,
			    u16 eof, u32 desired_write_length,
			    u32 offset, char *object_name,
			    struct efc_dma_s *dma)
{
	struct sli4_req_common_write_object_s *wr_obj = NULL;
	u32	sli_config_off = 0;
	struct sli4_bde_s *host_buffer;
	u32 val;
	u32 dwflags = 0;

	val = sizeof(struct sli4_req_common_write_object_s) +
		sizeof(struct sli4_bde_s);
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);

	wr_obj = (struct sli4_req_common_write_object_s *)
		((u8 *)buf + sli_config_off);

	wr_obj->hdr.opcode = SLI4_OPC_COMMON_WRITE_OBJECT;
	wr_obj->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	wr_obj->hdr.request_length =
		cpu_to_le32(sizeof(*wr_obj) - 4 * sizeof(u32) +
			sizeof(struct sli4_bde_s));
	wr_obj->hdr.timeout = 0;
	wr_obj->hdr.dw3_version = 0;

	if (noc)
		dwflags |= SLI4_RQ_DES_WRITE_LEN_NOC;
	if (eof)
		dwflags |= SLI4_RQ_DES_WRITE_LEN_EOF;
	dwflags |= (desired_write_length & SLI4_RQ_DES_WRITE_LEN);

	wr_obj->desired_write_len_dword = cpu_to_le32(dwflags);

	wr_obj->write_offset = cpu_to_le32(offset);
	strncpy(wr_obj->object_name, object_name,
		sizeof(wr_obj->object_name));
	wr_obj->host_buffer_descriptor_count = cpu_to_le32(1);

	host_buffer = (struct sli4_bde_s *)wr_obj->host_buffer_descriptor;

	/* Setup to transfer xfer_size bytes to device */
	host_buffer->bde_type_buflen =
		cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			    (desired_write_length & SLI4_BDE_MASK_BUFFER_LEN));
	host_buffer->u.data.buffer_address_low =
		cpu_to_le32(lower_32_bits(dma->phys));
	host_buffer->u.data.buffer_address_high =
		cpu_to_le32(upper_32_bits(dma->phys));

	val = sli_config_off + sizeof(struct sli4_req_common_write_object_s) +
				      sizeof(struct sli4_bde_s);
	return (int)val;
}

/**
 * @ingroup sli
 * @brief Write a COMMON_DELETE_OBJECT command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param object_name Name of the object to write.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_delete_object(struct sli4_s *sli4, void *buf, size_t size,
			     char *object_name)
{
	struct sli4_req_common_delete_object_s *del_obj = NULL;
	u32	sli_config_off = 0;
	u32	psize = sizeof(struct sli4_req_common_delete_object_s);

	sli_config_off = sli_cmd_sli_config(sli4, buf, size, psize, NULL);

	del_obj = (struct sli4_req_common_delete_object_s *)
		((u8 *)buf + sli_config_off);

	del_obj->hdr.opcode = SLI4_OPC_COMMON_DELETE_OBJECT;
	del_obj->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	del_obj->hdr.request_length = cpu_to_le32(sizeof(*del_obj));
	del_obj->hdr.timeout = 0;
	del_obj->hdr.dw3_version = 0;

	strncpy(del_obj->object_name, object_name,
		sizeof(del_obj->object_name));
	return sli_config_off + sizeof(struct sli4_req_common_delete_object_s);
}

/**
 * @ingroup sli
 * @brief Write a COMMON_READ_OBJECT command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param desired_read_length Number of bytes of data to read from the object.
 * @param offset Offset, in bytes, from the start of the object.
 * @param object_name Name of the object to read.
 * @param dma DMA structure from which the data will be copied.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_read_object(struct sli4_s *sli4, void *buf, size_t size,
			   u32 desired_read_length, u32 offset,
			   char *object_name, struct efc_dma_s *dma)
{
	struct sli4_req_common_read_object_s *rd_obj = NULL;
	u32	sli_config_off = 0;
	struct sli4_bde_s *host_buffer;
	u32 val;

	val = sizeof(struct sli4_req_common_read_object_s) +
		sizeof(struct sli4_bde_s);
	sli_config_off = sli_cmd_sli_config(sli4, buf, size, val,
					    NULL);

	rd_obj = (struct sli4_req_common_read_object_s *)
		((u8 *)buf + sli_config_off);

	rd_obj->hdr.opcode = SLI4_OPC_COMMON_READ_OBJECT;
	rd_obj->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	rd_obj->hdr.request_length =
		cpu_to_le32(sizeof(*rd_obj) - 4 * sizeof(u32) +
			sizeof(struct sli4_bde_s));
	rd_obj->hdr.timeout = 0;
	rd_obj->hdr.dw3_version = 0;

	rd_obj->desired_read_length_dword =
		cpu_to_le32(desired_read_length & SLI4_REQ_DESIRE_READLEN);

	rd_obj->read_offset = cpu_to_le32(offset);
	strncpy(rd_obj->object_name, object_name,
		sizeof(rd_obj->object_name));
	rd_obj->host_buffer_descriptor_count = cpu_to_le32(1);

	host_buffer = (struct sli4_bde_s *)rd_obj->host_buffer_descriptor;

	/* Setup to transfer xfer_size bytes to device */
	host_buffer->bde_type_buflen =
		cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			    (desired_read_length & SLI4_BDE_MASK_BUFFER_LEN));
	if (dma) {
		host_buffer->u.data.buffer_address_low =
			cpu_to_le32(lower_32_bits(dma->phys));
		host_buffer->u.data.buffer_address_high =
			cpu_to_le32(upper_32_bits(dma->phys));
	} else {
		host_buffer->u.data.buffer_address_low = 0;
		host_buffer->u.data.buffer_address_high = 0;
	}

	val = sli_config_off + sizeof(struct sli4_req_common_read_object_s) +
		sizeof(struct sli4_bde_s);
	return val;
}

/**
 * @ingroup sli
 * @brief Write a DMTF_EXEC_CLP_CMD command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param cmd DMA structure that describes the buffer for the command.
 * @param resp DMA structure that describes the buffer for the response.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_dmtf_exec_clp_cmd(struct sli4_s *sli4, void *buf, size_t size,
			  struct efc_dma_s *cmd,
			  struct efc_dma_s *resp)
{
	struct sli4_req_dmtf_exec_clp_cmd_s *clp_cmd = NULL;
	u32	sli_config_off = 0;
	u32	psize = sizeof(struct sli4_req_dmtf_exec_clp_cmd_s);

	sli_config_off = sli_cmd_sli_config(sli4, buf, size, psize, NULL);

	clp_cmd = (struct sli4_req_dmtf_exec_clp_cmd_s *)
		((u8 *)buf + sli_config_off);

	clp_cmd->hdr.opcode = SLI4_OPC_DMTF_EXEC_CLP_CMD;
	clp_cmd->hdr.subsystem = SLI4_SUBSYSTEM_DMTF;
	clp_cmd->hdr.request_length =
	cpu_to_le32(sizeof(struct sli4_req_dmtf_exec_clp_cmd_s) -
		sizeof(struct sli4_req_hdr_s));
	clp_cmd->hdr.timeout = 0;
	clp_cmd->hdr.dw3_version = 0;
	clp_cmd->cmd_buf_length = cpu_to_le32(cmd->size);
	clp_cmd->cmd_buf_addr_low =  cpu_to_le32(lower_32_bits(cmd->phys));
	clp_cmd->cmd_buf_addr_high =  cpu_to_le32(upper_32_bits(cmd->phys));
	clp_cmd->resp_buf_length = cpu_to_le32(resp->size);
	clp_cmd->resp_buf_addr_low =  cpu_to_le32(lower_32_bits(resp->phys));
	clp_cmd->resp_buf_addr_high =  cpu_to_le32(upper_32_bits(resp->phys));

	return sli_config_off + sizeof(struct sli4_req_dmtf_exec_clp_cmd_s);
}

/**
 * @ingroup sli
 * @brief Write a COMMON_SET_DUMP_LOCATION command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param query Zero to set dump location, non-zero to query dump size
 * @param is_buffer_list Set to one if the buffer
 * is a set of buffer descriptors or
 * set to 0 if the buffer is a contiguous dump area.
 * @param buffer DMA structure to which the dump will be copied.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_set_dump_location(struct sli4_s *sli4, void *buf,
				 size_t size, bool query,
				 bool is_buffer_list,
				 struct efc_dma_s *buffer, u8 fdb)
{
	struct sli4_req_common_set_dump_location_s *set_dump_loc = NULL;
	u32	sli_config_off = 0;
	u32	buffer_length_flag = 0;
	u32	psize = sizeof(struct sli4_req_common_set_dump_location_s);

	sli_config_off = sli_cmd_sli_config(sli4, buf, size, psize, NULL);

	set_dump_loc = (struct sli4_req_common_set_dump_location_s *)
		((u8 *)buf + sli_config_off);

	set_dump_loc->hdr.opcode = SLI4_OPC_COMMON_SET_DUMP_LOCATION;
	set_dump_loc->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	set_dump_loc->hdr.request_length =
	cpu_to_le32(sizeof(struct sli4_req_common_set_dump_location_s) -
		sizeof(struct sli4_req_hdr_s));
	set_dump_loc->hdr.timeout = 0;
	set_dump_loc->hdr.dw3_version = 0;

	if (is_buffer_list)
		buffer_length_flag |= SLI4_RQ_COM_SET_DUMP_BLP;

	if (query)
		buffer_length_flag |= SLI4_RQ_COM_SET_DUMP_QRY;

	if (fdb)
		buffer_length_flag |= SLI4_RQ_COM_SET_DUMP_FDB;

	if (buffer) {
		set_dump_loc->buf_addr_low =
			cpu_to_le32(lower_32_bits(buffer->phys));
		set_dump_loc->buf_addr_high =
			cpu_to_le32(upper_32_bits(buffer->phys));

		buffer_length_flag |= (buffer->len &
				       SLI4_RQ_COM_SET_DUMP_BUFFER_LEN);
	} else {
		set_dump_loc->buf_addr_low = 0;
		set_dump_loc->buf_addr_high = 0;
		set_dump_loc->buffer_length_dword = 0;
	}
	set_dump_loc->buffer_length_dword = cpu_to_le32(buffer_length_flag);
	return sli_config_off +
			sizeof(struct sli4_req_common_set_dump_location_s);
}

/**
 * @ingroup sli
 * @brief Write a COMMON_SET_FEATURES command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param feature Feature to set.
 * @param param_len Length of the parameter (must be a multiple of 4 bytes).
 * @param parameter Pointer to the parameter value.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_common_set_features(struct sli4_s *sli4, void *buf, size_t size,
			    u32 feature,
			    u32 param_len,
			    void *parameter)
{
	struct sli4_req_common_set_features_s *cmd = NULL;
	u32	sli_config_off = 0;
	u32	psize = sizeof(struct sli4_req_common_set_features_s);

	sli_config_off = sli_cmd_sli_config(sli4, buf, size, psize, NULL);

	cmd = (struct sli4_req_common_set_features_s *)
		((u8 *)buf + sli_config_off);

	cmd->hdr.opcode = SLI4_OPC_COMMON_SET_FEATURES;
	cmd->hdr.subsystem = SLI4_SUBSYSTEM_COMMON;
	cmd->hdr.request_length =
	cpu_to_le32(sizeof(struct sli4_req_common_set_features_s) -
		sizeof(struct sli4_req_hdr_s));
	cmd->hdr.timeout = 0;
	cmd->hdr.dw3_version = 0;

	cmd->feature = cpu_to_le32(feature);
	cmd->param_len = cpu_to_le32(param_len);
	memcpy(cmd->params, parameter, param_len);

	return sli_config_off + sizeof(struct sli4_req_common_set_features_s);
}

/**
 * @ingroup sli
 * @brief Check the mailbox/queue completion entry.
 *
 * @param buf Pointer to the MCQE.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_cqe_mq(void *buf)
{
	struct sli4_mcqe_s	*mcqe = buf;
	u32 dwflags = le32_to_cpu(mcqe->dw3_flags);
	/*
	 * Firmware can split mbx completions into two MCQEs: first with only
	 * the "consumed" bit set and a second with the "complete" bit set.
	 * Thus, ignore MCQE unless "complete" is set.
	 */
	if (!(dwflags & SLI4_MCQE_COMPLETED))
		return -2;

	if (le16_to_cpu(mcqe->completion_status)) {
		pr_info("status(st=%#x ext=%#x con=%d cmp=%d ae=%d val=%d)\n",
			le16_to_cpu(mcqe->completion_status),
			      le16_to_cpu(mcqe->extended_status),
			      (dwflags & SLI4_MCQE_CONSUMED),
			      (dwflags & SLI4_MCQE_COMPLETED),
			      (dwflags & SLI4_MCQE_AE),
			      (dwflags & SLI4_MCQE_VALID));
	}

	return le16_to_cpu(mcqe->completion_status);
}

/**
 * @ingroup sli
 * @brief Check the asynchronous event completion entry.
 *
 * @param sli4 SLI context.
 * @param buf Pointer to the ACQE.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_cqe_async(struct sli4_s *sli4, void *buf)
{
	struct sli4_acqe_s	*acqe = buf;
	int		rc = -1;

	if (!sli4 || !buf) {
		pr_err("bad parameter sli4=%p buf=%p\n", sli4, buf);
		return -1;
	}

	switch (acqe->event_code) {
	case SLI4_ACQE_EVENT_CODE_LINK_STATE:
		rc = sli_fc_process_link_state(sli4, buf);
		break;
	case SLI4_ACQE_EVENT_CODE_GRP_5:
		pr_info("ACQE GRP5\n");
		break;
	case SLI4_ACQE_EVENT_CODE_SLI_PORT_EVENT:
		pr_info("ACQE SLI Port, type=0x%x, data1,2=0x%08x,0x%08x\n",
			acqe->event_type,
			le32_to_cpu(acqe->event_data[0]),
			le32_to_cpu(acqe->event_data[1]));
		break;
	case SLI4_ACQE_EVENT_CODE_FC_LINK_EVENT:
		rc = sli_fc_process_link_attention(sli4, buf);
		break;
	default:
		pr_info("ACQE unknown=%#x\n",
			acqe->event_code);
	}

	return rc;
}

/**
 * @brief Check the SLI_CONFIG response.
 *
 * @par Description
 * Function checks the SLI_CONFIG response and the payload status.
 *
 * @param buf Pointer to SLI_CONFIG response.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
sli_res_sli_config(void *buf)
{
	struct sli4_cmd_sli_config_s	*sli_config = buf;

	/* sanity check */
	if (!buf || sli_config->hdr.command !=
		    SLI4_MBOX_COMMAND_SLI_CONFIG) {
		pr_err("bad parameter buf=%p cmd=%#x\n", buf,
		       buf ? sli_config->hdr.command : -1);
		return -1;
	}

	if (le16_to_cpu(sli_config->hdr.status))
		return le16_to_cpu(sli_config->hdr.status);

	if (le32_to_cpu(sli_config->dw1_flags) & SLI4_SLICONFIG_EMB)
		return sli_config->payload.embed[4];

	pr_info("external buffers not supported\n");
	return -1;
}

/**
 * @brief check to see if the FW is ready.
 *
 * @par Description
 * Based on <i>SLI-4 Architecture Specification, Revision 4.x0-13 (2012).</i>.
 *
 * @param sli4 SLI context.
 * @param timeout_ms Time, in milliseconds, to wait for the port to be ready
 * before failing.
 *
 * @return Returns TRUE for ready, or FALSE otherwise.
 */
static bool
sli_wait_for_fw_ready(struct sli4_s *sli4, u32 timeout_ms)
{
	u32	iter = timeout_ms / (SLI4_INIT_PORT_DELAY_US / 1000);
	bool ready = false;

	do {
		iter--;
		mdelay(10);	/* 10 ms */
		if (sli_fw_ready(sli4) == 1)
			ready = true;
	} while (!ready && (iter > 0));

	return ready;
}

/**
 * @brief Initialize the firmware.
 *
 * @par Description
 * Based on <i>SLI-4 Architecture Specification, Revision 4.x0-13 (2012).</i>.
 *
 * @param sli4 SLI context.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
sli_fw_init(struct sli4_s *sli4)
{
	bool ready;

	/*
	 * Is firmware ready for operation?
	 */
	ready = sli_wait_for_fw_ready(sli4, SLI4_FW_READY_TIMEOUT_MSEC);
	if (!ready) {
		pr_crit("FW status is NOT ready\n");
		return -1;
	}

	/*
	 * Reset port to a known state
	 */
	if (sli_sliport_reset(sli4))
		return -1;

	return 0;
}

/**
 * @brief Terminate the firmware.
 *
 * @param sli4 SLI context.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
sli_fw_term(struct sli4_s *sli4)
{
	/* type 2 etc. use SLIPORT_CONTROL to initialize port */
	sli_sliport_reset(sli4);
	return 0;
}

static int
sli_request_features(struct sli4_s *sli4, u32 *features, bool query)
{
	if (sli_cmd_request_features(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE,
				     *features, query)) {
		struct sli4_cmd_request_features_s *req_features =
							sli4->bmbx.virt;

		if (sli_bmbx_command(sli4)) {
			pr_crit("%s: bootstrap mailbox write fail\n",
				__func__);
			return -1;
		}
		if (le16_to_cpu(req_features->hdr.status)) {
			pr_err("REQUEST_FEATURES bad status %#x\n",
			       le16_to_cpu(req_features->hdr.status));
			return -1;
		}
		*features = le32_to_cpu(req_features->resp);
	} else {
		pr_err("bad REQUEST_FEATURES write\n");
		return -1;
	}

	return 0;
}

/**
 * @brief Calculate max queue entries.
 *
 * @param sli4 SLI context.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
void
sli_calc_max_qentries(struct sli4_s *sli4)
{
	enum sli4_qtype_e q;
	u32 alloc_size, qentries, qentry_size;

	for (q = SLI_QTYPE_EQ; q < SLI_QTYPE_MAX; q++) {
		sli4->max_qentries[q] =
			sli_convert_mask_to_count(sli4->count_method[q],
						  sli4->count_mask[q]);
	}

	/* single, continguous DMA allocations will be called for each queue
	 * of size (max_qentries * queue entry size); since these can be large,
	 * check against the OS max DMA allocation size
	 */
	for (q = SLI_QTYPE_EQ; q < SLI_QTYPE_MAX; q++) {
		qentries = sli4->max_qentries[q];
		qentry_size = sli_get_queue_entry_size(sli4, q);
		alloc_size = qentries * qentry_size;
		while (alloc_size > (~((u32)0))) {
			/*
			 * cut the qentries in hwf
			 * until alloc_size <= max DMA alloc size
			 */
			qentries >>= 1;
			alloc_size = qentries * qentry_size;
		}
		pr_info("[%s]: max_qentries from %d to %d\n",
			SLI_QNAME[q],
			      sli4->max_qentries[q],
			      qentries);
		sli4->max_qentries[q] = qentries;
	}
}

/**
 * @brief Issue a FW_CONFIG mailbox command and store the results.
 *
 * @param sli4 SLI context.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
static int
sli_query_fw_config(struct sli4_s *sli4)
{
	/*
	 * Read the device configuration
	 *
	 * Note: Only ulp0 fields contain values
	 */
	if (sli_cmd_common_query_fw_config(sli4, sli4->bmbx.virt,
					   SLI4_BMBX_SIZE)) {
		struct sli4_res_common_query_fw_config_s   *fw_config =
			(struct sli4_res_common_query_fw_config_s *)
			(((u8 *)sli4->bmbx.virt) +
			offsetof(struct sli4_cmd_sli_config_s, payload.embed));

		if (sli_bmbx_command(sli4)) {
			pr_info("bootstrap mailbox fail (QUERY_FW_CONFIG)\n");
			return -1;
		}
		if (fw_config->hdr.status) {
			pr_err("COMMON_QUERY_FW_CONFIG bad status %#x\n",
			       fw_config->hdr.status);
			return -1;
		}

		sli4->physical_port = le32_to_cpu(fw_config->physical_port);
		if ((le32_to_cpu(fw_config->function_mode) &
		    SLI4_FUNCTION_MODE_DUA_MODE) == 0)
			sli4->dual_ulp_capable = 0;
		else
			sli4->dual_ulp_capable = 1;

		if ((le32_to_cpu(fw_config->ulp0_mode) &
		     (SLI4_ULP_MODE_INI | SLI4_ULP_MODE_TGT)) == 0)
			sli4->is_ulp_fc[0] = 0;
		else
			sli4->is_ulp_fc[0] = 1;

		if ((le32_to_cpu(fw_config->ulp1_mode) &
		     (SLI4_ULP_MODE_INI | SLI4_ULP_MODE_TGT)) == 0)
			sli4->is_ulp_fc[1] = 0;
		else
			sli4->is_ulp_fc[1] = 1;

		if (sli4->dual_ulp_capable) {
			/*
			 * Lancer will not support this, so we use the values
			 * from the READ_CONFIG.
			 */
			if (sli4->is_ulp_fc[0] &&
			    sli4->is_ulp_fc[1]) {
				sli4->max_qcount[SLI_QTYPE_WQ] =
				    le32_to_cpu(fw_config->ulp0_toe_wq_total) +
				    le32_to_cpu(fw_config->ulp1_toe_wq_total);
				sli4->max_qcount[SLI_QTYPE_RQ] =
				le32_to_cpu(fw_config->ulp0_toe_defrq_total) +
				le32_to_cpu(fw_config->ulp1_toe_defrq_total);
			} else if (sli4->is_ulp_fc[0]) {
				sli4->max_qcount[SLI_QTYPE_WQ] =
				le32_to_cpu(fw_config->ulp0_toe_wq_total);
				sli4->max_qcount[SLI_QTYPE_RQ] =
				le32_to_cpu(fw_config->ulp0_toe_defrq_total);
			} else {
				sli4->max_qcount[SLI_QTYPE_WQ] =
				le32_to_cpu(fw_config->ulp1_toe_wq_total);
				sli4->max_qcount[SLI_QTYPE_RQ] =
				le32_to_cpu(fw_config->ulp1_toe_defrq_total);
			}
		}
	} else {
		pr_err("bad QUERY_FW_CONFIG write\n");
		return -1;
	}
	return 0;
}

static int
sli_get_config(struct sli4_s *sli4)
{
	struct efc_dma_s	data;
	u32 psize;

	/*
	 * Read the device configuration
	 */
	if (sli_cmd_read_config(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE)) {
		struct sli4_res_read_config_s	*read_config = sli4->bmbx.virt;
		u32	i;
		u32	total, total_size;

		if (sli_bmbx_command(sli4)) {
			pr_crit("bootstrap mailbox fail (READ_CONFIG)\n");
			return -1;
		}
		if (le16_to_cpu(read_config->hdr.status)) {
			pr_err("READ_CONFIG bad status %#x\n",
			       le16_to_cpu(read_config->hdr.status));
			return -1;
		}

		sli4->has_extents =
			le32_to_cpu(read_config->ext_dword) &
				    SLI4_READ_CFG_RESP_RESOURCE_EXT;
		if (!sli4->has_extents) {
			u32	i = 0, size = 0;
			u32	*base = sli4->extent[0].base;

			if (!base) {
				size = SLI_RSRC_MAX * sizeof(u32);
				base = kzalloc(size, GFP_ATOMIC);
				if (!base)
					return -1;

				memset(base, 0,
				       SLI_RSRC_MAX * sizeof(u32));
			}

			for (i = 0; i < SLI_RSRC_MAX; i++) {
				sli4->extent[i].number = 1;
				sli4->extent[i].n_alloc = 0;
				sli4->extent[i].base = &base[i];
			}

			sli4->extent[SLI_RSRC_VFI].base[0] =
				le16_to_cpu(read_config->vfi_base);
			sli4->extent[SLI_RSRC_VFI].size =
				le16_to_cpu(read_config->vfi_count);

			sli4->extent[SLI_RSRC_VPI].base[0] =
				le16_to_cpu(read_config->vpi_base);
			sli4->extent[SLI_RSRC_VPI].size =
				le16_to_cpu(read_config->vpi_count);

			sli4->extent[SLI_RSRC_RPI].base[0] =
				le16_to_cpu(read_config->rpi_base);
			sli4->extent[SLI_RSRC_RPI].size =
				le16_to_cpu(read_config->rpi_count);

			sli4->extent[SLI_RSRC_XRI].base[0] =
				le16_to_cpu(read_config->xri_base);
			sli4->extent[SLI_RSRC_XRI].size =
				le16_to_cpu(read_config->xri_count);

			sli4->extent[SLI_RSRC_FCFI].base[0] = 0;
			sli4->extent[SLI_RSRC_FCFI].size =
				le16_to_cpu(read_config->fcfi_count);
		} else {
			;
		}

		for (i = 0; i < SLI_RSRC_MAX; i++) {
			total = sli4->extent[i].number *
				sli4->extent[i].size;
			total_size = BITS_TO_LONGS(total) * sizeof(long);
			sli4->extent[i].use_map =
				kzalloc(total_size, GFP_ATOMIC);
			if (!sli4->extent[i].use_map) {
				pr_err("bitmap memory allocation failed %d\n",
				       i);
				return -1;
			}
			sli4->extent[i].map_size = total;
		}

		sli4->topology =
				(le32_to_cpu(read_config->topology_dword) &
				 SLI4_READ_CFG_RESP_TOPOLOGY) >> 24;
		switch (sli4->topology) {
		case SLI4_READ_CFG_TOPO_FC:
			pr_info("FC (unknown)\n");
			break;
		case SLI4_READ_CFG_TOPO_FC_DA:
			pr_info("FC (direct attach)\n");
			break;
		case SLI4_READ_CFG_TOPO_FC_AL:
			pr_info("FC (arbitrated loop)\n");
			break;
		default:
			pr_info("bad topology %#x\n",
				sli4->topology);
		}

		sli4->e_d_tov = le16_to_cpu(read_config->e_d_tov);
		sli4->r_a_tov = le16_to_cpu(read_config->r_a_tov);

		sli4->link_module_type = le16_to_cpu(read_config->lmt);

		sli4->max_qcount[SLI_QTYPE_EQ] =
				le16_to_cpu(read_config->eq_count);
		sli4->max_qcount[SLI_QTYPE_CQ] =
				le16_to_cpu(read_config->cq_count);
		sli4->max_qcount[SLI_QTYPE_WQ] =
				le16_to_cpu(read_config->wq_count);
		sli4->max_qcount[SLI_QTYPE_RQ] =
				le16_to_cpu(read_config->rq_count);

		/*
		 * READ_CONFIG doesn't give the max number of MQ. Applications
		 * will typically want 1, but we may need another at some future
		 * date. Dummy up a "max" MQ count here.
		 */
		sli4->max_qcount[SLI_QTYPE_MQ] = SLI_USER_MQ_COUNT;
	} else {
		pr_err("bad READ_CONFIG write\n");
		return -1;
	}

	if (sli_cmd_common_get_sli4_parameters(sli4, sli4->bmbx.virt,
					       SLI4_BMBX_SIZE)) {
		struct sli4_res_common_get_sli4_parameters_s	*parms =
			(struct sli4_res_common_get_sli4_parameters_s *)
			(((u8 *)sli4->bmbx.virt) +
			offsetof(struct sli4_cmd_sli_config_s, payload.embed));
		u32 dwflags_loopback;
		u32 dwflags_eq_page_cnt;
		u32 dwflags_cq_page_cnt;
		u32 dwflags_mq_page_cnt;
		u32 dwflags_wq_page_cnt;
		u32 dwflags_rq_page_cnt;
		u32 dwflags_sgl_page_cnt;

		if (sli_bmbx_command(sli4)) {
			pr_crit("%s: bootstrap mailbox write fail\n",
				__func__);
			return -1;
		} else if (parms->hdr.status) {
			pr_err("COMMON_GET_SLI4_PARAMETERS bad status %#x",
			       parms->hdr.status);
			pr_err("additional status %#x\n",
			       parms->hdr.additional_status);
			return -1;
		}

		dwflags_loopback = le32_to_cpu(parms->dw16_loopback_scope);
		dwflags_eq_page_cnt = le32_to_cpu(parms->dw6_eq_page_cnt);
		dwflags_cq_page_cnt = le32_to_cpu(parms->dw8_cq_page_cnt);
		dwflags_mq_page_cnt = le32_to_cpu(parms->dw10_mq_page_cnt);
		dwflags_wq_page_cnt = le32_to_cpu(parms->dw12_wq_page_cnt);
		dwflags_rq_page_cnt = le32_to_cpu(parms->dw14_rq_page_cnt);

		sli4->auto_reg =
			(dwflags_loopback & SLI4_RES_GET_PARAM_AREG);
		sli4->auto_xfer_rdy =
			(dwflags_loopback & SLI4_RES_GET_PARAM_AGXF);
		sli4->hdr_template_req =
			(dwflags_loopback & SLI4_RES_GET_PARAM_HDRR);
		sli4->t10_dif_inline_capable =
			(dwflags_loopback & SLI4_RES_GET_PARAM_TIMM);
		sli4->t10_dif_separate_capable =
			(dwflags_loopback & SLI4_RES_GET_PARAM_TSMM);

		sli4->mq_create_version =
			(dwflags_mq_page_cnt & SLI4_RES_GET_PARAM_MQV) >> 14;
		sli4->cq_create_version =
			(dwflags_cq_page_cnt & SLI4_RES_GET_PARAM_CQV) >> 14;

		sli4->rq_min_buf_size =
			le16_to_cpu(parms->min_rq_buffer_size);
		sli4->rq_max_buf_size =
			le32_to_cpu(parms->max_rq_buffer_size);

		sli4->qpage_count[SLI_QTYPE_EQ] =
			(dwflags_eq_page_cnt & SLI4_RES_GET_PARAM_EQ_PAGE_CNT);
		sli4->qpage_count[SLI_QTYPE_CQ] =
			(dwflags_cq_page_cnt & SLI4_RES_GET_PARAM_CQ_PAGE_CNT);
		sli4->qpage_count[SLI_QTYPE_MQ] =
			(dwflags_mq_page_cnt & SLI4_RES_GET_PARAM_MQ_PAGE_CNT);
		sli4->qpage_count[SLI_QTYPE_WQ] =
			(dwflags_wq_page_cnt & SLI4_RES_GET_PARAM_WQ_PAGE_CNT);
		sli4->qpage_count[SLI_QTYPE_RQ] =
			(dwflags_rq_page_cnt & SLI4_RES_GET_PARAM_RQ_PAGE_CNT);

		/* save count methods and masks for each queue type */

		sli4->count_mask[SLI_QTYPE_EQ] =
				le16_to_cpu(parms->eqe_count_mask);
		sli4->count_method[SLI_QTYPE_EQ] =
		(dwflags_eq_page_cnt & SLI4_RES_GET_PARAM_EQE_CNT_MTHD) >> 24;

		sli4->count_mask[SLI_QTYPE_CQ] =
				le16_to_cpu(parms->cqe_count_mask);
		sli4->count_method[SLI_QTYPE_CQ] =
				(dwflags_cq_page_cnt &
				 SLI4_RES_GET_PARAM_CQE_CNT_MTHD) >> 24;

		sli4->count_mask[SLI_QTYPE_MQ] =
				le16_to_cpu(parms->mqe_count_mask);
		sli4->count_method[SLI_QTYPE_MQ] =
				(dwflags_mq_page_cnt &
				 SLI4_RES_GET_PARAM_MQE_CNT_MTHD) >> 24;

		sli4->count_mask[SLI_QTYPE_WQ] =
				le16_to_cpu(parms->wqe_count_mask);
		sli4->count_method[SLI_QTYPE_WQ] =
				(dwflags_wq_page_cnt &
				 SLI4_RES_GET_PARAM_WQE_CNT_MTHD) >> 24;

		sli4->count_mask[SLI_QTYPE_RQ] =
				le16_to_cpu(parms->rqe_count_mask);
		sli4->count_method[SLI_QTYPE_RQ] =
				(dwflags_rq_page_cnt &
				 SLI4_RES_GET_PARAM_RQE_CNT_MTHD) >> 24;

		/* now calculate max queue entries */
		sli_calc_max_qentries(sli4);

		dwflags_sgl_page_cnt = le32_to_cpu(parms->dw18_sgl_page_cnt);

		/* max # of pages */
		sli4->max_sgl_pages =
				(dwflags_sgl_page_cnt &
				 SLI4_RES_GET_PARAM_SGL_PAGE_CNT);

		/* bit map of available sizes */
		sli4->sgl_page_sizes =
				(dwflags_sgl_page_cnt &
				 SLI4_RES_GET_PARAM_SGL_PAGE_SIZES) >> 8;
		/* ignore HLM here. Use value from REQUEST_FEATURES */
		sli4->sge_supported_length =
				le32_to_cpu(parms->sge_supported_length);
		sli4->sgl_pre_registration_required =
			(dwflags_loopback & SLI4_RES_GET_PARAM_SGLR);
		/* default to using pre-registered SGL's */
		sli4->sgl_pre_registered = true;

		sli4->perf_hint =
			(dwflags_loopback & SLI4_RES_GET_PARAM_PHON);
		sli4->perf_wq_id_association =
			(dwflags_loopback & SLI4_RES_GET_PARAM_PHWQ);

		sli4->rq_batch =
			(le16_to_cpu(parms->dw15w1_rq_db_window) &
			 SLI4_RES_GET_PARAM_RQ_DB_WINDOW) >> 12;

		/* save the fields for SGL chaining */
		sli4->sgl_chaining_params.chaining_capable =
			(((dwflags_loopback &
			 SLI4_RES_GET_PARAM_SGLC) >> 21) == 1);

		sli4->sgl_chaining_params.frag_num_field_offset =
			le16_to_cpu(parms->frag_num_field_offset);
		sli4->sgl_chaining_params.frag_num_field_mask =
			(1ull << le16_to_cpu(parms->frag_num_field_size)) - 1;
		sli4->sgl_chaining_params.sgl_index_field_offset =
			le16_to_cpu(parms->sgl_index_field_offset);
		sli4->sgl_chaining_params.sgl_index_field_mask =
			(1ull << le16_to_cpu(parms->sgl_index_field_size)) - 1;
		sli4->sgl_chaining_params.chain_sge_initial_value_lo =
			le32_to_cpu(parms->chain_sge_initial_value_lo);
		sli4->sgl_chaining_params.chain_sge_initial_value_hi =
			le32_to_cpu(parms->chain_sge_initial_value_hi);

		/* Use the highest available WQE size. */
		if (((dwflags_wq_page_cnt &
		    SLI4_RES_GET_PARAM_WQE_SIZES) >> 8) &
		    SLI4_128BYTE_WQE_SUPPORT)
			sli4->wqe_size = SLI4_WQE_EXT_BYTES;
		else
			sli4->wqe_size = SLI4_WQE_BYTES;
	}

	if (sli_query_fw_config(sli4)) {
		pr_err("Error sending QUERY_FW_CONFIG\n");
		return -1;
	}

	sli4->port_number = 0;

	/*
	 * Issue COMMON_GET_CNTL_ATTRIBUTES to get port_number. Temporarily
	 * uses VPD DMA buffer as the response won't fit in the embedded
	 * buffer.
	 */
	if (sli_cmd_common_get_cntl_attributes(sli4, sli4->bmbx.virt,
					       SLI4_BMBX_SIZE,
					       &sli4->vpd_data)) {
		struct sli4_res_common_get_cntl_attributes_s *attr =
			sli4->vpd_data.virt;

		if (sli_bmbx_command(sli4)) {
			pr_crit("%s: bootstrap mailbox write fail\n",
				__func__);
			return -1;
		} else if (attr->hdr.status) {
			pr_err("COMMON_GET_CNTL_ATTRIBUTES bad status %#x",
			       attr->hdr.status);
			pr_err("additional status %#x\n",
			       attr->hdr.additional_status);
			return -1;
		}

		sli4->port_number = (attr->port_num_type_flags &
					    SLI4_CNTL_ATTR_PORTNUM);

		memcpy(sli4->bios_version_string,
		       attr->bios_version_string,
		       sizeof(sli4->bios_version_string));
	} else {
		pr_err("bad COMMON_GET_CNTL_ATTRIBUTES write\n");
		return -1;
	}

	psize = sizeof(struct sli4_res_common_get_cntl_addl_attributes_s);
	data.size = psize;
	data.virt = dma_alloc_coherent(&sli4->pdev->dev, data.size,
				       &data.phys, GFP_DMA);
	if (!data.virt) {
		memset(&data, 0, sizeof(struct efc_dma_s));
		pr_err("Failed to allocate memory for GET_CNTL_ADDL_ATTR\n");
	} else {
		if (sli_cmd_common_get_cntl_addl_attributes(sli4,
							    sli4->bmbx.virt,
							    SLI4_BMBX_SIZE,
							    &data)) {
			struct sli4_res_common_get_cntl_addl_attributes_s *attr;

			attr = data.virt;
			if (sli_bmbx_command(sli4)) {
				pr_crit("mailbox fail (GET_CNTL_ADDL_ATTR)\n");
				dma_free_coherent(&sli4->pdev->dev, data.size,
						  data.virt, data.phys);
				return -1;
			}
			if (attr->hdr.status) {
				pr_err("GET_CNTL_ADDL_ATTR bad status %#x\n",
				       attr->hdr.status);
				dma_free_coherent(&sli4->pdev->dev, data.size,
						  data.virt, data.phys);
				return -1;
			}

			memcpy(sli4->ipl_name, attr->ipl_file_name,
			       sizeof(sli4->ipl_name));

			pr_info("IPL:%s\n",
				(char *)sli4->ipl_name);
		} else {
			pr_err("bad GET_CNTL_ADDL_ATTR write\n");
			dma_free_coherent(&sli4->pdev->dev, data.size,
					  data.virt, data.phys);
			return -1;
		}

		dma_free_coherent(&sli4->pdev->dev, data.size, data.virt,
				  data.phys);
		memset(&data, 0, sizeof(struct efc_dma_s));
	}

	if (sli_cmd_common_get_port_name(sli4, sli4->bmbx.virt,
					 SLI4_BMBX_SIZE)) {
		struct sli4_res_common_get_port_name_s	*port_name =
			(struct sli4_res_common_get_port_name_s *)
			(((u8 *)sli4->bmbx.virt) +
			offsetof(struct sli4_cmd_sli_config_s, payload.embed));

		if (sli_bmbx_command(sli4)) {
			pr_crit("%s: bootstrap mailbox write fail\n",
				__func__);
			return -1;
		}

		sli4->port_name[0] =
			port_name->port_name[sli4->port_number];
	}
	sli4->port_name[1] = '\0';

	if (sli_cmd_read_rev(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE,
			     &sli4->vpd_data)) {
		struct sli4_cmd_read_rev_s	*read_rev = sli4->bmbx.virt;

		if (sli_bmbx_command(sli4)) {
			pr_crit("bootstrap mailbox write fail (READ_REV)\n");
			return -1;
		}
		if (le16_to_cpu(read_rev->hdr.status)) {
			pr_err("READ_REV bad status %#x\n",
			       le16_to_cpu(read_rev->hdr.status));
			return -1;
		}

		sli4->fw_rev[0] =
				le32_to_cpu(read_rev->first_fw_id);
		memcpy(sli4->fw_name[0], read_rev->first_fw_name,
		       sizeof(sli4->fw_name[0]));

		sli4->fw_rev[1] =
				le32_to_cpu(read_rev->second_fw_id);
		memcpy(sli4->fw_name[1], read_rev->second_fw_name,
		       sizeof(sli4->fw_name[1]));

		sli4->hw_rev[0] = le32_to_cpu(read_rev->first_hw_rev);
		sli4->hw_rev[1] = le32_to_cpu(read_rev->second_hw_rev);
		sli4->hw_rev[2] = le32_to_cpu(read_rev->third_hw_rev);

		pr_info("FW1:%s (%08x) / FW2:%s (%08x)\n",
			read_rev->first_fw_name,
			      le32_to_cpu(read_rev->first_fw_id),
			      read_rev->second_fw_name,
			      le32_to_cpu(read_rev->second_fw_id));

		pr_info("HW1: %08x / HW2: %08x\n",
			le32_to_cpu(read_rev->first_hw_rev),
			      le32_to_cpu(read_rev->second_hw_rev));

		/* Check that all VPD data was returned */
		if (le32_to_cpu(read_rev->returned_vpd_length) !=
		    le32_to_cpu(read_rev->actual_vpd_length)) {
			pr_info("VPD length: avail=%d returned=%d actual=%d\n",
				le32_to_cpu(read_rev->available_length_dword) &
					    SLI4_READ_REV_AVAILABLE_LENGTH,
				le32_to_cpu(read_rev->returned_vpd_length),
				le32_to_cpu(read_rev->actual_vpd_length));
		}
		sli4->vpd_length = le32_to_cpu(read_rev->returned_vpd_length);
	} else {
		pr_err("bad READ_REV write\n");
		return -1;
	}

	if (sli_cmd_read_nvparms(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE)) {
		struct sli4_cmd_read_nvparms_s *read_nvparms = sli4->bmbx.virt;

		if (sli_bmbx_command(sli4)) {
			pr_crit("bootstrap mailbox fail (READ_NVPARMS)\n");
			return -1;
		}
		if (le16_to_cpu(read_nvparms->hdr.status)) {
			pr_err("READ_NVPARMS bad status %#x\n",
			       le16_to_cpu(read_nvparms->hdr.status));
			return -1;
		}

		memcpy(sli4->wwpn, read_nvparms->wwpn,
		       sizeof(sli4->wwpn));
		memcpy(sli4->wwnn, read_nvparms->wwnn,
		       sizeof(sli4->wwnn));

		pr_info("WWPN %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
			sli4->wwpn[0],
			      sli4->wwpn[1],
			      sli4->wwpn[2],
			      sli4->wwpn[3],
			      sli4->wwpn[4],
			      sli4->wwpn[5],
			      sli4->wwpn[6],
			      sli4->wwpn[7]);
		pr_info("WWNN %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
			sli4->wwnn[0],
			      sli4->wwnn[1],
			      sli4->wwnn[2],
			      sli4->wwnn[3],
			      sli4->wwnn[4],
			      sli4->wwnn[5],
			      sli4->wwnn[6],
			      sli4->wwnn[7]);
	} else {
		pr_err("bad READ_NVPARMS write\n");
		return -1;
	}

	return 0;
}

/*
 * Public functions
 */

/**
 * @ingroup sli
 * @brief Set up the SLI context.
 *
 * @param sli4 SLI context.
 * @param os Device abstraction.
 * @param port_type Protocol type of port (for example, FC and NIC).
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_setup(struct sli4_s *sli4, void *os, struct pci_dev  *pdev,
	  void __iomem *reg[])
{
	u32 intf = U32_MAX;
	u32 pci_class_rev = 0;
	u32 rev_id = 0;
	u32 family = 0;
	u32 asic_id = 0;
	u32 i;
	struct sli4_asic_entry_t *asic;

	memset(sli4, 0, sizeof(struct sli4_s));

	sli4->os = os;
	sli4->pdev = pdev;

	for (i = 0; i < 6; i++)
		sli4->reg[i] = reg[i];
	/*
	 * Read the SLI_INTF register to discover the register layout
	 * and other capability information
	 */
	pci_read_config_dword(pdev, SLI4_INTF_REGOFFSET, &intf);

	if ((intf & SLI4_INTF_VALID_MASK) != (u32)SLI4_INTF_VALID_VALUE) {
		pr_err("SLI_INTF is not valid\n");
		return -1;
	}

	/* driver only support SLI-4 */
	if ((intf & SLI4_INTF_SLI_REV_MASK) != SLI4_INTF_SLI_REV_S4) {
		pr_err("Unsupported SLI revision (intf=%#x)\n",
		       intf);
		return -1;
	}

	sli4->sli_family = intf & SLI4_INTF_SLI_FAMILY_MASK;

	sli4->if_type = intf & SLI4_INTF_IF_TYPE_MASK;
	pr_info("status=%#x error1=%#x error2=%#x\n",
		sli_reg_read_status(sli4),
			sli_reg_read_err1(sli4),
			sli_reg_read_err2(sli4));

	/*
	 * set the ASIC type and revision
	 */
	pci_read_config_dword(pdev, PCI_CLASS_REVISION, &pci_class_rev);
	rev_id = pci_class_rev & 0xff;
	family = sli4->sli_family;
	if (family == SLI4_FAMILY_CHECK_ASIC_TYPE) {
		pci_read_config_dword(pdev, SLI4_ASIC_ID_REGOFFSET, &asic_id);

		family = asic_id & SLI4_ASIC_GEN_MASK;
	}

	for (i = 0, asic = sli4_asic_table; i < ARRAY_SIZE(sli4_asic_table);
	     i++, asic++) {
		if (rev_id == asic->rev_id && family == asic->family) {
			sli4->asic_type = asic->type;
			sli4->asic_rev = asic->rev;
			break;
		}
	}
	/* Fail if no matching asic type/rev was found */
	if (!sli4->asic_type || !sli4->asic_rev) {
		pr_err("no matching asic family/rev found: %02x/%02x\n",
		       family, rev_id);
		return -1;
	}

	/*
	 * The bootstrap mailbox is equivalent to a MQ with a single 256 byte
	 * entry, a CQ with a single 16 byte entry, and no event queue.
	 * Alignment must be 16 bytes as the low order address bits in the
	 * address register are also control / status.
	 */
	sli4->bmbx.size = SLI4_BMBX_SIZE + sizeof(struct sli4_mcqe_s);
	sli4->bmbx.virt = dma_alloc_coherent(&pdev->dev, sli4->bmbx.size,
					     &sli4->bmbx.phys, GFP_DMA);
	if (!sli4->bmbx.virt) {
		memset(&sli4->bmbx, 0, sizeof(struct efc_dma_s));
		pr_err("bootstrap mailbox allocation failed\n");
		return -1;
	}

	if (sli4->bmbx.phys & SLI4_BMBX_MASK_LO) {
		pr_err("bad alignment for bootstrap mailbox\n");
		return -1;
	}

	pr_info("bmbx v=%p p=0x%x %08x s=%zd\n", sli4->bmbx.virt,
		upper_32_bits(sli4->bmbx.phys),
		      lower_32_bits(sli4->bmbx.phys), sli4->bmbx.size);

	/* 4096 is arbitrary. What should this value actually be? */
	sli4->vpd_data.size = 4096;
	sli4->vpd_data.virt = dma_alloc_coherent(&pdev->dev,
						 sli4->vpd_data.size,
						 &sli4->vpd_data.phys,
						 GFP_DMA);
	if (!sli4->vpd_data.virt) {
		memset(&sli4->vpd_data, 0, sizeof(struct efc_dma_s));
		/* Note that failure isn't fatal in this specific case */
		pr_info("VPD buffer allocation failed\n");
	}

	if (sli_fw_init(sli4)) {
		pr_err("FW initialization failed\n");
		return -1;
	}

	/*
	 * Set one of fcpi(initiator), fcpt(target), fcpc(combined) to true
	 * in addition to any other desired features
	 */
	sli4->features = (SLI4_REQFEAT_IAAB | SLI4_REQFEAT_NPIV |
				 SLI4_REQFEAT_DIF | SLI4_REQFEAT_VF |
				 SLI4_REQFEAT_FCPC | SLI4_REQFEAT_IAAR |
				 SLI4_REQFEAT_HLM | SLI4_REQFEAT_PERFH |
				 SLI4_REQFEAT_RXSEQ | SLI4_REQFEAT_RXRI |
				 SLI4_REQFEAT_MRQP);

	/* use performance hints if available */
	if (sli4->perf_hint)
		sli4->features |= SLI4_REQFEAT_PERFH;

	if (sli_request_features(sli4, &sli4->features, true))
		return -1;

	if (sli_get_config(sli4))
		return -1;

	return 0;
}

int
sli_init(struct sli4_s *sli4)
{
	if (sli4->has_extents) {
		pr_info("XXX need to implement extent allocation\n");
		return -1;
	}

	if (sli4->high_login_mode)
		sli4->features |= SLI4_REQFEAT_HLM;
	else
		sli4->features &= (~SLI4_REQFEAT_HLM);
	sli4->features &= (~SLI4_REQFEAT_RXSEQ);
	sli4->features &= (~SLI4_REQFEAT_RXRI);

	if (sli_request_features(sli4, &sli4->features, false))
		return -1;

	return 0;
}

int
sli_reset(struct sli4_s *sli4)
{
	u32	i;

	if (sli_fw_init(sli4)) {
		pr_crit("FW initialization failed\n");
		return -1;
	}

	kfree(sli4->extent[0].base);
	sli4->extent[0].base = NULL;

	for (i = 0; i < SLI_RSRC_MAX; i++) {
		kfree(sli4->extent[i].use_map);
		sli4->extent[i].use_map = NULL;
		sli4->extent[i].base = NULL;
	}

	if (sli_get_config(sli4))
		return -1;

	return 0;
}

/**
 * @ingroup sli
 * @brief Issue a Firmware Reset.
 *
 * @par Description
 * Issues a Firmware Reset to the chip.  This reset affects the entire chip,
 * so all PCI function on the same PCI bus and device are affected.
 * @n @n This type of reset can be used to activate newly downloaded firmware.
 * @n @n The driver should be considered to be in an unknown state after this
 * reset and should be reloaded.
 *
 * @param sli4 SLI context.
 *
 * @return Returns 0 on success, or -1 otherwise.
 */

int
sli_fw_reset(struct sli4_s *sli4)
{
	u32 val;
	bool ready;

	/*
	 * Firmware must be ready before issuing the reset.
	 */
	ready = sli_wait_for_fw_ready(sli4, SLI4_FW_READY_TIMEOUT_MSEC);
	if (!ready) {
		pr_crit("FW status is NOT ready\n");
		return -1;
	}
	/* Lancer uses PHYDEV_CONTROL */

	val = SLI4_PHYDEV_CONTROL_FRST;
	writel(val, (sli4->reg[0] + SLI4_PHYDEV_CONTROL_REGOFFSET));

	/* wait for the FW to become ready after the reset */
	ready = sli_wait_for_fw_ready(sli4, SLI4_FW_READY_TIMEOUT_MSEC);
	if (!ready) {
		pr_crit("Failed to become ready after firmware reset\n");
		return -1;
	}
	return 0;
}

/**
 * @ingroup sli
 * @brief Tear down a SLI context.
 *
 * @param sli4 SLI context.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
sli_teardown(struct sli4_s *sli4)
{
	u32 i;

	kfree(sli4->extent[0].base);
	sli4->extent[0].base = NULL;

	for (i = 0; i < SLI_RSRC_MAX; i++) {
		sli4->extent[i].base = NULL;

		kfree(sli4->extent[i].use_map);
		sli4->extent[i].use_map = NULL;
	}

	if (sli_fw_term(sli4))
		pr_err("FW deinitialization failed\n");

	dma_free_coherent(&sli4->pdev->dev, sli4->vpd_data.size,
			  sli4->vpd_data.virt, sli4->vpd_data.phys);
	dma_free_coherent(&sli4->pdev->dev, sli4->bmbx.size,
			  sli4->bmbx.virt, sli4->bmbx.phys);

	return 0;
}

/**
 * @ingroup sli
 * @brief Register a callback for the given event.
 *
 * @param sli4 SLI context.
 * @param which Event of interest.
 * @param func Function to call when the event occurs.
 * @param arg Argument passed to the callback function.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
sli_callback(struct sli4_s *sli4, enum sli4_callback_e which,
	     void *func, void *arg)
{
	if (!sli4 || !func || which >= SLI4_CB_MAX) {
		pr_err("bad parameter sli4=%p which=%#x func=%p\n",
		       sli4, which, func);
		return -1;
	}

	switch (which) {
	case SLI4_CB_LINK:
		sli4->link = func;
		sli4->link_arg = arg;
		break;
	default:
		pr_info("unknown callback %#x\n", which);
		return -1;
	}

	return 0;
}

/**
 * @ingroup sli
 * @brief Initialize a queue object.
 *
 * @par Description
 * This initializes the sli4_queue_s object members, including the underlying
 * DMA memory.
 *
 * @param sli4 SLI context.
 * @param q Pointer to queue object.
 * @param qtype Type of queue to create.
 * @param size Size of each entry.
 * @param n_entries Number of entries to allocate.
 * @param align Starting memory address alignment.
 *
 * @note Checks if using the existing DMA memory (if any) is possible. If not,
 * it frees the existing memory and re-allocates.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
__sli_queue_init(struct sli4_s *sli4, struct sli4_queue_s *q,
		 u32 qtype, size_t size, u32 n_entries,
		      u32 align)
{
	if (!q->dma.virt || size != q->size ||
	    n_entries != q->length) {
		if (q->dma.size)
			dma_free_coherent(&sli4->pdev->dev, q->dma.size,
					  q->dma.virt, q->dma.phys);

		memset(q, 0, sizeof(struct sli4_queue_s));

		q->dma.size = size * n_entries;
		q->dma.virt = dma_alloc_coherent(&sli4->pdev->dev, q->dma.size,
						 &q->dma.phys, GFP_DMA);
		if (!q->dma.virt) {
			memset(&q->dma, 0, sizeof(struct efc_dma_s));
			pr_err("%s allocation failed\n",
			       SLI_QNAME[qtype]);
			return -1;
		}

		memset(q->dma.virt, 0, size * n_entries);

		spin_lock_init(&q->lock);

		q->type = qtype;
		q->size = size;
		q->length = n_entries;

		if (q->type == SLI_QTYPE_EQ || q->type == SLI_QTYPE_CQ) {
			/* For prism, phase will be flipped after
			 * a sweep through eq and cq
			 */
			q->phase = 1;
		}

		/* Limit to hwf the queue size per interrupt */
		q->proc_limit = n_entries / 2;

		switch (q->type) {
		case SLI_QTYPE_EQ:
			q->posted_limit = q->length / 2;
			break;
		default:
			q->posted_limit = 64;
			break;
		}
	} else {
		pr_err("%s failed\n", __func__);
	}

	return 0;
}

/**
 * @ingroup sli
 * @brief Issue the command to create a queue.
 *
 * @param sli4 SLI context.
 * @param q Pointer to queue object.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
__sli_create_queue(struct sli4_s *sli4, struct sli4_queue_s *q)
{
	struct sli4_res_common_create_queue_s *res_q = NULL;

	if (sli_bmbx_command(sli4)) {
		pr_crit("bootstrap mailbox write fail %s\n",
			SLI_QNAME[q->type]);
		dma_free_coherent(&sli4->pdev->dev, q->dma.size,
				  q->dma.virt, q->dma.phys);
		return -1;
	}
	if (sli_res_sli_config(sli4->bmbx.virt)) {
		pr_err("bad status create %s\n",
		       SLI_QNAME[q->type]);
		dma_free_coherent(&sli4->pdev->dev, q->dma.size,
				  q->dma.virt, q->dma.phys);
		return -1;
	}
	res_q = (void *)((u8 *)sli4->bmbx.virt +
			offsetof(struct sli4_cmd_sli_config_s, payload));

	if (res_q->hdr.status) {
		pr_err("bad create %s status=%#x addl=%#x\n",
		       SLI_QNAME[q->type], res_q->hdr.status,
			    res_q->hdr.additional_status);
		dma_free_coherent(&sli4->pdev->dev, q->dma.size,
				  q->dma.virt, q->dma.phys);
		return -1;
	}
	q->id = le16_to_cpu(res_q->q_id);
	switch (q->type) {
	case SLI_QTYPE_EQ:
		/* No doorbell information in response for EQs */
		if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
			q->db_regaddr = sli4->reg[1] +
					SLI4_IF6_EQ_DOORBELL_REGOFFSET;
		else
			q->db_regaddr =	sli4->reg[0] +
					SLI4_EQCQ_DOORBELL_REGOFFSET;
		break;
	case SLI_QTYPE_CQ:
		/* No doorbell information in response for CQs */
		if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
			q->db_regaddr = sli4->reg[1] +
					SLI4_IF6_CQ_DOORBELL_REGOFFSET;
		else
			q->db_regaddr =	sli4->reg[0] +
					SLI4_EQCQ_DOORBELL_REGOFFSET;
		break;
	case SLI_QTYPE_MQ:
		/* No doorbell information in response for MQs */
		if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
			q->db_regaddr = sli4->reg[1] +
					 SLI4_IF6_MQ_DOORBELL_REGOFFSET;
		else
			q->db_regaddr =	sli4->reg[0] +
					SLI4_MQ_DOORBELL_REGOFFSET;
		break;
	case SLI_QTYPE_RQ:
		if (sli4->dual_ulp_capable)
			break;

		if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
			q->db_regaddr = sli4->reg[1] +
					SLI4_IF6_RQ_DOORBELL_REGOFFSET;
		else
			q->db_regaddr =	sli4->reg[0] +
					 SLI4_RQ_DOORBELL_REGOFFSET;
		break;
	case SLI_QTYPE_WQ:
		if (sli4->dual_ulp_capable)
			break;

		if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
			q->db_regaddr = sli4->reg[1] +
					SLI4_IF6_WQ_DOORBELL_REGOFFSET;
		else
			q->db_regaddr =	sli4->reg[0] +
					SLI4_IO_WQ_DOORBELL_REGOFFSET;
		break;
	default:
		break;
	}

	return 0;
}

/**
 * @ingroup sli
 * @brief Get queue entry size.
 *
 * Get queue entry size given queue type.
 *
 * @param sli4 SLI context
 * @param qtype Type for which the entry size is returned.
 *
 * @return Returns > 0 on success (queue entry size),
 * or a negative value on failure.
 */
int
sli_get_queue_entry_size(struct sli4_s *sli4, u32 qtype)
{
	u32	size = 0;

	if (!sli4) {
		pr_err("bad parameter sli4=%p\n", sli4);
		return -1;
	}

	switch (qtype) {
	case SLI_QTYPE_EQ:
		size = sizeof(u32);
		break;
	case SLI_QTYPE_CQ:
		size = 16;
		break;
	case SLI_QTYPE_MQ:
		size = 256;
		break;
	case SLI_QTYPE_WQ:
		size = sli4->wqe_size;
		break;
	case SLI_QTYPE_RQ:
		size = SLI4_RQE_SIZE;
		break;
	default:
		pr_info("unknown queue type %d\n", qtype);
		return -1;
	}
	return size;
}

/**
 * @ingroup sli
 * @brief Modify the delay timer for all the EQs
 *
 * @param sli4 SLI context.
 * @param eq Array of EQs.
 * @param num_eq Count of EQs.
 * @param shift Phase shift for staggering interrupts.
 * @param delay_mult Delay multiplier for limiting interrupt frequency.
 *
 * @return Returns 0 on success, or -1 otherwise.
 */
int
sli_eq_modify_delay(struct sli4_s *sli4, struct sli4_queue_s *eq,
		    u32 num_eq, u32 shift, u32 delay_mult)
{
	sli_cmd_common_modify_eq_delay(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE,
				       eq, num_eq, shift, delay_mult);

	if (sli_bmbx_command(sli4)) {
		pr_crit("bootstrap mailbox write fail (MODIFY EQ DELAY)\n");
		return -1;
	}
	if (sli_res_sli_config(sli4->bmbx.virt)) {
		pr_err("bad status MODIFY EQ DELAY\n");
		return -1;
	}

	return 0;
}

/**
 * @ingroup sli
 * @brief Allocate a queue.
 *
 * @par Description
 * Allocates DMA memory and configures the requested queue type.
 *
 * @param sli4 SLI context.
 * @param qtype Type of queue to create.
 * @param q Pointer to the queue object.
 * @param n_entries Number of entries to allocate.
 * @param assoc Associated queue
 * (that is, the EQ for a CQ, the CQ for a MQ, and so on).
 * @param ulp The ULP to bind, which is only used for WQ and RQs
 *
 * @return Returns 0 on success, or -1 otherwise.
 */
int
sli_queue_alloc(struct sli4_s *sli4, u32 qtype,
		struct sli4_queue_s *q, u32 n_entries,
		     struct sli4_queue_s *assoc, u16 ulp)
{
	int		size;
	u32	align = 0;

	if (!sli4 || !q) {
		pr_err("bad parameter sli4=%p q=%p\n", sli4, q);
		return -1;
	}

	/* get queue size */
	size = sli_get_queue_entry_size(sli4, qtype);
	if (size < 0)
		return -1;
	align = SLI_PAGE_SIZE;

	if (__sli_queue_init(sli4, q, qtype, size, n_entries, align)) {
		pr_err("%s allocation failed\n",
		       SLI_QNAME[qtype]);
		return -1;
	}

	switch (qtype) {
	case SLI_QTYPE_EQ:
		if (sli_cmd_common_create_eq(sli4, sli4->bmbx.virt,
					     SLI4_BMBX_SIZE, &q->dma,
					assoc ? assoc->id : 0, ulp)) {
			if (__sli_create_queue(sli4, q)) {
				pr_err("create %s failed\n",
				       SLI_QNAME[qtype]);
				return -1;
			}
			q->ulp = ulp;
		} else {
			pr_err("cannot create %s\n", SLI_QNAME[qtype]);
			return -1;
		}

		break;
	case SLI_QTYPE_CQ:
		if (sli_cmd_common_create_cq(sli4, sli4->bmbx.virt,
					     SLI4_BMBX_SIZE, &q->dma,
						assoc ? assoc->id : 0, ulp)) {
			if (__sli_create_queue(sli4, q)) {
				pr_err("create %s failed\n",
				       SLI_QNAME[qtype]);
				return -1;
			}
			q->ulp = ulp;
		} else {
			pr_err("cannot create %s\n", SLI_QNAME[qtype]);
			return -1;
		}
		break;
	case SLI_QTYPE_MQ:
		assoc->u.flag.dword |= SLI4_QUEUE_FLAG_MQ;
		if (sli_cmd_common_create_mq_ext(sli4, sli4->bmbx.virt,
						 SLI4_BMBX_SIZE, &q->dma,
						assoc ? assoc->id : 0, ulp)) {
			if (__sli_create_queue(sli4, q)) {
				pr_err("create %s failed\n",
				       SLI_QNAME[qtype]);
				return -1;
			}
			q->ulp = ulp;
		} else {
			pr_err("cannot create %s\n", SLI_QNAME[qtype]);
			return -1;
		}

		break;
	case SLI_QTYPE_WQ:
		if (sli_cmd_wq_create_v1(sli4, sli4->bmbx.virt,
					 SLI4_BMBX_SIZE, &q->dma,
					assoc ? assoc->id : 0, ulp)) {
			if (__sli_create_queue(sli4, q)) {
				pr_err("create %s failed\n",
				       SLI_QNAME[qtype]);
				return -1;
			}
			q->ulp = ulp;
		} else {
			pr_err("cannot create %s\n", SLI_QNAME[qtype]);
			return -1;
		}
		break;
	default:
		pr_info("unknown queue type %d\n", qtype);
		return -1;
	}

	return 0;
}

/**
 * @ingroup sli
 * @brief Allocate a c queue set.
 *
 * @param sli4 SLI context.
 * @param num_cqs to create
 * @param qs Pointers to the queue objects.
 * @param n_entries Number of entries to allocate per CQ.
 * @param eqs Associated event queues
 *
 * @return Returns 0 on success, or -1 otherwise.
 */
int
sli_cq_alloc_set(struct sli4_s *sli4, struct sli4_queue_s *qs[],
		 u32 num_cqs, u32 n_entries, struct sli4_queue_s *eqs[])
{
	u32 i, offset = 0,  page_bytes = 0, payload_size;
	u32 p = 0, page_size = 0, n_cqe = 0, num_pages_cq;
	uintptr_t addr;
	size_t cmd_size = 0;
	struct efc_dma_s dma;
	struct sli4_req_common_create_cq_set_v0_s  *req = NULL;
	struct sli4_res_common_create_queue_set_s *res = NULL;
	u32 dw5_flags = 0;
	u16 dw6w1_flags = 0;
	void __iomem *db_regaddr = NULL;

	if (!sli4) {
		pr_err("bad parameter sli4=%p\n", sli4);
		return -1;
	}

	/* Align the queue DMA memory */
	for (i = 0; i < num_cqs; i++) {
		if (__sli_queue_init(sli4, qs[i], SLI_QTYPE_CQ,
				     SLI4_CQE_BYTES,
					  n_entries, SLI_PAGE_SIZE)) {
			pr_err("Queue init failed.\n");
			goto error;
		}
	}

	n_cqe = qs[0]->dma.size / SLI4_CQE_BYTES;
	switch (n_cqe) {
	case 256:
	case 512:
	case 1024:
	case 2048:
		page_size = 1;
		break;
	case 4096:
		page_size = 2;
		break;
	default:
		return -1;
	}

	page_bytes = page_size * SLI_PAGE_SIZE;
	num_pages_cq = sli_page_count(qs[0]->dma.size, page_bytes);
	cmd_size = sizeof(struct sli4_req_common_create_cq_set_v0_s) +
		(8 * num_pages_cq * num_cqs);
	payload_size = max(cmd_size,
			   sizeof(struct sli4_res_common_create_queue_set_s));

	dma.size = payload_size;
	dma.virt = dma_alloc_coherent(&sli4->pdev->dev, dma.size,
				      &dma.phys, GFP_DMA);
	if (!dma.virt) {
		memset(&dma, 0, sizeof(struct efc_dma_s));
		pr_err("DMA allocation failed\n");
		goto error;
	}
	memset(dma.virt, 0, payload_size);

	if (sli_cmd_sli_config(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE,
			       payload_size, &dma) == -1) {
		goto error;
	}

	/* Fill the request structure */

	req = (struct sli4_req_common_create_cq_set_v0_s *)
				((u8 *)dma.virt);
	req->hdr.opcode = SLI4_OPC_COMMON_CREATE_CQ_SET;
	req->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	req->hdr.dw3_version = 0;
	req->hdr.request_length =
		cpu_to_le32(cmd_size - sizeof(struct sli4_req_hdr_s));
	req->page_size = page_size;

	req->num_pages = cpu_to_le16(num_pages_cq);
	switch (num_pages_cq) {
	case 1:
		dw5_flags |= (SLI4_CQ_CNT_256 << 27);
		break;
	case 2:
		dw5_flags |= (SLI4_CQ_CNT_512 << 27);
		break;
	case 4:
		dw5_flags |= (SLI4_CQ_CNT_1024 << 27);
		break;
	case 8:
		dw5_flags |= (SLI4_CQ_CNT_LARGE << 27);
		dw6w1_flags |= (n_cqe & SLI4_REQCREATE_CQSETV0_CQE_COUNT);
		break;
	default:
		pr_info("num_pages %d not valid\n", num_pages_cq);
		goto error;
	}

	dw5_flags |= SLI4_REQCREATE_CQSETV0_EVT;
	dw5_flags |= SLI4_REQCREATE_CQSETV0_VALID;
	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		dw5_flags |= SLI4_REQCREATE_CQSETV0_AUTOVALID;

	dw6w1_flags &= (~SLI4_REQCREATE_CQSETV0_ARM);

	req->dw5_flags = cpu_to_le32(dw5_flags);
	req->dw6w1_flags = cpu_to_le16(dw6w1_flags);

	req->num_cq_req = cpu_to_le16(num_cqs);

	/* Fill page addresses of all the CQs. */
	for (i = 0; i < num_cqs; i++) {
		req->eq_id[i] = cpu_to_le16(eqs[i]->id);
		for (p = 0, addr = qs[i]->dma.phys; p < num_pages_cq;
		     p++, addr += page_bytes) {
			req->page_physical_address[offset].low =
				cpu_to_le32(lower_32_bits(addr));
			req->page_physical_address[offset].high =
				cpu_to_le32(upper_32_bits(addr));
			offset++;
		}
	}

	if (sli_bmbx_command(sli4)) {
		pr_crit("bootstrap mailbox write fail CQSet\n");
		goto error;
	}

	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		db_regaddr = sli4->reg[1] + SLI4_IF6_CQ_DOORBELL_REGOFFSET;
	else
		db_regaddr = sli4->reg[0] + SLI4_EQCQ_DOORBELL_REGOFFSET;

	res = (void *)((u8 *)dma.virt);
	if (res->hdr.status) {
		pr_err("bad create CQSet status=%#x addl=%#x\n",
		       res->hdr.status, res->hdr.additional_status);
		goto error;
	} else {
		/* Check if we got all requested CQs. */
		if (le16_to_cpu(res->num_q_allocated) != num_cqs) {
			pr_crit("Requested count CQs doesn't match.\n");
			goto error;
		}
		/* Fill the resp cq ids. */
		for (i = 0; i < num_cqs; i++) {
			qs[i]->id = le16_to_cpu(res->q_id) + i;
			qs[i]->db_regaddr = db_regaddr;
		}
	}

	dma_free_coherent(&sli4->pdev->dev, dma.size, dma.virt, dma.phys);

	return 0;

error:
	for (i = 0; i < num_cqs; i++) {
		if (qs[i]->dma.size)
			dma_free_coherent(&sli4->pdev->dev, qs[i]->dma.size,
					  qs[i]->dma.virt, qs[i]->dma.phys);
	}

	if (dma.size)
		dma_free_coherent(&sli4->pdev->dev, dma.size, dma.virt,
				  dma.phys);

	return -1;
}

/**
 * @ingroup sli
 * @brief Free a queue.
 *
 * @par Description
 * Frees DMA memory and de-registers the requested queue.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object.
 * @param destroy_queues Non-zero if the mailbox commands
 * should be sent to destroy the queues.
 * @param free_memory Non-zero if the DMA memory associated
 * with the queue should be freed.
 *
 * @return Returns 0 on success, or -1 otherwise.
 */
int
sli_queue_free(struct sli4_s *sli4, struct sli4_queue_s *q,
	       u32 destroy_queues, u32 free_memory)
{
	int		rc = -1;

	if (!sli4 || !q) {
		pr_err("bad parameter sli4=%p q=%p\n", sli4, q);
		return -1;
	}

	if (destroy_queues) {
		switch (q->type) {
		case SLI_QTYPE_EQ:
			rc = sli_cmd_common_destroy_eq(sli4, sli4->bmbx.virt,
						       SLI4_BMBX_SIZE,	q->id);
			break;
		case SLI_QTYPE_CQ:
			rc = sli_cmd_common_destroy_cq(sli4, sli4->bmbx.virt,
						       SLI4_BMBX_SIZE,	q->id);
			break;
		case SLI_QTYPE_MQ:
			rc = sli_cmd_common_destroy_mq(sli4, sli4->bmbx.virt,
						       SLI4_BMBX_SIZE,	q->id);
			break;
		case SLI_QTYPE_WQ:
			rc = sli_cmd_wq_destroy(sli4, sli4->bmbx.virt,
						SLI4_BMBX_SIZE,	q->id);
			break;
		case SLI_QTYPE_RQ:
			rc = sli_cmd_rq_destroy(sli4, sli4->bmbx.virt,
						SLI4_BMBX_SIZE,	q->id);
			break;
		default:
			pr_info("bad queue type %d\n",
				q->type);
			return -1;
		}

		if (rc) {
			struct sli4_res_hdr_s	*res = NULL;

			if (sli_bmbx_command(sli4)) {
				pr_crit("bootstrap mailbox fail destroy %s\n",
					SLI_QNAME[q->type]);
			} else if (sli_res_sli_config(sli4->bmbx.virt)) {
				pr_err("bad status destroy %s\n",
				       SLI_QNAME[q->type]);
			} else {
				res = (void *)((u8 *)sli4->bmbx.virt +
					offsetof(struct sli4_cmd_sli_config_s,
						 payload));

				if (res->status) {
					pr_err("destroy %s st=%#x addl=%#x\n",
					       SLI_QNAME[q->type],
						res->status,
						res->additional_status);
				} else {
					rc = 0;
				}
			}
		}
	}

	if (free_memory) {
		dma_free_coherent(&sli4->pdev->dev, q->dma.size,
				  q->dma.virt, q->dma.phys);
	}

	return rc;
}

/**
 * @ingroup sli
 * @brief Arm an EQ.
 *
 * @param sli4 SLI context.
 * @param q Pointer to queue object.
 * @param arm If TRUE, arm the EQ.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
sli_queue_eq_arm(struct sli4_s *sli4, struct sli4_queue_s *q, bool arm)
{
	u32	val = 0;
	unsigned long flags = 0;

	spin_lock_irqsave(&q->lock, flags);
	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		val = SLI4_IF6_EQ_DOORBELL(q->n_posted, q->id, arm);
	else
		val = SLI4_EQ_DOORBELL(q->n_posted, q->id, arm);

	writel(val, q->db_regaddr);
	q->n_posted = 0;
	spin_unlock_irqrestore(&q->lock, flags);

	return 0;
}

/**
 * @ingroup sli
 * @brief Arm a queue.
 *
 * @param sli4 SLI context.
 * @param q Pointer to queue object.
 * @param arm If TRUE, arm the queue.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
sli_queue_arm(struct sli4_s *sli4, struct sli4_queue_s *q, bool arm)
{
	u32	val = 0;
	unsigned long flags = 0;

	spin_lock_irqsave(&q->lock, flags);

	switch (q->type) {
	case SLI_QTYPE_EQ:
		if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
			val = SLI4_IF6_EQ_DOORBELL(q->n_posted, q->id, arm);
		else
			val = SLI4_EQ_DOORBELL(q->n_posted, q->id, arm);

		writel(val, q->db_regaddr);
		q->n_posted = 0;
		break;
	case SLI_QTYPE_CQ:
		if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
			val = SLI4_IF6_CQ_DOORBELL(q->n_posted, q->id, arm);
		else
			val = SLI4_CQ_DOORBELL(q->n_posted, q->id, arm);

		writel(val, q->db_regaddr);
		q->n_posted = 0;
		break;
	default:
		pr_info("should only be used for EQ/CQ, not %s\n",
			SLI_QNAME[q->type]);
	}

	spin_unlock_irqrestore(&q->lock, flags);

	return 0;
}

/**
 * @ingroup sli
 * @brief Write a WQ entry to the queue object.
 *
 * Note: Assumes the q->lock will be locked and released by the caller.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object.
 * @param entry Pointer to the entry contents.
 *
 * @return Returns queue index on success, or negative error value otherwise.
 */
int
sli_wq_write(struct sli4_s *sli4, struct sli4_queue_s *q,
	     u8 *entry)
{
	u8		*qe = q->dma.virt;
	u32	qindex;
	u32	val = 0;

	qindex = q->index;
	qe += q->index * q->size;

	if (sli4->perf_wq_id_association)
		sli_set_wq_id_association(entry, q->id);

	memcpy(qe, entry, q->size);
	q->n_posted = 1;

	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		/* non-dpp write for iftype = 6 */
		val = SLI4_WQ_DOORBELL(q->n_posted, 0, q->id);
	else
		val = SLI4_WQ_DOORBELL(q->n_posted, q->index, q->id);

	writel(val, q->db_regaddr);
	q->index = (q->index + q->n_posted) & (q->length - 1);
	q->n_posted = 0;

	return qindex;
}

/**
 * @ingroup sli
 * @brief Write a MQ entry to the queue object.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object.
 * @param entry Pointer to the entry contents.
 *
 * @return Returns queue index on success, or negative error value otherwise.
 */
int
sli_mq_write(struct sli4_s *sli4, struct sli4_queue_s *q,
	     u8 *entry)
{
	u8		*qe = q->dma.virt;
	u32	qindex;
	u32	val = 0;
	unsigned long	flags;

	spin_lock_irqsave(&q->lock, flags);
	qindex = q->index;
	qe += q->index * q->size;

	memcpy(qe, entry, q->size);
	q->n_posted = 1;

	val = SLI4_MQ_DOORBELL(q->n_posted, q->id);
	writel(val, q->db_regaddr);
	q->index = (q->index + q->n_posted) & (q->length - 1);
	q->n_posted = 0;
	spin_unlock_irqrestore(&q->lock, flags);

	return qindex;
}

/**
 * @ingroup sli
 * @brief Write a RQ entry to the queue object.
 *
 * Note: Assumes the q->lock will be locked and released by the caller.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object.
 * @param entry Pointer to the entry contents.
 *
 * @return Returns queue index on success, or negative error value otherwise.
 */
int
sli_rq_write(struct sli4_s *sli4, struct sli4_queue_s *q,
	     u8 *entry)
{
	u8		*qe = q->dma.virt;
	u32	qindex, n_posted;
	u32	val = 0;

	qindex = q->index;
	qe += q->index * q->size;

	memcpy(qe, entry, q->size);
	q->n_posted = 1;

	n_posted = q->n_posted;

	/*
	 * In RQ-pair, an RQ either contains the FC header
	 * (i.e. is_hdr == TRUE) or the payload.
	 *
	 * Don't ring doorbell for payload RQ
	 */
	if (!(q->u.flag.dword & SLI4_QUEUE_FLAG_HDR))
		goto skip;

	/*
	 * Some RQ cannot be incremented one entry at a time.
	 * Instead, the driver collects a number of entries
	 * and updates the RQ in batches.
	 */
	if (q->u.flag.dword & SLI4_QUEUE_FLAG_RQBATCH) {
		if (((q->index + q->n_posted) %
		    SLI4_QUEUE_RQ_BATCH)) {
			goto skip;
		}
		n_posted = SLI4_QUEUE_RQ_BATCH;
	}

	val = SLI4_RQ_DOORBELL(n_posted, q->id);
	writel(val, q->db_regaddr);
skip:
	q->index = (q->index + q->n_posted) & (q->length - 1);
	q->n_posted = 0;

	return qindex;
}

/**
 * @ingroup sli
 * @brief Read an EQ entry from the queue object.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object.
 * @param entry Destination pointer for the queue entry contents.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
sli_eq_read(struct sli4_s *sli4,
	    struct sli4_queue_s *q, u8 *entry)
{
	int		rc = 0;
	u8		*qe = q->dma.virt;
	u32	*qindex = NULL;
	unsigned long	flags = 0;
	u8		clear = false, valid = false;
	u16		wflags = 0;

	clear = (sli4->if_type == SLI4_INTF_IF_TYPE_6) ?  false : true;

	qindex = &q->index;

	spin_lock_irqsave(&q->lock, flags);

	qe += *qindex * q->size;

	/* Check if eqe is valid */
	wflags = le16_to_cpu(((struct sli4_eqe_s *)qe)->dw0w0_flags);
	valid = ((wflags & SLI4_EQE_VALID) == q->phase);
	if (!valid) {
		spin_unlock_irqrestore(&q->lock, flags);
		return -1;
	}

	if (valid && clear) {
		wflags &= ~SLI4_EQE_VALID;
		((struct sli4_eqe_s *)qe)->dw0w0_flags =
						cpu_to_le16(wflags);
	}

	memcpy(entry, qe, q->size);
	*qindex = (*qindex + 1) & (q->length - 1);
	q->n_posted++;
	/*
	 * For prism, the phase value will be used
	 * to check the validity of eq/cq entries.
	 * The value toggles after a complete sweep
	 * through the queue.
	 */

	if (sli4->if_type == SLI4_INTF_IF_TYPE_6 && *qindex == 0)
		q->phase ^= (u16)0x1;

	spin_unlock_irqrestore(&q->lock, flags);

	return rc;
}

/**
 * @ingroup sli
 * @brief Read an CQ entry from the queue object.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object.
 * @param entry Destination pointer for the queue entry contents.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
sli_cq_read(struct sli4_s *sli4,
	    struct sli4_queue_s *q, u8 *entry)
{
	int		rc = 0;
	u8		*qe = q->dma.virt;
	u32	*qindex = NULL;
	unsigned long	flags = 0;
	u8		clear = false;
	u32 dwflags = 0;
	bool valid = false, valid_bit_set = false;

	clear = (sli4->if_type == SLI4_INTF_IF_TYPE_6) ?  false : true;

	qindex = &q->index;

	spin_lock_irqsave(&q->lock, flags);

	qe += *qindex * q->size;

	/* Check if cqe is valid */
	dwflags = le32_to_cpu(((struct sli4_mcqe_s *)qe)->dw3_flags);
	valid_bit_set = (dwflags & SLI4_MCQE_VALID) != 0;

	valid = (valid_bit_set == q->phase);
	if (!valid) {
		spin_unlock_irqrestore(&q->lock, flags);
		return -1;
	}

	if (valid && clear) {
		dwflags &= ~SLI4_MCQE_VALID;
		((struct sli4_mcqe_s *)qe)->dw3_flags =
					cpu_to_le32(dwflags);
	}

	memcpy(entry, qe, q->size);
	*qindex = (*qindex + 1) & (q->length - 1);
	q->n_posted++;
	/*
	 * For prism, the phase value will be used
	 * to check the validity of eq/cq entries.
	 * The value toggles after a complete sweep
	 * through the queue.
	 */

	if (sli4->if_type == SLI4_INTF_IF_TYPE_6 && *qindex == 0)
		q->phase ^= (u16)0x1;

	spin_unlock_irqrestore(&q->lock, flags);

	return rc;
}

/**
 * @ingroup sli
 * @brief Read an MQ entry from the queue object.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object.
 * @param entry Destination pointer for the queue entry contents.
 *
 * @return Returns 0 on success, or non-zero otherwise.
 */
int
sli_mq_read(struct sli4_s *sli4,
	    struct sli4_queue_s *q, u8 *entry)
{
	int		rc = 0;
	u8		*qe = q->dma.virt;
	u32	*qindex = NULL;
	unsigned long	flags = 0;

	qindex = &q->u.r_idx;

	spin_lock_irqsave(&q->lock, flags);

	qe += *qindex * q->size;

	/* Check if mqe is valid */
	if (q->index == q->u.r_idx) {
		spin_unlock_irqrestore(&q->lock, flags);
		return -1;
	}

	memcpy(entry, qe, q->size);
	*qindex = (*qindex + 1) & (q->length - 1);

	spin_unlock_irqrestore(&q->lock, flags);

	return rc;
}

int
sli_queue_index(struct sli4_s *sli4, struct sli4_queue_s *q)
{
	if (q)
		return q->index;
	else
		return -1;
}

int
sli_queue_poke(struct sli4_s *sli4, struct sli4_queue_s *q,
	       u32 index, u8 *entry)
{
	int rc;
	unsigned long flags = 0;

	spin_lock_irqsave(&q->lock, flags);
	rc = _sli_queue_poke(sli4, q, index, entry);
	spin_unlock_irqrestore(&q->lock, flags);

	return rc;
}

int
_sli_queue_poke(struct sli4_s *sli4, struct sli4_queue_s *q,
		u32 index, u8 *entry)
{
	int		rc = 0;
	u8		*qe = q->dma.virt;

	if (index >= q->length)
		return -1;

	qe += index * q->size;

	if (entry)
		memcpy(qe, entry, q->size);

	return rc;
}

/**
 * @ingroup sli
 * @brief Allocate SLI Port resources.
 *
 * @par Description
 * Allocate port-related resources, such as VFI, RPI, XRI, and so on.
 * Resources are modeled using extents, regardless of whether the underlying
 * device implements resource extents. If the device does not implement
 * extents, the SLI layer models this as a single (albeit large) extent.
 *
 * @param sli4 SLI context.
 * @param rtype Resource type (for example, RPI or XRI)
 * @param rid Allocated resource ID.
 * @param index Index into the bitmap.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_resource_alloc(struct sli4_s *sli4, enum sli4_resource_e rtype,
		   u32 *rid, u32 *index)
{
	int		rc = 0;
	u32	size;
	u32	extent_idx;
	u32	item_idx;
	u32	position;

	*rid = U32_MAX;
	*index = U32_MAX;

	switch (rtype) {
	case SLI_RSRC_VFI:
	case SLI_RSRC_VPI:
	case SLI_RSRC_RPI:
	case SLI_RSRC_XRI:
		position =
		find_first_zero_bit(sli4->extent[rtype].use_map,
				    sli4->extent[rtype].map_size);
		if (position >= sli4->extent[rtype].map_size) {
			pr_err("out of resource %d (alloc=%d)\n", rtype,
			       sli4->extent[rtype].n_alloc);
			rc = -1;
			break;
		}
		set_bit(position, sli4->extent[rtype].use_map);
		*index = position;

		size = sli4->extent[rtype].size;

		extent_idx = *index / size;
		item_idx   = *index % size;

		*rid = sli4->extent[rtype].base[extent_idx] + item_idx;

		sli4->extent[rtype].n_alloc++;
		break;
	default:
		rc = -1;
	}

	return rc;
}

/**
 * @ingroup sli
 * @brief Free the SLI Port resources.
 *
 * @par Description
 * Free port-related resources, such as VFI, RPI, XRI, and so.
 * See discussion of "extent" usage in sli_resource_alloc.
 *
 * @param sli4 SLI context.
 * @param rtype Resource type (for example, RPI or XRI).
 * @param rid Allocated resource ID.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_resource_free(struct sli4_s *sli4,
		  enum sli4_resource_e rtype, u32 rid)
{
	int		rc = -1;
	u32	x;
	u32	size, *base;

	switch (rtype) {
	case SLI_RSRC_VFI:
	case SLI_RSRC_VPI:
	case SLI_RSRC_RPI:
	case SLI_RSRC_XRI:
		/*
		 * Figure out which extent contains the resource ID. I.e. find
		 * the extent such that
		 *   extent->base <= resource ID < extent->base + extent->size
		 */
		base = sli4->extent[rtype].base;
		size = sli4->extent[rtype].size;

		/*
		 * In the case of FW reset, this may be cleared
		 * but the force_free path will still attempt to
		 * free the resource. Prevent a NULL pointer access.
		 */
		if (base) {
			for (x = 0; x < sli4->extent[rtype].number;
			     x++) {
				if (rid >= base[x] &&
				    (rid < (base[x] + size))) {
					rid -= base[x];
					clear_bit((x * size) + rid,
						  sli4->extent[rtype].use_map);
					rc = 0;
					break;
				}
			}
		}
		break;
	default:
		break;
	}

	return rc;
}

int
sli_resource_reset(struct sli4_s *sli4, enum sli4_resource_e rtype)
{
	int		rc = -1;
	u32	i;

	switch (rtype) {
	case SLI_RSRC_VFI:
	case SLI_RSRC_VPI:
	case SLI_RSRC_RPI:
	case SLI_RSRC_XRI:
		for (i = 0; i < sli4->extent[rtype].map_size; i++)
			clear_bit(i, sli4->extent[rtype].use_map);
		rc = 0;
		break;
	default:
		break;
	}

	return rc;
}

/**
 * @ingroup sli
 * @brief Parse an EQ entry to retrieve the CQ_ID for this event.
 *
 * @param sli4 SLI context.
 * @param buf Pointer to the EQ entry.
 * @param cq_id CQ_ID for this entry (only valid on success).
 *
 * @return
 * - 0 if success.
 * - < 0 if error.
 * - > 0 if firmware detects EQ overflow.
 */
int
sli_eq_parse(struct sli4_s *sli4, u8 *buf, u16 *cq_id)
{
	struct sli4_eqe_s	*eqe = (void *)buf;
	int		rc = 0;
	u16 flags = 0;
	u16 majorcode;
	u16 minorcode;

	if (!sli4 || !buf || !cq_id) {
		pr_err("bad parameters sli4=%p buf=%p cq_id=%p\n",
		       sli4, buf, cq_id);
		return -1;
	}

	flags = le16_to_cpu(eqe->dw0w0_flags);
	majorcode = (flags & SLI4_EQE_MJCODE) >> 1;
	minorcode = (flags & SLI4_EQE_MNCODE) >> 4;
	switch (majorcode) {
	case SLI4_MAJOR_CODE_STANDARD:
		*cq_id = le16_to_cpu(eqe->resource_id);
		break;
	case SLI4_MAJOR_CODE_SENTINEL:
		pr_info("sentinel EQE\n");
		rc = 1;
		break;
	default:
		pr_info("Unsupported EQE: major %x minor %x\n",
			majorcode, minorcode);
		rc = -1;
	}

	return rc;
}

/**
 * @ingroup sli
 * @brief Parse a CQ entry to retrieve the event type and the associated queue.
 *
 * @param sli4 SLI context.
 * @param cq CQ to process.
 * @param cqe Pointer to the CQ entry.
 * @param etype CQ event type.
 * @param q_id Queue ID associated with this completion message
 * (that is, MQ_ID, RQ_ID, and so on).
 *
 * @return
 * - 0 if call completed correctly and CQE status is SUCCESS.
 * - -1 if call failed (no CQE status).
 * - Other value if call completed correctly and return value is a
 *   CQE status value.
 */
int
sli_cq_parse(struct sli4_s *sli4, struct sli4_queue_s *cq, u8 *cqe,
	     enum sli4_qentry_e *etype, u16 *q_id)
{
	int	rc = 0;

	if (!sli4 || !cq || !cqe || !etype) {
		pr_err("bad params sli4=%p cq=%p cqe=%p etype=%p q_id=%p\n",
		       sli4, cq, cqe, etype, q_id);
		return -1;
	}

	if (cq->u.flag.dword & SLI4_QUEUE_FLAG_MQ) {
		struct sli4_mcqe_s	*mcqe = (void *)cqe;

		if (le32_to_cpu(mcqe->dw3_flags) & SLI4_MCQE_AE) {
			*etype = SLI_QENTRY_ASYNC;
		} else {
			*etype = SLI_QENTRY_MQ;
			rc = sli_cqe_mq(mcqe);
		}
		*q_id = -1;
	} else {
		rc = sli_fc_cqe_parse(sli4, cq, cqe, etype, q_id);
	}

	return rc;
}

/**
 * @ingroup sli
 * @brief Cause chip to enter an unrecoverable error state.
 *
 * @par Description
 * Cause chip to enter an unrecoverable error state. This is
 * used when detecting unexpected FW behavior so FW can be
 * hwted from the driver as soon as error is detected.
 *
 * @param sli4 SLI context.
 * @param dump Generate dump as part of reset.
 *
 * @return Returns 0 if call completed correctly,
 * or -1 if call failed (unsupported chip).
 */
int sli_raise_ue(struct sli4_s *sli4, u8 dump)
{
	u32 val = 0;
#define FDD 2
	if (dump == FDD) {
		val = SLI4_SLIPORT_CONTROL_FDD | SLI4_SLIPORT_CONTROL_IP;
		writel(val, (sli4->reg[0] + SLI4_SLIPORT_CONTROL_REGOFFSET));
	} else {
		val = SLI4_PHYDEV_CONTROL_FRST;

		if (dump == 1)
			val |= SLI4_PHYDEV_CONTROL_DD;
		writel(val, (sli4->reg[0] + SLI4_PHYDEV_CONTROL_REGOFFSET));
	}

	return 0;
}

/**
 * @ingroup sli
 * @brief Read the SLIPORT_STATUS register to to check if a dump is present.
 *
 * @param sli4 SLI context.
 *
 * @return  Returns 1 if the chip is ready,
 * or 0 if the chip is not ready, 2 if fdp is present.
 */
int sli_dump_is_ready(struct sli4_s *sli4)
{
	int	rc = 0;
	u32 port_val;
	u32 bmbx_val;

	/*
	 * Ensure that the port is ready AND the mailbox is
	 * ready before signaling that the dump is ready to go.
	 */
	port_val = sli_reg_read_status(sli4);
	bmbx_val = readl(sli4->reg[0] + SLI4_BMBX_REGOFFSET);

	if ((bmbx_val & SLI4_BMBX_RDY) &&
	    (port_val & SLI4_PORT_STATUS_RDY)) {
		if (port_val & SLI4_PORT_STATUS_DIP)
			rc = 1;
		else if (port_val & SLI4_PORT_STATUS_FDP)
			rc = 2;
	}

	return rc;
}

/**
 * @ingroup sli
 * @brief Read the SLIPORT_STATUS register to check if a dump is present.
 *
 * @param sli4 SLI context.
 *
 * @return
 * - 0 if call completed correctly and no dump is present.
 * - 1 if call completed and dump is present.
 * - -1 if call failed (unsupported chip).
 */
int sli_dump_is_present(struct sli4_s *sli4)
{
	u32 val;
	bool ready;

	/* If the chip is not ready, then there cannot be a dump */
	ready = sli_wait_for_fw_ready(sli4, SLI4_INIT_PORT_DELAY_US);
	if (!ready)
		return 0;

	val = sli_reg_read_status(sli4);
	if (val == U32_MAX) {
		pr_err("error reading SLIPORT_STATUS\n");
		return -1;
	} else {
		return (val & SLI4_PORT_STATUS_DIP) ? 1 : 0;
	}
}

/**
 * @ingroup sli
 * @brief Read the SLIPORT_STATUS register to check if
 * the reset required is set.
 *
 * @param sli4 SLI context.
 *
 * @return
 * - 0 if call completed correctly and reset is not required.
 * - 1 if call completed and reset is required.
 * - -1 if call failed.
 */
int sli_reset_required(struct sli4_s *sli4)
{
	u32 val;

	val = sli_reg_read_status(sli4);
	if (val == U32_MAX) {
		pr_err("error reading SLIPORT_STATUS\n");
		return -1;
	} else {
		return (val & SLI4_PORT_STATUS_RN) ? 1 : 0;
	}
}

/**
 * @ingroup sli
 * @brief Determine if the chip FW is in a ready state
 *
 * @param sli4 SLI context.
 *
 * @return
 * - 0 if call completed correctly and FW is not ready.
 * - 1 if call completed correctly and FW is ready.
 * - -1 if call failed.
 */
int
sli_fw_ready(struct sli4_s *sli4)
{
	u32 val;
	/*
	 * Is firmware ready for operation? Check needed depends on IF_TYPE
	 */
	val = sli_reg_read_status(sli4);
	return (val & SLI4_PORT_STATUS_RDY) ? 1 : 0;
}

/**
 * @ingroup sli
 * @brief Determine if the link can be configured
 *
 * @param sli4 SLI context.
 *
 * @return
 * - 0 if link is not configurable.
 * - 1 if link is configurable.
 */
int sli_link_is_configurable(struct sli4_s *sli)
{
	int rc = 0;
	/*
	 * Link config works on: Lancer
	 * Link config does not work on: LancerG6
	 */

	switch (sli->asic_type) {
	case SLI4_ASIC_INTF_2:
		rc = 1;
		break;
	case SLI4_ASIC_INTF_2_G6:
	case SLI4_ASIC_INTF_2_G7:
	default:
		rc = 0;
		break;
	}

	return rc;
}

/**
 * @ingroup sli_fc
 * @brief Write an WQ_CREATE command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param qmem DMA memory for the queue.
 * @param cq_id Associated CQ_ID.
 * @param ulp The ULP to bind
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_wq_create(struct sli4_s *sli4, void *buf, size_t size,
		  struct efc_dma_s *qmem, u16 cq_id, u16 ulp)
{
	struct sli4_req_wq_create_s	*wq = NULL;
	u32	sli_config_off = 0;
	u32	p;
	uintptr_t	addr;

	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_wq_create_s),
			   sizeof(struct sli4_res_common_create_queue_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);

	wq = (struct sli4_req_wq_create_s *)((u8 *)buf +
							sli_config_off);

	wq->hdr.opcode = SLI4_OPC_WQ_CREATE;
	wq->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	wq->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_wq_create_s) -
			    sizeof(struct sli4_req_hdr_s));
	/* valid values for number of pages: 1-4 (sec 4.5.1) */
	wq->num_pages = sli_page_count(qmem->size, SLI_PAGE_SIZE);
	if (!wq->num_pages ||
	    wq->num_pages > SLI4_WQ_CREATE_V0_MAX_PAGES)
		return 0;

	wq->cq_id = cpu_to_le16(cq_id);

	if (sli4->dual_ulp_capable) {
		wq->dua_byte |= 1;
		wq->bqu_byte |= 1;
		wq->ulp = ulp;
	}

	for (p = 0, addr = qmem->phys;
			p < wq->num_pages;
			p++, addr += SLI_PAGE_SIZE) {
		wq->page_physical_address[p].low  =
				cpu_to_le32(lower_32_bits(addr));
		wq->page_physical_address[p].high =
				cpu_to_le32(upper_32_bits(addr));
	}

	return(sli_config_off + sizeof(struct sli4_req_wq_create_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an WQ_CREATE_V1 command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param qmem DMA memory for the queue.
 * @param cq_id Associated CQ_ID.
 * @param ignored This parameter carries the ULP for WQ (ignored for V1)

 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_wq_create_v1(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *qmem,
		     u16 cq_id, u16 ignored)
{
	struct sli4_req_wq_create_v1_s	*wq = NULL;
	u32	sli_config_off = 0;
	u32	p;
	uintptr_t	addr;
	u32	page_size = 0;
	u32	page_bytes = 0;
	u32	n_wqe = 0;
	u16	num_pages;

	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_wq_create_v1_s),
			   sizeof(struct sli4_res_common_create_queue_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);

	wq = (struct sli4_req_wq_create_v1_s *)((u8 *)buf +
							sli_config_off);

	wq->hdr.opcode = SLI4_OPC_WQ_CREATE;
	wq->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	wq->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_wq_create_v1_s) -
			    sizeof(struct sli4_req_hdr_s));
	wq->hdr.dw3_version = cpu_to_le32(1);

	n_wqe = qmem->size / sli4->wqe_size;

	/*
	 * This heuristic to determine the page size is simplistic but could
	 * be made more sophisticated
	 */
	switch (qmem->size) {
	case 4096:
	case 8192:
	case 16384:
	case 32768:
		page_size = 1;
		break;
	case 65536:
		page_size = 2;
		break;
	case 131072:
		page_size = 4;
		break;
	case 262144:
		page_size = 8;
		break;
	case 524288:
		page_size = 10;
		break;
	default:
		return 0;
	}
	page_bytes = page_size * SLI_PAGE_SIZE;

	/* valid values for number of pages: 1-8 */
	num_pages = sli_page_count(qmem->size, page_bytes);
	wq->num_pages = cpu_to_le16(num_pages);
	if (!num_pages ||
	    num_pages > SLI4_WQ_CREATE_V1_MAX_PAGES)
		return 0;

	wq->cq_id = cpu_to_le16(cq_id);

	wq->page_size = page_size;

	if (sli4->wqe_size == SLI4_WQE_EXT_BYTES)
		wq->wqe_size_byte |= SLI4_WQE_EXT_SIZE;
	else
		wq->wqe_size_byte |= SLI4_WQE_SIZE;

	wq->wqe_count = cpu_to_le16(n_wqe);

	for (p = 0, addr = qmem->phys;
			p < num_pages;
			p++, addr += page_bytes) {
		wq->page_physical_address[p].low  =
					cpu_to_le32(lower_32_bits(addr));
		wq->page_physical_address[p].high =
					cpu_to_le32(upper_32_bits(addr));
	}

	return(sli_config_off + sizeof(struct sli4_req_wq_create_v1_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an WQ_DESTROY command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param wq_id WQ_ID.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_wq_destroy(struct sli4_s *sli4, void *buf, size_t size,
		   u16 wq_id)
{
	struct sli4_req_wq_destroy_s	*wq = NULL;
	u32	sli_config_off = 0;

	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_wq_destroy_s),
			   sizeof(struct sli4_res_hdr_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);

	wq = (struct sli4_req_wq_destroy_s *)((u8 *)buf +
							sli_config_off);

	wq->hdr.opcode = SLI4_OPC_WQ_DESTROY;
	wq->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	wq->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_wq_destroy_s) -
			    sizeof(struct sli4_req_hdr_s));

	wq->wq_id = cpu_to_le16(wq_id);

	return(sli_config_off + sizeof(struct sli4_req_wq_destroy_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an POST_SGL_PAGES command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param xri starting XRI
 * @param xri_count XRI
 * @param page0 First SGL memory page.
 * @param page1 Second SGL memory page (optional).
 * @param dma DMA buffer for non-embedded mailbox command (options)
 *
 * if non-embedded mbx command is used, dma buffer must be at least
 * (32 + xri_count*16) in length
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_post_sgl_pages(struct sli4_s *sli4, void *buf, size_t size,
		       u16 xri,
		       u32 xri_count, struct efc_dma_s *page0[],
		       struct efc_dma_s *page1[], struct efc_dma_s *dma)
{
	struct sli4_req_post_sgl_pages_s	*post = NULL;
	u32	sli_config_off = 0;
	u32	i;

	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size =
		max(sizeof(struct sli4_req_post_sgl_pages_s),
		    sizeof(struct sli4_res_hdr_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, dma);
	if (dma) {
		post = dma->virt;
		memset(post, 0, dma->size);
	} else {
		post = (struct sli4_req_post_sgl_pages_s *)
			((u8 *)buf + sli_config_off);
	}

	post->hdr.opcode = SLI4_OPC_POST_SGL_PAGES;
	post->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	/* payload size calculation */
	/* 4 = xri_start + xri_count */
	/* xri_count = # of XRI's registered */
	/* sizeof(uint64_t) = physical address size */
	/* 2 = # of physical addresses per page set */
	post->hdr.request_length =
		cpu_to_le32(4 + (xri_count * (sizeof(uint64_t) * 2)));

	post->xri_start = cpu_to_le16(xri);
	post->xri_count = cpu_to_le16(xri_count);

	for (i = 0; i < xri_count; i++) {
		post->page_set[i].page0_low  =
				cpu_to_le32(lower_32_bits(page0[i]->phys));
		post->page_set[i].page0_high =
				cpu_to_le32(upper_32_bits(page0[i]->phys));
	}

	if (page1) {
		for (i = 0; i < xri_count; i++) {
			post->page_set[i].page1_low =
				lower_32_bits(page1[i]->phys);
			post->page_set[i].page1_high =
				upper_32_bits(page1[i]->phys);
		}
	}

	return dma ? sli_config_off :
		 (sli_config_off +
		 sizeof(struct sli4_req_post_sgl_pages_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an RQ_CREATE command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param qmem DMA memory for the queue.
 * @param cq_id Associated CQ_ID.
 * @param ulp This parameter carries the ULP for the RQ
 * @param buffer_size Buffer size pointed to by each RQE.
 *
 * @note This creates a Version 0 message.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_rq_create(struct sli4_s *sli4, void *buf, size_t size,
		  struct efc_dma_s *qmem,
		  u16 cq_id, u16 ulp, u16 buffer_size)
{
	struct sli4_req_rq_create_s	*rq = NULL;
	u32	sli_config_off = 0;
	u32	p;
	uintptr_t	addr;
	u16	num_pages;
	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_rq_create_s),
			   sizeof(struct sli4_res_common_create_queue_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);
	rq = (struct sli4_req_rq_create_s *)((u8 *)buf +
							sli_config_off);

	rq->hdr.opcode = SLI4_OPC_RQ_CREATE;
	rq->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	rq->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_rq_create_s) -
			    sizeof(struct sli4_req_hdr_s));
	/* valid values for number of pages: 1-8 (sec 4.5.6) */
	num_pages = sli_page_count(qmem->size, SLI_PAGE_SIZE);
	rq->num_pages = cpu_to_le16(num_pages);
	if (!num_pages ||
	    num_pages > SLI4_RQ_CREATE_V0_MAX_PAGES) {
		pr_info("num_pages %d not valid\n", num_pages);
		return 0;
	}

	/*
	 * RQE count is the log base 2 of the total number of entries
	 */
	rq->rqe_count_byte |= 31 - __builtin_clz(qmem->size / SLI4_RQE_SIZE);

	if (buffer_size < SLI4_RQ_CREATE_V0_MIN_BUF_SIZE ||
	    buffer_size > SLI4_RQ_CREATE_V0_MAX_BUF_SIZE) {
		pr_err("buffer_size %d out of range (%d-%d)\n",
		       buffer_size,
				SLI4_RQ_CREATE_V0_MIN_BUF_SIZE,
				SLI4_RQ_CREATE_V0_MAX_BUF_SIZE);
		return -1;
	}
	rq->buffer_size = cpu_to_le16(buffer_size);

	rq->cq_id = cpu_to_le16(cq_id);

	if (sli4->dual_ulp_capable) {
		rq->dua_bqu_byte |= SLI4_RQCREATE_DUA;
		rq->dua_bqu_byte |= SLI4_RQCREATE_BQU;
		rq->ulp = ulp;
	}

	for (p = 0, addr = qmem->phys;
			p < num_pages;
			p++, addr += SLI_PAGE_SIZE) {
		rq->page_physical_address[p].low  =
				cpu_to_le32(lower_32_bits(addr));
		rq->page_physical_address[p].high =
				cpu_to_le32(upper_32_bits(addr));
	}

	return(sli_config_off + sizeof(struct sli4_req_rq_create_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an RQ_CREATE_V1 command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param qmem DMA memory for the queue.
 * @param cq_id Associated CQ_ID.
 * @param ulp This parameter carries the ULP for RQ (ignored for V1)
 * @param buffer_size Buffer size pointed to by each RQE.
 *
 * @note This creates a Version 0 message
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_rq_create_v1(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *qmem, u16 cq_id,
		     u16 ulp, u16 buffer_size)
{
	struct sli4_req_rq_create_v1_s	*rq = NULL;
	u32	sli_config_off = 0;
	u32	p;
	uintptr_t	addr;
	u32	num_pages;

	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_rq_create_v1_s),
			   sizeof(struct sli4_res_common_create_queue_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);
	rq = (struct sli4_req_rq_create_v1_s *)((u8 *)buf +
							sli_config_off);

	rq->hdr.opcode = SLI4_OPC_RQ_CREATE;
	rq->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	rq->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_rq_create_v1_s) -
			    sizeof(struct sli4_req_hdr_s));
	rq->hdr.dw3_version = cpu_to_le32(1);

	/* Disable "no buffer warnings" to avoid Lancer bug */
	rq->dim_dfd_dnb |= SLI4_RQCREATEV1_DNB;

	/* valid values for number of pages: 1-8 (sec 4.5.6) */
	num_pages = sli_page_count(qmem->size, SLI_PAGE_SIZE);
	rq->num_pages = cpu_to_le16(num_pages);
	if (!num_pages ||
	    num_pages > SLI4_RQ_CREATE_V1_MAX_PAGES) {
		pr_info("num_pages %d not valid, max %d\n",
			num_pages, SLI4_RQ_CREATE_V1_MAX_PAGES);
		return 0;
	}

	/*
	 * RQE count is the total number of entries (note not lg2(# entries))
	 */
	rq->rqe_count = cpu_to_le16(qmem->size / SLI4_RQE_SIZE);

	rq->rqe_size_byte |= SLI4_RQE_SIZE_8;

	rq->page_size = SLI4_RQ_PAGE_SIZE_4096;

	if (buffer_size < sli4->rq_min_buf_size ||
	    buffer_size > sli4->rq_max_buf_size) {
		pr_err("buffer_size %d out of range (%d-%d)\n",
		       buffer_size,
				sli4->rq_min_buf_size,
				sli4->rq_max_buf_size);
		return -1;
	}
	rq->buffer_size = cpu_to_le32(buffer_size);

	rq->cq_id = cpu_to_le16(cq_id);

	for (p = 0, addr = qmem->phys;
			p < num_pages;
			p++, addr += SLI_PAGE_SIZE) {
		rq->page_physical_address[p].low  =
					cpu_to_le32(lower_32_bits(addr));
		rq->page_physical_address[p].high =
					cpu_to_le32(upper_32_bits(addr));
	}

	return(sli_config_off + sizeof(struct sli4_req_rq_create_v1_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an RQ_DESTROY command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param rq_id RQ_ID.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_rq_destroy(struct sli4_s *sli4, void *buf, size_t size,
		   u16 rq_id)
{
	struct sli4_req_rq_destroy_s	*rq = NULL;
	u32	sli_config_off = 0;
	u32 payload_size;

	/* Payload length must accommodate both request and response */
	payload_size = max(sizeof(struct sli4_req_rq_destroy_s),
			   sizeof(struct sli4_res_hdr_s));

	sli_config_off = sli_cmd_sli_config(sli4, buf, size,
					    payload_size, NULL);
	rq = (struct sli4_req_rq_destroy_s *)((u8 *)buf +
							sli_config_off);

	rq->hdr.opcode = SLI4_OPC_RQ_DESTROY;
	rq->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	rq->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_rq_destroy_s) -
			    sizeof(struct sli4_req_hdr_s));

	rq->rq_id = cpu_to_le16(rq_id);

	return(sli_config_off + sizeof(struct sli4_req_rq_destroy_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an READ_FCF_TABLE command.
 *
 * @note
 * The response of this command exceeds the size of an embedded
 * command and requires an external buffer with DMA capability to hold the
 * results.
 * The caller should allocate the struct efc_dma_s structure / memory.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param dma Pointer to DMA memory structure. This is allocated by the caller.
 * @param index FCF table index to retrieve.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_read_fcf_table(struct sli4_s *sli4, void *buf, size_t size,
		       struct efc_dma_s *dma, u16 index)
{
	struct sli4_req_read_fcf_table_s *read_fcf = NULL;

	read_fcf = dma->virt;

	memset(read_fcf, 0, sizeof(struct sli4_req_read_fcf_table_s));

	read_fcf->hdr.opcode = SLI4_OPC_READ_FCF_TABLE;
	read_fcf->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	read_fcf->hdr.request_length =
		cpu_to_le32(dma->size -
			    sizeof(struct sli4_req_read_fcf_table_s));
	read_fcf->fcf_index = cpu_to_le16(index);

	return sli_cmd_sli_config(sli4, buf, size, 0, dma);
}

/**
 * @ingroup sli_fc
 * @brief Write an POST_HDR_TEMPLATES command.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the command.
 * @param size Buffer size, in bytes.
 * @param dma Pointer to DMA memory structure. This is allocated by the caller.
 * @param rpi Starting RPI index for the header templates.
 * @param payload_dma Pointer to DMA memory used to hold larger descriptor
 * counts.
 *
 * @return Returns the number of bytes written.
 */
int
sli_cmd_post_hdr_templates(struct sli4_s *sli4, void *buf,
			   size_t size, struct efc_dma_s *dma,
			   u16 rpi,
			   struct efc_dma_s *payload_dma)
{
	struct sli4_req_post_hdr_templates_s *template = NULL;
	u32	sli_config_off = 0;
	uintptr_t	phys = 0;
	u32	i = 0;
	u32	page_count;
	u32	payload_size;
	u32	reqsz;

	page_count = sli_page_count(dma->size, SLI_PAGE_SIZE);

	payload_size = sizeof(struct sli4_req_post_hdr_templates_s) +
			      page_count *
			      sizeof(struct sli4_physical_page_descriptor_s);

	if (page_count > 16) {
		/*
		 * We can't fit more than 16 descriptors into an embedded mbox
		 * command, it has to be non-embedded
		 */
		payload_dma->size = payload_size;
		payload_dma->virt = dma_alloc_coherent(&sli4->pdev->dev,
						       payload_dma->size,
					     &payload_dma->phys, GFP_DMA);
		if (!payload_dma->virt) {
			memset(payload_dma, 0, sizeof(struct efc_dma_s));
			pr_err("mbox payload memory allocation fail\n");
			return 0;
		}
		sli_config_off = sli_cmd_sli_config(sli4, buf, size,
						    payload_size, payload_dma);
		template = (struct sli4_req_post_hdr_templates_s *)
				payload_dma->virt;
	} else {
		sli_config_off = sli_cmd_sli_config(sli4, buf, size,
						    payload_size, NULL);
		template = (struct sli4_req_post_hdr_templates_s *)
				((u8 *)buf + sli_config_off);
	}

	if (rpi == U16_MAX)
		rpi = sli4->extent[SLI_RSRC_RPI].base[0];

	template->hdr.opcode = SLI4_OPC_POST_HDR_TEMPLATES;
	template->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	reqsz = sizeof(struct sli4_req_post_hdr_templates_s) -
			sizeof(struct sli4_req_hdr_s);
	template->hdr.request_length = cpu_to_le32(reqsz);

	template->rpi_offset = cpu_to_le16(rpi);
	template->page_count = cpu_to_le16(page_count);
	phys = dma->phys;
	for (i = 0; i < page_count; i++) {
		template->page_descriptor[i].low  =
				cpu_to_le32(lower_32_bits(phys));
		template->page_descriptor[i].high =
				cpu_to_le32(upper_32_bits(phys));

		phys += SLI_PAGE_SIZE;
	}

	return(sli_config_off + payload_size);
}

int
sli_cmd_rediscover_fcf(struct sli4_s *sli4, void *buf, size_t size,
		       u16 index)
{
	struct sli4_req_rediscover_fcf_s *redisc = NULL;
	u32	sli_config_off = 0;
	u32	psize = sizeof(struct sli4_req_rediscover_fcf_s);

	sli_config_off = sli_cmd_sli_config(sli4, buf, size, psize, NULL);

	redisc = (struct sli4_req_rediscover_fcf_s *)
			((u8 *)buf + sli_config_off);

	redisc->hdr.opcode = SLI4_OPC_REDISCOVER_FCF;
	redisc->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	redisc->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_rediscover_fcf_s) -
			    sizeof(struct sli4_req_hdr_s));

	if (index == U16_MAX) {
		redisc->fcf_count = 0;
	} else {
		redisc->fcf_count = cpu_to_le16(1);
		redisc->fcf_index[0] = cpu_to_le16(index);
	}

	return(sli_config_off + sizeof(struct sli4_req_rediscover_fcf_s));
}

/**
 * @ingroup sli_fc
 * @brief Write an ABORT_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param type Abort type, such as XRI, abort tag, and request tag.
 * @param send_abts Boolean to cause the hardware to automatically generate an
 * ABTS.
 * @param ids ID of IOs to abort.
 * @param mask Mask applied to the ID values to abort.
 * @param tag Tag value associated with this abort.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param dnrx When set to 1, this field indicates that the SLI Port must not
 * return the associated XRI to the SLI
 *             Port's optimized write XRI pool.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_abort_wqe(struct sli4_s *sli4, void *buf, size_t size,
	      enum sli4_abort_type_e type, bool send_abts, u32 ids,
	      u32 mask, u16 tag, u16 cq_id)
{
	struct sli4_abort_wqe_s	*abort = buf;

	memset(buf, 0, size);

	switch (type) {
	case SLI_ABORT_XRI:
		abort->criteria = SLI4_ABORT_CRITERIA_XRI_TAG;
		if (mask) {
			pr_warn("%#x aborting XRI %#x warning non-zero mask",
				mask, ids);
			mask = 0;
		}
		break;
	case SLI_ABORT_ABORT_ID:
		abort->criteria = SLI4_ABORT_CRITERIA_ABORT_TAG;
		break;
	case SLI_ABORT_REQUEST_ID:
		abort->criteria = SLI4_ABORT_CRITERIA_REQUEST_TAG;
		break;
	default:
		pr_info("unsupported type %#x\n", type);
		return -1;
	}

	abort->ia_ir_byte |= send_abts ? 0 : 1;

	/* Suppress ABTS retries */
	abort->ia_ir_byte |= SLI4_ABRT_WQE_IR;

	abort->t_mask = cpu_to_le32(mask);
	abort->t_tag  = cpu_to_le32(ids);
	abort->command = SLI4_WQE_ABORT;
	abort->request_tag = cpu_to_le16(tag);

	abort->dw10w0_flags = cpu_to_le16(SLI4_ABRT_WQE_QOSD);

	abort->cq_id = cpu_to_le16(cq_id);
	abort->cmdtype_wqec_byte |= SLI4_CMD_ABORT_WQE;

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write an ELS_REQUEST64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the ELS request.
 * @param req_type ELS request type.
 * @param req_len Length of ELS request in bytes.
 * @param max_rsp_len Max length of ELS response in bytes.
 * @param timeout Time, in seconds, before an IO times out. Zero means 2 *
 *  R_A_TOV.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rnode Destination of ELS request (that is, the remote node).
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_els_request64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		      struct efc_dma_s *sgl,
		      u8 req_type, u32 req_len, u32 max_rsp_len,
		      u8 timeout, u16 xri, u16 tag,
		      u16 cq_id, u16 rnodeindicator, u16 sportindicator,
		      bool hlm, bool rnodeattached, u32 rnode_fcid,
		      u32 sport_fcid)
{
	struct sli4_els_request64_wqe_s	*els = buf;
	struct sli4_sge_s	*sge = sgl->virt;
	bool is_fabric = false;
	struct sli4_bde_s *bptr;

	memset(buf, 0, size);

	bptr = &els->els_request_payload;
	if (sli4->sgl_pre_registered) {
		els->qosd_xbl_hlm_iod_dbde_wqes &= ~SLI4_REQ_WQE_XBL;

		els->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_REQ_WQE_DBDE;
		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				    (req_len & SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.data.buffer_address_low  = sge[0].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[0].buffer_address_high;
	} else {
		els->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_REQ_WQE_XBL;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BLP << 24) |
				    ((2 * sizeof(struct sli4_sge_s)) &
				     SLI4_BDE_MASK_BUFFER_LEN));
		bptr->u.blp.sgl_segment_address_low  =
				cpu_to_le32(lower_32_bits(sgl->phys));
		bptr->u.blp.sgl_segment_address_high =
				cpu_to_le32(upper_32_bits(sgl->phys));
	}

	els->els_request_payload_length = cpu_to_le32(req_len);
	els->max_response_payload_length = cpu_to_le32(max_rsp_len);

	els->xri_tag = cpu_to_le16(xri);
	els->timer = timeout;
	els->class_byte |= SLI4_GENERIC_CLASS_CLASS_3;

	els->command = SLI4_WQE_ELS_REQUEST64;

	els->request_tag = cpu_to_le16(tag);

	if (hlm) {
		els->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_REQ_WQE_HLM;
		els->remote_id_dword = cpu_to_le32(rnode_fcid & 0x00ffffff);
	}

	els->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_REQ_WQE_IOD;

	els->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_REQ_WQE_QOSD;

	/* figure out the ELS_ID value from the request buffer */

	switch (req_type) {
	case ELS_LOGO:
		els->cmdtype_elsid_byte |=
			SLI4_ELS_REQUEST64_LOGO << SLI4_REQ_WQE_ELSID_SHFT;
		if (rnodeattached) {
			els->ct_byte |= (SLI4_GENERIC_CONTEXT_RPI <<
					 SLI4_REQ_WQE_CT_SHFT);
			els->context_tag = cpu_to_le16(rnodeindicator);
		} else {
			els->ct_byte |=
			SLI4_GENERIC_CONTEXT_VPI << SLI4_REQ_WQE_CT_SHFT;
			els->context_tag =
				cpu_to_le16(sportindicator);
		}
		if (rnode_fcid == FC_FID_FLOGI)
			is_fabric = true;
		break;
	case ELS_FDISC:
		if (rnode_fcid == FC_FID_FLOGI)
			is_fabric = true;
		if (sport_fcid == 0) {
			els->cmdtype_elsid_byte |=
			SLI4_ELS_REQUEST64_FDISC << SLI4_REQ_WQE_ELSID_SHFT;
			is_fabric = true;
		} else {
			els->cmdtype_elsid_byte |=
			SLI4_ELS_REQUEST64_OTHER << SLI4_REQ_WQE_ELSID_SHFT;
		}
		els->ct_byte |= (SLI4_GENERIC_CONTEXT_VPI <<
				 SLI4_REQ_WQE_CT_SHFT);
		els->context_tag = cpu_to_le16(sportindicator);
		els->sid_sp_dword |= cpu_to_le32(1 << SLI4_REQ_WQE_SP_SHFT);
		break;
	case ELS_FLOGI:
		els->ct_byte |=
			SLI4_GENERIC_CONTEXT_VPI << SLI4_REQ_WQE_CT_SHFT;
		els->context_tag = cpu_to_le16(sportindicator);
		/*
		 * Set SP here ... we haven't done a REG_VPI yet
		 * need to maybe not set this when we have
		 * completed VFI/VPI registrations ...
		 *
		 * Use the FC_ID of the SPORT if it has been allocated,
		 * otherwise use an S_ID of zero.
		 */
		els->sid_sp_dword |= cpu_to_le32(1 << SLI4_REQ_WQE_SP_SHFT);
		if (sport_fcid != U32_MAX)
			els->sid_sp_dword |= cpu_to_le32(sport_fcid);
		break;
	case ELS_PLOGI:
		els->cmdtype_elsid_byte |=
			SLI4_ELS_REQUEST64_PLOGI << SLI4_REQ_WQE_ELSID_SHFT;
		els->ct_byte |=
			SLI4_GENERIC_CONTEXT_VPI << SLI4_REQ_WQE_CT_SHFT;
		els->context_tag = cpu_to_le16(sportindicator);
		break;
	case ELS_SCR:
		els->cmdtype_elsid_byte |=
			SLI4_ELS_REQUEST64_OTHER << SLI4_REQ_WQE_ELSID_SHFT;
		els->ct_byte |=
			SLI4_GENERIC_CONTEXT_VPI << SLI4_REQ_WQE_CT_SHFT;
		els->context_tag = cpu_to_le16(sportindicator);
		break;
	default:
		els->cmdtype_elsid_byte |=
			SLI4_ELS_REQUEST64_OTHER << SLI4_REQ_WQE_ELSID_SHFT;
		if (rnodeattached) {
			els->ct_byte |= (SLI4_GENERIC_CONTEXT_RPI <<
					 SLI4_REQ_WQE_CT_SHFT);
			els->context_tag = cpu_to_le16(sportindicator);
		} else {
			els->ct_byte |=
			SLI4_GENERIC_CONTEXT_VPI << SLI4_REQ_WQE_CT_SHFT;
			els->context_tag =
				cpu_to_le16(sportindicator);
		}
		break;
	}

	if (is_fabric)
		els->cmdtype_elsid_byte |= SLI4_ELS_REQUEST64_CMD_FABRIC;
	else
		els->cmdtype_elsid_byte |= SLI4_ELS_REQUEST64_CMD_NON_FABRIC;

	els->cq_id = cpu_to_le16(cq_id);

	if (((els->ct_byte & SLI4_REQ_WQE_CT) >> SLI4_REQ_WQE_CT_SHFT) !=
					SLI4_GENERIC_CONTEXT_RPI)
		els->remote_id_dword = cpu_to_le32(rnode_fcid);

	if (((els->ct_byte & SLI4_REQ_WQE_CT) >> SLI4_REQ_WQE_CT_SHFT) ==
					SLI4_GENERIC_CONTEXT_VPI)
		els->temporary_rpi = cpu_to_le16(rnodeindicator);

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write an FCP_ICMND64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the scatter gather list.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rpi remote node indicator (RPI)
 * @param rnode Destination request (that is, the remote node).
 * @param timeout Time, in seconds, before an IO times out. Zero means no
 * timeout.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fcp_icmnd64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    struct efc_dma_s *sgl, u16 xri, u16 tag,
		    u16 cq_id, u32 rpi, bool hlm,
		    u32 rnode_fcid, u8 timeout)
{
	struct sli4_fcp_icmnd64_wqe_s *icmnd = buf;
	struct sli4_sge_s	*sge = NULL;
	struct sli4_bde_s *bptr;

	memset(buf, 0, size);

	if (!sgl || !sgl->virt) {
		pr_err("bad parameter sgl=%p virt=%p\n",
		       sgl, sgl ? sgl->virt : NULL);
		return -1;
	}
	sge = sgl->virt;
	bptr = &icmnd->bde;
	if (sli4->sgl_pre_registered) {
		icmnd->qosd_xbl_hlm_iod_dbde_wqes &= ~SLI4_ICMD_WQE_XBL;

		icmnd->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_ICMD_WQE_DBDE;
		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				    (le32_to_cpu(sge[0].buffer_length) &
				     SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.data.buffer_address_low  = sge[0].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[0].buffer_address_high;
	} else {
		icmnd->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_ICMD_WQE_XBL;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BLP << 24) |
				    (sgl->size & SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.blp.sgl_segment_address_low  =
				cpu_to_le32(lower_32_bits(sgl->phys));
		bptr->u.blp.sgl_segment_address_high =
				cpu_to_le32(upper_32_bits(sgl->phys));
	}

	icmnd->payload_offset_length = (sge[0].buffer_length +
					 sge[1].buffer_length);
	icmnd->xri_tag = cpu_to_le16(xri);
	icmnd->context_tag = cpu_to_le16(rpi);
	icmnd->timer = timeout;

	/* WQE word 4 contains read transfer length */
	icmnd->class_pu_byte |= 2 << SLI4_ICMD_WQE_PU_SHFT;
	icmnd->class_pu_byte |= SLI4_GENERIC_CLASS_CLASS_3;
	icmnd->command = SLI4_WQE_FCP_ICMND64;
	icmnd->dif_ct_bs_byte |=
		SLI4_GENERIC_CONTEXT_RPI << SLI4_ICMD_WQE_CT_SHFT;

	icmnd->abort_tag = cpu_to_le32(xri);

	icmnd->request_tag = cpu_to_le16(tag);
	icmnd->len_loc1_byte |= SLI4_ICMD_WQE_LEN_LOC_BIT1;
	icmnd->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_ICMD_WQE_LEN_LOC_BIT2;
	if (hlm) {
		icmnd->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_ICMD_WQE_HLM;
		icmnd->remote_n_port_id_dword =
				cpu_to_le32(rnode_fcid & 0x00ffffff);
	}
	icmnd->cmd_type_byte |= SLI4_CMD_FCP_ICMND64_WQE;
	icmnd->cq_id = cpu_to_le16(cq_id);

	return  0;
}

/**
 * @ingroup sli_fc
 * @brief Write an FCP_IREAD64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the scatter gather list.
 * @param first_data_sge Index of first data sge (used if perf hints are
 * enabled)
 * @param xfer_len Data transfer length.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rpi remote node indicator (RPI)
 * @param rnode Destination request (i.e. remote node).
 * @param dif T10 DIF operation, or 0 to disable.
 * @param bs T10 DIF block size, or 0 if DIF is disabled.
 * @param timeout Time, in seconds, before an IO times out. Zero means no
 * timeout.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fcp_iread64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    struct efc_dma_s *sgl, u32 first_data_sge,
		    u32 xfer_len, u16 xri, u16 tag,
		    u16 cq_id, u32 rpi, bool hlm, u32 rnode_fcid,
		    u8 dif, u8 bs, u8 timeout)
{
	struct sli4_fcp_iread64_wqe_s *iread = buf;
	struct sli4_sge_s	*sge = NULL;
	struct sli4_bde_s *bptr;
	u32 sge_flags = 0;

	memset(buf, 0, size);

	if (!sgl || !sgl->virt) {
		pr_err("bad parameter sgl=%p virt=%p\n",
		       sgl, sgl ? sgl->virt : NULL);
		return -1;
	}
	sge = sgl->virt;
	bptr = &iread->bde;
	if (sli4->sgl_pre_registered) {
		iread->qosd_xbl_hlm_iod_dbde_wqes &= ~SLI4_IR_WQE_XBL;

		iread->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IR_WQE_DBDE;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				    (le32_to_cpu(sge[0].buffer_length) &
				     SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.data.buffer_address_low  = sge[0].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[0].buffer_address_high;
	} else {
		iread->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IR_WQE_XBL;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BLP << 24) |
				    (sgl->size & SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.blp.sgl_segment_address_low  =
				cpu_to_le32(lower_32_bits(sgl->phys));
		bptr->u.blp.sgl_segment_address_high =
				cpu_to_le32(upper_32_bits(sgl->phys));

		/*
		 * fill out fcp_cmnd buffer len and change resp buffer to be of
		 * type "skip" (note: response will still be written to sge[1]
		 * if necessary)
		 */
		iread->fcp_cmd_buffer_length =
					cpu_to_le16(sge[0].buffer_length);

		sge_flags = sge[1].dw2_flags;
		sge_flags &= (~SLI4_SGE_TYPE);
		sge_flags |= (SLI4_SGE_TYPE_SKIP << 27);
		sge[1].dw2_flags = sge_flags;
	}

	iread->payload_offset_length = (sge[0].buffer_length +
					 sge[1].buffer_length);
	iread->total_transfer_length = cpu_to_le32(xfer_len);

	iread->xri_tag = cpu_to_le16(xri);
	iread->context_tag = cpu_to_le16(rpi);

	iread->timer = timeout;

	/* WQE word 4 contains read transfer length */
	iread->class_pu_byte |= 2 << SLI4_IR_WQE_PU_SHFT;
	iread->class_pu_byte |= SLI4_GENERIC_CLASS_CLASS_3;
	iread->command = SLI4_WQE_FCP_IREAD64;
	iread->dif_ct_bs_byte |=
		SLI4_GENERIC_CONTEXT_RPI << SLI4_IR_WQE_CT_SHFT;
	iread->dif_ct_bs_byte |= dif;
	iread->dif_ct_bs_byte  |= bs << SLI4_IR_WQE_BS_SHFT;

	iread->abort_tag = cpu_to_le32(xri);

	iread->request_tag = cpu_to_le16(tag);
	iread->len_loc1_byte |= SLI4_IR_WQE_LEN_LOC_BIT1;
	iread->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IR_WQE_LEN_LOC_BIT2;
	if (hlm) {
		iread->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IR_WQE_HLM;
		iread->remote_n_port_id_dword =
				cpu_to_le32(rnode_fcid & 0x00ffffff);
	}
	iread->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IR_WQE_IOD;
	iread->cmd_type_byte |= SLI4_CMD_FCP_IREAD64_WQE;
	iread->cq_id = cpu_to_le16(cq_id);

	if (sli4->perf_hint) {
		bptr = &iread->first_data_bde;
		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			  (le32_to_cpu(sge[first_data_sge].buffer_length) &
			     SLI4_BDE_MASK_BUFFER_LEN));
		bptr->u.data.buffer_address_low =
			sge[first_data_sge].buffer_address_low;
		bptr->u.data.buffer_address_high =
			sge[first_data_sge].buffer_address_high;
	}

	return  0;
}

/**
 * @ingroup sli_fc
 * @brief Write an FCP_IWRITE64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the scatter gather list.
 * @param first_data_sge Index of first data sge (used if perf hints are
 * enabled)
 * @param xfer_len Data transfer length.
 * @param first_burst The number of first burst bytes
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rpi remote node indicator (RPI)
 * @param rnode Destination request (i.e. remote node)
 * @param dif T10 DIF operation, or 0 to disable
 * @param bs T10 DIF block size, or 0 if DIF is disabled
 * @param timeout Time, in seconds, before an IO times out. Zero means no
 * timeout.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fcp_iwrite64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		     struct efc_dma_s *sgl,
		     u32 first_data_sge, u32 xfer_len,
		     u32 first_burst, u16 xri, u16 tag,
		     u16 cq_id, u32 rpi,
		     bool hlm, u32 rnode_fcid,
		     u8 dif, u8 bs, u8 timeout)
{
	struct sli4_fcp_iwrite64_wqe_s *iwrite = buf;
	struct sli4_sge_s	*sge = NULL;
	struct sli4_bde_s *bptr;
	u32 sge_flags = 0, min = 0;

	memset(buf, 0, size);

	if (!sgl || !sgl->virt) {
		pr_err("bad parameter sgl=%p virt=%p\n",
		       sgl, sgl ? sgl->virt : NULL);
		return -1;
	}
	sge = sgl->virt;
	bptr = &iwrite->bde;
	if (sli4->sgl_pre_registered) {
		iwrite->qosd_xbl_hlm_iod_dbde_wqes &= ~SLI4_IWR_WQE_XBL;

		iwrite->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IWR_WQE_DBDE;
		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				     (le32_to_cpu(sge[0].buffer_length) &
				      SLI4_BDE_MASK_BUFFER_LEN));
		bptr->u.data.buffer_address_low  = sge[0].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[0].buffer_address_high;
	} else {
		iwrite->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IWR_WQE_XBL;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BLP << 24) |
				    (sgl->size & SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.blp.sgl_segment_address_low  =
			cpu_to_le32(lower_32_bits(sgl->phys));
		bptr->u.blp.sgl_segment_address_high =
			cpu_to_le32(upper_32_bits(sgl->phys));

		/*
		 * fill out fcp_cmnd buffer len and change resp buffer to be of
		 * type "skip" (note: response will still be written to sge[1]
		 * if necessary)
		 */
		iwrite->fcp_cmd_buffer_length =
					cpu_to_le16(sge[0].buffer_length);
		sge_flags = sge[1].dw2_flags;
		sge_flags &= ~SLI4_SGE_TYPE;
		sge_flags |= (SLI4_SGE_TYPE_SKIP << 27);
		sge[1].dw2_flags = sge_flags;
	}

	iwrite->payload_offset_length = (sge[0].buffer_length +
					 sge[1].buffer_length);
	iwrite->total_transfer_length = cpu_to_le16(xfer_len);
	min = (xfer_len < first_burst) ? xfer_len : first_burst;
	iwrite->initial_transfer_length = cpu_to_le16(min);

	iwrite->xri_tag = cpu_to_le16(xri);
	iwrite->context_tag = cpu_to_le16(rpi);

	iwrite->timer = timeout;
	/* WQE word 4 contains read transfer length */
	iwrite->class_pu_byte |= 2 << SLI4_IWR_WQE_PU_SHFT;
	iwrite->class_pu_byte |= SLI4_GENERIC_CLASS_CLASS_3;
	iwrite->command = SLI4_WQE_FCP_IWRITE64;
	iwrite->dif_ct_bs_byte |=
			SLI4_GENERIC_CONTEXT_RPI << SLI4_IWR_WQE_CT_SHFT;
	iwrite->dif_ct_bs_byte |= dif;
	iwrite->dif_ct_bs_byte |= bs << SLI4_IWR_WQE_BS_SHFT;

	iwrite->abort_tag = cpu_to_le32(xri);

	iwrite->request_tag = cpu_to_le16(tag);
	iwrite->len_loc1_byte |= SLI4_IWR_WQE_LEN_LOC_BIT1;
	iwrite->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IWR_WQE_LEN_LOC_BIT2;
	if (hlm) {
		iwrite->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_IWR_WQE_HLM;
		iwrite->remote_n_port_id_dword =
			cpu_to_le32(rnode_fcid & 0x00ffffff);
	}
	iwrite->cmd_type_byte |= SLI4_CMD_FCP_IWRITE64_WQE;
	iwrite->cq_id = cpu_to_le16(cq_id);

	if (sli4->perf_hint) {
		bptr = &iwrite->first_data_bde;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			 (le32_to_cpu(sge[first_data_sge].buffer_length) &
			     SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.data.buffer_address_low =
			sge[first_data_sge].buffer_address_low;
		bptr->u.data.buffer_address_high =
			sge[first_data_sge].buffer_address_high;
	}

	return  0;
}

/**
 * @ingroup sli_fc
 * @brief Write an FCP_TRECEIVE64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the Scatter-Gather List.
 * @param first_data_sge Index of first data sge (used if perf hints are
 * enabled)
 * @param relative_off Relative offset of the IO (if any).
 * @param xfer_len Data transfer length.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param xid OX_ID for the exchange.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rpi remote node indicator (RPI)
 * @param rnode Destination request (i.e. remote node).
 * @param flags Optional attributes, including:
 *  - ACTIVE - IO is already active.
 *  - AUTO RSP - Automatically generate a good FCP_RSP.
 * @param dif T10 DIF operation, or 0 to disable.
 * @param bs T10 DIF block size, or 0 if DIF is disabled.
 * @param csctl value of csctl field.
 * @param app_id value for VM application header.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fcp_treceive64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		       struct efc_dma_s *sgl,
		       u32 first_data_sge, u32 relative_off,
		       u32 xfer_len, u16 xri, u16 tag,
		       u16 cq_id, u16 xid, u32 rpi, bool hlm,
		       u32 rnode_fcid, u32 flags, u8 dif,
		       u8 bs, u8 csctl, u32 app_id)
{
	struct sli4_fcp_treceive64_wqe_s *trecv = buf;
	struct sli4_fcp_128byte_wqe_s *trecv_128 = buf;
	struct sli4_sge_s	*sge = NULL;
	struct sli4_bde_s *bptr;

	memset(buf, 0, size);

	if (!sgl || !sgl->virt) {
		pr_err("bad parameter sgl=%p virt=%p\n",
		       sgl, sgl ? sgl->virt : NULL);
		return -1;
	}
	sge = sgl->virt;
	bptr = &trecv->bde;
	if (sli4->sgl_pre_registered) {
		trecv->qosd_xbl_hlm_iod_dbde_wqes &= ~SLI4_TRCV_WQE_XBL;

		trecv->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_TRCV_WQE_DBDE;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				    (le32_to_cpu(sge[0].buffer_length)
					& SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.data.buffer_address_low  = sge[0].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[0].buffer_address_high;

		trecv->payload_offset_length = sge[0].buffer_length;
	} else {
		trecv->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_TRCV_WQE_XBL;

		/* if data is a single physical address, use a BDE */
		if (!dif && xfer_len <= le32_to_cpu(sge[2].buffer_length)) {
			trecv->qosd_xbl_hlm_iod_dbde_wqes |=
							SLI4_TRCV_WQE_DBDE;
			bptr->bde_type_buflen =
				cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
					    (le32_to_cpu(sge[2].buffer_length)
						& SLI4_BDE_MASK_BUFFER_LEN));

			bptr->u.data.buffer_address_low =
				sge[2].buffer_address_low;
			bptr->u.data.buffer_address_high =
				sge[2].buffer_address_high;
		} else {
			bptr->bde_type_buflen =
				cpu_to_le32((SLI4_BDE_TYPE_BLP << 24) |
				(sgl->size & SLI4_BDE_MASK_BUFFER_LEN));
			bptr->u.blp.sgl_segment_address_low =
				cpu_to_le32(lower_32_bits(sgl->phys));
			bptr->u.blp.sgl_segment_address_high =
				cpu_to_le32(upper_32_bits(sgl->phys));
		}
	}

	trecv->relative_offset = cpu_to_le32(relative_off);

	if (flags & SLI4_IO_CONTINUATION)
		trecv->eat_xc_ccpe |= SLI4_TRCV_WQE_XC;

	trecv->xri_tag = cpu_to_le16(xri);

	trecv->context_tag = cpu_to_le16(rpi);

	/* WQE uses relative offset */
	trecv->class_ar_pu_byte |= 1 << SLI4_TRCV_WQE_PU_SHFT;

	if (flags & SLI4_IO_AUTO_GOOD_RESPONSE)
		trecv->class_ar_pu_byte |= SLI4_TRCV_WQE_AR;

	trecv->command = SLI4_WQE_FCP_TRECEIVE64;
	trecv->class_ar_pu_byte |= SLI4_GENERIC_CLASS_CLASS_3;
	trecv->dif_ct_bs_byte |=
		SLI4_GENERIC_CONTEXT_RPI << SLI4_TRCV_WQE_CT_SHFT;
	trecv->dif_ct_bs_byte |= bs << SLI4_TRCV_WQE_BS_SHFT;

	trecv->remote_xid = cpu_to_le16(xid);

	trecv->request_tag = cpu_to_le16(tag);

	trecv->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_TRCV_WQE_IOD;

	trecv->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_TRCV_WQE_LEN_LOC_BIT2;

	if (hlm) {
		trecv->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_TRCV_WQE_HLM;
		trecv->dword5.dword = cpu_to_le32(rnode_fcid & 0x00ffffff);
	}

	trecv->cmd_type_byte |= SLI4_CMD_FCP_TRECEIVE64_WQE;

	trecv->cq_id = cpu_to_le16(cq_id);

	trecv->fcp_data_receive_length = cpu_to_le32(xfer_len);

	if (sli4->perf_hint) {
		bptr = &trecv->first_data_bde;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			    (le32_to_cpu(sge[first_data_sge].buffer_length) &
			     SLI4_BDE_MASK_BUFFER_LEN));
		bptr->u.data.buffer_address_low =
			sge[first_data_sge].buffer_address_low;
		bptr->u.data.buffer_address_high =
			sge[first_data_sge].buffer_address_high;
	}

	/* The upper 7 bits of csctl is the priority */
	if (csctl & SLI4_MASK_CCP) {
		trecv->eat_xc_ccpe |= SLI4_TRCV_WQE_CCPE;
		trecv->ccp = (csctl & SLI4_MASK_CCP);
	}

	if (app_id && sli4->wqe_size == SLI4_WQE_EXT_BYTES &&
	    !(trecv->eat_xc_ccpe & SLI4_TRSP_WQE_EAT)) {
		trecv->lloc1_appid |= SLI4_TRCV_WQE_APPID;
		trecv->qosd_xbl_hlm_iod_dbde_wqes |= SLI4_TRCV_WQE_WQES;
		trecv_128->dw[31] = cpu_to_le32(app_id);
	}
	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write an FCP_CONT_TRECEIVE64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the Scatter-Gather List.
 * @param first_data_sge Index of first data sge (used if perf hints are
 * enabled)
 * @param relative_off Relative offset of the IO (if any).
 * @param xfer_len Data transfer length.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param xid OX_ID for the exchange.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rpi remote node indicator (RPI)
 * @param rnode Destination request (i.e. remote node).
 * @param flags Optional attributes, including:
 *  - ACTIVE - IO is already active.
 *  - AUTO RSP - Automatically generate a good FCP_RSP.
 * @param dif T10 DIF operation, or 0 to disable.
 * @param bs T10 DIF block size, or 0 if DIF is disabled.
 * @param csctl value of csctl field.
 * @param app_id value for VM application header.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fcp_cont_treceive64_wqe(struct sli4_s *sli4, void *buf, size_t size,
			    struct efc_dma_s *sgl, u32 first_data_sge,
			    u32 relative_off, u32 xfer_len,
			    u16 xri, u16 sec_xri, u16 tag,
			    u16 cq_id, u16 xid, u32 rpi,
			    bool hlm, u32 rnode_fcid, u32 flags,
			    u8 dif, u8 bs, u8 csctl,
			    u32 app_id)
{
	int rc;

	rc = sli_fcp_treceive64_wqe(sli4, buf, size, sgl, first_data_sge,
				    relative_off, xfer_len, xri, tag, cq_id,
				    xid, rpi, hlm, rnode_fcid, flags, dif, bs,
				    csctl, app_id);
	if (rc == 0) {
		struct sli4_fcp_treceive64_wqe_s *trecv = buf;

		trecv->command = SLI4_WQE_FCP_CONT_TRECEIVE64;
		trecv->dword5.sec_xri_tag = cpu_to_le16(sec_xri);
	}
	return rc;
}

/**
 * @ingroup sli_fc
 * @brief Write an FCP_TRSP64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the Scatter-Gather List.
 * @param rsp_len Response data length.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param xid OX_ID for the exchange.
 * @param rpi remote node indicator (RPI)
 * @param rnode Destination request (i.e. remote node).
 * @param flags Optional attributes, including:
 *  - ACTIVE - IO is already active
 *  - AUTO RSP - Automatically generate a good FCP_RSP.
 * @param csctl value of csctl field.
 * @param port_owned 0/1 to indicate if the XRI is port owned (used to seti
 * XBL=0)
 * @param app_id value for VM application header.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fcp_trsp64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		   struct efc_dma_s *sgl,
		   u32 rsp_len, u16 xri, u16 tag, u16 cq_id,
		   u16 xid, u32 rpi, bool hlm, u32 rnode_fcid,
		   u32 flags, u8 csctl, u8 port_owned,
		   u32 app_id)
{
	struct sli4_fcp_trsp64_wqe_s *trsp = buf;
	struct sli4_fcp_128byte_wqe_s *trsp_128 = buf;
	struct sli4_bde_s *bptr;

	memset(buf, 0, size);

	if (flags & SLI4_IO_AUTO_GOOD_RESPONSE) {
		trsp->class_ag_byte |= SLI4_TRSP_WQE_AG;
	} else {
		struct sli4_sge_s	*sge = sgl->virt;

		if (sli4->sgl_pre_registered || port_owned)
			trsp->qosd_xbl_hlm_dbde_wqes |= SLI4_TRSP_WQE_DBDE;
		else
			trsp->qosd_xbl_hlm_dbde_wqes |= SLI4_TRSP_WQE_XBL;
		bptr = &trsp->bde;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				     (le32_to_cpu(sge[0].buffer_length) &
				      SLI4_BDE_MASK_BUFFER_LEN));
		bptr->u.data.buffer_address_low  = sge[0].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[0].buffer_address_high;

		trsp->fcp_response_length = cpu_to_le32(rsp_len);
	}

	if (flags & SLI4_IO_CONTINUATION)
		trsp->eat_xc_ccpe |= SLI4_TRSP_WQE_XC;

	if (hlm) {
		trsp->qosd_xbl_hlm_dbde_wqes |= SLI4_TRSP_WQE_HLM;
		trsp->dword5 = cpu_to_le32(rnode_fcid & 0x00ffffff);
	}

	trsp->xri_tag = cpu_to_le16(xri);
	trsp->rpi = cpu_to_le16(rpi);

	trsp->command = SLI4_WQE_FCP_TRSP64;
	trsp->class_ag_byte |= SLI4_GENERIC_CLASS_CLASS_3;

	trsp->remote_xid = cpu_to_le16(xid);
	trsp->request_tag = cpu_to_le16(tag);
	if (flags & SLI4_IO_DNRX)
		trsp->ct_dnrx_byte |= SLI4_TRSP_WQE_DNRX;
	else
		trsp->ct_dnrx_byte &= ~SLI4_TRSP_WQE_DNRX;

	trsp->lloc1_appid |= 0x1;
	trsp->cq_id = cpu_to_le16(cq_id);
	trsp->cmd_type_byte = SLI4_CMD_FCP_TRSP64_WQE;

	/* The upper 7 bits of csctl is the priority */
	if (csctl & SLI4_MASK_CCP) {
		trsp->eat_xc_ccpe |= SLI4_TRSP_WQE_CCPE;
		trsp->ccp = (csctl & SLI4_MASK_CCP);
	}

	if (app_id && sli4->wqe_size == SLI4_WQE_EXT_BYTES &&
	    !(trsp->eat_xc_ccpe & SLI4_TRSP_WQE_EAT)) {
		trsp->lloc1_appid |= SLI4_TRSP_WQE_APPID;
		trsp->qosd_xbl_hlm_dbde_wqes |= SLI4_TRSP_WQE_WQES;
		trsp_128->dw[31] = cpu_to_le32(app_id);
	}
	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write an FCP_TSEND64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the scatter gather list.
 * @param first_data_sge Index of first data sge (used if perf hints are
 * enabled)
 * @param relative_off Relative offset of the IO (if any).
 * @param xfer_len Data transfer length.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param xid OX_ID for the exchange.
 * @param rpi remote node indicator (RPI)
 * @param rnode Destination request (i.e. remote node).
 * @param flags Optional attributes, including:
 *  - ACTIVE - IO is already active.
 *  - AUTO RSP - Automatically generate a good FCP_RSP.
 * @param dif T10 DIF operation, or 0 to disable.
 * @param bs T10 DIF block size, or 0 if DIF is disabled.
 * @param csctl value of csctl field.
 * @param app_id value for VM application header.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fcp_tsend64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    struct efc_dma_s *sgl,
		    u32 first_data_sge, u32 relative_off,
		    u32 xfer_len, u16 xri, u16 tag,
		    u16 cq_id, u16 xid, u32 rpi,
		    bool hlm, u32 rnode_fcid, u32 flags, u8 dif,
		    u8 bs, u8 csctl, u32 app_id)
{
	struct sli4_fcp_tsend64_wqe_s *tsend = buf;
	struct sli4_fcp_128byte_wqe_s *tsend_128 = buf;
	struct sli4_sge_s	*sge = NULL;
	struct sli4_bde_s *bptr;

	memset(buf, 0, size);

	if (!sgl || !sgl->virt) {
		pr_err("bad parameter sgl=%p virt=%p\n",
		       sgl, sgl ? sgl->virt : NULL);
		return -1;
	}
	sge = sgl->virt;

	bptr = &tsend->bde;
	if (sli4->sgl_pre_registered) {
		tsend->ll_qd_xbl_hlm_iod_dbde &= ~SLI4_TSEND_WQE_XBL;

		tsend->ll_qd_xbl_hlm_iod_dbde |= SLI4_TSEND_WQE_DBDE;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				   (le32_to_cpu(sge[2].buffer_length) &
				    SLI4_BDE_MASK_BUFFER_LEN));

		/* TSEND64_WQE specifies first two SGE are skipped (3rd is
		 * valid)
		 */
		bptr->u.data.buffer_address_low  = sge[2].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[2].buffer_address_high;
	} else {
		tsend->ll_qd_xbl_hlm_iod_dbde |= SLI4_TSEND_WQE_XBL;

		/* if data is a single physical address, use a BDE */
		if (!dif && xfer_len <= sge[2].buffer_length) {
			tsend->ll_qd_xbl_hlm_iod_dbde |= SLI4_TSEND_WQE_DBDE;

			bptr->bde_type_buflen =
			    cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
					(le32_to_cpu(sge[2].buffer_length) &
					SLI4_BDE_MASK_BUFFER_LEN));
			/*
			 * TSEND64_WQE specifies first two SGE are skipped
			 * (i.e. 3rd is valid)
			 */
			bptr->u.data.buffer_address_low =
				sge[2].buffer_address_low;
			bptr->u.data.buffer_address_high =
				sge[2].buffer_address_high;
		} else {
			bptr->bde_type_buflen =
				cpu_to_le32((SLI4_BDE_TYPE_BLP << 24) |
					    (sgl->size &
					     SLI4_BDE_MASK_BUFFER_LEN));
			bptr->u.blp.sgl_segment_address_low =
				cpu_to_le32(lower_32_bits(sgl->phys));
			bptr->u.blp.sgl_segment_address_high =
				cpu_to_le32(upper_32_bits(sgl->phys));
		}
	}

	tsend->relative_offset = cpu_to_le32(relative_off);

	if (flags & SLI4_IO_CONTINUATION)
		tsend->dw10byte2 |= SLI4_TSEND_XC;

	tsend->xri_tag = cpu_to_le16(xri);

	tsend->rpi = cpu_to_le16(rpi);
	/* WQE uses relative offset */
	tsend->class_pu_ar_byte |= 1 << SLI4_TSEND_WQE_PU_SHFT;

	if (flags & SLI4_IO_AUTO_GOOD_RESPONSE)
		tsend->class_pu_ar_byte |= SLI4_TSEND_WQE_AR;

	tsend->command = SLI4_WQE_FCP_TSEND64;
	tsend->class_pu_ar_byte |= SLI4_GENERIC_CLASS_CLASS_3;
	tsend->ct_byte |= SLI4_GENERIC_CONTEXT_RPI << SLI4_TSEND_CT_SHFT;
	tsend->ct_byte |= dif;
	tsend->ct_byte |= bs << SLI4_TSEND_BS_SHFT;

	tsend->remote_xid = cpu_to_le16(xid);

	tsend->request_tag = cpu_to_le16(tag);

	tsend->ll_qd_xbl_hlm_iod_dbde |= SLI4_TSEND_LEN_LOC_BIT2;

	if (hlm) {
		tsend->ll_qd_xbl_hlm_iod_dbde |= SLI4_TSEND_WQE_HLM;
		tsend->dword5 = cpu_to_le32(rnode_fcid & 0x00ffffff);
	}

	tsend->cq_id = cpu_to_le16(cq_id);

	tsend->cmd_type_byte |= SLI4_CMD_FCP_TSEND64_WQE;

	tsend->fcp_data_transmit_length = cpu_to_le32(xfer_len);

	if (sli4->perf_hint) {
		bptr = &tsend->first_data_bde;
		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			    (le32_to_cpu(sge[first_data_sge].buffer_length) &
			     SLI4_BDE_MASK_BUFFER_LEN));
		bptr->u.data.buffer_address_low =
			sge[first_data_sge].buffer_address_low;
		bptr->u.data.buffer_address_high =
			sge[first_data_sge].buffer_address_high;
	}

	/* The upper 7 bits of csctl is the priority */
	if (csctl & SLI4_MASK_CCP) {
		tsend->dw10byte2 |= SLI4_TSEND_CCPE;
		tsend->ccp = (csctl & SLI4_MASK_CCP);
	}

	if (app_id && sli4->wqe_size == SLI4_WQE_EXT_BYTES &&
	    !(tsend->dw10byte2 & SLI4_TSEND_EAT)) {
		tsend->dw10byte0 |= SLI4_TSEND_APPID_VALID;
		tsend->ll_qd_xbl_hlm_iod_dbde |= SLI4_TSEND_WQES;
		tsend_128->dw[31] = cpu_to_le32(app_id);
	}
	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write a GEN_REQUEST64 work queue entry.
 *
 * @note This WQE is only used to send FC-CT commands.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sgl DMA memory for the request.
 * @param req_len Length of request.
 * @param max_rsp_len Max length of response.
 * @param timeout Time, in seconds, before an IO times out.
 * Zero means infinite.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rnode Destination of request (that is, the remote node).
 * @param r_ctl R_CTL value for sequence.
 * @param type TYPE value for sequence.
 * @param df_ctl DF_CTL value for sequence.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_gen_request64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		      struct efc_dma_s *sgl, u32 req_len,
		      u32 max_rsp_len, u8 timeout, u16 xri,
		      u16 tag, u16 cq_id, bool hlm, u32 rnode_fcid,
		      u16 rnodeindicator, u8 r_ctl,
		      u8 type, u8 df_ctl)
{
	struct sli4_gen_request64_wqe_s	*gen = buf;
	struct sli4_sge_s	*sge = NULL;
	struct sli4_bde_s *bptr;

	memset(buf, 0, size);

	if (!sgl || !sgl->virt) {
		pr_err("bad parameter sgl=%p virt=%p\n",
		       sgl, sgl ? sgl->virt : NULL);
		return -1;
	}
	sge = sgl->virt;
	bptr = &gen->bde;

	if (sli4->sgl_pre_registered) {
		gen->dw10flags1 &= ~SLI4_GEN_REQ64_WQE_XBL;

		gen->dw10flags1 |= SLI4_GEN_REQ64_WQE_DBDE;
		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
				    (req_len & SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.data.buffer_address_low  = sge[0].buffer_address_low;
		bptr->u.data.buffer_address_high = sge[0].buffer_address_high;
	} else {
		gen->dw10flags1 |= SLI4_GEN_REQ64_WQE_XBL;

		bptr->bde_type_buflen =
			cpu_to_le32((SLI4_BDE_TYPE_BLP << 24) |
				    ((2 * sizeof(struct sli4_sge_s)) &
				     SLI4_BDE_MASK_BUFFER_LEN));

		bptr->u.blp.sgl_segment_address_low =
			cpu_to_le32(lower_32_bits(sgl->phys));
		bptr->u.blp.sgl_segment_address_high =
			cpu_to_le32(upper_32_bits(sgl->phys));
	}

	gen->request_payload_length = cpu_to_le32(req_len);
	gen->max_response_payload_length = cpu_to_le32(max_rsp_len);

	gen->df_ctl = df_ctl;
	gen->type = type;
	gen->r_ctl = r_ctl;

	gen->xri_tag = cpu_to_le16(xri);

	gen->ct_byte = SLI4_GENERIC_CONTEXT_RPI << SLI4_GEN_REQ64_CT_SHFT;
	gen->context_tag = cpu_to_le16(rnodeindicator);

	gen->class_byte = SLI4_GENERIC_CLASS_CLASS_3;

	gen->command = SLI4_WQE_GEN_REQUEST64;

	gen->timer = timeout;

	gen->request_tag = cpu_to_le16(tag);

	gen->dw10flags1 |= SLI4_GEN_REQ64_WQE_IOD;

	gen->dw10flags0 |= SLI4_GEN_REQ64_WQE_QOSD;

	if (hlm) {
		gen->dw10flags1 |= SLI4_GEN_REQ64_WQE_HLM;
		gen->remote_n_port_id_dword =
			cpu_to_le32(rnode_fcid & 0x00ffffff);
	}

	gen->cmd_type_byte = SLI4_CMD_GEN_REQUEST64_WQE;

	gen->cq_id = cpu_to_le16(cq_id);

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write a SEND_FRAME work queue entry
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param sof Start of frame value
 * @param eof End of frame value
 * @param hdr Pointer to FC header data
 * @param payload DMA memory for the payload.
 * @param req_len Length of payload.
 * @param timeout Time, in seconds, before an IO times out. Zero means infinite.
 * @param xri XRI for this exchange.
 * @param req_tag IO tag value.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_send_frame_wqe(struct sli4_s *sli4, void *buf, size_t size,
		   u8 sof, u8 eof, u32 *hdr,
			struct efc_dma_s *payload, u32 req_len,
			u8 timeout, u16 xri, u16 req_tag)
{
	struct sli4_send_frame_wqe_s *sf = buf;

	memset(buf, 0, size);

	sf->dw10flags1 |= SLI4_SF_WQE_DBDE;
	sf->bde.bde_type_buflen = cpu_to_le32(req_len &
					      SLI4_BDE_MASK_BUFFER_LEN);
	sf->bde.u.data.buffer_address_low =
		cpu_to_le32(lower_32_bits(payload->phys));
	sf->bde.u.data.buffer_address_high =
		cpu_to_le32(upper_32_bits(payload->phys));

	/* Copy FC header */
	sf->fc_header_0_1[0] = cpu_to_le32(hdr[0]);
	sf->fc_header_0_1[1] = cpu_to_le32(hdr[1]);
	sf->fc_header_2_5[0] = cpu_to_le32(hdr[2]);
	sf->fc_header_2_5[1] = cpu_to_le32(hdr[3]);
	sf->fc_header_2_5[2] = cpu_to_le32(hdr[4]);
	sf->fc_header_2_5[3] = cpu_to_le32(hdr[5]);

	sf->frame_length = cpu_to_le32(req_len);

	sf->xri_tag = cpu_to_le16(xri);
	sf->dw7flags0 &= ~SLI4_SF_PU;
	sf->context_tag = 0;

	sf->ct_byte &= ~SLI4_SF_CT;
	sf->command = SLI4_WQE_SEND_FRAME;
	sf->dw7flags0 |= SLI4_GENERIC_CLASS_CLASS_3;
	sf->timer = timeout;

	sf->request_tag = cpu_to_le16(req_tag);
	sf->eof = eof;
	sf->sof = sof;

	sf->dw10flags1 &= ~SLI4_SF_QOSD;
	sf->dw10flags0 |= SLI4_SF_LEN_LOC_BIT1;
	sf->dw10flags2 &= ~SLI4_SF_XC;

	sf->dw10flags1 |= SLI4_SF_XBL;

	sf->cmd_type_byte |= SLI4_CMD_SEND_FRAME_WQE;
	sf->cq_id = 0xffff;

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write an XMIT_BLS_RSP64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param payload Contents of the BLS payload to be sent.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param rnode Destination of request (that is, the remote node).
 * @param s_id Source ID to use in the response. If U32_MAX, use SLI Port's
 * ID.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_xmit_bls_rsp64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		       struct sli_bls_payload_s *payload, u16 xri,
		       u16 tag, u16 cq_id,
		       bool rnodeattached, bool hlm, u16 rnodeindicator,
		       u16 sportindicator, u32 rnode_fcid,
		       u32 sport_fcid, u32 s_id)
{
	struct sli4_xmit_bls_rsp_wqe_s *bls = buf;
	u32 dw_ridflags = 0;

	/*
	 * Callers can either specify RPI or S_ID, but not both
	 */
	if (rnodeattached && s_id != U32_MAX) {
		pr_info("S_ID specified for attached remote node %d\n",
			rnodeindicator);
		return -1;
	}

	memset(buf, 0, size);

	if (payload->type == SLI4_SLI_BLS_ACC) {
		bls->payload_word0 =
			cpu_to_le32((payload->u.acc.seq_id_last << 16) |
				    (payload->u.acc.seq_id_validity << 24));
		bls->high_seq_cnt = cpu_to_le16(payload->u.acc.high_seq_cnt);
		bls->low_seq_cnt = cpu_to_le16(payload->u.acc.low_seq_cnt);
	} else if (payload->type == SLI4_SLI_BLS_RJT) {
		bls->payload_word0 =
				cpu_to_le32(*((u32 *)&payload->u.rjt));
		dw_ridflags |= SLI4_BLS_RSP_WQE_AR;
	} else {
		pr_info("bad BLS type %#x\n", payload->type);
		return -1;
	}

	bls->ox_id = cpu_to_le16(payload->ox_id);
	bls->rx_id = cpu_to_le16(payload->rx_id);

	if (rnodeattached) {
		bls->dw8flags0 |=
		SLI4_GENERIC_CONTEXT_RPI << SLI4_BLS_RSP_WQE_CT_SHFT;
		bls->context_tag = cpu_to_le16(rnodeindicator);
	} else {
		bls->dw8flags0 |=
		SLI4_GENERIC_CONTEXT_VPI << SLI4_BLS_RSP_WQE_CT_SHFT;
		bls->context_tag = cpu_to_le16(sportindicator);

		if (s_id != U32_MAX)
			bls->local_n_port_id_dword |=
				cpu_to_le32(s_id & 0x00ffffff);
		else
			bls->local_n_port_id_dword |=
				cpu_to_le32(sport_fcid & 0x00ffffff);

		dw_ridflags = (dw_ridflags & ~SLI4_BLS_RSP_RID) |
			       (rnode_fcid & SLI4_BLS_RSP_RID);

		bls->temporary_rpi = cpu_to_le16(rnodeindicator);
	}

	bls->xri_tag = cpu_to_le16(xri);

	bls->dw8flags1 |= SLI4_GENERIC_CLASS_CLASS_3;

	bls->command = SLI4_WQE_XMIT_BLS_RSP;

	bls->request_tag = cpu_to_le16(tag);

	bls->dw11flags1 |= SLI4_BLS_RSP_WQE_QOSD;

	if (hlm) {
		bls->dw11flags1 |= SLI4_BLS_RSP_WQE_HLM;
		dw_ridflags = (dw_ridflags & ~SLI4_BLS_RSP_RID) |
			       (rnode_fcid & SLI4_BLS_RSP_RID);
	}

	bls->remote_id_dword = cpu_to_le32(dw_ridflags);
	bls->cq_id = cpu_to_le16(cq_id);

	bls->dw12flags0 |= SLI4_CMD_XMIT_BLS_RSP64_WQE;

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write a XMIT_ELS_RSP64_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param rsp DMA memory for the ELS response.
 * @param rsp_len Length of ELS response, in bytes.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 * @param ox_id OX_ID of the exchange containing the request.
 * @param rnode Destination of the ELS response (that is, the remote node).
 * @param flags Optional attributes, including:
 *  - SLI4_IO_CONTINUATION - IO is already active.
 * @param s_id S_ID used for special responses.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_xmit_els_rsp64_wqe(struct sli4_s *sli4, void *buf, size_t size,
		       struct efc_dma_s *rsp, u32 rsp_len,
				u16 xri, u16 tag, u16 cq_id,
				u16 ox_id, u16 rnodeindicator,
				u16 sportindicator, bool hlm,
				bool rnodeattached, u32 rnode_fcid,
				u32 flags, u32 s_id)
{
	struct sli4_xmit_els_rsp64_wqe_s	*els = buf;

	memset(buf, 0, size);

	if (sli4->sgl_pre_registered)
		els->flags2 |= SLI4_ELS_DBDE;
	else
		els->flags2 |= SLI4_ELS_XBL;

	els->els_response_payload.bde_type_buflen =
		cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			    (rsp_len & SLI4_BDE_MASK_BUFFER_LEN));
	els->els_response_payload.u.data.buffer_address_low =
		cpu_to_le32(lower_32_bits(rsp->phys));
	els->els_response_payload.u.data.buffer_address_high =
		cpu_to_le32(upper_32_bits(rsp->phys));

	els->els_response_payload_length = rsp_len;

	els->xri_tag = cpu_to_le16(xri);

	els->class_byte |= SLI4_GENERIC_CLASS_CLASS_3;

	els->command = SLI4_WQE_ELS_RSP64;

	els->request_tag = cpu_to_le16(tag);

	els->ox_id = cpu_to_le16(ox_id);

	els->flags2 |= (SLI4_ELS_IOD & SLI4_ELS_REQUEST64_DIR_WRITE);

	els->flags2 |= SLI4_ELS_QOSD;

	if (flags & SLI4_IO_CONTINUATION)
		els->flags3 |= SLI4_ELS_XC;

	if (rnodeattached) {
		els->ct_byte |=
			SLI4_GENERIC_CONTEXT_RPI << SLI4_ELS_CT_OFFSET;
		els->context_tag = cpu_to_le16(rnodeindicator);
	} else {
		els->ct_byte |=
			SLI4_GENERIC_CONTEXT_VPI << SLI4_ELS_CT_OFFSET;
		els->context_tag = cpu_to_le16(sportindicator);
		els->rid_dw = cpu_to_le32(rnode_fcid & SLI4_ELS_RID);
		els->temporary_rpi = cpu_to_le16(rnodeindicator);
		if (s_id != U32_MAX) {
			els->sid_dw |= cpu_to_le32(SLI4_ELS_SP |
						   (s_id & SLI4_ELS_SID));
		}
	}

	if (hlm) {
		els->flags2 |= SLI4_ELS_HLM;
		els->rid_dw = cpu_to_le32(rnode_fcid & SLI4_ELS_RID);
	}

	els->cmd_type_wqec = SLI4_ELS_REQUEST64_CMD_GEN;

	els->cq_id = cpu_to_le16(cq_id);

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write a XMIT_SEQUENCE64 work queue entry.
 *
 * This WQE is used to send FC-CT response frames.
 *
 * @note This API implements a restricted use for this WQE,
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param payload DMA memory for the request.
 * @param payload_len Length of request.
 * @param timeout Time, in seconds, before an IO times out.
 * Zero means infinite.
 * @param ox_id originator exchange ID
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param rnode Destination of request (that is, the remote node).
 * @param r_ctl R_CTL value for sequence.
 * @param type TYPE value for sequence.
 * @param df_ctl DF_CTL value for sequence.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_xmit_sequence64_wqe(struct sli4_s *sli4, void *buf, size_t size,
			struct efc_dma_s *payload, u32 payload_len,
		u8 timeout, u16 ox_id, u16 xri,
		u16 tag, bool hlm, u32 rnode_fcid,
		u16 rnodeindicator, u8 r_ctl,
		u8 type, u8 df_ctl)
{
	struct sli4_xmit_sequence64_wqe_s	*xmit = buf;

	memset(buf, 0, size);

	if (!payload || !payload->virt) {
		pr_err("bad parameter sgl=%p virt=%p\n",
		       payload, payload ? payload->virt : NULL);
		return -1;
	}

	if (sli4->sgl_pre_registered)
		xmit->dw10w0 |= cpu_to_le16(SLI4_SEQ_WQE_DBDE);
	else
		xmit->dw10w0 |= cpu_to_le16(SLI4_SEQ_WQE_XBL);

	xmit->bde.bde_type_buflen =
		cpu_to_le32((SLI4_BDE_TYPE_BDE_64 << 24) |
			(payload_len & SLI4_BDE_MASK_BUFFER_LEN));
	xmit->bde.u.data.buffer_address_low  =
			cpu_to_le32(lower_32_bits(payload->phys));
	xmit->bde.u.data.buffer_address_high =
			cpu_to_le32(upper_32_bits(payload->phys));
	xmit->sequence_payload_len = cpu_to_le32(payload_len);

	xmit->remote_n_port_id_dword |= rnode_fcid & 0x00ffffff;

	xmit->relative_offset = 0;

	/* sequence initiative - this matches what is seen from
	 * FC switches in response to FCGS commands
	 */
	xmit->dw5flags0 &= (~SLI4_SEQ_WQE_SI);
	xmit->dw5flags0 &= (~SLI4_SEQ_WQE_FT);/* force transmit */
	xmit->dw5flags0 &= (~SLI4_SEQ_WQE_XO);/* exchange responder */
	xmit->dw5flags0 |= SLI4_SEQ_WQE_LS;/* last in seqence */
	xmit->df_ctl = df_ctl;
	xmit->type = type;
	xmit->r_ctl = r_ctl;

	xmit->xri_tag = cpu_to_le16(xri);
	xmit->context_tag = cpu_to_le16(rnodeindicator);

	xmit->dw7flags0 &= (~SLI4_SEQ_WQE_DIF);
	xmit->dw7flags0 |=
		SLI4_GENERIC_CONTEXT_RPI << SLI4_SEQ_WQE_CT_SHIFT;
	xmit->dw7flags0 &= (~SLI4_SEQ_WQE_BS);

	xmit->command = SLI4_WQE_XMIT_SEQUENCE64;
	xmit->dw7flags1 |= SLI4_GENERIC_CLASS_CLASS_3;
	xmit->dw7flags1 &= (~SLI4_SEQ_WQE_PU);
	xmit->timer = timeout;

	xmit->abort_tag = 0;
	xmit->request_tag = cpu_to_le16(tag);
	xmit->remote_xid = cpu_to_le16(ox_id);

	xmit->dw10w0 |=
	cpu_to_le16(SLI4_ELS_REQUEST64_DIR_READ << SLI4_SEQ_WQE_IOD_SHIFT);

	if (hlm) {
		xmit->dw10w0 |= cpu_to_le16(SLI4_SEQ_WQE_HLM);
		xmit->remote_n_port_id_dword |= rnode_fcid & 0x00ffffff;
	}

	xmit->cmd_type_wqec_byte |= SLI4_CMD_XMIT_SEQUENCE64_WQE;

	xmit->dw10w0 |= cpu_to_le16(2 << SLI4_SEQ_WQE_LEN_LOC_SHIFT);

	xmit->cq_id = cpu_to_le16(0xFFFF);

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Write a REQUEUE_XRI_WQE work queue entry.
 *
 * @param sli4 SLI context.
 * @param buf Destination buffer for the WQE.
 * @param size Buffer size, in bytes.
 * @param xri XRI for this exchange.
 * @param tag IO tag value.
 * @param cq_id The id of the completion queue where the WQE response is sent.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_requeue_xri_wqe(struct sli4_s *sli4, void *buf, size_t size,
		    u16 xri, u16 tag, u16 cq_id)
{
	struct sli4_requeue_xri_wqe_s	*requeue = buf;

	memset(buf, 0, size);

	requeue->command = SLI4_WQE_REQUEUE_XRI;
	requeue->xri_tag = cpu_to_le16(xri);
	requeue->request_tag = cpu_to_le16(tag);
	requeue->flags2 |= SLI4_REQU_XRI_WQE_XC;
	requeue->flags1 |= SLI4_REQU_XRI_WQE_QOSD;
	requeue->cq_id = cpu_to_le16(cq_id);
	requeue->cmd_type_wqec_byte = SLI4_CMD_REQUEUE_XRI_WQE;
	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Process an asynchronous Link State event entry.
 *
 * @par Description
 * Parses Asynchronous Completion Queue Entry (ACQE),
 * creates an abstracted event, and calls registered callback functions.
 *
 * @param sli4 SLI context.
 * @param acqe Pointer to the ACQE.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fc_process_link_state(struct sli4_s *sli4, void *acqe)
{
	struct sli4_link_state_s	*link_state = acqe;
	struct sli4_link_event_s	event = { 0 };
	int	rc = 0;
	u8 link_type = (link_state->link_num_type & SLI4_LINK_TYPE) >> 6;

	if (!sli4->link) {
		/* bail if there is no callback */
		return 0;
	}

	if (link_type == SLI4_SLI4_LINK_TYPE_ETHERNET) {
		event.topology = SLI_LINK_TOPO_NPORT;
		event.medium   = SLI_LINK_MEDIUM_ETHERNET;
	} else {
		pr_info("unsupported link type %#x\n",
			link_type);
		event.topology = SLI_LINK_TOPO_MAX;
		event.medium   = SLI_LINK_MEDIUM_MAX;
		rc = -1;
	}

	switch (link_state->port_link_status) {
	case SLI4_PORT_LINK_STATUS_PHYSICAL_DOWN:
	case SLI4_PORT_LINK_STATUS_LOGICAL_DOWN:
		event.status = SLI_LINK_STATUS_DOWN;
		break;
	case SLI4_PORT_LINK_STATUS_PHYSICAL_UP:
	case SLI4_PORT_LINK_STATUS_LOGICAL_UP:
		event.status = SLI_LINK_STATUS_UP;
		break;
	default:
		pr_info("unsupported link status %#x\n",
			link_state->port_link_status);
		event.status = SLI_LINK_STATUS_MAX;
		rc = -1;
	}

	switch (link_state->port_speed) {
	case 0:
		event.speed = 0;
		break;
	case 1:
		event.speed = 10;
		break;
	case 2:
		event.speed = 100;
		break;
	case 3:
		event.speed = 1000;
		break;
	case 4:
		event.speed = 10000;
		break;
	case 5:
		event.speed = 20000;
		break;
	case 6:
		event.speed = 25000;
		break;
	case 7:
		event.speed = 40000;
		break;
	case 8:
		event.speed = 100000;
		break;
	default:
		pr_info("unsupported port_speed %#x\n",
			link_state->port_speed);
		rc = -1;
	}

	sli4->link(sli4->link_arg, (void *)&event);

	return rc;
}

/**
 * @ingroup sli_fc
 * @brief Process an asynchronous Link Attention event entry.
 *
 * @par Description
 * Parses Asynchronous Completion Queue Entry (ACQE),
 * creates an abstracted event, and calls the registered callback functions.
 *
 * @param sli4 SLI context.
 * @param acqe Pointer to the ACQE.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fc_process_link_attention(struct sli4_s *sli4, void *acqe)
{
	struct sli4_link_attention_s	*link_attn = acqe;
	struct sli4_link_event_s	event = { 0 };

	pr_info("link=%d attn_type=%#x top=%#x speed=%#x pfault=%#x\n",
		link_attn->link_number, link_attn->attn_type,
		      link_attn->topology, link_attn->port_speed,
		      link_attn->port_fault);
	pr_info("shared_lnk_status=%#x logl_lnk_speed=%#x evnttag=%#x\n",
		link_attn->shared_link_status,
		      le16_to_cpu(link_attn->logical_link_speed),
		      le32_to_cpu(link_attn->event_tag));

	if (!sli4->link)
		return 0;

	event.medium   = SLI_LINK_MEDIUM_FC;

	switch (link_attn->attn_type) {
	case SLI4_LINK_ATTN_TYPE_LINK_UP:
		event.status = SLI_LINK_STATUS_UP;
		break;
	case SLI4_LINK_ATTN_TYPE_LINK_DOWN:
		event.status = SLI_LINK_STATUS_DOWN;
		break;
	case SLI4_LINK_ATTN_TYPE_NO_HARD_ALPA:
		pr_info("attn_type: no hard alpa\n");
		event.status = SLI_LINK_STATUS_NO_ALPA;
		break;
	default:
		pr_info("attn_type: unknown\n");
		break;
	}

	switch (link_attn->event_type) {
	case SLI4_FC_EVENT_LINK_ATTENTION:
		break;
	case SLI4_FC_EVENT_SHARED_LINK_ATTENTION:
		pr_info("event_type: FC shared link event\n");
		break;
	default:
		pr_info("event_type: unknown\n");
		break;
	}

	switch (link_attn->topology) {
	case SLI4_LINK_ATTN_P2P:
		event.topology = SLI_LINK_TOPO_NPORT;
		break;
	case SLI4_LINK_ATTN_FC_AL:
		event.topology = SLI_LINK_TOPO_LOOP;
		break;
	case SLI4_LINK_ATTN_INTERNAL_LOOPBACK:
		pr_info("topology Internal loopback\n");
		event.topology = SLI_LINK_TOPO_LOOPBACK_INTERNAL;
		break;
	case SLI4_LINK_ATTN_SERDES_LOOPBACK:
		pr_info("topology serdes loopback\n");
		event.topology = SLI_LINK_TOPO_LOOPBACK_EXTERNAL;
		break;
	default:
		pr_info("topology: unknown\n");
		break;
	}

	event.speed    = link_attn->port_speed * 1000;

	sli4->link(sli4->link_arg, (void *)&event);

	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Parse an FC/FCoE work queue CQ entry.
 *
 * @param sli4 SLI context.
 * @param cq CQ to process.
 * @param cqe Pointer to the CQ entry.
 * @param etype CQ event type.
 * @param r_id Resource ID associated with this completion message (such as the
 * IO tag).
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fc_cqe_parse(struct sli4_s *sli4, struct sli4_queue_s *cq,
		 u8 *cqe, enum sli4_qentry_e *etype, u16 *r_id)
{
	u8		code = cqe[SLI4_CQE_CODE_OFFSET];
	int		rc = -1;

	switch (code) {
	case SLI4_CQE_CODE_WORK_REQUEST_COMPLETION:
	{
		struct sli4_fc_wcqe_s *wcqe = (void *)cqe;

		*etype = SLI_QENTRY_WQ;
		*r_id = le16_to_cpu(wcqe->request_tag);
		rc = wcqe->status;

		/* Flag errors except for FCP_RSP_FAILURE */
		if (rc && rc != SLI4_FC_WCQE_STATUS_FCP_RSP_FAILURE) {
			pr_info("WCQE: status=%#x hw_status=%#x tag=%#x\n",
				wcqe->status, wcqe->hw_status,
				le16_to_cpu(wcqe->request_tag));
			pr_info("w1=%#x w2=%#x xb=%d\n",
				le32_to_cpu(wcqe->wqe_specific_1),
				     le32_to_cpu(wcqe->wqe_specific_2),
				     (wcqe->flags & SLI4_WCQE_XB));
			pr_info("      %08X %08X %08X %08X\n",
				((u32 *)cqe)[0],
				     ((u32 *)cqe)[1],
				     ((u32 *)cqe)[2],
				     ((u32 *)cqe)[3]);
		}

		break;
	}
	case SLI4_CQE_CODE_RQ_ASYNC:
	{
		struct sli4_fc_async_rcqe_s *rcqe = (void *)cqe;

		*etype = SLI_QENTRY_RQ;
		*r_id = le16_to_cpu(rcqe->fcfi_rq_id_word) & SLI4_RACQE_RQ_ID;
		rc = rcqe->status;
		break;
	}
	case SLI4_CQE_CODE_RQ_ASYNC_V1:
	{
		struct sli4_fc_async_rcqe_v1_s *rcqe = (void *)cqe;

		*etype = SLI_QENTRY_RQ;
		*r_id = rcqe->rq_id;
		rc = rcqe->status;
		break;
	}
	case SLI4_CQE_CODE_OPTIMIZED_WRITE_CMD:
	{
		struct sli4_fc_optimized_write_cmd_cqe_s *optcqe = (void *)cqe;

		*etype = SLI_QENTRY_OPT_WRITE_CMD;
		*r_id = le16_to_cpu(optcqe->rq_id);
		rc = optcqe->status;
		break;
	}
	case SLI4_CQE_CODE_OPTIMIZED_WRITE_DATA:
	{
		struct sli4_fc_optimized_write_data_cqe_s *dcqe = (void *)cqe;

		*etype = SLI_QENTRY_OPT_WRITE_DATA;
		*r_id = le16_to_cpu(dcqe->xri);
		rc = dcqe->status;

		/* Flag errors */
		if (rc != SLI4_FC_WCQE_STATUS_SUCCESS) {
			pr_info("Optimized DATA CQE: status=%#x\n",
				dcqe->status);
			pr_info("hstat=%#x xri=%#x dpl=%#x w3=%#x xb=%d\n",
				dcqe->hw_status, le16_to_cpu(dcqe->xri),
				le32_to_cpu(dcqe->total_data_placed),
				((u32 *)cqe)[3],
				(dcqe->flags & SLI4_OCQE_XB));
		}
		break;
	}
	case SLI4_CQE_CODE_RQ_COALESCING:
	{
		struct sli4_fc_coalescing_rcqe_s *rcqe = (void *)cqe;

		*etype = SLI_QENTRY_RQ;
		*r_id = le16_to_cpu(rcqe->rq_id);
		rc = rcqe->status;
		break;
	}
	case SLI4_CQE_CODE_XRI_ABORTED:
	{
		struct sli4_fc_xri_aborted_cqe_s *xa = (void *)cqe;

		*etype = SLI_QENTRY_XABT;
		*r_id = le16_to_cpu(xa->xri);
		rc = 0;
		break;
	}
	case SLI4_CQE_CODE_RELEASE_WQE: {
		struct sli4_fc_wqec_s *wqec = (void *)cqe;

		*etype = SLI_QENTRY_WQ_RELEASE;
		*r_id = le16_to_cpu(wqec->wq_id);
		rc = 0;
		break;
	}
	default:
		pr_info("CQE completion code %d not handled\n",
			code);
		*etype = SLI_QENTRY_MAX;
		*r_id = U16_MAX;
	}

	return rc;
}

/**
 * @ingroup sli_fc
 * @brief Return the ELS/CT response length.
 *
 * @param sli4 SLI context.
 * @param cqe Pointer to the CQ entry.
 *
 * @return Returns the length, in bytes.
 */
u32
sli_fc_response_length(struct sli4_s *sli4, u8 *cqe)
{
	struct sli4_fc_wcqe_s *wcqe = (void *)cqe;

	return le32_to_cpu(wcqe->wqe_specific_1);
}

/**
 * @ingroup sli_fc
 * @brief Return the FCP IO length.
 *
 * @param sli4 SLI context.
 * @param cqe Pointer to the CQ entry.
 *
 * @return Returns the length, in bytes.
 */
u32
sli_fc_io_length(struct sli4_s *sli4, u8 *cqe)
{
	struct sli4_fc_wcqe_s *wcqe = (void *)cqe;

	return le32_to_cpu(wcqe->wqe_specific_1);
}

/**
 * @ingroup sli_fc
 * @brief Retrieve the D_ID from the completion.
 *
 * @param sli4 SLI context.
 * @param cqe Pointer to the CQ entry.
 * @param d_id Pointer where the D_ID is written.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fc_els_did(struct sli4_s *sli4, u8 *cqe, u32 *d_id)
{
	struct sli4_fc_wcqe_s *wcqe = (void *)cqe;

	*d_id = 0;

	if (wcqe->status)
		return -1;
	*d_id = le32_to_cpu(wcqe->wqe_specific_2) & 0x00ffffff;
	return 0;
}

u32
sli_fc_ext_status(struct sli4_s *sli4, u8 *cqe)
{
	struct sli4_fc_wcqe_s *wcqe = (void *)cqe;
	u32	mask;

	switch (wcqe->status) {
	case SLI4_FC_WCQE_STATUS_FCP_RSP_FAILURE:
		mask = U32_MAX;
		break;
	case SLI4_FC_WCQE_STATUS_LOCAL_REJECT:
	case SLI4_FC_WCQE_STATUS_CMD_REJECT:
		mask = 0xff;
		break;
	case SLI4_FC_WCQE_STATUS_NPORT_RJT:
	case SLI4_FC_WCQE_STATUS_FABRIC_RJT:
	case SLI4_FC_WCQE_STATUS_NPORT_BSY:
	case SLI4_FC_WCQE_STATUS_FABRIC_BSY:
	case SLI4_FC_WCQE_STATUS_LS_RJT:
		mask = U32_MAX;
		break;
	case SLI4_FC_WCQE_STATUS_DI_ERROR:
		mask = U32_MAX;
		break;
	default:
		mask = 0;
	}

	return le32_to_cpu(wcqe->wqe_specific_2) & mask;
}

/**
 * @ingroup sli_fc
 * @brief Retrieve the RQ index from the completion.
 *
 * @param sli4 SLI context.
 * @param cqe Pointer to the CQ entry.
 * @param rq_id Pointer where the rq_id is written.
 * @param index Pointer where the index is written.
 *
 * @return Returns 0 on success, or a non-zero value on failure.
 */
int
sli_fc_rqe_rqid_and_index(struct sli4_s *sli4, u8 *cqe,
			  u16 *rq_id, u32 *index)
{
	struct sli4_fc_async_rcqe_s	*rcqe = (void *)cqe;
	struct sli4_fc_async_rcqe_v1_s	*rcqe_v1 = (void *)cqe;
	int	rc = -1;
	u8	code = 0;
	u16 rq_element_index;

	*rq_id = 0;
	*index = U32_MAX;

	code = cqe[SLI4_CQE_CODE_OFFSET];

	if (code == SLI4_CQE_CODE_RQ_ASYNC) {
		*rq_id = le16_to_cpu(rcqe->fcfi_rq_id_word) & SLI4_RACQE_RQ_ID;
		rq_element_index =
		le16_to_cpu(rcqe->rq_elmt_indx_word) & SLI4_RACQE_RQ_EL_INDX;
		*index = rq_element_index;
		if (rcqe->status == SLI4_FC_ASYNC_RQ_SUCCESS) {
			rc = 0;
		} else {
			rc = rcqe->status;
			pr_info("status=%02x (%s) rq_id=%d\n",
				rcqe->status,
				sli_fc_get_status_string(rcqe->status),
				le16_to_cpu(rcqe->fcfi_rq_id_word) &
				SLI4_RACQE_RQ_ID);

			pr_info("pdpl=%x sof=%02x eof=%02x hdpl=%x\n",
				le16_to_cpu(rcqe->data_placement_length),
				rcqe->sof_byte, rcqe->eof_byte,
				rcqe->hdpl_byte & SLI4_RACQE_HDPL);
		}
	} else if (code == SLI4_CQE_CODE_RQ_ASYNC_V1) {
		*rq_id = le16_to_cpu(rcqe_v1->rq_id);
		rq_element_index =
			(le16_to_cpu(rcqe_v1->rq_elmt_indx_word) &
			 SLI4_RACQE_RQ_EL_INDX);
		*index = rq_element_index;
		if (rcqe_v1->status == SLI4_FC_ASYNC_RQ_SUCCESS) {
			rc = 0;
		} else {
			rc = rcqe_v1->status;
			pr_info("status=%02x (%s) rq_id=%d, index=%x\n",
				rcqe_v1->status,
				sli_fc_get_status_string(rcqe_v1->status),
				le16_to_cpu(rcqe_v1->rq_id), rq_element_index);

			pr_info("pdpl=%x sof=%02x eof=%02x hdpl=%x\n",
				le16_to_cpu(rcqe_v1->data_placement_length),
			rcqe_v1->sof_byte, rcqe_v1->eof_byte,
			rcqe_v1->hdpl_byte & SLI4_RACQE_HDPL);
		}
	} else if (code == SLI4_CQE_CODE_OPTIMIZED_WRITE_CMD) {
		struct sli4_fc_optimized_write_cmd_cqe_s *optcqe = (void *)cqe;

		*rq_id = le16_to_cpu(optcqe->rq_id);
		*index = le16_to_cpu(optcqe->w1) & SLI4_OCQE_RQ_EL_INDX;
		if (optcqe->status == SLI4_FC_ASYNC_RQ_SUCCESS) {
			rc = 0;
		} else {
			rc = optcqe->status;
			pr_info("stat=%02x (%s) rqid=%d, idx=%x pdpl=%x\n",
				optcqe->status,
				sli_fc_get_status_string(optcqe->status),
				le16_to_cpu(optcqe->rq_id), *index,
				le16_to_cpu(optcqe->data_placement_length));

			pr_info("hdpl=%x oox=%d agxr=%d xri=0x%x rpi=%x\n",
				(optcqe->hdpl_vld & SLI4_OCQE_HDPL),
				(optcqe->flags1 & SLI4_OCQE_OOX),
				(optcqe->flags1 & SLI4_OCQE_AGXR), optcqe->xri,
				le16_to_cpu(optcqe->rpi));
		}
	} else if (code == SLI4_CQE_CODE_RQ_COALESCING) {
		struct sli4_fc_coalescing_rcqe_s	*rcqe = (void *)cqe;
		u16 rq_element_index =
				(le16_to_cpu(rcqe->rq_elmt_indx_word) &
				 SLI4_RCQE_RQ_EL_INDX);

		*rq_id = le16_to_cpu(rcqe->rq_id);
		if (rcqe->status == SLI4_FC_COALESCE_RQ_SUCCESS) {
			*index = rq_element_index;
			rc = 0;
		} else {
			*index = U32_MAX;
			rc = rcqe->status;

			pr_info("stat=%02x (%s) rq_id=%d, idx=%x\n",
				rcqe->status,
				sli_fc_get_status_string(rcqe->status),
				le16_to_cpu(rcqe->rq_id), rq_element_index);
			pr_info("rq_id=%#x sdpl=%x\n",
				le16_to_cpu(rcqe->rq_id),
		    le16_to_cpu(rcqe->sequence_reporting_placement_length));
		}
	} else {
		*index = U32_MAX;

		rc = rcqe->status;

		pr_info("status=%02x rq_id=%d, index=%x pdpl=%x\n",
			rcqe->status,
		le16_to_cpu(rcqe->fcfi_rq_id_word) & SLI4_RACQE_RQ_ID,
		(le16_to_cpu(rcqe->rq_elmt_indx_word) & SLI4_RACQE_RQ_EL_INDX),
		le16_to_cpu(rcqe->data_placement_length));
		pr_info("sof=%02x eof=%02x hdpl=%x\n",
			rcqe->sof_byte, rcqe->eof_byte,
			rcqe->hdpl_byte & SLI4_RACQE_HDPL);
	}

	return rc;
}

/**
 * @ingroup sli_fc
 * @brief Allocate a receive queue.
 *
 * @par Description
 * Allocates DMA memory and configures the requested queue type.
 *
 * @param sli4 SLI context.
 * @param q Pointer to the queue object for the header.
 * @param n_entries Number of entries to allocate.
 * @param buffer_size buffer size for the queue.
 * @param cq Associated CQ.
 * @param ulp The ULP to bind
 * @param is_hdr Used to validate the rq_id and set the type of queue
 *
 * @return Returns 0 on success, or -1 on failure.
 */
int
sli_fc_rq_alloc(struct sli4_s *sli4, struct sli4_queue_s *q,
		u32 n_entries, u32 buffer_size,
		struct sli4_queue_s *cq, u16 ulp, bool is_hdr)
{
	int (*rq_create)(struct sli4_s *sli4, void *buff, size_t size,
			 struct efc_dma_s *qmem, u16 cqid,
			     u16 ulp, u16 buff_size);

	if (!sli4 || !q) {
		pr_err("bad parameter sli4=%p q=%p\n", sli4, q);
		return -1;
	}

	if (__sli_queue_init(sli4, q, SLI_QTYPE_RQ, SLI4_RQE_SIZE,
			     n_entries, SLI_PAGE_SIZE)) {
		return -1;
	}

	rq_create = sli_cmd_rq_create_v1;

	if (rq_create(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE, &q->dma,
		      cq->id, ulp, buffer_size)) {
		if (__sli_create_queue(sli4, q)) {
			dma_free_coherent(&sli4->pdev->dev, q->dma.size,
					  q->dma.virt, q->dma.phys);
			return -1;
		}
		if (is_hdr && q->id & 1) {
			pr_info("bad header RQ_ID %d\n", q->id);
			dma_free_coherent(&sli4->pdev->dev, q->dma.size,
					  q->dma.virt, q->dma.phys);
			return -1;
		} else if (!is_hdr  && (q->id & 1) == 0) {
			pr_info("bad data RQ_ID %d\n", q->id);
			dma_free_coherent(&sli4->pdev->dev, q->dma.size,
					  q->dma.virt, q->dma.phys);
			return -1;
		}
	} else {
		return -1;
	}
	if (is_hdr)
		q->u.flag.dword |= SLI4_QUEUE_FLAG_HDR;
	else
		q->u.flag.dword &= ~SLI4_QUEUE_FLAG_HDR;
	return 0;
}

/**
 * @ingroup sli_fc
 * @brief Allocate a receive queue set.
 *
 * @param sli4 SLI context.
 * @param num_rq_pairs to create
 * @param qs Pointers to the queue objects for both header and data.
 *	Length of this arrays should be 2 * num_rq_pairs
 * @param base_cq_id. Assumes base_cq_id : (base_cq_id + num_rq_pairs) cqs as
 * allotted.
 * @param n_entries number of entries in each RQ queue.
 * @param header_buffer_size
 * @param payload_buffer_size
 * @param ulp The ULP to bind
 *
 * @return Returns 0 on success, or -1 on failure.
 */
int
sli_fc_rq_set_alloc(struct sli4_s *sli4, u32 num_rq_pairs,
		    struct sli4_queue_s *qs[], u32 base_cq_id,
		    u32 n_entries, u32 header_buffer_size,
		    u32 payload_buffer_size,  u16 ulp)
{
	u32 i, p, offset = 0;
	u32 payload_size, total_page_count = 0;
	u32 pc = 0;
	u16 num_pages;
	uintptr_t addr;
	struct efc_dma_s dma;
	struct sli4_res_common_create_queue_set_s *rsp = NULL;
	struct sli4_req_rq_create_v2_s    *req = NULL;
	void __iomem *db_regaddr = NULL;

	for (i = 0; i < (num_rq_pairs * 2); i++) {
		if (__sli_queue_init(sli4, qs[i], SLI_QTYPE_RQ,
				     SLI4_RQE_SIZE, n_entries,
				     SLI_PAGE_SIZE))
			goto error;
	}

	pc =  sli_page_count(qs[0]->dma.size, SLI_PAGE_SIZE);
	total_page_count = pc * num_rq_pairs * 2;

	/* Payload length must accommodate both request and response */
	payload_size = max((sizeof(struct sli4_req_rq_create_v1_s) +
			   (8 * total_page_count)),
			   sizeof(struct sli4_res_common_create_queue_set_s));

	dma.size = payload_size;
	dma.virt = dma_alloc_coherent(&sli4->pdev->dev, dma.size,
				      &dma.phys, GFP_DMA);
	if (!dma.virt) {
		memset(&dma, 0, sizeof(struct efc_dma_s));
		pr_err("DMA allocation failed\n");
		goto error;
	}
	memset(dma.virt, 0, payload_size);

	if (sli_cmd_sli_config(sli4, sli4->bmbx.virt, SLI4_BMBX_SIZE,
			       payload_size, &dma) == -1)
		goto error;

	req = (struct sli4_req_rq_create_v2_s *)((u8 *)dma.virt);

	/* Fill Header fields */
	req->hdr.opcode    = SLI4_OPC_RQ_CREATE;
	req->hdr.subsystem = SLI4_SUBSYSTEM_FC;
	req->hdr.dw3_version   = cpu_to_le32(2);
	req->hdr.request_length =
		cpu_to_le32(sizeof(struct sli4_req_rq_create_v2_s) -
			    sizeof(struct sli4_req_hdr_s) +
			    (8 * total_page_count));

	/* Fill Payload fields */
	req->dim_dfd_dnb  |= SLI4_RQCREATEV2_DNB;
	num_pages = sli_page_count(qs[0]->dma.size, SLI_PAGE_SIZE);
	req->num_pages	   = cpu_to_le16(num_pages);
	req->rqe_count     = cpu_to_le16(qs[0]->dma.size / SLI4_RQE_SIZE);
	req->rqe_size_byte |= SLI4_RQE_SIZE_8;
	req->page_size     = SLI4_RQ_PAGE_SIZE_4096;
	req->rq_count      = num_rq_pairs * 2;
	req->base_cq_id    = cpu_to_le16(base_cq_id);
	req->hdr_buffer_size     = cpu_to_le16(header_buffer_size);
	req->payload_buffer_size = cpu_to_le16(payload_buffer_size);

	for (i = 0; i < (num_rq_pairs * 2); i++) {
		for (p = 0, addr = qs[i]->dma.phys; p < num_pages;
		     p++, addr += SLI_PAGE_SIZE) {
			req->page_physical_address[offset].low =
					cpu_to_le32(lower_32_bits(addr));
			req->page_physical_address[offset].high =
					cpu_to_le32(upper_32_bits(addr));
			offset++;
		}
	}

	if (sli_bmbx_command(sli4)) {
		pr_err("bootstrap mailbox write failed RQSet\n");
		goto error;
	}

	if (sli4->if_type == SLI4_INTF_IF_TYPE_6)
		db_regaddr = sli4->reg[1] + SLI4_IF6_RQ_DOORBELL_REGOFFSET;
	else
		db_regaddr = sli4->reg[0] + SLI4_RQ_DOORBELL_REGOFFSET;

	rsp = (void *)((u8 *)dma.virt);
	if (rsp->hdr.status) {
		pr_err("bad create RQSet status=%#x addl=%#x\n",
		       rsp->hdr.status, rsp->hdr.additional_status);
		goto error;
	} else {
		for (i = 0; i < (num_rq_pairs * 2); i++) {
			qs[i]->id = i + le16_to_cpu(rsp->q_id);
			if ((qs[i]->id & 1) == 0)
				qs[i]->u.flag.dword |= SLI4_QUEUE_FLAG_HDR;
			else
				qs[i]->u.flag.dword &= ~SLI4_QUEUE_FLAG_HDR;

			qs[i]->db_regaddr = db_regaddr;
		}
	}

	dma_free_coherent(&sli4->pdev->dev, dma.size, dma.virt, dma.phys);

	return 0;

error:
	for (i = 0; i < (num_rq_pairs * 2); i++) {
		if (qs[i]->dma.size)
			dma_free_coherent(&sli4->pdev->dev, qs[i]->dma.size,
					  qs[i]->dma.virt, qs[i]->dma.phys);
	}

	if (dma.size)
		dma_free_coherent(&sli4->pdev->dev, dma.size, dma.virt,
				  dma.phys);

	return -1;
}

/**
 * @ingroup sli_fc
 * @brief Get the RPI resource requirements.
 *
 * @param sli4 SLI context.
 * @param n_rpi Number of RPIs desired.
 *
 * @return Returns the number of bytes needed. This value may be zero.
 */
u32
sli_fc_get_rpi_requirements(struct sli4_s *sli4, u32 n_rpi)
{
	u32	bytes = 0;

	/* Check if header templates needed */
	if (sli4->hdr_template_req)
		/* round up to a page */
		bytes = SLI_ROUND_PAGE(n_rpi * SLI4_HDR_TEMPLATE_SIZE);

	return bytes;
}

/**
 * @ingroup sli_fc
 * @brief Return a text string corresponding to a CQE status value
 *
 * @param status Status value
 *
 * @return Returns corresponding string, otherwise "unknown"
 */
const char *
sli_fc_get_status_string(u32 status)
{
	static struct {
		u32 code;
		const char *label;
	} lookup[] = {
		{SLI4_FC_WCQE_STATUS_SUCCESS,		"SUCCESS"},
		{SLI4_FC_WCQE_STATUS_FCP_RSP_FAILURE,	"FCP_RSP_FAILURE"},
		{SLI4_FC_WCQE_STATUS_REMOTE_STOP,	"REMOTE_STOP"},
		{SLI4_FC_WCQE_STATUS_LOCAL_REJECT,	"LOCAL_REJECT"},
		{SLI4_FC_WCQE_STATUS_NPORT_RJT,		"NPORT_RJT"},
		{SLI4_FC_WCQE_STATUS_FABRIC_RJT,	"FABRIC_RJT"},
		{SLI4_FC_WCQE_STATUS_NPORT_BSY,		"NPORT_BSY"},
		{SLI4_FC_WCQE_STATUS_FABRIC_BSY,	"FABRIC_BSY"},
		{SLI4_FC_WCQE_STATUS_LS_RJT,		"LS_RJT"},
		{SLI4_FC_WCQE_STATUS_CMD_REJECT,	"CMD_REJECT"},
		{SLI4_FC_WCQE_STATUS_FCP_TGT_LENCHECK,	"FCP_TGT_LENCHECK"},
		{SLI4_FC_WCQE_STATUS_RQ_BUF_LEN_EXCEEDED, "BUF_LEN_EXCEEDED"},
		{SLI4_FC_WCQE_STATUS_RQ_INSUFF_BUF_NEEDED,
				"RQ_INSUFF_BUF_NEEDED"},
		{SLI4_FC_WCQE_STATUS_RQ_INSUFF_FRM_DISC, "RQ_INSUFF_FRM_DESC"},
		{SLI4_FC_WCQE_STATUS_RQ_DMA_FAILURE,	"RQ_DMA_FAILURE"},
		{SLI4_FC_WCQE_STATUS_FCP_RSP_TRUNCATE,	"FCP_RSP_TRUNCATE"},
		{SLI4_FC_WCQE_STATUS_DI_ERROR,		"DI_ERROR"},
		{SLI4_FC_WCQE_STATUS_BA_RJT,		"BA_RJT"},
		{SLI4_FC_WCQE_STATUS_RQ_INSUFF_XRI_NEEDED,
				"RQ_INSUFF_XRI_NEEDED"},
		{SLI4_FC_WCQE_STATUS_RQ_INSUFF_XRI_DISC, "INSUFF_XRI_DISC"},
		{SLI4_FC_WCQE_STATUS_RX_ERROR_DETECT,	"RX_ERROR_DETECT"},
		{SLI4_FC_WCQE_STATUS_RX_ABORT_REQUEST,	"RX_ABORT_REQUEST"},
		};
	u32 i;

	for (i = 0; i < ARRAY_SIZE(lookup); i++) {
		if (status == lookup[i].code)
			return lookup[i].label;
	}
	return "unknown";
}
