/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__EFCT_ELS_H__)
#define __EFCT_ELS_H__

enum efct_els_role_e {
	EFCT_ELS_ROLE_ORIGINATOR,
	EFCT_ELS_ROLE_RESPONDER,
};

void _efct_els_io_free(struct kref *arg);
extern struct efct_io_s *
efct_els_io_alloc(struct efc_node_s *node, u32 reqlen,
		  enum efct_els_role_e role);
extern struct efct_io_s *
efct_els_io_alloc_size(struct efc_node_s *node, u32 reqlen,
		       u32 rsplen,
				       enum efct_els_role_e role);
void efct_els_io_free(struct efct_io_s *els);

extern void *
efct_els_req_send(struct efc_lport *efc, struct efc_node_s *node,
		  u32 cmd, u32 timeout_sec, u32 retries);
extern void *
efct_els_send_ct(struct efc_lport *efc, struct efc_node_s *node,
		 u32 cmd, u32 timeout_sec, u32 retries);
extern void *
efct_els_resp_send(struct efc_lport *efc, struct efc_node_s *node,
		   u32 cmd, u16 ox_id);
void
efct_els_abort(struct efct_io_s *els, struct efc_node_cb_s *arg);
/* ELS command send */
typedef void (*els_cb_t)(struct efc_node_s *node,
			 struct efc_node_cb_s *cbdata, void *arg);
extern struct efct_io_s *
efct_send_plogi(struct efc_node_s *node, u32 timeout_sec,
		u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_flogi(struct efc_node_s *node, u32 timeout_sec,
		u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_fdisc(struct efc_node_s *node, u32 timeout_sec,
		u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_prli(struct efc_node_s *node, u32 timeout_sec,
	       u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_prlo(struct efc_node_s *node, u32 timeout_sec,
	       u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_logo(struct efc_node_s *node, u32 timeout_sec,
	       u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_adisc(struct efc_node_s *node, u32 timeout_sec,
		u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_pdisc(struct efc_node_s *node, u32 timeout_sec,
		u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_scr(struct efc_node_s *node, u32 timeout_sec,
	      u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_rrq(struct efc_node_s *node, u32 timeout_sec,
	      u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_ns_send_rftid(struct efc_node_s *node,
		   u32 timeout_sec,
		  u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_ns_send_rffid(struct efc_node_s *node,
		   u32 timeout_sec,
		  u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_ns_send_gidpt(struct efc_node_s *node, u32 timeout_sec,
		   u32 retries, els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_rscn(struct efc_node_s *node, u32 timeout_sec,
	       u32 retries, void *port_ids,
	      u32 port_ids_count, els_cb_t cb, void *cbarg);
extern void
efct_els_io_cleanup(struct efct_io_s *els, enum efc_hw_node_els_event_e,
		    void *arg);

/* ELS acc send */
extern struct efct_io_s *
efct_send_ls_acc(struct efc_node_s *node, u32 ox_id,
		 els_cb_t cb, void *cbarg);

extern void *
efct_send_ls_rjt(struct efc_lport *efc, struct efc_node_s *node, u32 ox_id,
		 u32 reason_cod, u32 reason_code_expl,
		u32 vendor_unique);
extern void *
efct_send_flogi_p2p_acc(struct efc_lport *efc, struct efc_node_s *node,
			u32 ox_id, u32 s_id);
extern struct efct_io_s *
efct_send_flogi_acc(struct efc_node_s *node, u32 ox_id,
		    u32 is_fport, els_cb_t cb,
		   void *cbarg);
extern struct efct_io_s *
efct_send_plogi_acc(struct efc_node_s *node, u32 ox_id,
		    els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_prli_acc(struct efc_node_s *node, u32 ox_id,
		   els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_logo_acc(struct efc_node_s *node, u32 ox_id,
		   els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_prlo_acc(struct efc_node_s *node, u32 ox_id,
		   els_cb_t cb, void *cbarg);
extern struct efct_io_s *
efct_send_adisc_acc(struct efc_node_s *node, u32 ox_id,
		    els_cb_t cb, void *cbarg);

/* BLS acc send */
extern void *
efct_bls_send_acc_hdr(struct efc_lport *efc, struct efc_node_s *node,
		      struct fc_frame_header *hdr);
/* BLS rjt send */
extern struct efct_io_s *
efct_bls_send_rjt_hdr(struct efct_io_s *io, struct fc_frame_header *hdr);

/* Misc */
extern int
efct_els_io_list_empty(struct efc_node_s *node, struct list_head *list);

/* CT */
extern int
efct_send_ct_rsp(struct efc_lport *efc, struct efc_node_s *node, __be16 ox_id,
		 struct fcct_iu_header_s *ct_hdr,
		u32 cmd_rsp_code, u32 reason_code,
		u32 reason_code_explanation);

#endif /* __EFCT_ELS_H__ */
