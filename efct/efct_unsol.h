/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__OSC_UNSOL_H__)
#define __OSC_UNSOL_H__

extern int
efct_unsolicited_cb(void *arg, struct efc_hw_sequence_s *seq);
extern int
efct_node_purge_pending(struct efc_lport *efc, struct efc_node_s *node);
extern int
efct_process_node_pending(struct efc_node_s *domain);
extern int
efct_domain_process_pending(struct efc_domain_s *domain);
extern int
efct_domain_purge_pending(struct efc_domain_s *domain);
extern int
efct_dispatch_unsolicited_bls(struct efc_node_s *node,
			      struct efc_hw_sequence_s *seq);
extern void
efct_domain_hold_frames(struct efc_lport *efc, struct efc_domain_s *domain);
extern void
efct_domain_accept_frames(struct efc_lport *efc, struct efc_domain_s *domain);
extern void
efct_seq_coalesce_cleanup(struct efct_hw_io_s *io, u8 count);
extern int
efct_sframe_send_bls_acc(struct efc_node_s *node,
			 struct efc_hw_sequence_s *seq);
extern int
efct_dispatch_fcp_cmd(struct efc_node_s *node, struct efc_hw_sequence_s *seq);

extern int
efct_dispatch_fcp_cmd_auto_xfer_rdy(struct efc_node_s *node,
				    struct efc_hw_sequence_s *seq);
extern int
efct_dispatch_fcp_data(struct efc_node_s *node,
		       struct efc_hw_sequence_s *seq);

extern int
efct_node_recv_abts_frame(struct efc_lport *efc, struct efc_node_s *node,
			  struct efc_hw_sequence_s *seq);
extern void
efct_node_els_cleanup(struct efc_lport *efc, struct efc_node_s *node,
		      bool force);

extern void
efct_node_io_cleanup(struct efc_lport *efc, struct efc_node_s *node,
		     bool force);

void
efct_node_abort_all_els(struct efc_lport *efc, struct efc_node_s *node);

#endif /* __OSC_UNSOL_H__ */
