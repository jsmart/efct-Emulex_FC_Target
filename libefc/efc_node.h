/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__EFC_NODE_H__)
#define __EFC_NODE_H__
#include "scsi/fc/fc_ns.h"

#define EFC_NODEDB_PAUSE_FABRIC_LOGIN	BIT(0)
#define EFC_NODEDB_PAUSE_NAMESERVER	BIT(1)
#define EFC_NODEDB_PAUSE_NEW_NODES	BIT(2)

static inline void
efc_node_evt_set(struct efc_sm_ctx_s *ctx, enum efc_sm_event_e evt,
		 const char *handler)
{
	struct efc_node_s *node = ctx->app;

	if (evt == EFC_EVT_ENTER) {
		strncpy(node->current_state_name, handler,
			sizeof(node->current_state_name));
	} else if (evt == EFC_EVT_EXIT) {
		strncpy(node->prev_state_name, node->current_state_name,
			sizeof(node->prev_state_name));
		strncpy(node->current_state_name, "invalid",
			sizeof(node->current_state_name));
	}
	node->prev_evt = node->current_evt;
	node->current_evt = evt;
}

/**
 * @brief hold frames in pending frame list
 *
 * Unsolicited receive frames are held on the node pending frame list,
 * rather than being processed.
 *
 * @param node pointer to node structure
 *
 * @return none
 */

static inline void
efc_node_hold_frames(struct efc_node_s *node)
{
	efc_assert(node);
	node->hold_frames = true;
}

/**
 * @brief accept frames
 *
 * Unsolicited receive frames processed rather than being held on the node
 * pending frame list.
 *
 * @param node pointer to node structure
 *
 * @return none
 */

static inline void
efc_node_accept_frames(struct efc_node_s *node)
{
	efc_assert(node);
	node->hold_frames = false;
}

extern int
efc_node_create_pool(struct efc_lport *efc, u32 node_count);
extern void
efc_node_free_pool(struct efc_lport *efc);
extern struct efc_node_s *
efc_node_get_instance(struct efc_lport *efc, u32 instance);

static inline void
efc_node_lock_init(struct efc_node_s *node)
{
	efc_rlock_init(node->efc, &node->lock, "node rlock");
}

static inline int
efc_node_lock_try(struct efc_node_s *node)
{
	return efc_rlock_try(&node->lock);
}

static inline void
efc_node_lock(struct efc_node_s *node)
{
	efc_rlock_acquire(&node->lock);
}

static inline void
efc_node_unlock(struct efc_node_s *node)
{
	efc_rlock_release(&node->lock);
}

/**
 * @brief Node initiator/target enable defines
 *
 * All combinations of the SLI port (sport) initiator/target enable, and remote
 * node initiator/target enable are enumerated.
 *
 */

enum efc_node_enable_e {
	EFC_NODE_ENABLE_x_TO_x,
	EFC_NODE_ENABLE_x_TO_T,
	EFC_NODE_ENABLE_x_TO_I,
	EFC_NODE_ENABLE_x_TO_IT,
	EFC_NODE_ENABLE_T_TO_x,
	EFC_NODE_ENABLE_T_TO_T,
	EFC_NODE_ENABLE_T_TO_I,
	EFC_NODE_ENABLE_T_TO_IT,
	EFC_NODE_ENABLE_I_TO_x,
	EFC_NODE_ENABLE_I_TO_T,
	EFC_NODE_ENABLE_I_TO_I,
	EFC_NODE_ENABLE_I_TO_IT,
	EFC_NODE_ENABLE_IT_TO_x,
	EFC_NODE_ENABLE_IT_TO_T,
	EFC_NODE_ENABLE_IT_TO_I,
	EFC_NODE_ENABLE_IT_TO_IT,
};

static inline enum efc_node_enable_e
efc_node_get_enable(struct efc_node_s *node)
{
	u32 retval = 0;

	if (node->sport->enable_ini)
		retval |= (1U << 3);
	if (node->sport->enable_tgt)
		retval |= (1U << 2);
	if (node->init)
		retval |= (1U << 1);
	if (node->targ)
		retval |= (1U << 0);
	return (enum efc_node_enable_e)retval;
}

extern int
efc_node_check_els_req(struct efc_sm_ctx_s *ctx,
		       enum efc_sm_event_e evt, void *arg,
		       u8 cmd, void *(*efc_node_common_func)(const char *,
		       struct efc_sm_ctx_s *, enum efc_sm_event_e, void *),
		       const char *funcname);
extern int
efc_node_check_ns_req(struct efc_sm_ctx_s *ctx,
		      enum efc_sm_event_e evt, void *arg,
		  u16 cmd, void *(*efc_node_common_func)(const char *,
		  struct efc_sm_ctx_s *, enum efc_sm_event_e, void *),
		  const char *funcname);
extern int
efc_node_attach(struct efc_node_s *node);
extern struct efc_node_s *
efc_node_find_wwpn(struct efc_sli_port_s *sport, uint64_t wwpn);
extern void
efc_node_dump(struct efc_lport *efc);
extern struct efc_node_s *
efc_node_alloc(struct efc_sli_port_s *sport, u32 port_id,
		bool init, bool targ);
extern int
efc_node_free(struct efc_node_s *efc);
extern void
efc_node_force_free(struct efc_node_s *efc);
extern void
efc_node_update_display_name(struct efc_node_s *node);
void efc_node_post_event(struct efc_node_s *node, enum efc_sm_event_e evt,
			 void *arg);

extern void *
__efc_node_shutdown(struct efc_sm_ctx_s *ctx,
		    enum efc_sm_event_e evt, void *arg);
extern void *
__efc_node_wait_node_free(struct efc_sm_ctx_s *ctx,
			  enum efc_sm_event_e evt, void *arg);
extern void *
__efc_node_wait_els_shutdown(struct efc_sm_ctx_s *ctx,
			     enum efc_sm_event_e evt, void *arg);
extern void *
__efc_node_wait_ios_shutdown(struct efc_sm_ctx_s *ctx,
			     enum efc_sm_event_e evt, void *arg);
extern void
efc_node_save_sparms(struct efc_node_s *node, void *payload);
extern void
efc_node_transition(struct efc_node_s *node,
		    void *(*state)(struct efc_sm_ctx_s *,
		    enum efc_sm_event_e, void *), void *data);
extern void *
__efc_node_common(const char *funcname, struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *arg);

extern void
efc_node_initiate_cleanup(struct efc_node_s *node);

extern void
efc_node_build_eui_name(char *buffer, u32 buffer_len, uint64_t eui_name);
extern uint64_t
efc_node_get_wwpn(struct efc_node_s *node);

extern void
efc_node_pause(struct efc_node_s *node,
	       void *(*state)(struct efc_sm_ctx_s *ctx,
			      enum efc_sm_event_e evt, void *arg));
extern int
efc_node_resume(struct efc_node_s *node);
extern void *
__efc_node_paused(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *arg);
extern int
efc_node_active_ios_empty(struct efc_node_s *node);
extern void
efc_node_send_ls_io_cleanup(struct efc_node_s *node);

extern int
efc_els_io_list_empty(struct efc_node_s *node, struct list_head *list);

extern int
efc_process_node_pending(struct efc_node_s *domain);

extern int
efc_node_recv_link_services_frame(struct efc_node_s *node,
				  struct efc_hw_sequence_s *seq);

#endif /* __EFC_NODE_H__ */
